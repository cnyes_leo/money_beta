﻿//---------------------------------
//購屋試算  [brian]
//---------------------------------
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;//導入命名空間(正則表達式)
using System.Text;
using System.Net;



public partial class Housing : System.Web.UI.Page
{
    #region override void OnPreInit (EventArgs e)
    protected override void OnPreInit(EventArgs e)
    {
        base.OnPreInit(e);

        MPs_MP1 master = (MPs_MP1)this.Master as MPs_MP1;

        master.NavigationCategory = Enums.euNavigationCategory.試算工具;
    }
    #endregion override void OnPreInit (EventArgs e)


    public string strID = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        #region loading狀態bar

        string sScript2 = "$.blockUI();";
        ScriptManager.RegisterOnSubmitStatement(this, this.GetType(), "blockUI", sScript2);

        string sScript = "$.unblockUI();";
        ScriptManager.RegisterStartupScript(this, this.GetType(), "unblockUI", sScript, true);
        #endregion


        #region Qry
        Filter fi = new Filter();
        String[] qs = new String[1] { "t" };
        List<string> myReturn = new List<string>();
        myReturn = fi.isSafeQryString(this, qs);
        string strQry = myReturn[0];
        #endregion Qry


        string loadUCsID = "";
        if (strQry == "")
        {
            loadUCsID = "01";
        }
        else
        {
            loadUCsID = (fi.IsSafeNumeric(strQry))
                        ? strQry.PadLeft(2, '0')
                        : "01";
        }

        //載入UCs
        Control c = LoadControl("UCs/HS" + loadUCsID + ".ascx");
        pnContent.Controls.Add(c);

        //左側選單光標
        strID = "'hs" + loadUCsID + "'";


        #region 載入右側選單
        try
        {
            //WebClient wc = new WebClient();
            //Uri uri = Request.Url;
            //string strUrl = uri.AbsoluteUri.Replace(uri.Segments[uri.Segments.Length - 1], "");
            //byte[] b = wc.DownloadData(strUrl + "batRightList.htm");
            //string strHCode = Encoding.UTF8.GetString(b);

            //Literal1.Text = strHCode;
            //wc.Dispose();

            //FileStream myFile = File.Open(Server.MapPath("batRightList.htm"), FileMode.Open, FileAccess.Read);
            //StreamReader myReader = new StreamReader(myFile);
            //int dl = System.Convert.ToInt32(myFile.Length);
            //Literal1.Text = myReader.ReadToEnd();
            //myReader.Close();
            //myFile.Close();


            Literal1.Text = System.IO.File.ReadAllText(Server.MapPath("batRightList.htm"));
        }
        catch (InvalidCastException ex)
        {
            Literal1.Text = "";
        }
        #endregion


        #region seo
        MPs_MP1 master = (MPs_MP1)this.Master as MPs_MP1;

        Seo seo = new Seo("Housing_" + loadUCsID);
        master.Title = seo.sTitle;
        master.Description = seo.sDesc;
        master.Keywords = seo.keyword;
        master.H1 = seo.sH1;
        seo = null;
        #endregion


        fi = null;
    }
}