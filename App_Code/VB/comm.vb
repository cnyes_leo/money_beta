﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.OleDb
Imports System.Configuration.ConfigurationManager
Imports System.Web
Imports cnYES.AicMember.aspx

Public Class comm

    Public Shared Conn_195_Cnyes_News_r As String = ConnectionStrings("_195_cnYESNews_ConnStr").ConnectionString
    Public Shared Conn_24_global_ConnStr_r As String = ConnectionStrings("_24_global_ConnStr").ConnectionString
    Public Shared Conn_24_ofund_ConnStr_r As String = ConnectionStrings("_24_ofund_Read").ConnectionString
    Public Shared Conn_24_news_ConnStr_r As String = ConnectionStrings("_24_news_ConnStr").ConnectionString
    Public Shared Conn_122_global_r As String = ConnectionStrings("_122_global_Read").ConnectionString

    '以下都是web.config沒有設定值....先mark，要使用的人請自行打開   [brian   20120308....婦女節唷]
    Public Shared _24_Promote_r As String = ConnectionStrings("_24_Promote_r").ConnectionString
    Public Shared Conn_122_cnbond_r As String = ConnectionStrings("_122_cnbond_Read").ConnectionString

    Public Shared Function GetData(ByVal ProOrSql As String, ByVal culnameAy() As String, ByVal valueAy() As String, ByVal sqltype As CommandType, ByVal connstr As String) As DataTable

        Dim BackDT As New DataTable
        Dim conn_R As SqlConnection = New SqlConnection(connstr)
        conn_R.Open()
        Dim dataComm As SqlCommand = New SqlCommand(ProOrSql, conn_R)
        dataComm.CommandType = sqltype

        Dim i As Integer
        For i = 0 To culnameAy.Length - 1
            dataComm.Parameters.Add(New SqlParameter(culnameAy(i), SQLSafe(valueAy(i))))
        Next

        Dim dataR As SqlDataReader

        Try
            dataR = dataComm.ExecuteReader()

            Do Until Not dataR.HasRows
                BackDT.Load(dataR)

            Loop

            Do Until Not dataR.NextResult
                Do Until Not dataR.HasRows
                    BackDT.Load(dataR)
                Loop
            Loop
        Catch ex As Exception
            Err.Clear()
        End Try

        dataComm.Dispose()
        conn_R.Close()


        Return (BackDT)

    End Function

    Public Shared Function GetDataSet(ByVal ProOrSql As String, ByVal culnameAy() As String, ByVal valueAy() As String, ByVal sqltype As CommandType, ByVal connstr As String) As Data.DataSet
        Dim ds As New DataSet
        Dim conn_R As SqlConnection = New SqlConnection(connstr)
        conn_R.Open()
        Dim dataComm As SqlCommand = New SqlCommand(ProOrSql, conn_R)
        dataComm.CommandType = sqltype

        Dim i As Integer
        For i = 0 To culnameAy.Length - 1
            dataComm.Parameters.Add(New SqlParameter(culnameAy(i), SQLSafe(valueAy(i))))
        Next

        Dim dataR As New SqlDataAdapter(dataComm)

        Try
            dataR.Fill(ds)

        Catch ex As Exception

            Err.Clear()
        End Try

        dataComm.Dispose()
        conn_R.Close()

        Return (ds)

    End Function

    Public Shared Function UpateData(ByVal ProOrSql As String, ByVal culnameAy() As String, ByVal valueAy() As String, ByVal sqltype As CommandType, Optional ByVal conn As String = "") As String
        Dim conn_w As SqlConnection
        Dim msg As String = ""
        conn_w = New SqlConnection(conn)
        conn_w.Open()
        Dim dataComm As SqlCommand = New SqlCommand(ProOrSql, conn_w)
        dataComm.CommandType = sqltype

        Dim i As Integer
        For i = 0 To culnameAy.Length - 1
            dataComm.Parameters.Add(New SqlParameter(culnameAy(i), SQLSafe(valueAy(i))))
        Next
        Try
            dataComm.ExecuteNonQuery()
        Catch ex As Exception
            msg = Err.Number.ToString()
            Err.Clear()
        End Try

        dataComm.Dispose()
        conn_w.Close()

        Return (msg)

    End Function

    Public Shared Function UpateData_noSQLSafe(ByVal ProOrSql As String, ByVal culnameAy() As String, ByVal valueAy() As String, ByVal sqltype As CommandType, Optional ByVal conn As String = "") As String
        Dim conn_w As SqlConnection
        Dim msg As String = ""
        conn_w = New SqlConnection(conn)
        conn_w.Open()
        Dim dataComm As SqlCommand = New SqlCommand(ProOrSql, conn_w)
        dataComm.CommandType = sqltype

        Dim i As Integer
        For i = 0 To culnameAy.Length - 1
            dataComm.Parameters.Add(New SqlParameter(culnameAy(i), valueAy(i)))
        Next
        Try
            dataComm.ExecuteNonQuery()
        Catch ex As Exception
            msg = Err.Number.ToString()
            Err.Clear()
        End Try

        dataComm.Dispose()
        conn_w.Close()

        Return (msg)

    End Function

    Public Shared Function SQLSafe(ByVal pStr As String) As String
        '檢查存入的資料

        Dim errSpec As Boolean = False
        Dim strStr As String = "" '傳入的原始資料(保留英文大小寫)
        If pStr Is Nothing Then
            strStr = ""
        Else
            strStr = pStr.ToString.Trim()
        End If

        Dim strOK As String = strStr.ToLower() '被檢查的資料
        Dim flag As Boolean = False

        Dim SpecStr As String() = New String(22) {}

        SpecStr(0) = "net%20user"
        SpecStr(1) = "cmdshell"
        SpecStr(2) = "%3C"
        SpecStr(3) = "exec%20master.dbo.xp_cmdshell"
        SpecStr(4) = "net localgroup administrators"
        SpecStr(5) = "update"
        SpecStr(6) = "exec%20"
        SpecStr(7) = "char("
        SpecStr(8) = "insert"
        SpecStr(9) = "delete"
        SpecStr(10) = "drop"
        SpecStr(11) = "truncate"
        SpecStr(12) = "script"
        SpecStr(13) = "iframe"
        SpecStr(14) = "exec%20"
        SpecStr(15) = "declare"
        SpecStr(16) = "cursor"
        SpecStr(17) = "fetch"
        SpecStr(18) = "cast("
        SpecStr(19) = "'"
        SpecStr(20) = "--"
        SpecStr(21) = """"""
        SpecStr(22) = "|"
        'SpecStr(23) = ";"

        For i As Integer = 0 To SpecStr.Length - 1
            If strOK.IndexOf(SpecStr(i), 0) > -1 Then
                'strOK = strOK.Replace(SpecStr(i), "").ToString()
                flag = True
                Exit For
            End If
        Next

        If flag Then

            'Return strOK
            HttpContext.Current.Response.Redirect("index.htm")
        Else
            Return strStr
        End If
    End Function

    Public Shared Sub GetTB(ByVal DT As DataTable, ByRef TB As HtmlTable)
        '回傳填好資料的TB
        Try
            For i As Integer = 0 To DT.Rows.Count - 1
                Dim TR As HtmlTableRow = GetTBRow(TB.Rows(0).Cells.Count)
                For j As Integer = 0 To DT.Columns.Count - 1
                    If IsDBNull(DT.Rows(i).Item(j)) Then
                        TR.Cells(j).InnerText = " "
                    ElseIf DT.Rows(i).Item(j).ToString() = "" Then
                        TR.Cells(j).InnerText = " "
                    Else
                        TR.Cells(j).InnerText = DT.Rows(i).Item(j).ToString()
                    End If
                Next
                TB.Rows.Add(TR)
            Next
        Catch ex As Exception
            Err.Clear()
        End Try


    End Sub

    Public Shared Function GetTBRow(ByVal TDcount As Integer) As HtmlTableRow

        Dim row As New HtmlTableRow
        Try
            For i As Integer = 0 To TDcount - 1
                Dim td As New HtmlTableCell
                td.InnerText = ""
                row.Cells.Add(td)
            Next
        Catch ex As Exception
        End Try
        Return row
    End Function


    Public Shared Function NoHTML(ByVal Htmlstring As String) As String

        '踢除文字中的HTML碼
        Htmlstring = Regex.Replace(Htmlstring, "<script[^>]*?>.*?</script>", "", RegexOptions.IgnoreCase)

        Htmlstring = Regex.Replace(Htmlstring, "<(.[^>]*)>", "", RegexOptions.IgnoreCase)
        Htmlstring = Regex.Replace(Htmlstring, "([\r\n])[\s]+", "", RegexOptions.IgnoreCase)
        Htmlstring = Regex.Replace(Htmlstring, "-->", "", RegexOptions.IgnoreCase)
        Htmlstring = Regex.Replace(Htmlstring, "<!--.*", "", RegexOptions.IgnoreCase)

        Htmlstring = Regex.Replace(Htmlstring, "&(quot|#34);", """", RegexOptions.IgnoreCase)
        Htmlstring = Regex.Replace(Htmlstring, "&(amp|#38);", "&", RegexOptions.IgnoreCase)
        Htmlstring = Regex.Replace(Htmlstring, "&(lt|#60);", "<", RegexOptions.IgnoreCase)
        Htmlstring = Regex.Replace(Htmlstring, "&(gt|#62);", ">", RegexOptions.IgnoreCase)
        Htmlstring = Regex.Replace(Htmlstring, "&(nbsp|#160);", " ", RegexOptions.IgnoreCase)
        Htmlstring = Regex.Replace(Htmlstring, "&(iexcl|#161);", "¡", RegexOptions.IgnoreCase)
        Htmlstring = Regex.Replace(Htmlstring, "&(cent|#162);", "¢", RegexOptions.IgnoreCase)

        Htmlstring = Regex.Replace(Htmlstring, "&(pound|#163);", "£", RegexOptions.IgnoreCase)
        Htmlstring = Regex.Replace(Htmlstring, "&(copy|#169);", "©", RegexOptions.IgnoreCase)
        Htmlstring = Regex.Replace(Htmlstring, "&#(\d+);", "", RegexOptions.IgnoreCase)


        Htmlstring.Replace("<", "")
        Htmlstring.Replace(">", "")
        Htmlstring.Replace(vbCr & vbLf, "")
        'Htmlstring = HttpContext.Current.Server.HtmlEncode(Htmlstring).Trim()

        Return Htmlstring
    End Function


End Class