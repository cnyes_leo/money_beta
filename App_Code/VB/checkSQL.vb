﻿Imports Microsoft.VisualBasic

Public Class checkSQL

    Shared Function CheckSQL(ByVal sqlcmd As String) As String

        Dim chk(24) As String
        chk(0) = "xp_cmdshell"
        chk(1) = "script"
        chk(2) = "iframe"
        chk(3) = "%"
        chk(4) = "cast "
        chk(5) = "exec "
        chk(6) = "--"
        chk(7) = "/*"
        chk(8) = "@@"
        chk(9) = "select "
        chk(10) = "insert "
        chk(11) = "update "
        chk(12) = "delete "
        chk(13) = "create "
        chk(14) = "truncate "
        chk(15) = "declare "
        chk(16) = "drop "
        chk(17) = "grant "
        chk(18) = "case "
        chk(19) = "'sa'"
        chk(20) = " or "
        chk(21) = " sa "
        chk(22) = "char("
        chk(23) = "<"
        chk(24) = ">"
        For i As Integer = 0 To chk.Length - 1

            Try
                If System.Web.HttpUtility.UrlDecode(sqlcmd).ToLowerInvariant.IndexOf(chk(i)) > 0 Then
                    Return ""
                End If
            Catch ex As Exception
                Return ""
            End Try

        Next

        Return sqlcmd

    End Function
End Class
