﻿Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.SqlClient
Imports System.Text
Imports System.IO
Imports System.Collections.Generic
Public Class MoneyDataAccess


    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="newsColumnList">使用逗點格開的新聞欄目的英文名稱</param>
    ''' <param name="counts">每一個欄目要多少則新聞</param>
    ''' <returns>回傳一個 DataTable 的 List泛型</returns>
    ''' <remarks></remarks>
    Shared Function getNewsList(ByVal newsColumnList As String, ByVal counts As Integer) As List(Of DataTable)



        Dim returnDataTableList As New List(Of DataTable)


        Dim connstr As String = ""
        Dim sqlcmd As String = ""

        connstr = System.Web.Configuration.WebConfigurationManager.ConnectionStrings("_195_cnYESNews_ConnStr").ToString

        sqlcmd = "Money_NewsList"

        Dim conn As New SqlConnection(connstr)
        Dim cmd As New SqlCommand(sqlcmd, conn)
        cmd.CommandType = CommandType.StoredProcedure

        Dim p1 As New SqlParameter("@myclassename", SqlDbType.NVarChar, 500)
        p1.Direction = ParameterDirection.Input
        p1.Value = checkSQL.CheckSQL(newsColumnList)
        cmd.Parameters.Add(p1)

        Dim p2 As New SqlParameter("@counts", SqlDbType.Int)
        p2.Direction = ParameterDirection.Input
        p2.Value = checkSQL.CheckSQL(counts)
        cmd.Parameters.Add(p2)

        Dim dr As SqlDataReader

        Try
            conn.Open()
            dr = cmd.ExecuteReader()


            Do
                returnDataTableList.Add(New DataTable())
                returnDataTableList.Item(returnDataTableList.Count - 1).Load(dr)
            Loop While dr.NextResult
 
            dr.Close()
        Catch ex As Exception

        Finally


            cmd.Dispose()
            conn.Close()
            conn.Dispose()
        End Try


        Return returnDataTableList

    End Function



    ''' <summary>
    ''' 取得油價
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Shared Function getOilPrice() As DataTable



        Dim returnDataTable As New DataTable


        Dim connstr As String = ""
        Dim sqlcmd As String = ""

        connstr = System.Web.Configuration.WebConfigurationManager.ConnectionStrings("_24_Money_ConnStr").ToString

        sqlcmd = "Money_OilPrice"

        Dim conn As New SqlConnection(connstr)
        Dim cmd As New SqlCommand(sqlcmd, conn)
        cmd.CommandType = CommandType.StoredProcedure

        Dim dr As SqlDataReader

        Try
            conn.Open()
            dr = cmd.ExecuteReader()

            returnDataTable.Load(dr)
    
            dr.Close()
        Catch ex As Exception
      
        Finally


            cmd.Dispose()
            conn.Close()
            conn.Dispose()
        End Try


        Return returnDataTable

    End Function



    ''' <summary>
    ''' 回傳民生物價指數
    ''' </summary>
    ''' <param name="statList">民生物價指數的索引清單</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Shared Function getGovStat(ByVal statList As String) As List(Of DataTable)



        Dim returnDataTableList As New List(Of DataTable)


        Dim connstr As String = ""
        Dim sqlcmd As String = ""

        connstr = System.Web.Configuration.WebConfigurationManager.ConnectionStrings("_24_Money_ConnStr").ToString

        sqlcmd = "Money_GovStatData"

        Dim conn As New SqlConnection(connstr)
        Dim cmd As New SqlCommand(sqlcmd, conn)
        cmd.CommandType = CommandType.StoredProcedure

        Dim p1 As New SqlParameter("@myindex", SqlDbType.NVarChar, 500)
        p1.Direction = ParameterDirection.Input
        p1.Value = checkSQL.CheckSQL(statList)
        cmd.Parameters.Add(p1)

        Dim dr As SqlDataReader

        Try
            conn.Open()
            dr = cmd.ExecuteReader()


            Do
                returnDataTableList.Add(New DataTable())
                returnDataTableList.Item(returnDataTableList.Count - 1).Load(dr)
            Loop While dr.NextResult

            dr.Close()
        Catch ex As Exception
            If 1 = 1 Then

            End If
        Finally


            cmd.Dispose()
            conn.Close()
            conn.Dispose()
        End Try


        Return returnDataTableList

    End Function


    ''' <summary>
    ''' 回傳台灣理財雜誌
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Shared Function getMAGTW() As DataTable

        Dim dt As New DataTable


        Dim connstr As String = ""
        Dim sqlcmd As String = ""

        connstr = System.Web.Configuration.WebConfigurationManager.ConnectionStrings("_24_promote_ConnStr").ToString

        sqlcmd = "SELECT  Top 10 MagazineName , MagazineCoverUrl , MagazineUrl FROM mag_yesfafa WHERE mag_type like '%台%' and mag_type like '%理財%' Group by MagazineName , MagazineCoverUrl , MagazineUrl -- Money default.aspx"



        Dim conn As New SqlConnection(connstr)
        Dim cmd As New SqlCommand(sqlcmd, conn)
        cmd.CommandType = CommandType.Text




        conn.Open()

        Try
            Dim dr As SqlDataReader = cmd.ExecuteReader()

            dt.Load(dr)

            dr.Close()
        Catch ex As Exception


        End Try


        cmd.Dispose()
        conn.Close()
        conn.Dispose()


        Return dt


    End Function



    ''' <summary>
    ''' 取得基金研究報告的每一條內文日期/分類/檔案位置/標題
    ''' </summary>
    ''' <returns>回傳一個 DataTable 的 List泛型(基金報告/國際股市/國際期貨/外匯市場)</returns>
    ''' <remarks></remarks>
    Shared Function getFundReport() As List(Of DataTable)

        Dim returnDataTableList As New List(Of DataTable)


        Dim connstr As String = ""
        Dim sqlcmd As String = ""

        connstr = System.Web.Configuration.WebConfigurationManager.ConnectionStrings("_24_news_ConnStr").ToString

        sqlcmd = "Money_FundReport"



        Dim conn As New SqlConnection(connstr)
        Dim cmd As New SqlCommand(sqlcmd, conn)
        cmd.CommandType = CommandType.StoredProcedure




        conn.Open()

        Try
            Dim dr As SqlDataReader = cmd.ExecuteReader()


            Do
                returnDataTableList.Add(New DataTable())
                returnDataTableList.Item(returnDataTableList.Count - 1).Load(dr)
            Loop While dr.NextResult

            dr.Close()
        Catch ex As Exception


        End Try


        cmd.Dispose()
        conn.Close()
        conn.Dispose()


        Return returnDataTableList


    End Function




    ''' <summary>
    ''' 取得主要績效外匯的統計結果
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Shared Function getForexEfficiency() As List(Of DataTable)

        Dim returnDataTableList As New List(Of DataTable)


        Dim connstr As String = ""
        Dim sqlcmd As String = ""

        connstr = System.Web.Configuration.WebConfigurationManager.ConnectionStrings("_24_global_ConnStr").ToString

        sqlcmd = "Money_getForexEfficiency"



        Dim conn As New SqlConnection(connstr)
        Dim cmd As New SqlCommand(sqlcmd, conn)
        cmd.CommandType = CommandType.StoredProcedure




        conn.Open()

        Try
            Dim dr As SqlDataReader = cmd.ExecuteReader()


            Do
                returnDataTableList.Add(New DataTable())
                returnDataTableList.Item(returnDataTableList.Count - 1).Load(dr)
            Loop While dr.NextResult

            dr.Close()
        Catch ex As Exception


        End Try


        cmd.Dispose()
        conn.Close()
        conn.Dispose()


        Return returnDataTableList


    End Function


    ''' <summary>
    ''' 取得各種基金的漲跌績效排行
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Shared Function getFundEfficiencyRank() As List(Of DataTable)

        Dim returnDataTableList As New List(Of DataTable)


        Dim connstr As String = ""
        Dim sqlcmd As String = ""

        connstr = System.Web.Configuration.WebConfigurationManager.ConnectionStrings("_24_ofund_Read").ToString

        sqlcmd = "Money_FundGroupRank"



        Dim conn As New SqlConnection(connstr)
        Dim cmd As New SqlCommand(sqlcmd, conn)
        cmd.CommandType = CommandType.StoredProcedure




        conn.Open()

        Try
            Dim dr As SqlDataReader = cmd.ExecuteReader()


            Do
                returnDataTableList.Add(New DataTable())
                returnDataTableList.Item(returnDataTableList.Count - 1).Load(dr)
            Loop While dr.NextResult

            dr.Close()
        Catch ex As Exception


        End Try


        cmd.Dispose()
        conn.Close()
        conn.Dispose()


        Return returnDataTableList


    End Function





    ''' <summary>
    ''' 取得基金配息排行
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Shared Function getFundDividend() As List(Of DataTable)

        Dim returnDataTableList As New List(Of DataTable)


        Dim connstr As String = ""
        Dim sqlcmd As String = ""

        connstr = System.Web.Configuration.WebConfigurationManager.ConnectionStrings("_24_ofund_Read").ToString

        sqlcmd = "Money_Dividend"



        Dim conn As New SqlConnection(connstr)
        Dim cmd As New SqlCommand(sqlcmd, conn)
        cmd.CommandType = CommandType.StoredProcedure




        conn.Open()

        Try
            Dim dr As SqlDataReader = cmd.ExecuteReader()


            Do
                returnDataTableList.Add(New DataTable())
                returnDataTableList.Item(returnDataTableList.Count - 1).Load(dr)
            Loop While dr.NextResult

            dr.Close()
        Catch ex As Exception


        End Try


        cmd.Dispose()
        conn.Close()
        conn.Dispose()


        Return returnDataTableList


    End Function




    ''' <summary>
    ''' 取得台股值利率配股配息資料
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Shared Function getTWStockDividend() As List(Of DataTable)

        Dim returnDataTableList As New List(Of DataTable)

        Dim connstr As String = ""
        Dim sqlcmd As String = ""

        connstr = System.Web.Configuration.WebConfigurationManager.ConnectionStrings("_122_twstock_ConnStr").ToString

        sqlcmd = "Money_TwstockDividend"



        Dim conn As New SqlConnection(connstr)
        Dim cmd As New SqlCommand(sqlcmd, conn)
        cmd.CommandType = CommandType.StoredProcedure




        conn.Open()

        Try
            Dim dr As SqlDataReader = cmd.ExecuteReader()


            Do
                returnDataTableList.Add(New DataTable())
                returnDataTableList.Item(returnDataTableList.Count - 1).Load(dr)
            Loop While dr.NextResult

            dr.Close()
        Catch ex As Exception


        End Try


        cmd.Dispose()
        conn.Close()
        conn.Dispose()


        Return returnDataTableList


    End Function




    ''' <summary>
    ''' 取得全球股市排行
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Shared Function getGlobalMarketRank() As List(Of DataTable)

        Dim returnDataTableList As New List(Of DataTable)

        Dim connstr As String = ""
        Dim sqlcmd As String = ""

        connstr = System.Web.Configuration.WebConfigurationManager.ConnectionStrings("_24_global_ConnStr").ToString

        sqlcmd = "Money_GlobalMarketRank"



        Dim conn As New SqlConnection(connstr)
        Dim cmd As New SqlCommand(sqlcmd, conn)
        cmd.CommandType = CommandType.StoredProcedure




        conn.Open()

        Try
            Dim dr As SqlDataReader = cmd.ExecuteReader()


            Do
                returnDataTableList.Add(New DataTable())
                returnDataTableList.Item(returnDataTableList.Count - 1).Load(dr)
            Loop While dr.NextResult

            dr.Close()
        Catch ex As Exception


        End Try


        cmd.Dispose()
        conn.Close()
        conn.Dispose()


        Return returnDataTableList


    End Function




    ''' <summary>
    ''' 取得 Blog名人堂 相關資料
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Shared Function getBLOG() As List(Of DataTable)

        Dim returnDataTableList As New List(Of DataTable)

        Dim connstr As String = ""
        Dim sqlcmd As String = ""

        connstr = System.Web.Configuration.WebConfigurationManager.ConnectionStrings("_123_DigiAgent_Test_ConnStr").ToString

        sqlcmd = "Money_Excerpt"



        Dim conn As New SqlConnection(connstr)
        Dim cmd As New SqlCommand(sqlcmd, conn)
        cmd.CommandType = CommandType.StoredProcedure




        conn.Open()

        Try
            Dim dr As SqlDataReader = cmd.ExecuteReader()


            Do
                returnDataTableList.Add(New DataTable())
                returnDataTableList.Item(returnDataTableList.Count - 1).Load(dr)
            Loop While dr.NextResult

            dr.Close()
        Catch ex As Exception


        End Try


        cmd.Dispose()
        conn.Close()
        conn.Dispose()


        Return returnDataTableList


    End Function




    ''' <summary>
    ''' 取得鉅亨吧區塊的所有資料 共六個table 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Shared Function getBar() As List(Of DataTable)

        Dim returnDataTableList As New List(Of DataTable)

        Dim connstr As String = ""
        Dim sqlcmd As String = ""

        connstr = System.Web.Configuration.WebConfigurationManager.ConnectionStrings("_167_Bar_Connstr").ToString

        sqlcmd = "Money_FundBar"



        Dim conn As New SqlConnection(connstr)
        Dim cmd As New SqlCommand(sqlcmd, conn)
        cmd.CommandType = CommandType.StoredProcedure




        conn.Open()

        Try
            Dim dr As SqlDataReader = cmd.ExecuteReader()


            Do
                returnDataTableList.Add(New DataTable())
                returnDataTableList.Item(returnDataTableList.Count - 1).Load(dr)
            Loop While dr.NextResult

            dr.Close()
        Catch ex As Exception


        End Try


        cmd.Dispose()
        conn.Close()
        conn.Dispose()


        Return returnDataTableList


    End Function




    ''' <summary>
    ''' 首頁右上角的交叉匯率計算
    ''' </summary>
    ''' <param name="typeDollar"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Shared Function getExchangeForex(ByVal typeDollar As String) As String

        Dim returnValue As New StringBuilder(5000)


        Dim connstr As String = ""
        Dim sqlcmd As String = ""

        connstr = System.Web.Configuration.WebConfigurationManager.ConnectionStrings("_24_global_ConnStr").ToString

        'sqlcmd = "SELECT a.CurrCode, a.sourceCName, b.fcPrice FROM fcExchangeDB a,fcExchangeDB2 b where a.currcode=b.currcode and a.currcode <> 'DX'"
        sqlcmd += " SELECT a.CurrCode, a.sourceCName, b.fcPrice , "
        sqlcmd += " 				CASE "
        sqlcmd += " 					WHEN (PATINDEX('%美元/台幣%', a.sourceCName)<>0 ) THEN 1  "
        sqlcmd += " 					WHEN (PATINDEX('%歐元%', a.sourceCName)<>0 ) THEN 1  "
        sqlcmd += " 					WHEN (PATINDEX('%英鎊%', a.sourceCName)<>0 ) THEN 1  "
        sqlcmd += " 					WHEN (PATINDEX('%澳元%', a.sourceCName)<>0 ) THEN 1  "
        sqlcmd += " 					WHEN (PATINDEX('%紐元%', a.sourceCName)<>0 ) THEN 1  "
        sqlcmd += " 					WHEN (PATINDEX('%/加元%', a.sourceCName)<>0 ) THEN 1  "
        sqlcmd += " 					WHEN (PATINDEX('加元%', a.sourceCName)<>0 ) THEN 1  "
        sqlcmd += " 					WHEN (PATINDEX('%日圓%', a.sourceCName)<>0 ) THEN 1  "
        sqlcmd += " 					WHEN (PATINDEX('%港幣%', a.sourceCName)<>0 ) THEN 1  "
        sqlcmd += " 					WHEN (PATINDEX('%星元%', a.sourceCName)<>0 ) THEN 1 "
        sqlcmd += " 					WHEN (PATINDEX('%泰銖%', a.sourceCName)<>0 ) THEN 1  "
        sqlcmd += " 					WHEN (PATINDEX('%印尼%', a.sourceCName)<>0 ) THEN 1  "
        sqlcmd += " 					WHEN (PATINDEX('%馬來西亞%', a.sourceCName)<>0 ) THEN 1  "
        sqlcmd += " 					WHEN (PATINDEX('%韓元%', a.sourceCName)<>0 ) THEN 1  "
        sqlcmd += " 					WHEN (PATINDEX('%人民%', a.sourceCName)<>0 ) THEN 1  "
        sqlcmd += " 					WHEN (PATINDEX('%越南盾%', a.sourceCName)<>0 ) THEN 1  "
        sqlcmd += " 				ELSE 999 "
        sqlcmd += " 				END	'orderID' "
        sqlcmd += " FROM fcExchangeDB a,fcExchangeDB2 b "
        sqlcmd += " where a.currcode=b.currcode and a.currcode <> 'DX' "
        sqlcmd += " order by orderID asc "

        Dim conn As New SqlConnection(connstr)
        Dim cmd As New SqlCommand(sqlcmd, conn)
        cmd.CommandType = CommandType.Text
        Dim dr As SqlDataReader

        Try
            conn.Open()
            dr = cmd.ExecuteReader()

            While dr.Read


                Dim cnameary() As String = dr(1).ToString().Trim().Split("/")
                Dim fcprice As String = dr(2).ToString().Trim()

                Dim CName As String = ""


                If cnameary(0) = "美元" Then  '取股名的斜線左右邊name
                    CName = cnameary(1)
                Else
                    CName = cnameary(0)
                End If

                If CName = "台幣" Then

                    If typeDollar = "台幣" Then
                        returnValue.Append("<option selected=selected value=""" + CName + "-" + dr(2).ToString().Trim() + """>" + CName + "</option>" + "<option value=""美元-1"">美元</option>")
                    Else
                        returnValue.Append("<option value=""" + CName + "-" + dr(2).ToString().Trim() + """>" + CName + "</option>" + "<option selected=selected value=""美元-1"">美元</option>")
                    End If

                Else
                    returnValue.Append("<option value=""" + CName + "-" + fcprice + """>" + CName + "</option>")
                End If

            End While

            dr.Close()
        Catch ex As Exception

        Finally


            cmd.Dispose()
            conn.Close()
            conn.Dispose()
        End Try

        Return pageInforTxt.chgTXT(returnValue.ToString())
    End Function




    ''' <summary>
    ''' 取得黃金的牌價
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Shared Function getGoldPrice() As DataTable



        Dim returnDataTable As New DataTable


        Dim connstr As String = ""
        Dim sqlcmd As String = ""

        connstr = System.Web.Configuration.WebConfigurationManager.ConnectionStrings("_124_Gold_ConnStr").ToString

        sqlcmd = "Money_GoldPrice"

        Dim conn As New SqlConnection(connstr)
        Dim cmd As New SqlCommand(sqlcmd, conn)
        cmd.CommandType = CommandType.StoredProcedure

        Dim dr As SqlDataReader

        Try
            conn.Open()
            dr = cmd.ExecuteReader()

            returnDataTable.Load(dr)

            dr.Close()
        Catch ex As Exception

        Finally


            cmd.Dispose()
            conn.Close()
            conn.Dispose()
        End Try


        Return returnDataTable

    End Function




    ''' <summary>
    ''' 外匯看盤室
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Shared Function getForexExchangeRoom() As DataTable



        Dim returnDataTable As New DataTable


        Dim connstr As String = ""
        Dim sqlcmd As String = ""

        connstr = System.Web.Configuration.WebConfigurationManager.ConnectionStrings("_24_global_ConnStr").ToString

        sqlcmd = "exec Money_forexNowPrice  'USD_TWD__CNY_TWD__USD_CNY__USD_HKD__DX_DX__EUR_USD__USD_JPY__GBP_USD__USD_CHF__AUD_USD'"

        Dim conn As New SqlConnection(connstr)
        Dim cmd As New SqlCommand(sqlcmd, conn)
        cmd.CommandType = CommandType.Text

        Dim dr As SqlDataReader

        Try
            conn.Open()
            dr = cmd.ExecuteReader()

            returnDataTable.Load(dr)

            dr.Close()
        Catch ex As Exception

        Finally


            cmd.Dispose()
            conn.Close()
            conn.Dispose()
        End Try


        Return returnDataTable

    End Function



    'http://chart.cnyes.com/intraday/am/TW_TSE_s_" + FrameCheck("GIF", DateTime.Today) + ".PNG""
    'FrameCheck("GIF", DateTime.Today)
    ''' <summary>
    ''' 圖片計算
    ''' </summary>
    ''' <param name="BeName"></param>
    ''' <param name="NowDate"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Shared Function FrameCheck(ByVal BeName As String, ByVal NowDate As DateTime) As String

        Dim dateval As Integer
        Dim sum As Integer
        Dim file As String
        Dim yy As String = NowDate.ToString("yyyy")
        Dim mm As String = NowDate.ToString("MM")
        Dim dd As String = Convert.ToString(Convert.ToInt32(NowDate.ToString("dd")) - 1)
        Dim Map() As Int16 = {24, 88, 22, 3, 17, 7, 80, 35, 71, 10, 24, 11, 15, 1, 19, 25, 49, 6, 16, 31, 58, 26, 62, 18, 2, 93, 20, 9, 27, 64, 13}

        dd = Convert.ToString(Map(Convert.ToInt32(dd)))

        dateval = Convert.ToInt32(yy.ToString() + mm.ToString() + dd.ToString())
        dateval = dateval - 9999
        file = BeName + dateval.ToString() + "cnYES"

        For i As Integer = 0 To file.Length - 1
            sum += Convert.ToInt32(file(i)) * (i + 1)
        Next

        Array.Clear(Map, 0, Map.Length)

        Return Convert.ToString(sum)
    End Function





    ''' <summary>
    ''' 獲取 長期債券公債殖利率
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Shared Function getBondRate() As DataTable



        Dim returnDataTable As New DataTable


        Dim connstr As String = ""
        Dim sqlcmd As String = ""

        connstr = System.Web.Configuration.WebConfigurationManager.ConnectionStrings("_24_global_ConnStr").ToString

        sqlcmd = "Money_BondRate"

        Dim conn As New SqlConnection(connstr)
        Dim cmd As New SqlCommand(sqlcmd, conn)
        cmd.CommandType = CommandType.StoredProcedure

        Dim dr As SqlDataReader

        Try
            conn.Open()
            dr = cmd.ExecuteReader()

            returnDataTable.Load(dr)

            dr.Close()
        Catch ex As Exception

        Finally


            cmd.Dispose()
            conn.Close()
            conn.Dispose()
        End Try


        Return returnDataTable

    End Function



    ''' <summary>
    ''' 全球關鍵利率 / 台灣關鍵利率 / 中國關鍵利率
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Shared Function getKeyRate() As List(Of String)


        Dim returnStringList As New List(Of String)

        Const LOGON32_PROVIDER_DEFAULT As Integer = 0
        Const LOGON32_LOGON_NEW_CREDENTIALS As Integer = 9

        Dim tokenHandle As IntPtr = New IntPtr(0)
        tokenHandle = IntPtr.Zero

        Dim returnBool As Boolean = LogonUser("administrator", _
                                        "10.65.1.25", _
                                        "yes20090", _
                                        LOGON32_LOGON_NEW_CREDENTIALS, _
                                        LOGON32_PROVIDER_DEFAULT, _
                                        tokenHandle)



        Dim sb1 As New StringBuilder(100)
        Dim sb2 As New StringBuilder(100)
        Dim sb3 As New StringBuilder(100)
        Dim sb4 As New StringBuilder(100)

        Dim tempUSA As String = ""

        If returnBool Then
            Dim sr As StreamReader = My.Computer.FileSystem.OpenTextFileReader("\\10.65.1.25\webpage\DiskE\TSEOTC\entry\關鍵利率.txt", Encoding.GetEncoding(950))


            Dim startBool As Boolean = False
            Dim doTWBool As Boolean = False
            Dim doChinaBool As Boolean = False

            Dim i As Integer = 0


            Do While sr.Peek() >= 0



                Dim tempStr As String = sr.ReadLine()

                If tempStr.Trim.IndexOf("#主要工業國家關鍵利率表") >= 0 Then
                    startBool = True
                ElseIf tempStr.Trim.IndexOf("台灣") >= 0 Then
                    doTWBool = True
                    doChinaBool = False
                ElseIf tempStr.Trim.IndexOf("中國") >= 0 Then
                    doTWBool = False
                    doChinaBool = True
                ElseIf tempStr.Trim.IndexOf("香港") >= 0 Then
                    doTWBool = False
                    doChinaBool = False
                ElseIf (doTWBool = True Or doChinaBool = True) _
                    And tempStr.Trim.IndexOf("中國") < 0 And tempStr.Trim.IndexOf("台灣") < 0 _
                    And tempStr.Trim.IndexOf(",") <> 0 _
                Then
                    doTWBool = False
                    doChinaBool = False
                ElseIf tempStr.Trim.IndexOf("#歐洲國家關鍵利率表") >= 0 Then
                    Exit Do
                End If

                If startBool = True Then

                    Dim img As String = ""
                    If tempStr.Trim.IndexOf("美國") >= 0 Then
                        img = "usd.gif"
                        i += 1
                    ElseIf tempStr.Trim.IndexOf("日本") >= 0 Then
                        i += 1
                        img = "jpy.gif"
                    ElseIf tempStr.Trim.IndexOf("歐元") >= 0 Then
                        i += 1
                        img = "eur.gif"
                    ElseIf tempStr.Trim.IndexOf("英國") >= 0 Then
                        i += 1
                        img = "gbp.gif"
                    ElseIf tempStr.Trim.IndexOf("澳大利亞") >= 0 Then
                        i += 1
                        img = "aud.gif"
                    ElseIf tempStr.Trim.IndexOf("紐西蘭") >= 0 Then
                        i += 1
                        img = "nzd.gif"
                    ElseIf tempStr.Trim.IndexOf("加拿大") >= 0 Then
                        i += 1
                        img = "cad.gif"
                    ElseIf tempStr.Trim.IndexOf("瑞士") >= 0 Then
                        i += 1
                        img = "chf.gif"
                    End If

                    If doTWBool = True And tempStr.Trim().Split(",").Length >= 5 Then
                        sb3.Append("<tr>")
                        sb3.Append("<td class=""cnlt"">" + tempStr.Trim().Split(",")(1).Split("(")(0) + "</td>")
                        sb3.Append("<td>" + tempStr.Trim().Split(",")(2) + "</td>")
                        sb3.Append("<td>" + tempStr.Trim().Split(",")(3) + "</td>")
                        sb3.Append("<td class=""cr"">" + tempStr.Trim().Split(",")(4) + "</td>")
                        sb3.Append("</tr>")
                    ElseIf doChinaBool = True And tempStr.Trim().Split(",").Length >= 5 Then
                        sb4.Append("<tr>")
                        sb4.Append("<td class=""cnlt"">" + tempStr.Trim().Split(",")(1).Split("(")(0) + "</td>")
                        sb4.Append("<td>" + tempStr.Trim().Split(",")(2) + "</td>")
                        sb4.Append("<td>" + tempStr.Trim().Split(",")(3) + "</td>")
                        sb4.Append("<td class=""cr"">" + tempStr.Trim().Split(",")(4) + "</td>")
                        sb4.Append("</tr>")
                    End If
                    If img <> "" Then
                        If tempStr.Trim().Split(",").Length >= 2 Then
                            If i <= 4 Then
                                sb1.Append("<td>")
                                sb1.Append("<img src=""http://www.cnyes.com/forex/images/" + img + """ width=""25"" height=""17"" /><span>" + tempStr.Trim().Split(",")(2) + "</span>")
                                sb1.Append("</td>")
                            Else
                                sb2.Append("<td>")
                                sb2.Append("<img src=""http://www.cnyes.com/forex/images/" + img + """ width=""25"" height=""17"" /><span>" + tempStr.Trim().Split(",")(2) + "</span>")
                                sb2.Append("</td>")
                            End If
                        End If


                    End If

                End If
            Loop

            sr.Close()
            sr.Dispose()
        End If


        returnStringList.Add(sb1.ToString())
        returnStringList.Add(sb2.ToString())
        returnStringList.Add(sb3.ToString())
        returnStringList.Add(sb4.ToString())

        Return returnStringList

    End Function



    '登入
    <Runtime.InteropServices.DllImport("advapi32.DLL")> _
    Public Shared Function LogonUser( _
                                        ByVal lpszUsername As String, _
                                        ByVal lpszDomain As String, _
                                        ByVal lpszPassword As String, _
                                        ByVal dwLogonType As Integer, _
                                        ByVal dwLogonProvider As Integer, _
                                        ByRef phToken As IntPtr _
                                    ) As Boolean

    End Function

    '登出
    <Runtime.InteropServices.DllImport("kernel32.dll")> _
    Public Shared Function CloseHandle(ByVal phToken As IntPtr) As Boolean

    End Function





    ''' <summary>
    ''' 講座頻道的 投資講座課程
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Shared Function getLectureInvestClass() As DataTable

        Dim dt As New DataTable

        Dim connstr As String = ""
        Dim sqlcmd As String = ""

        connstr = System.Web.Configuration.WebConfigurationManager.ConnectionStrings("_24_AD_order_ConnStr").ToString

        sqlcmd = "Money_Lecture_InvestClass"

        Dim conn As New SqlConnection(connstr)
        Dim cmd As New SqlCommand(sqlcmd, conn)
        cmd.CommandType = CommandType.StoredProcedure

        Dim dr As SqlDataReader

        Try
            conn.Open()
            dr = cmd.ExecuteReader()
            dt.Load(dr)

            dr.Close()
        Catch ex As Exception

        Finally


            cmd.Dispose()
            conn.Close()
            conn.Dispose()
        End Try


        Return dt




    End Function




    ''' <summary>
    ''' 取得全球休市市場的清單 為了方便串資料庫與輸出資料 回傳值是字串
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Shared Function getCountryStockHoliday() As String

        Dim starti As Integer = 0
        Dim tempD As String = ""
        Dim tempDShort As String = ""
        Dim tempW As String = ""

        Dim returnValue As New StringBuilder(5000)


        Select Case Date.Now().DayOfWeek()
            Case "1"
                starti = -3
            Case "2"
                starti = -4
            Case Else
                starti = -1
        End Select


        Dim connstr As String = ""
        Dim sqlcmd As String = ""
        connstr = System.Web.Configuration.WebConfigurationManager.ConnectionStrings("_24_news_ConnStr").ToString
        Dim conn As New SqlConnection(connstr)
        conn.Open()

        For datei As Integer = starti To 6

            tempD = Date.Now.AddDays(datei).ToString("yyyyMMdd")
            tempDShort = Date.Now.AddDays(datei).ToString("M/dd")
            tempW = Date.Now.AddDays(datei).DayOfWeek()

            If tempW = 0 Or tempW = 6 Then
                Continue For
            End If

            Select Case tempW
                Case "0"
                    tempW = "(日)"
                Case "1"
                    tempW = "(一)"
                Case "2"
                    tempW = "(二)"
                Case "3"
                    tempW = "(三)"
                Case "4"
                    tempW = "(四)"
                Case "5"
                    tempW = "(五)"
                Case "6"
                    tempW = "(六)"
            End Select


            sqlcmd = "select nationname,nationcode from globalrest where restday = '" + tempD + "'"

            Dim cmd As New SqlCommand(sqlcmd, conn)

            cmd.CommandType = CommandType.Text
            Dim dr As SqlDataReader


            dr = cmd.ExecuteReader()

            '<tr>
            '    <td class=""cr"" style=""width: 60px;"">6/23(四)</td>
            '    <td class=""cnlt"">
            '    <a href=""/economy/indicator/GlobalRest/GlobalRest_Major.aspx?code=ATX&id=8&lv=0"">奧地利</a>, 
            '    <a href=""/economy/indicator/GlobalRest/GlobalRest_Major.aspx?code=LU&id=8&lv=0"">盧森堡</a>
            '    </td>
            '</tr>
            If dr.HasRows = True Then

                returnValue.Append("<tr>")
                returnValue.Append("<td class=""cr"" style=""width: 60px;"">" + tempDShort + tempW + "</td>")
                returnValue.Append("<td class=""cnlt"">")
                While dr.Read

                    returnValue.Append("<a target='_blank' href='http://www.cnyes.com/economy/indicator/GlobalRest/GlobalRest_Major.aspx?id=8&lv=0&code=" + dr(1).ToString().Trim() & "' >" + dr(0).ToString().Trim() + "</a>, ")

                End While
                returnValue.Append("</td>")
                returnValue.Append("</tr>")
            Else
                returnValue.Append("<tr>")
                returnValue.Append("<td class=""cr"" style=""width: 60px;"">" + tempDShort + tempW + "</td>")
                returnValue.Append("<td class=""cnlt"">無</td>")
                returnValue.Append("</tr>")
            End If


            dr.Close()
            cmd.Dispose()
        Next


        conn.Close()
        conn.Dispose()


        Return returnValue.ToString()
    End Function








    ''' <summary>
    ''' 最新鉅亨公告 / 鉅亨活動推薦
    ''' </summary>
    ''' <param name="cnyesTypes">要取得哪一種資料  Bulletin(最新鉅亨公告) 或 Commend(鉅亨活動推薦)</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Shared Function getCnyesInformation(ByVal cnyesTypes As String) As DataTable

        '最新鉅亨公告 / 鉅亨活動推薦
        'http://www.cnyes.com/personal/membercenter/

        'Bulletin / Commend
        'http://imgcache.cnyes.com/cnews/edm/images/point_forex.gif

        Dim dt As New DataTable

        cnyesTypes = checkSQL.CheckSQL(cnyesTypes)
        If cnyesTypes <> "Bulletin" And cnyesTypes <> "Commend" Then
            cnyesTypes = "Bulletin"
        End If


        Dim connstr As String = ""
        Dim sqlcmd As String = ""

        connstr = System.Web.Configuration.WebConfigurationManager.ConnectionStrings("_24_promote_ConnStr").ToString()
        sqlcmd = "exec Pro_NewBulletin_Show '" + cnyesTypes + "'"

        Dim conn As New SqlConnection(connstr)
        Dim cmd As New SqlCommand(sqlcmd, conn)
        cmd.CommandType = CommandType.Text



        conn.Open()

        Try
            Dim dr As SqlDataReader = cmd.ExecuteReader()
            dt.Load(dr)

            dr.Close()
        Catch ex As Exception

        End Try


        cmd.Dispose()
        conn.Close()
        conn.Dispose()


        Return dt





    End Function



    ''' <summary>
    ''' 取得銀行換匯區塊
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Shared Function getBankCurrencyExchange() As List(Of DataTable)


        Dim dtLIST As New List(Of DataTable)


        Dim connstr As String = ""
        Dim sqlcmd As String = ""

        connstr = System.Web.Configuration.WebConfigurationManager.ConnectionStrings("_24_bank_ConnStr").ToString()
        sqlcmd = "Money_getBankCurrencyExchange"

        Dim conn As New SqlConnection(connstr)
        Dim cmd As New SqlCommand(sqlcmd, conn)
        cmd.CommandType = CommandType.StoredProcedure



        conn.Open()

        Try
            Dim dr As SqlDataReader = cmd.ExecuteReader()

            Do
                Dim dt As New DataTable
                dt.Load(dr)

                dtLIST.Add(dt)
            Loop While dr.NextResult

            dr.Close()
        Catch ex As Exception

        End Try


        cmd.Dispose()
        conn.Close()
        conn.Dispose()


        Return dtLIST





    End Function




    ''' <summary>
    ''' 取得銀行存款利率
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Shared Function getgetBankRate() As List(Of DataTable)


        Dim dtLIST As New List(Of DataTable)


        Dim connstr As String = ""
        Dim sqlcmd As String = ""

        connstr = System.Web.Configuration.WebConfigurationManager.ConnectionStrings("_24_bank_ConnStr").ToString()
        sqlcmd = "Money_getBankRate"

        Dim conn As New SqlConnection(connstr)
        Dim cmd As New SqlCommand(sqlcmd, conn)
        cmd.CommandType = CommandType.StoredProcedure



        conn.Open()

        Try
            Dim dr As SqlDataReader = cmd.ExecuteReader()



            Do
                Dim dt As New DataTable
                dt.Load(dr)

                dtLIST.Add(dt)
            Loop While dr.NextResult

            dr.Close()
        Catch ex As Exception

        End Try


        cmd.Dispose()
        conn.Close()
        conn.Dispose()


        Return dtLIST





    End Function

End Class
