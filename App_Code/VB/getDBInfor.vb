﻿Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports System.IO
Imports System.Xml

Public Class getDBInfor

    Shared Function getExchangeForex(ByVal typeDollar As String) As String

        Dim returnValue As New StringBuilder(5000)


        Dim connstr As String = ""
        Dim sqlcmd As String = ""

        connstr = System.Web.Configuration.WebConfigurationManager.ConnectionStrings("_24_global_ConnStr").ToString

        sqlcmd = "SELECT a.CurrCode, a.sourceCName, b.fcPrice FROM fcExchangeDB a,fcExchangeDB2 b where a.currcode=b.currcode and a.currcode <> 'DX'"

        Dim conn As New SqlConnection(connstr)
        Dim cmd As New SqlCommand(sqlcmd, conn)
        cmd.CommandType = CommandType.Text
        Dim dr As SqlDataReader

        Try
            conn.Open()
            dr = cmd.ExecuteReader()

            While dr.Read


                Dim cnameary() As String = dr(1).ToString().Trim().Split("/")
                Dim fcprice As String = dr(2).ToString().Trim()

                Dim CName As String = ""


                If cnameary(0) = "美元" Then  '取股名的斜線左右邊name
                    CName = cnameary(1)
                Else
                    CName = cnameary(0)
                End If

                If CName = "台幣" Then

                    If typeDollar = "台幣" Then
                        returnValue.Append("<option selected=selected value=""" + CName + "-" + dr(2).ToString().Trim() + """>" + CName + "</option>" + "<option value=""美元-1"">美元</option>")
                    Else
                        returnValue.Append("<option value=""" + CName + "-" + dr(2).ToString().Trim() + """>" + CName + "</option>" + "<option selected=selected value=""美元-1"">美元</option>")
                    End If

                Else
                    returnValue.Append("<option value=""" + CName + "-" + fcprice + """>" + CName + "</option>")
                End If

            End While

            dr.Close()
        Catch ex As Exception

        Finally


            cmd.Dispose()
            conn.Close()
            conn.Dispose()
        End Try

        Return pageInforTxt.chgTXT(returnValue.ToString())
    End Function
    Shared Function getExchangeListFinal(ByVal bl As String, ByVal mycode As String, Optional ByVal TWD_CNY As Boolean = False) As String
        Dim returnValue As New StringBuilder(5000)


        Dim connstr As String = ""
        Dim sqlcmd As String = ""

        connstr = System.Web.Configuration.WebConfigurationManager.ConnectionStrings("_24_global_ConnStr").ToString

        sqlcmd = "forex_exchange"

        Dim conn As New SqlConnection(connstr)
        Dim cmd As New SqlCommand(sqlcmd, conn)
        cmd.CommandType = CommandType.StoredProcedure


        Dim p1 As New SqlParameter("@beforeOrLast", SqlDbType.VarChar, 10)
        p1.Direction = ParameterDirection.Input
        p1.Value = checkSQL.CheckSQL(bl)
        cmd.Parameters.Add(p1)

        Dim p2 As New SqlParameter("@CurrCode", SqlDbType.VarChar, 10)
        p2.Direction = ParameterDirection.Input
        p2.Value = checkSQL.CheckSQL(mycode)
        cmd.Parameters.Add(p2)


        Dim p3 As New SqlParameter("@searchCode", SqlDbType.VarChar, 10)
        p3.Direction = ParameterDirection.Input
        p3.Value = ""
        cmd.Parameters.Add(p3)


        Dim dr As SqlDataReader


        If TWD_CNY = False Then
            returnValue.Append("<table>")
            returnValue.Append("<tr class='bgclr'>")
            returnValue.Append("<th width='20%' class='rt'>商品名稱</th>")
            returnValue.Append("<th width='20%' class='rt'>成交價</th>")
            returnValue.Append("<th width='20%' class='rt'>漲跌 </th>")
            returnValue.Append("<th width='20%' class='rt'>漲% </th>")
            returnValue.Append("<th width='20%' class='rt'>前日收盤價</th>")
            returnValue.Append("</tr>")
        Else

            returnValue.Append("<table>")
            returnValue.Append("<tr>")
            returnValue.Append("<th width='20%' class='rt'>名稱</th>")
            returnValue.Append("<th width='16%' class='rt'>成交價</th>")
            returnValue.Append("<th width='16%' class='rt'>漲跌</th>")
            returnValue.Append("<th width='16%' class='rt'>漲%</th>")
            returnValue.Append("<th width='16%' class='rt'>昨收</th>")
            returnValue.Append("<th width='16%' class='rt'>時間</th>")
            returnValue.Append("</tr>")
        End If


        Try
            conn.Open()
            dr = cmd.ExecuteReader()

            Dim highV As Double = 0.0
            Dim lowV As Double = 0.0


            While dr.Read


                '移除委內瑞拉幣
                If dr(0).ToString().Trim().ToUpper().IndexOf("VEB") < 0 Then

                    If TWD_CNY = False Then

                        returnValue.Append("<tr>")

                        'returnValue.Append("<td><a title='" + dr(1).ToString().Trim().ToUpper() + "' href='flashchart.aspx?fccode=" + dr(0).ToString().Trim().ToUpper() + "&fcname=" + System.Web.HttpUtility.UrlEncode(dr(1).ToString().Trim().ToUpper()) + "&rate=exchange'>" + dr(1).ToString().Trim().ToUpper() + "</a></td>")
                        returnValue.Append("<td><a title='" + dr(1).ToString().Trim().ToUpper() + "' href='http://b2b.cnyes.com/news/msn/forex_chart.aspx?symbol=" + dr(0).ToString().Trim().ToUpper() + "&fcname=" + System.Web.HttpUtility.UrlEncode(dr(1).ToString().Trim().ToUpper()) + "&rate=exchange'>" + dr(1).ToString().Trim().ToUpper() + "</a></td>")

                        If Convert.ToDouble(dr(5)) > 0 Then

                            returnValue.Append("<td class='rt r'>" + Convert.ToDouble(dr(2)).ToString("f4") + "</td>")
                            returnValue.Append("<td class='rt r'>" + Convert.ToDouble(dr(4)).ToString("f4") + "</td>")
                            returnValue.Append("<td class='rt r'>" + Convert.ToDouble(dr(5)).ToString("f4") + "</td>")

                        ElseIf Convert.ToDouble(dr(5)) < 0 Then

                            returnValue.Append("<td class='rt g'>" + Convert.ToDouble(dr(2)).ToString("f4") + "</td>")
                            returnValue.Append("<td class='rt g'>" + Convert.ToDouble(dr(4)).ToString("f4") + "</td>")
                            returnValue.Append("<td class='rt g'>" + Convert.ToDouble(dr(5)).ToString("f4") + "</td>")

                        Else

                            returnValue.Append("<td class='rt'>" + Convert.ToDouble(dr(2)).ToString("f4") + "</td>")
                            returnValue.Append("<td class='rt'>" + Convert.ToDouble(dr(4)).ToString("f4") + "</td>")
                            returnValue.Append("<td class='rt'>" + Convert.ToDouble(dr(5)).ToString("f4") + "</td>")

                        End If


                        returnValue.Append("<td class='rt'>" + Convert.ToDouble(dr(3)).ToString("f4") + "</td>")

                        returnValue.Append("</tr>")

                    Else

                        If dr(0).ToString().Trim().ToUpper().IndexOf("USD") >= 0 Or dr(0).ToString().Trim().ToUpper().IndexOf("EUR") >= 0 Or dr(0).ToString().Trim().ToUpper().IndexOf("CNY") >= 0 Or dr(0).ToString().Trim().ToUpper().IndexOf("HKD") >= 0 Or dr(0).ToString().Trim().ToUpper().IndexOf("JPY") >= 0 Or dr(0).ToString().Trim().ToUpper().IndexOf("KRW") >= 0 Or dr(0).ToString().Trim().ToUpper().IndexOf("AUD") >= 0 Or dr(0).ToString().Trim().ToUpper().IndexOf("NZD") >= 0 Or dr(0).ToString().Trim().ToUpper().IndexOf("ZAR") >= 0 Or dr(0).ToString().Trim().ToUpper().IndexOf("CAD") >= 0 Or dr(0).ToString().Trim().ToUpper().IndexOf("GBP") >= 0 Then
                            returnValue.Append("<tr>")

                            returnValue.Append("<td><a title='" + dr(1).ToString().Trim().ToUpper() + "' href='flashchart.aspx?fccode=" + dr(0).ToString().Trim().ToUpper() + "&fcname=" + System.Web.HttpUtility.UrlEncode(dr(1).ToString().Trim().ToUpper()) + "&rate=exchange'>" + dr(1).ToString().Trim().ToUpper() + "</a></td>")

                            If Convert.ToDouble(dr(5)) > 0 Then

                                returnValue.Append("<td class='rt r'>" + Convert.ToDouble(dr(2)).ToString("f4") + "</td>")
                                returnValue.Append("<td class='rt r'>" + Convert.ToDouble(dr(4)).ToString("f4") + "</td>")
                                returnValue.Append("<td class='rt r'>" + Convert.ToDouble(dr(5)).ToString("f4") + "</td>")

                            ElseIf Convert.ToDouble(dr(5)) < 0 Then

                                returnValue.Append("<td class='rt g'>" + Convert.ToDouble(dr(2)).ToString("f4") + "</td>")
                                returnValue.Append("<td class='rt g'>" + Convert.ToDouble(dr(4)).ToString("f4") + "</td>")
                                returnValue.Append("<td class='rt g'>" + Convert.ToDouble(dr(5)).ToString("f4") + "</td>")

                            Else

                                returnValue.Append("<td class='rt'>" + Convert.ToDouble(dr(2)).ToString("f4") + "</td>")
                                returnValue.Append("<td class='rt'>" + Convert.ToDouble(dr(4)).ToString("f4") + "</td>")
                                returnValue.Append("<td class='rt'>" + Convert.ToDouble(dr(5)).ToString("f4") + "</td>")

                            End If


                            returnValue.Append("<td class='rt'>" + Convert.ToDouble(dr(3)).ToString("f4") + "</td>")
                            returnValue.Append("<td class='rt'>" + Date.Now.ToString("HH:mm") + "</td>")

                            returnValue.Append("</tr>")
                        End If





                    End If

                End If




            End While


            dr.Close()
        Catch ex As Exception

        Finally


            cmd.Dispose()
            conn.Close()
            conn.Dispose()
        End Try

        returnValue.Append("</table>")

        Return pageInforTxt.chgTXT(returnValue.ToString())

    End Function
    Shared Function getExchangeList(ByVal selectType As String) As String

        Dim returnValue As New StringBuilder(5000)


        Dim connstr As String = ""
        Dim sqlcmd As String = ""

        connstr = System.Web.Configuration.WebConfigurationManager.ConnectionStrings("_24_news_ConnStr").ToString

        sqlcmd = "SELECT * FROM Extable2"

        Dim conn As New SqlConnection(connstr)
        Dim cmd As New SqlCommand(sqlcmd, conn)
        cmd.CommandType = CommandType.Text
        Dim dr As SqlDataReader

        Try
            conn.Open()
            dr = cmd.ExecuteReader()

            While dr.Read
                '<option value="" selected="selected">美元</option>
                If selectType = dr(0).ToString().Trim() Then
                    returnValue.Append("<option value='" + dr(0).ToString().Trim() + "' selected='selected' >" + dr(1).ToString().Trim() + "</option>")
                Else
                    returnValue.Append("<option value='" + dr(0).ToString().Trim() + "' >" + dr(1).ToString().Trim() + "</option>")
                End If

            End While

            dr.Close()

        Catch ex As Exception

        Finally


            cmd.Dispose()
            conn.Close()
            conn.Dispose()
        End Try

        Return pageInforTxt.chgTXT(returnValue.ToString())
    End Function


End Class
