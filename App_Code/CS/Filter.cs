﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Text.RegularExpressions;//導入命名空間(正則表達式)


/// <summary>
/// 過濾變數內容
/// </summary>
public class Filter
{
    public Filter()
    { }


    //過濾指定範圍字串
    public string KillCodeFilter(string StrContent, string strBinCode, string strEndCode)
    {
        string strRegex = strBinCode + "(.+?)" + strEndCode;
        Regex reg = new Regex(strRegex, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
        StrContent = reg.Replace(StrContent, "");
        return StrContent;
    }

    public bool IsNumeric(String strNumber)
    {
        Regex NumberPattern = new Regex("[^0-9.-]");
        return !NumberPattern.IsMatch(strNumber);
    }

    public bool IsSafeNumeric(String strNumber)
    {
        bool reSult = false;
        if (isSafeString(strNumber))
        {
            Regex NumberPattern = new Regex("[^0-9.-]");
            reSult = !NumberPattern.IsMatch(strNumber);
        }

        return reSult;
    }

    public bool isSafeString(string str)
    {
        Boolean errSpec = true;
        string strStr = "";

        if (str != "") strStr = str.ToString().Trim();

        string strOK = strStr.ToLower();
        string[] SpecStr = new string[23];

        SpecStr[0] = "net%20user";
        SpecStr[1] = "cmdshell";
        SpecStr[2] = "%3C";
        SpecStr[3] = "exec%20master.dbo.xp_cmdshell";
        SpecStr[4] = "net localgroup administrators";
        SpecStr[5] = "update";
        SpecStr[6] = "exec%20";
        SpecStr[7] = "char(";
        SpecStr[8] = "insert";
        SpecStr[9] = "delete";
        SpecStr[10] = "drop";
        SpecStr[11] = "truncate";
        SpecStr[12] = "script";
        SpecStr[13] = "iframe";
        SpecStr[14] = "exec%20";
        SpecStr[15] = "declare";
        SpecStr[16] = "cursor";
        SpecStr[17] = "fetch";
        SpecStr[18] = "cast(";
        SpecStr[19] = "'";
        SpecStr[20] = "--";
        SpecStr[21] = @"""";
        SpecStr[22] = "|";

        for (int i = 0; i < SpecStr.Length - 1; i++)
        {
            if (strOK.IndexOf(SpecStr[i], 0) > -1)
            {
                errSpec = false;
                break;
            }
        }

        return errSpec;
    }


    //過濾Query字串
    public List<string> isSafeQryString(Page pg, string[] QryList)
    {
        List<string> list = new List<string>();
        string val = string.Empty;

        if (QryList.Length == 1)
        {
            try
            {
                val = pg.Request.QueryString[QryList[0]].ToString();
            }
            catch (Exception)
            {
                val = "";
                //throw;
            }

            val = comm.SQLSafe(val);
            list.Add(val);
        }
        else
        {
            for (int i = 0; i < QryList.Length - 1; i++)
            {
                val = "";

                try
                {
                    val = pg.Request.QueryString[QryList[i]].ToString();
                }
                catch (Exception)
                {
                    val = "";
                    //throw;
                }

                val = comm.SQLSafe(val);
                list.Add(val);
            }
        }

        return list;
    }
    public List<string> isSafeQryString(UserControl pg, string[] QryList)
    {
        List<string> list = new List<string>();
        string val = string.Empty;

        if (QryList.Length == 1)
        {
            try
            {
                val = pg.Request.QueryString[QryList[0]].ToString();
            }
            catch (Exception)
            {
                val = "";
                //throw;
            }

            val = comm.SQLSafe(val);
            list.Add(val);
        }
        else
        {
            for (int i = 0; i < QryList.Length - 1; i++)
            {
                val = "";

                try
                {
                    val = pg.Request.QueryString[QryList[i]].ToString();
                }
                catch (Exception)
                {
                    val = "";
                    //throw;
                }

                val = comm.SQLSafe(val);
                list.Add(val);
            }
        }

        return list;
    }
}