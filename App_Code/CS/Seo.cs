﻿using System;
using System.Collections.Generic;
using System.Web;

/// <summary>
/// Seo 的摘要描述
/// </summary>
public class Seo
{
    #region 變數
    private string _Val = String.Empty;

    private Enums.euNavigationCategory? _NavigationCategory = null;

    private Enums.euBankServiceNavigations? _BankServiceNavigations = null;

    private Enums.eu牌告利率Navigations? _牌告利率Navigations = null;

    private Enums.eu外幣存款利率Navigations? _外幣存款利率Navigations = null;

    private Enums.eu存款利率Navigations? _存款利率Navigations = null;

    private Enums.eu銀行換匯比較表Navigations? _銀行換匯比較表Navigations = null;

    private string _BName = String.Empty;

    private string _CurrencyName = String.Empty;
    #endregion 變數

    #region 屬性

    #region Enums.euNavigationCategory? NavigationCategory
    public Enums.euNavigationCategory? NavigationCategory
    {
        set
        {
            _NavigationCategory = value;
        }
    }
    #endregion Enums.euNavigationCategory? NavigationCategory

    #region Enums.euBankServiceNavigations? BankServiceNavigations
    public Enums.euBankServiceNavigations? BankServiceNavigations
    {
        set
        {
            _BankServiceNavigations = value;
        }
    }
    #endregion Enums.euBankServiceNavigations? BankServiceNavigations

    #region Enums.eu牌告利率Navigations? 牌告利率Navigations
    public Enums.eu牌告利率Navigations? 牌告利率Navigations
    {
        set
        {
            _牌告利率Navigations = value;
        }
    }
    #endregion Enums.eu牌告利率Navigations? 牌告利率Navigations

    #region Enums.eu外幣存款利率Navigations? 外幣存款利率Navigations
    public Enums.eu外幣存款利率Navigations? 外幣存款利率Navigations
    {
        set
        {
            _外幣存款利率Navigations = value;
        }
    }
    #endregion Enums.eu外幣存款利率Navigations? 外幣存款利率Navigations

    #region Enums.eu存款利率Navigations? 存款利率Navigations
    public Enums.eu存款利率Navigations? 存款利率Navigations
    {
        set
        {
            _存款利率Navigations = value;
        }
    }
    #endregion Enums.eu存款利率Navigations? 存款利率Navigations

    #region Enums.eu銀行換匯比較表Navigations? 銀行換匯比較表Navigations
    public Enums.eu銀行換匯比較表Navigations? 銀行換匯比較表Navigations
    {
        set
        {
            _銀行換匯比較表Navigations = value;
        }
    }
    #endregion Enums.eu銀行換匯比較表Navigations? 銀行換匯比較表Navigations

    #region string BName
    public string BName
    {
        set
        {
            _BName = value;
        }
    }
    #endregion string BName

    #region string CurrencyName
    public string CurrencyName
    {
        set
        {
            _CurrencyName = value;
        }
    }
    #endregion string CurrencyName

    #endregion 屬性

    #region keyword
    private string _keyword;
    public string keyword
    {
        set
        {
            _keyword = value;
        }
        get
        {
            return _keyword;
        }
    }
    #endregion

    #region title
    private string _sTitle;
    public string sTitle
    {
        set
        {
            _sTitle = value;
        }
        get
        {
            return _sTitle;
        }
    }
    #endregion

    #region desc
    private string _sDesc;
    public string sDesc
    {
        set
        {
            _sDesc = value;
        }
        get
        {
            return _sDesc;
        }
    }
    #endregion

    #region H1
    private string _sH1;
    public string sH1
    {
        set
        {
            _sH1 = value;
        }
        get
        {
            return _sH1;
        }
    }
    #endregion


    public Seo()
    {
        
    }

    public Seo (string val)
    {
        _Val = val;
        Create();
    }

    #region void Create ()
    public void Create ()
    {
        _sTitle = "理財_理財中心_鉅亨網";
        _sH1 = "理財規劃與財富管理";
        _sDesc = "鉅亨網理財頻道提供：兩岸三地各家銀行利率查詢、投資屬性分析、房屋買賣觀測、理財規劃、理財雜誌、民生物價、基金配息、高股息、基金試算、銀行試算、投資試算、購屋試算、貸款試算、退休試算等各項理財資訊與試算工具，幫助您做好人生理財規劃的好幫手。";
        _keyword = "理財,財富管理,存款,定存,放款,貸款,信用卡,現金卡,外幣,基金,房產,退休,保險,油價,黃金,理財雜誌,高股息,配息";

        switch (_Val)
        {
            //-----------------------------
            //試算工具	銀行試算
            //-----------------------------
            case "BankCalculation_01":
                _sTitle = "銀行定存_試算工具_理財";
                _sH1 = "銀行定存_試算工具_理財";
                _sDesc = "提供您銀行台幣定存整存整付、存本取息、零存整付等利息收入的試算工具";
                _sH1 = "銀行定存";
                _keyword = "銀行,存款,台幣定存,整存整付,存本取息,零存整付";
                break;
            case "BankCalculation_02":
                _sTitle = "外幣定存_試算工具_理財";
                _sH1 = "外幣定存_試算工具_理財";
                _sDesc = "提供您銀行外幣定存利息收入的試算工具";
                _keyword = "銀行,存款,外幣定存";
                break;
            case "BankCalculation_03":
                _sTitle = "外幣定存_試算工具_理財";
                _sH1 = "外幣定存_試算工具_理財";
                _sDesc = "提供您單利利息的試算工具";
                _keyword = "單利";
                break;
            case "BankCalculation_04":
                _sTitle = "外幣定存_試算工具_理財";
                _sH1 = "外幣定存_試算工具_理財";
                _sDesc = "提供您單利利息的試算工具";
                _keyword = "複利現值,PVIF";
                break;
            case "BankCalculation_05":
                _sTitle = "外幣定存_試算工具_理財";
                _sH1 = "外幣定存_試算工具_理財";
                _sDesc = "提供您單利利息的試算工具";
                _keyword = "複利終值,FVIF";
                break;
            case "BankCalculation_06":
                _sTitle = "外幣定存_試算工具_理財";
                _sH1 = "外幣定存_試算工具_理財";
                _sDesc = "提供您單利利息的試算工具";
                _keyword = "年金現值,PVIFA";
                break;
            case "BankCalculation_07":
                _sTitle = "外幣定存_試算工具_理財";
                _sH1 = "外幣定存_試算工具_理財";
                _sDesc = "提供您單利利息的試算工具";
                _keyword = "年金終值,FVIFA";
                break;
            //-----------------------------
            //試算工具	投資試算
            //-----------------------------
            case "InvestCalculation_01":
                _sTitle = "基金試算_試算工具_理財";
                _sH1 = "基金試算";
                _sDesc = "提供您基金單筆投資、定期定額報酬率試算工具";
                _keyword = "基金試算,單筆申購,定期定額";
                break;
            case "InvestCalculation_02":
                _sTitle = "租金報酬率_試算工具_理財";
                _sH1 = "租金報酬率";
                _sDesc = "提供您房屋出租收入、租賃報酬率試算工具";
                _keyword = "租金收入,租賃所得";
                break;
            case "InvestCalculation_03":
                _sTitle = "股票報酬率_試算工具_理財";
                _sH1 = "股票報酬率";
                _sDesc = "提供您股票投資報酬率試算工具";
                _keyword = "股票報酬,股票所得";
                break;
            //-----------------------------
            //試算工具	購屋試算
            //-----------------------------
            case "Housing_01":
                _sTitle = "購屋出租投資報酬率_試算工具_理財";
                _sH1 = "購屋出租-投資報酬率";
                _sDesc = "提供您房屋出租報酬率試算工具";
                _keyword = "購屋,出租";
                break;
            case "Housing_02":
                _sTitle = "購屋和租屋哪個划算_試算工具_理財";
                _sH1 = "購屋和租屋哪個划算";
                _sDesc = "提供您購屋與租屋比較試算工具";
                _keyword = "購屋,租屋";
                break;
            case "Housing_03":
                _sTitle = "購屋需準備多少錢_試算工具_理財";
                _sH1 = "購屋需準備多少錢";
                _sDesc = "提供您購屋買房規劃試算工具";
                _keyword = "購屋,買房";
                break;
            case "Housing_04":
                _sTitle = "房貸本息平均攤還_試算工具_理財";
                _sH1 = "房貸本息平均攤還";
                _sDesc = "提供您房貸本息平均攤還利息試算工具";
                _keyword = "房貸,本息平均攤還";
                break;
            case "Housing_05":
                _sTitle = "房貸本金平均攤還_試算工具_理財";
                _sH1 = "房貸本金平均攤還";
                _sDesc = "提供您房貸本金平均攤還利息試算工具";
                _keyword = "房貸,本金平均攤還";
                break;
            case "Housing_06":
                _sTitle = "房貸雙週還款_試算工具_理財";
                _sH1 = "房貸雙週還款";
                _sDesc = "提供您房貸雙週還款利息試算工具";
                _keyword = "房貸,雙週還款";
                break;
            case "Housing_07":
                _sTitle = "房貸多段利率_試算工具_理財";
                _sH1 = "房貸多段利率";
                _sDesc = "提供您房貸多段利率利息試算工具";
                _keyword = "房貸,多段利率";
                break;
            //-----------------------------
            //試算工具	貸款試算
            //-----------------------------
            case "CreditCalculation_01":
                _sTitle = "貸款金額試算(分期付款)_試算工具_理財";
                _sH1 = "貸款金額試算(分期付款)";
                _sDesc = "提供您貸款分期付款試算工具";
                _keyword = "貸款,分期付款";
                break;
            case "CreditCalculation_02":
                _sTitle = "貸款利率試算_試算工具_理財";
                _sH1 = "貸款利率試算";
                _sDesc = "提供您貸款利率試算工具";
                _keyword = "貸款,貸款利息";
                break;
            case "CreditCalculation_03":
                _sTitle = "信用卡手續費利率_試算工具_理財";
                _sH1 = "信用卡手續費利率";
                _sDesc = "提供您信用卡手續費率試算工具";
                _keyword = "信用卡,手續費";
                break;
            //-----------------------------
            //試算工具	退休試算
            //-----------------------------
            case "RetireCalculation_01":
                _sTitle = "退休要準備多少錢_試算工具_理財";
                _sH1 = "退休理財規劃";
                _sDesc = "提供您退休理財規劃試算工具";
                _keyword = "退休,退休規劃";
                break;
            //-----------------------------
            //投資分析	
            //-----------------------------
            //基金試算
            case "fundptcalcu_01":
                _sTitle = "基金試算_投資分析_理財";
                _sH1 = "基金試算";
                _sDesc = "提供基金單筆申購與定期定額報酬率模擬試算。";
                _keyword = "基金投資,單筆申購,定期定額";
                break;
            //投資屬性分析
            case "InvestmentRisk_01":
                _sTitle = "投資屬性分析_投資分析_理財";
                _sH1 = "投資屬性分析";
                _sDesc = "提供投資風險屬性測驗，以衡量自己所能承受的風格等級，了解自己適合的投資風格。";
                _keyword = "投資風險屬性測試,投資風格,風險等級";
                break;
            //-----------------------------
            //理財比較	
            //-----------------------------
            //存款利率
            case "bcdeposit_01":
                _sTitle = "存款利率_理財比較_理財";
                _sH1 = "存款利率";
                _sDesc = "提供台灣、香港、中國地區各家銀行存款利率比較圖。";
                _keyword = "存款,活存,定存";
                break;
            //外幣存款利率
            case "bcforeign_01":
                _sTitle = "外幣存款利率_理財比較_理財";
                _sH1 = "外幣存款利率";
                _sDesc = "提供台灣、香港、中國地區各家銀行外幣存款利率比較圖。";
                _keyword = "外幣,外幣存款,外幣定存";
                break;
            //指數型房貸利率
            case "housebc_01":
                _sTitle = "指數型房貸利率_理財比較_理財";
                _sH1 = "指數型房貸利率";
                _sDesc = "提供台灣地區各家銀行指數型房貸利率比較圖。";
                _keyword = "房貸, 指數房貸";
                break;
            //信用卡利率
            case "creditbc_01":
                _sTitle = "信用卡利率_理財比較_理財";
                _sH1 = "信用卡利率";
                _sDesc = "提供台灣地區各家銀行信用卡最高、最低利率比較圖。";
                _keyword = "信用卡";
                break;
            //銀行換匯
            case "bkexchge_01":
                _sTitle = "銀行換匯_理財比較_理財";
                _sH1 = "銀行換匯";
                _sDesc = "提供台灣地區各家銀行換匯價格比較圖。";
                _keyword = "換匯,外匯,匯價";
                break;
            //現金卡利率
            case "cashbc_01":
                _sTitle = "現金卡利率_理財比較_理財";
                _sH1 = "現金卡利率";
                _sDesc = "提供台灣地區各家銀行現金卡最高、最低利率比較圖。";
                _keyword = "現金卡";
                break;
            //-----------------------------
            //民生物價
            //-----------------------------
            case "livelihoodPrice_01":
                _sTitle = "民生物價_理財";
                _sH1 = "民生物價";
                _sDesc = "提供資料來源為台灣行政院主計處所統計之民生物價統計圖表。";
                _keyword = "食用油,肉類,蛋類,水果,蔬菜,水產,油料費,交通,水電,房租,成衣,醫療,教養";
                break;
            //-----------------------------
            //首頁
            //-----------------------------
            default:
                if (_NavigationCategory != null && ((Enums.euNavigationCategory)_NavigationCategory) == Enums.euNavigationCategory.銀行服務)
                {
                    //System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    //sb.AppendLine(String.Format("{0} = {1}", "_NavigationCategory", _NavigationCategory));
                    //sb.AppendLine(String.Format("{0} = {1}", "_BankServiceNavigations", _BankServiceNavigations));
                    //sb.AppendLine(String.Format("{0} = {1}", "_牌告利率Navigations", _牌告利率Navigations));
                    //WriteLog.Log("2   " + sb.ToString());

                    switch ((Enums.euBankServiceNavigations)_BankServiceNavigations)
                    {
                        #region Enums.euBankServiceNavigations.牌告利率
                        case Enums.euBankServiceNavigations.牌告利率:
                            if (_牌告利率Navigations == null || ((Enums.eu牌告利率Navigations)_牌告利率Navigations) == Enums.eu牌告利率Navigations.None)
                                _牌告利率Navigations = Enums.eu牌告利率Navigations.銀行牌告利率;

                            if (_牌告利率Navigations == null || ((Enums.eu牌告利率Navigations)_牌告利率Navigations) == Enums.eu牌告利率Navigations.銀行牌告利率)
                            {
                                _sH1 = Enums.eu牌告利率Navigations.銀行牌告利率.ToString();
                                _sDesc = "提供台灣、香港、中國地區，各家銀行牌告利率表。";
                                _keyword = "存款利率、定存利率、放款利率、房貸利率、信用卡利率";
                            }
                            else
                            {
                                _sDesc = "提供台灣地區銀行各項業務查詢，包含存款利率、定存利率、放款利率、房貸利率、信用卡利率等。";
                                _keyword = "基準利率,活期存款,定期存款,信用卡利率,活期儲蓄存款,信託資金存款,薪資轉帳活期儲蓄存款,";
                            }

                            _sTitle = String.Format("{0}_{1}_{2}_理財", _牌告利率Navigations, _BankServiceNavigations, _NavigationCategory);

                            break;
                        #endregion Enums.euBankServiceNavigations.牌告利率
                        #region Enums.euBankServiceNavigations.外幣存款利率
                        case Enums.euBankServiceNavigations.外幣存款利率:

                            if (_外幣存款利率Navigations == null || ((Enums.eu外幣存款利率Navigations)_外幣存款利率Navigations) == Enums.eu外幣存款利率Navigations.None)
                                _外幣存款利率Navigations = Enums.eu外幣存款利率Navigations.台灣利率排行;

                            _sTitle = String.Format("{0}_{1}_{2}_理財", ((_外幣存款利率Navigations != Enums.eu外幣存款利率Navigations.綜合利率排行) ? String.Format("{0}地區銀行", _外幣存款利率Navigations.ToString().Substring(0, 2)) : _外幣存款利率Navigations.ToString().Substring(0, 4)), _BankServiceNavigations, _NavigationCategory);

                            if (_外幣存款利率Navigations != Enums.eu外幣存款利率Navigations.綜合利率排行)
                                _sDesc = String.Format("提供{0}地區各家銀行外幣存款利率表。", _外幣存款利率Navigations.ToString().Substring(0, 2));
                            else
                                _sDesc = "提供台灣、香港、中國地區，各家銀行外幣存款利率表。";

                            #region //Mark
                            //switch ((Enums.eu外幣存款利率Navigations)_外幣存款利率Navigations)
                            //{
                            //    case Enums.eu外幣存款利率Navigations.台灣利率排行:
                            //        _keyword = "美元,歐元,英鎊,澳幣,紐幣,加拿大幣,新加坡幣,瑞士法郎,港幣";
                            //        break;
                            //    case Enums.eu外幣存款利率Navigations.中國利率排行:
                            //        _keyword = "人民幣,美元,歐元,英鎊,澳幣,紐幣,加拿大幣,新加坡幣,瑞士法郎,日元,泰銖";
                            //        break;
                            //    case Enums.eu外幣存款利率Navigations.香港利率排行:
                            //        _keyword = "美元,歐元,英鎊,澳幣,加拿大幣,新加坡幣,瑞士法郎,日元,港幣";
                            //        break;
                            //    case Enums.eu外幣存款利率Navigations.綜合利率排行:
                            //        _keyword = "人民幣,美元,歐元,英鎊,澳幣,加拿大幣,新加坡幣,瑞士法郎,港幣";
                            //        break;
                            //}
                            #endregion //Mark

                            string[] keys = { "紐幣,港幣", "紐幣,人民幣,日元,泰銖", "港幣,日元", "港幣,人民幣" };
                            _keyword = String.Format("美元,歐元,英鎊,澳幣,加拿大幣,新加坡幣,瑞士法郎,{0}", keys[((byte)_外幣存款利率Navigations) - 1]);
                            break;
                        #endregion Enums.euBankServiceNavigations.外幣存款利率
                        #region Enums.euBankServiceNavigations.存款利率
                        case Enums.euBankServiceNavigations.存款利率:
                            if (_存款利率Navigations == null || ((Enums.eu存款利率Navigations)_存款利率Navigations) == Enums.eu存款利率Navigations.None)
                                _存款利率Navigations = Enums.eu存款利率Navigations.台灣利率排行;

                            _sTitle = String.Format("{0}地區銀行_{1}_{2}_理財", _存款利率Navigations.ToString().Substring(0, 2), _BankServiceNavigations, _NavigationCategory);

                            _sDesc = String.Format("提供{0}地區各家銀行存款利率表", _存款利率Navigations.ToString().Substring(0, 2));

                            _keyword = String.Format("活期儲蓄存款,定期存款{0}", ((((Enums.eu存款利率Navigations)_存款利率Navigations) == Enums.eu存款利率Navigations.台灣利率排行) ? ",定期儲蓄存款" : String.Empty));
                            break;
                        #endregion Enums.euBankServiceNavigations.存款利率
                        #region Enums.euBankServiceNavigations.放款利率
                        case Enums.euBankServiceNavigations.放款利率:
                            _sTitle = String.Format("台灣放款利率_{0}_{1}_理財", _BankServiceNavigations, _NavigationCategory);
                            _sDesc = "提供台灣地區各家銀行放款利率表。";
                            _keyword = "信用卡利率,現金卡利率,指數房貸利率	放款利率";
                            break;
                        #endregion Enums.euBankServiceNavigations.放款利率
                        #region Enums.euBankServiceNavigations.銀行換匯
                        case Enums.euBankServiceNavigations.銀行換匯:
                            _sTitle = String.Format("{0}{1}_{2}_理財", ((_BName.Length > 0 || _CurrencyName.Length > 0) ? ((_BName.Length > 0) ? _BName : _CurrencyName) + "_" : String.Empty), _BankServiceNavigations, _NavigationCategory);

                            if (_CurrencyName.Length > 0)
                            {
                                _sDesc = "提供台灣地區銀行兌換各國外匯價格。";
                                _keyword = "美元,歐元,英鎊,澳幣,紐幣,加拿大幣,新加坡幣,瑞士法郎,港幣";
                            }
                            else
                            {
                                _sDesc = "提供台灣地區各家銀行牌告匯價。";
                                _keyword = "銀行";
                            }
                            break;
                        #endregion Enums.euBankServiceNavigations.銀行換匯
                        #region Enums.euBankServiceNavigations.銀行換匯比較表
                        case Enums.euBankServiceNavigations.銀行換匯比較表:
                            if (_銀行換匯比較表Navigations == null || ((Enums.eu銀行換匯比較表Navigations)_銀行換匯比較表Navigations) == Enums.eu銀行換匯比較表Navigations.None)
                                _銀行換匯比較表Navigations = Enums.eu銀行換匯比較表Navigations.現金;

                            _sTitle = String.Format("{0}_{1}_{2}_理財", _銀行換匯比較表Navigations, _BankServiceNavigations, _NavigationCategory);

                            _sDesc = ((((Enums.eu銀行換匯比較表Navigations)_銀行換匯比較表Navigations) == Enums.eu銀行換匯比較表Navigations.現金) ? "提供台灣地區各家銀行即期買進賣出牌告匯價。" : "提供台灣地區各家銀行外匯現金買進賣出牌告匯價。");

                            _keyword = "美元,USD,歐元,EUR,日元,JPY,港幣,HKD,人民幣,CNY,澳幣,AUD";
                            break;
                        #endregion Enums.euBankServiceNavigations.銀行換匯比較表
                    }
                }
                break;
        }
    }
    #endregion void Create ()
}