﻿using System;
using System.Reflection;
using System.ComponentModel;

/// <summary>
/// Enums 的摘要描述
/// </summary>
public class Enums
{
    #region euOrderDirection : byte
    public enum euOrderDirection : byte
    {
        ASC,
        DESC
    }
    #endregion euOrderDirection : byte

    #region euNavigationCategory : byte
    /// <summary>
    /// 導覽列選單
    /// </summary>
    /// <![CDATA[
    /// 銀行服務 Description 必須包含 Rule.xml 的 BankService.aspx $1
    /// ]]>
    public enum euNavigationCategory : byte
    {
        [Description("index.htm")]
        首頁,
        [Description("BankService.aspx")]
        銀行服務,
        [Description("fundptcalcu.aspx")]
        投資分析,
        //[Description("houseSale.html")]
        //房產買賣,
        //[Description("moneyPlan.aspx")]
        //理財規劃,
        //[Description("bcdeposit.aspx")]
        //理財比較,
        [Description("livelihoodPrice.htm")]
        民生物價,
        [Description("BankCalculation.aspx")]
        試算工具,
        [Description("http://news.cnyes.com/tw_money/list.shtml")]
        理財新聞,
        [Description("http://tw.mag.cnyes.com/Column/5/")]
        理財雜誌,
        [Description("http://www.cnyes.com/insurance/")]
        保險,
        [Description("http://www.cnyes.com/globalinvest/GlobalStock.htm")]
        股市地圖
    }
    #endregion euNavigationCategory : byte

    #region //euBankServiceNavigations : byte
    ///// <summary>
    ///// 銀行服務下的子選單
    ///// </summary>
    //public enum euBankServiceNavigations : byte
    //{
    //    None = 0,
    //    [Description("ValutaDepositRate.aspx")]
    //    外幣存款利率 = 1,
    //    [Description("DepositRate.aspx")]
    //    存款利率 = 2,
    //    [Description("UsuryRate.aspx")]
    //    放款利率,
    //    [Description("BankMoneyExchange.aspx")]
    //    銀行換匯
    //}
    #endregion //euBankServiceNavigations : byte

    #region euBankServiceNavigations : byte
    /// <summary>
    /// 銀行服務下的子選單
    /// </summary>
    public enum euBankServiceNavigations : byte
    {
        None = 0,
        [Description("PublicRate.htm")]
        牌告利率 = 1,
        [Description("ValutaDepositRate.htm")]
        外幣存款利率 = 2,
        [Description("DepositRate.htm")]
        存款利率 = 3,
        [Description("UsuryRate.htm")]
        放款利率,
        [Description("BankMoneyExchange.htm")]
        銀行換匯,
        [Description("BankMoneyExchangeCompareTable.htm")]
        銀行換匯比較表,
        Detail
    }
    #endregion euBankServiceNavigations : byte

    #region eu牌告利率Navigations : byte
    /// <summary>
    /// 牌告利率 下頁籤
    /// </summary>
    public enum eu牌告利率Navigations : byte
    {
        None = 0,
        銀行牌告利率 = 1,
        各項業務利率 = 2,
        [Description("q_crate4,基準利率,")]
        基準利率 = 3,
        [Description("q_crate,,")]
        活期存款 = 4,
        [Description("q_crate6,定期儲蓄存款,1年")]
        定期存款 = 5,
        [Description("q_crate5,信用卡最高利率,")]
        信用卡最高利率 = 6,
        [Description("q_crate4,基準利率之指標利率,")]
        基準利率之指標利率 = 7,
        [Description("q_crate1,,")]
        活期儲蓄存款 = 8,
        [Description("q_crate9,,")]
        信託資金存款 = 9,
        [Description("q_crate5,信用卡最低利率,")]
        信用卡最低利率 = 10,
        [Description("q_crate4,基準利率之一定比率,")]
        基準利率之一定比率 = 11,
        [Description("q_crate2,,")]
        薪資轉帳活期儲蓄存款 = 12,

        台灣郵政儲金利率 = 13,
        [Description("q_crate8,現金卡最高利率,")]
        現金卡最高利率 = 14,
        [Description("q_crate7,,")]
        指數房貸利率 = 15,
        [Description("q_crate3,,")]
        證券戶活期儲蓄存款 = 16,
        [Description("q_crate8,現金卡最低利率,")]
        現金卡最低利率 = 18
    }
    #endregion eu牌告利率Navigations : byte

    #region eu外幣存款利率Navigations : byte
    /// <summary>
    /// 外幣存款利率 下頁籤
    /// </summary>
    public enum eu外幣存款利率Navigations : byte
    {
        None = 0,
        [Description("TW")]
        台灣利率排行 = 1,
        [Description("HK")]
        香港利率排行,
        [Description("CN")]
        中國利率排行,
        綜合利率排行
    }
    #endregion eu外幣存款利率Navigations : byte

    #region eu外幣存款利率SubNavigations : byte
    /// <summary>
    /// 外幣存款利率 下次選單
    /// </summary>
    public enum eu外幣存款利率SubNavigations : byte
    {
        [Description("全部幣別")]
        All = 1,
        [Description("各幣別")]
        Currency = 2
    }
    #endregion eu外幣存款利率SubNavigations : byte

    #region eu存款利率Navigations : byte
    /// <summary>
    /// 存款利率 下頁籤
    /// </summary>
    public enum eu存款利率Navigations : byte
    {
        None = 0,
        台灣利率排行 = 1,
        香港利率排行,
        中國利率排行
    }
    #endregion eu存款利率Navigations : byte

    #region //eu放款利率Navigations : byte
    ///// <summary>
    ///// 
    ///// </summary>
    //public enum eu放款利率Navigations : byte
    //{
    //    None = 0,
    //    台灣放款利率,
    //    牌告利率,
    //    基準利率,
    //    基準利率之指標利率,
    //    基準利率之一定比率 
    //}
    #endregion //eu放款利率Navigations : byte

    #region eu銀行換匯比較表Navigations : byte
    /// <summary>
    /// 銀行換匯比較表 下頁籤
    /// </summary>
    public enum eu銀行換匯比較表Navigations : byte
    {
        None = 0,
        現金,
        即期
    }
    #endregion eu銀行換匯比較表Navigations : byte

    #region //euBankService子頁籤下方連結 : byte
    ///// <summary>
    ///// 
    ///// </summary>
    //public enum euBankService子頁籤下方連結 : byte
    //{
    //    [Description("21,22,23,24")]
    //    全部幣別,
    //    [Description("21,22,23,24")]
    //    各別幣別,
    //    //[Description("4")]
    //    //台灣放款利率,
    //    //[Description("21,4")]
    //    //牌告利率,
    //    //[Description("31")]
    //    //台幣存款,
    //    //[Description("31")]
    //    //銀行_郵局牌告利率,
    //    //[Description("31,4")]
    //    //基準利率,
    //    //[Description("31,4")]
    //    //基準利率之指標利率,
    //    //[Description("31,4")]
    //    //基準利率之一定比率,
    //    //[Description("32")]
    //    //港幣存款,
    //    //[Description("33")]
    //    //人民幣存款
    //}
    #endregion //euBankService子頁籤下方連結 : byte

    #region euRateType : byte
    public enum euRateType : byte
    {
        [Description("MRate")]
        機動,
        [Description("FRate")]
        固定
    }
    #endregion euRateType : byte

    #region euDepositRate13_14_15_Item : byte
    public enum euDepositRate13_14_15_Item : byte
    {
        基準利率,
        基準利率之指標利率,
        基準利率之一定比率
    }
    #endregion euDepositRate13_14_15_Item : byte

    #region euBankServiceDetail台灣地區 : byte
    public enum euBankServiceDetail台灣地區 : byte
    {
        是,
        否
    }
    #endregion euBankServiceDetail台灣地區 : byte

    #region euBankMoneyExchangeType : byte
    public enum euBankMoneyExchangeType : byte
    {
        Bank,
        Currency
    }
    #endregion euBankMoneyExchangeType : byte

    #region euCurrency : byte
    public enum euCurrency : byte
    {
        [Description("美元")]
        USD,
        [Description("歐元")]
        EUR,
        [Description("港幣")]
        HKD,
        [Description("人民幣")]
        CNY,
        [Description("日元")]
        JPY,
        [Description("韓圜")]
        KRW,
        [Description("印尼幣")]
        IDR,
        [Description("菲律賓披索")]
        PHP,
        [Description("泰幣")]
        THB,
        [Description("新加坡幣")]
        SGD,
        [Description("紐元")]
        NZD,
        [Description("澳幣")]
        AUD,
        [Description("瑞士法郎")]
        CHF,
        [Description("英鎊")]
        GBP,
        [Description("加拿大幣")]
        CAD,
        [Description("南非幣")]
        ZAR,
        [Description("瑞典幣")]
        SEK,
        [Description("澳門元")]
        MOP,
        [Description("馬來西亞幣")]
        MYR,
        [Description("越南盾")]
        VND,
        [Description("墨西哥披索")]
        MXN,
        [Description("丹麥幣")]
        DKK
    }
    #endregion euCurrency : byte

    #region euConnectionString : byte
    public enum euConnectionString : byte
    {
        _24_bank_ConnStr,
        _5_224_ADSRVDB
    }
    #endregion euConnectionString : byte

    #region euCountryCode : byte
    public enum euCountryCode : byte
    {
        [Description("TW")]
        台灣,
        [Description("HK")]
        香港,
        [Description("CN")]
        中國
    }
    #endregion euCountryCode : byte

    #region euXml_ADs_Path : byte
    /// <summary>
    /// AD
    /// </summary>
    public enum euXml_ADs_Path : byte
    {
        HouseV2 = 0,
        DateTime,
        Tag,
        creative_id,
        url_value,
        text_value
    }
    #endregion euXml_ADs_Path : byte

    #region eu外幣存款利率抓取幣別 : byte
    public enum eu外幣存款利率抓取幣別 : byte
    {
        [Description("美元")]
        USD,
        [Description("歐元")]
        EUR,
        [Description("人民幣")]
        CNY,
        [Description("英鎊")]
        GBP,
        [Description("澳幣")]
        AUD,
        [Description("紐幣")]
        NZD,
        [Description("加拿大幣")]
        CAD,
        [Description("新加坡幣")]
        SGD,
        [Description("瑞士法郎")]
        CHF,
        [Description("港幣")]
        HKD,
        [Description("日元")]
        JPY,
        [Description("南非幣")]
        ZAR
    }
    #endregion eu外幣存款利率抓取幣別 : byte

    #region 方法

    #region String GetEnumDesc (Enum e)
    public static String GetEnumDesc (Enum e)
    {
        FieldInfo EnumInfo = e.GetType().GetField(e.ToString());
        DescriptionAttribute[] EnumAttributes = (DescriptionAttribute[])EnumInfo.GetCustomAttributes(typeof(DescriptionAttribute), false);

        if (EnumAttributes.Length > 0)
            return EnumAttributes[0].Description;

        return e.ToString();
    }
    #endregion String GetEnumDesc (Enum e)

    #endregion 方法
}
