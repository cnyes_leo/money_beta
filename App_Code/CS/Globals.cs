﻿using System;
using System.Data;
using System.Configuration;
using System.Text;
using System.IO;
using System.Collections;
using System.Web.UI;

/// <summary>
/// Globals 的摘要描述
/// </summary>
public class Globals
{
    #region 變數
    public static int PagerMaxCount = 10;
    #endregion 變數

    #region 方法

    #region Log (string msg)
    public void Log (string msg)
    {
        try
        {
            string[] missMsgs = new string[]
                {
                    "工作階段狀態已經建立工作階段 ID，但由於應用程式已經清除回應而無法儲存。"
                };

            bool isExist = false;

            foreach (string s in missMsgs)
            {
                if (msg.IndexOf(s, StringComparison.InvariantCultureIgnoreCase) != -1)
                {
                    isExist = true;
                    break;
                }
            }

            if (!isExist)
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.AppendLine(String.Format("{0:yyyy/MM/dd HH:mm:ss}  {1}  {2}  {3}", DateTime.Now, System.Web.HttpContext.Current.Request.Url.LocalPath,
                                                                                         System.Web.HttpContext.Current.Request.ServerVariables["LOCAL_ADDR"],
                                                                                         System.Web.HttpContext.Current.Request.UserHostAddress));

                if (System.Web.HttpContext.Current.Request.UrlReferrer != null)
                    sb.AppendLine("UrlReferrer = " + System.Web.HttpContext.Current.Request.UrlReferrer.AbsoluteUri);

                sb.AppendLine(System.Web.HttpContext.Current.Request.Url.AbsoluteUri);
                sb.AppendLine(String.Format("{0}", msg + Environment.NewLine));

                WriteLog.Log<string>(sb.ToString());

                if (msg.IndexOf("System.Data.SqlClient.SqlException") != -1)
                {
                    System.Web.HttpContext.Current.Response.Redirect("Index.htm", true);
                    System.Web.HttpContext.Current.Response.End();
                }
            }
            else
            {
                System.Web.HttpContext.Current.Response.StatusCode = 404;
                System.Web.HttpContext.Current.Response.Flush();
                System.Web.HttpContext.Current.Response.End();
            }
        }
        finally
        {
            //Response.End (); 
        }
    }
    #endregion Log (string msg)

    #region string GetClassName (float value, string upClassName, string downClassName, string equalClassName)
    public string GetClassName (float value, string upClassName, string downClassName, string equalClassName)
    {
        if (value > 0)
            return upClassName;
        else if (value < 0)
            return downClassName;

        return equalClassName;
    }
    #endregion string GetClassName (float value, string upClassName, string downClassName, string equalClassName)

    #region string GetClassName (float value1, float value2, string upClassName, string downClassName, string equalClassName)
    public string GetClassName (float value1, float value2, string upClassName, string downClassName, string equalClassName)
    {
        return GetClassName(value1 - value2, upClassName, downClassName, equalClassName);
    }
    #endregion string GetClassName (float value1, float value2, string upClassName, string downClassName, string equalClassName)

    #region string GetClassName (string value, string upClassName, string downClassName, string equalClassName)
    public string GetClassName (string value, string upClassName, string downClassName, string equalClassName)
    {
        float f = 0.0F;
        if (float.TryParse(value, out f))
        {
            return GetClassName(f, upClassName, downClassName, equalClassName);
        }

        return equalClassName;
    }
    #endregion string GetClassName (string value, string upClassName, string downClassName, string equalClassName)

    #region string GetClassName (string value1, string value2, string upClassName, string downClassName, string equalClassName)
    public string GetClassName (string value1, string value2, string upClassName, string downClassName, string equalClassName)
    {
        float f1 = 0.0F;
        float f2 = 0.0F;
        if (float.TryParse(value1, out f1) && float.TryParse(value2, out f2))
            return GetClassName(f1 - f2, upClassName, downClassName, equalClassName);

        return equalClassName;
    }
    #endregion string GetClassName (string value1, string value2, string upClassName, string downClassName, string equalClassName)

    #region string MyFloatConvert (string s, string format, string emptyStr, string errorStr)
    public string MyFloatConvert (string s, string format, string emptyStr, string errorStr)
    {
        if (s.Length == 0)
            return emptyStr;

        double tmp = 0.0D;

        if (double.TryParse(s, out tmp))
        {
            if (tmp > 0)
                return tmp.ToString(format);
            else
                return ((decimal)tmp).ToString();
        }

        return errorStr;
    }
    #endregion string MyFloatConvert (string s, string format, string emptyStr, string errorStr)

    #region string MyFloatConvert (string s, string format, string emptyStr, string errorStr, int amplify)
    public string MyFloatConvert (string s, string format, string emptyStr, string errorStr, int amplify)
    {
        if (s.Length == 0)
            return emptyStr;

        double tmp = 0.0D;

        if (double.TryParse(s, out tmp))
        {
            if (tmp > 0)
                return (tmp * amplify).ToString(format);
            else
                return ((decimal)tmp).ToString();
        }

        return errorStr;
    }
    #endregion string MyFloatConvert (string s, string format, string emptyStr, string errorStr, int amplify)

    #region string BankServicePager (int totalPage, int currentPage, ushort pagerMaxCount, string baseURL)
    public string BankServicePager (int totalPage, int currentPage, ushort pagerMaxCount, string baseURL)
    {
        System.Text.StringBuilder sb = new System.Text.StringBuilder();

        sb.Append(String.Format("<div class='pages'> 第<span>{0}</span>頁 / 共<span>{1}</span>頁</div>", currentPage, totalPage));
        sb.Append("<div class='pagination'>");
        if (totalPage == 1)
        {
            sb.Append("<span class='current'>1</span>");
        }
        else
        {
            int startIdx = 0;
            int endIdx = 0;

            // 現在頁面的檔名
            string pageName = System.IO.Path.GetFileNameWithoutExtension(System.Web.HttpContext.Current.Request.RawUrl) + ".aspx";

            // 如果 總頁數 < 分頁的 HyperLink 的數目
            if (totalPage <= pagerMaxCount)
            {
                startIdx = 0;
                endIdx = totalPage;
            }
            // 如果 總頁數 > 分頁的 HyperLink 的數目
            else
            {
                // 如果 當頁前 <= 分頁的 HyperLink 數的一半
                if (currentPage <= (pagerMaxCount / 2) + 1)
                {
                    startIdx = 0;
                    endIdx = pagerMaxCount;
                }
                // 如果 當頁前 > 分頁的 HyperLink 數的一半
                else
                {
                    startIdx = (ushort)(currentPage - pagerMaxCount / 2 - 1);
                    endIdx = (ushort)(startIdx + pagerMaxCount);

                    if (endIdx > totalPage)
                    {
                        endIdx = totalPage;
                        startIdx = (ushort)(endIdx - pagerMaxCount);
                    }
                }
            }

            // 第一頁/上十頁/上一頁
            if (currentPage != 1)
            {
                if (startIdx > 0)
                    sb.Append(String.Format("<a href='{0}{1}'>第一頁</a>", pageName, String.Format(baseURL, 1)));

                sb.Append(String.Format("<a href='{0}{1}'>上一頁</a>", pageName, String.Format(baseURL, currentPage - 1)));
            }

            sb.Append("<span class='fg'>");
            for (int i = startIdx; i < endIdx; i++)
            {
                if (i == currentPage - 1)
                {
                    sb.Append(String.Format("<span class='current'>{0}</span>", currentPage));
                }
                else
                {
                    sb.Append(String.Format("<a href='{0}{1}'>{2}</a>", pageName, String.Format(baseURL, i + 1), i + 1));
                }
            }
            sb.Append("</span>");

            // 下一頁/下十頁/最後一頁
            if (currentPage != totalPage && totalPage > 0)
            {
                sb.Append(String.Format("<a href='{0}{1}'>下一頁</a>", pageName, String.Format(baseURL, currentPage + 1)));

                if (endIdx != totalPage)
                    sb.Append(String.Format("<a href='{0}{1}'>最後一頁</a>", pageName, String.Format(baseURL, totalPage)));
            }
        }
        sb.Append("</div><!-- pagination:end -->");

        return sb.ToString();
    }
    #endregion string BankServicePager (int totalPage, int currentPage, ushort pagerMaxCount, string baseURL)

    #region string AicLogin (System.Web.UI.Page oPage)
    public static string AicLogin (System.Web.UI.Page oPage)
    {
        return AicLogin(oPage.Request);
    }
    #endregion string AicLogin (System.Web.UI.Page oPage)

    #region string AicLogin (System.Web.HttpRequest request)
    public static string AicLogin (System.Web.HttpRequest request)
    {
        if (request.Cookies["AicJava"] != null && request.Cookies["AicJava"].Value.ToString().Trim() != "")
        {
            string strValue = request.Cookies["AicMember2"].Value;

            System.Text.RegularExpressions.Regex rex = new System.Text.RegularExpressions.Regex("CN=[^&]+");

            System.Text.RegularExpressions.MatchCollection mc = rex.Matches(strValue);

            if (mc.Count > 0)
                return mc[0].Value.Substring(3);
        }

        return String.Empty;
    }
    #endregion string AicLogin (System.Web.HttpRequest request)

    #endregion 方法
}
