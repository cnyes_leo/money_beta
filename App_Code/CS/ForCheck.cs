﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;

/// <summary>
///HtmlEncode
/// </summary>
public abstract class ForCheck
{
    #region HtmlEncode
    public static string HtmlEncode(string str)//Replace
    {
        bool errSpec = false;
        string strStr = "";
        if (str == null)
        {
            strStr = "";
        }
        else
        {
            strStr = str.Replace(" ", "%20").Replace("'", "").Replace("--", "").Replace("\"\"", "").ToString();
        }


        string[] SpecStr = new string[23];

        SpecStr[0] = "net%20user";
        SpecStr[1] = "cmdshell";
        SpecStr[2] = "%3C";
        SpecStr[3] = "exec%20master.dbo.xp_cmdshell";
        SpecStr[4] = "net%20localgroup%20administrators";
        SpecStr[5] = "update";
        SpecStr[6] = "exec%20";
        SpecStr[7] = "char(";
        SpecStr[8] = "insert";
        SpecStr[9] = "delete";
        SpecStr[10] = "drop";
        SpecStr[11] = "truncate";
        SpecStr[12] = "script";
        SpecStr[13] = "iframe";
        SpecStr[14] = "exec%20";
        SpecStr[15] = "declare";
        SpecStr[16] = "cursor";
        SpecStr[17] = "fetch";
        SpecStr[18] = "cast(";
        SpecStr[19] = "href";
        SpecStr[20] = "passwd";
        SpecStr[21] = "ascii";
        SpecStr[22] = "sysobjects";

        for (int i = 0; i < SpecStr.Length; i++)
        {
            if (str.ToLower().IndexOf(SpecStr[i]) > -1) { errSpec = true; break; }
        }

        if (errSpec)
        {  HttpContext.Current.Response.Redirect("http://www.cnyes.com/");     }

        return strStr;
    }

    #endregion  
}
