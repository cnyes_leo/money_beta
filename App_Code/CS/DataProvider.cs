﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

#region class DataProvider
public partial class DataProvider : IDisposable
{
    #region 變數
    private String _ConnectionString = String.Empty;
    private SqlConnection _SqlCn = null;
    private SqlCommand _SqlCm = null;
    private SqlDataReader _SqlDr = null;
    private DataTable _ResultDataTable = null;
    private DataSet _ResultDataSet = null;
    private Object _ResultObject = null;

    private bool _Disposed;

    private readonly byte _RetryCount = 5;

    private readonly int _SleepTime = 200;
    #endregion 變數

    #region ReturnType : byte
    public enum ReturnType : byte
    {
        DateSet,
        DataTable,
        String,
        Long,
        ULong,
        Int,
        Uint,
        Short,
        UShort,
        Byte
    }
    #endregion ReturnType : byte

    #region 屬性

    #region String ConnectionString
    protected String ConnectionString
    {
        set
        {
            _ConnectionString = value;
        }
    }
    #endregion String ConnectionString

    #region DataTable ResultDataTable
    public DataTable ResultDataTable
    {
        get
        {
            return _ResultDataTable;
        }
    }
    #endregion DataTable ResultDataTable

    #region DataSet ResultDataSet
    public DataSet ResultDataSet
    {
        get
        {
            return _ResultDataSet;
        }
    }
    #endregion DataSet ResultDataSet

    #region DataTableCollection ResultDataTables
    public DataTableCollection ResultDataTables
    {
        get
        {
            return ((_ResultDataSet != null && _ResultDataSet.Tables.Count > 0) ? _ResultDataSet.Tables : null);
        }
    }
    #endregion DataTableCollection ResultDataTables

    #region Object ResultObject
    public Object ResultObject
    {
        get
        {
            return _ResultObject;
        }
    }
    #endregion Object ResultObject

    #region SqlDataReader ReturnDataReader
    public SqlDataReader ReturnDataReader
    {
        get
        {
            return _SqlDr;
        }
    }
    #endregion SqlDataReader ReturnDataReader

    #endregion 屬性

    #region 建構子
    public DataProvider ()
    {
    }
    #endregion 建構子

    #region 解構子
    ~DataProvider ()
    {
        Dispose(false);
    }
    #endregion 解構子

    #region 方法/私有方法/靜態方法

    #region Dispose()
    void IDisposable.Dispose ()
    {
        Dispose(true);
        GC.SuppressFinalize(this);
    }
    #endregion Dispose()

    #region Dispose(bool isDisposing)
    protected virtual void Dispose (bool isDisposing)
    {
        if (!_Disposed)
        {
            CloseConnection();

            _Disposed = true;
        }
    }
    #endregion Dispose(bool isDisposing)

    #region Close()
    public void Close ()
    {
        CloseConnection();

        if (_ResultDataTable != null)
        {
            _ResultDataTable.Clear();
            _ResultDataTable.Dispose();
            _ResultDataTable = null;
        }

        if (_ResultDataSet != null)
        {
            _ResultDataSet.Clear();
            _ResultDataSet.Dispose();
            _ResultDataSet = null;
        }

        _ResultObject = null;

        ((IDisposable)this).Dispose();
    }
    #endregion Close()

    #region void CloseConnection ()
    public void CloseConnection ()
    {
        if (_SqlDr != null)
        {
            _SqlDr.Dispose();
            _SqlDr = null;
        }
        if (_SqlCm != null)
        {
            _SqlCm.Dispose();
            _SqlCm = null;
        }
        if (_SqlCn != null)
        {
            //_SqlCn.Close();
            //_SqlCn.Dispose();
            _SqlCn = null;
        }
        _ConnectionString = String.Empty;
    }
    #endregion void CloseConnection ()

    #region 私有方法

    #region void Connect (Enums.euConnectionString cs)
    private void Connect (Enums.euConnectionString cs)
    {
        CloseConnection();
        _ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings[cs.ToString()].ConnectionString;
        _SqlCn = new SqlConnection(_ConnectionString);
        _SqlCn.Open();
    }
    #endregion void Connect (Enums.euConnectionString cs)

    #region protected void GetDataReturnDataTable (Enums.euConnectionString connectionString, string commandText, SqlParameter[] parameters)
    protected void GetDataReturnDataTable (Enums.euConnectionString connectionString, string commandText, SqlParameter[] parameters)
    {
        GetDataReturnDataTable(connectionString, CommandType.Text, commandText, parameters);
    }
    #endregion protected void GetDataReturnDataTable (Enums.euConnectionString connectionString, CommandType commandType string commandText, SqlParameter[] parameters)

    #region protected void GetDataReturnDataTable (Enums.euConnectionString connectionString, CommandType commandType, string commandText, SqlParameter[] parameters)
    protected void GetDataReturnDataTable (Enums.euConnectionString connectionString, CommandType commandType, string commandText, SqlParameter[] parameters)
    {
        GetDataReturnDataTable(connectionString, commandType, -1, commandText, parameters);
    }
    #endregion protected void GetDataReturnDataTable (Enums.euConnectionString connectionString, CommandType commandType string commandText, SqlParameter[] parameters)

    #region protected void GetDataReturnDataSet (Enums.euConnectionString connectionString, string commandText, SqlParameter[] parameters)
    protected void GetDataReturnDataSet (Enums.euConnectionString connectionString, string commandText, SqlParameter[] parameters)
    {
        GetDataReturnDataSet(connectionString, CommandType.Text, commandText, parameters);
    }
    #endregion protected void GetDataReturnDataSet (Enums.euConnectionString connectionString, string commandText, SqlParameter[] parameters)

    #region protected void GetDataReturnDataSet (Enums.euConnectionString connectionString, CommandType commandType, string commandText, SqlParameter[] parameters)
    protected void GetDataReturnDataSet (Enums.euConnectionString connectionString, CommandType commandType, string commandText, SqlParameter[] parameters)
    {
        GetDataReturnDataSet(connectionString, CommandType.Text, -1, commandText, parameters);
    }
    #endregion protected void GetDataReturnDataSet (Enums.euConnectionString connectionString, CommandType commandType, string commandText, SqlParameter[] parameters)

    #region protected int InsertUpdateDeleteData (Enums.euConnectionString connectionString, string commandText, SqlParameter[] parameters)
    protected int InsertUpdateDeleteData (Enums.euConnectionString connectionString, string commandText, SqlParameter[] parameters)
    {
        return InsertUpdateDeleteData(connectionString, CommandType.Text, commandText, parameters);
    }
    #endregion protected int InsertUpdateDeleteData (Enums.euConnectionString connectionString, string commandText, SqlParameter[] parameters)

    #region protected int InsertUpdateDeleteData (Enums.euConnectionString connectionString, CommandType commandType, string commandText, SqlParameter[] parameters)
    protected int InsertUpdateDeleteData (Enums.euConnectionString connectionString, CommandType commandType, string commandText, SqlParameter[] parameters)
    {
        return InsertUpdateDeleteData(connectionString, CommandType.Text, -1, commandText, parameters);
    }
    #endregion protected int InsertUpdateDeleteData (Enums.euConnectionString connectionString, CommandType commandType, string commandText, SqlParameter[] parameters)

    #region protected void InsertUpdateDeleteDataThenSelectData (Enums.euConnectionString connectionString, string commandText, SqlParameter[] parameters, ReturnType returnType)
    protected void InsertUpdateDeleteDataThenSelectData (Enums.euConnectionString connectionString, string commandText, SqlParameter[] parameters, ReturnType returnType)
    {
        InsertUpdateDeleteDataThenSelectData(connectionString, CommandType.Text, commandText, parameters, returnType);
    }
    #endregion protected void InsertUpdateDeleteDataThenSelectData (Enums.euConnectionString connectionString, string commandText, SqlParameter[] parameters, ReturnType returnType)

    #region protected void InsertUpdateDeleteDataThenSelectData (Enums.euConnectionString connectionString, CommandType commandType, string commandText, SqlParameter[] parameters, ReturnType returnType)
    protected void InsertUpdateDeleteDataThenSelectData (Enums.euConnectionString connectionString, CommandType commandType, string commandText, SqlParameter[] parameters, ReturnType returnType)
    {
        InsertUpdateDeleteDataThenSelectData(connectionString, CommandType.Text, -1, commandText, parameters, returnType);
    }
    #endregion protected void InsertUpdateDeleteDataThenSelectData (Enums.euConnectionString connectionString, CommandType commandType, string commandText, SqlParameter[] parameters, ReturnType returnType)

    #region protected void GetDataReturnObject (Enums.euConnectionString connectionString, string commandText, SqlParameter[] parameters)
    protected void GetDataReturnObject (Enums.euConnectionString connectionString, string commandText, SqlParameter[] parameters)
    {
        GetDataReturnObject(connectionString, CommandType.Text, commandText, parameters);
    }
    #endregion protected void GetDataReturnObject (Enums.euConnectionString connectionString, string commandText, SqlParameter[] parameters)

    #region protected void GetDataReturnObject (Enums.euConnectionString connectionString, CommandType commandType, string commandText, SqlParameter[] parameters)
    protected void GetDataReturnObject (Enums.euConnectionString connectionString, CommandType commandType, string commandText, SqlParameter[] parameters)
    {
        GetDataReturnObject(connectionString, CommandType.Text, -1, commandText, parameters);
    }
    #endregion protected void GetDataReturnObject (Enums.euConnectionString connectionString, CommandType commandType, string commandText, SqlParameter[] parameters)

    #region protected void GetDataReturnDataTable (Enums.euConnectionString connectionString, CommandType commandType, int commandTimeout, string commandText, SqlParameter[] parameters)
    protected void GetDataReturnDataTable (Enums.euConnectionString connectionString, CommandType commandType, int commandTimeout, string commandText, SqlParameter[] parameters)
    {
        Connect(connectionString);

        _SqlCm = new SqlCommand();
        _SqlCm.Connection = _SqlCn;

        _SqlCm.CommandType = commandType;
        if (commandTimeout > 0)
            _SqlCm.CommandTimeout = commandTimeout;
        _SqlCm.CommandText = commandText;

        if (parameters != null)
            _SqlCm.Parameters.AddRange(parameters);

        SqlDataAdapter da = new SqlDataAdapter(_SqlCm);

        byte retryCount = 0;

        while (retryCount < _RetryCount)
        {
            try
            {
                if (_ResultDataTable != null)
                {
                    _ResultDataTable.Clear();
                    _ResultDataTable.Dispose();
                    _ResultDataTable = null;
                }
                _ResultDataTable = new DataTable();

                da.Fill(_ResultDataTable);

                da.Dispose();
                da = null;

                _SqlCm.Dispose();
                _SqlCm = null;

                _SqlCn.Close();
                _SqlCn.Dispose();
                _SqlCn = null;

                break;
            }
            catch (SqlException sqlEx)
            {
                if (retryCount < _RetryCount)
                {
                    System.Threading.Thread.Sleep(_SleepTime);
                    retryCount++;
                }
                else
                {
                    WriteError("DataProvider GetDataReturnDataTable Error.", ref sqlEx, ref commandText);
                    throw new Exception(String.Format("DataProvider GetDataReturnDataTable Error. {0}", sqlEx.ToString()));
                }
            }
        }
    }
    #endregion protected void GetDataReturnDataTable (Enums.euConnectionString connectionString, CommandType commandType, int commandTimeout, string commandText, SqlParameter[] parameters)

    #region protected void GetDataReturnDataSet (Enums.euConnectionString connectionString, CommandType commandType, int commandTimeout, string commandText, SqlParameter[] parameters)
    protected void GetDataReturnDataSet (Enums.euConnectionString connectionString, CommandType commandType, int commandTimeout, string commandText, SqlParameter[] parameters)
    {
        Connect(connectionString);

        _SqlCm = new SqlCommand();
        _SqlCm.Connection = _SqlCn;

        _SqlCm.CommandType = commandType;
        if (commandTimeout > 0)
            _SqlCm.CommandTimeout = commandTimeout;
        _SqlCm.CommandText = commandText;

        if (parameters != null)
            _SqlCm.Parameters.AddRange(parameters);

        SqlDataAdapter da = new SqlDataAdapter(_SqlCm);

        byte retryCount = 0;

        while (retryCount < _RetryCount)
        {
            try
            {
                if (_ResultDataSet == null)
                    _ResultDataSet = new DataSet();
                else
                    _ResultDataSet.Clear();

                da.Fill(_ResultDataSet);

                da.Dispose();
                da = null;

                _SqlCm.Dispose();
                _SqlCm = null;

                _SqlCn.Close();
                _SqlCn.Dispose();
                _SqlCn = null;

                break;
            }
            catch (SqlException sqlEx)
            {
                if (retryCount < _RetryCount)
                {
                    System.Threading.Thread.Sleep(_SleepTime);
                    retryCount++;
                }
                else
                {
                    WriteError("DataProvider GetDataReturnDataSet Error.", ref sqlEx, ref commandText);
                    throw new Exception(String.Format("DataProvider GetDataReturnDataSet Error. {0}", sqlEx.ToString()));
                }
            }
            catch (Exception ex)
            {
                if (retryCount < _RetryCount)
                {
                    System.Threading.Thread.Sleep(_SleepTime);
                    retryCount++;
                }
                else
                {
                    WriteError("DataProvider GetDataReturnDataSet Error.", ref ex, ref commandText);
                }
            }
        }
    }
    #endregion protected void GetDataReturnDataSet (Enums.euConnectionString connectionString, CommandType commandType, int commandTimeout, string commandText, SqlParameter[] parameters)

    #region protected int InsertUpdateDeleteData (Enums.euConnectionString connectionString, CommandType commandType, int commandTimeout, string commandText, SqlParameter[] parameters)
    protected int InsertUpdateDeleteData (Enums.euConnectionString connectionString, CommandType commandType, int commandTimeout, string commandText, SqlParameter[] parameters)
    {
        Connect(connectionString);

        _SqlCm = new SqlCommand();
        _SqlCm.Connection = _SqlCn;

        _SqlCm.CommandType = commandType;
        if (commandTimeout > 0)
            _SqlCm.CommandTimeout = commandTimeout;
        _SqlCm.CommandText = commandText;

        if (parameters != null)
            _SqlCm.Parameters.AddRange(parameters);

        int result = -1;

        byte retryCount = 0;

        while (retryCount < _RetryCount)
        {
            try
            {
                result = _SqlCm.ExecuteNonQuery();

                _SqlCm.Dispose();
                _SqlCm = null;

                _SqlCn.Close();
                _SqlCn.Dispose();
                _SqlCn = null;

                break;
            }
            catch (SqlException sqlEx)
            {
                if (retryCount < _RetryCount)
                {
                    System.Threading.Thread.Sleep(_SleepTime);
                    retryCount++;
                }
                else
                {
                    WriteError("DataProvider InsertUpdateDeleteData Error.", ref sqlEx, ref commandText);
                    throw new Exception(String.Format("DataProvider InsertUpdateDeleteData Error. {0}", sqlEx.ToString()));
                }
            }
        }

        return result;
    }
    #endregion protected int InsertUpdateDeleteData (Enums.euConnectionString connectionString, CommandType commandType, int commandTimeout, string commandText, SqlParameter[] parameters)

    #region protected void InsertUpdateDeleteDataThenSelectData (Enums.euConnectionString connectionString, CommandType commandType, int commandTimeout, string commandText, SqlParameter[] parameters, ReturnType returnType)
    protected void InsertUpdateDeleteDataThenSelectData (Enums.euConnectionString connectionString, CommandType commandType, int commandTimeout, string commandText, SqlParameter[] parameters, ReturnType returnType)
    {
        Connect(connectionString);

        _SqlCm = new SqlCommand();
        _SqlCm.Connection = _SqlCn;

        _SqlCm.CommandType = commandType;
        if (commandTimeout > 0)
            _SqlCm.CommandTimeout = commandTimeout;
        _SqlCm.CommandText = commandText;

        if (parameters != null)
            _SqlCm.Parameters.AddRange(parameters);

        SqlDataAdapter da = new SqlDataAdapter(_SqlCm);

        byte retryCount = 0;
        while (retryCount < _RetryCount)
        {
            try
            {
                if (returnType == ReturnType.DateSet)
                {
                    if (_ResultDataSet == null)
                        _ResultDataSet = new DataSet();
                    da.Fill(_ResultDataSet);
                }
                else
                {
                    if (_ResultDataTable == null)
                        _ResultDataTable = new DataTable();

                    da.Fill(_ResultDataTable);

                    if (_ResultDataTable.Rows.Count == 1 && returnType != ReturnType.DataTable)
                    {
                        _ResultObject = _ResultDataTable.Rows[0][0].ToString();
                        _ResultDataTable.Clear();
                        _ResultDataTable = null;
                    }
                }

                da.Dispose();
                da = null;

                _SqlCm.Dispose();
                _SqlCm = null;

                _SqlCn.Close();
                _SqlCn.Dispose();
                _SqlCn = null;

                break;
            }
            catch (SqlException sqlEx)
            {
                if (retryCount < _RetryCount)
                {
                    System.Threading.Thread.Sleep(_SleepTime);
                    retryCount++;
                }
                else
                {
                    WriteError("DataProvider InsertUpdateDeleteDataThenSelectData Error.", ref sqlEx, ref commandText);
                    throw new Exception(String.Format("DataProvider InsertUpdateDeleteDataThenSelectData Error. {0}", sqlEx.ToString()));
                }
            }
        }
    }
    #endregion protected void InsertUpdateDeleteDataThenSelectData (Enums.euConnectionString connectionString, CommandType commandType, int commandTimeout, string commandText, SqlParameter[] parameters, ReturnType returnType)

    #region protected void GetDataReturnObject (Enums.euConnectionString connectionString, CommandType commandType, int commandTimeout, string commandText, SqlParameter[] parameters)
    protected void GetDataReturnObject (Enums.euConnectionString connectionString, CommandType commandType, int commandTimeout, string commandText, SqlParameter[] parameters)
    {
        Connect(connectionString);

        _SqlCm = new SqlCommand();
        _SqlCm.Connection = _SqlCn;

        _SqlCm.CommandType = commandType;
        if (commandTimeout > 0)
            _SqlCm.CommandTimeout = commandTimeout;
        _SqlCm.CommandText = commandText;

        if (parameters != null)
            _SqlCm.Parameters.AddRange(parameters);

        byte retryCount = 0;

        while (retryCount < _RetryCount)
        {
            try
            {
                SqlDataReader dr = _SqlCm.ExecuteReader();
                dr.Read();

                if (dr.HasRows)
                    _ResultObject = dr[0];

                dr.Close();
                dr = null;

                _SqlCm.Dispose();
                _SqlCm = null;

                _SqlCn.Close();
                _SqlCn.Dispose();
                _SqlCn = null;

                break;
            }
            catch (SqlException sqlEx)
            {
                if (retryCount < _RetryCount)
                {
                    System.Threading.Thread.Sleep(_SleepTime);
                    retryCount++;
                }
                else
                {
                    WriteError("DataProvider GetDataReturnObject Error.", ref sqlEx, ref commandText);
                    throw new Exception(String.Format("DataProvider GetDataReturnObject Error. {0}", sqlEx.ToString()));
                }
            }
        }
    }
    #endregion protected void GetDataReturnObject (Enums.euConnectionString connectionString, CommandType commandType, int commandTimeout, string commandText, SqlParameter[] parameters)

    #region void WriteError (string errorKey, ref SqlException sqlEx, ref String commandText)
    private void WriteError (string errorKey, ref SqlException sqlEx, ref String commandText)
    {
        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        sb.AppendLine(String.Format("ip = {0}, path = {1}", System.Web.HttpContext.Current.Request.UserHostAddress, System.Web.HttpContext.Current.Request.Path));
        sb.AppendLine("httpContext.Request.ApplicationPath = " + System.Web.HttpContext.Current.Request.ApplicationPath);
        sb.AppendLine("httpContext.Request.Url.AbsoluteUri = " + System.Web.HttpContext.Current.Request.Url.AbsoluteUri);
        sb.AppendLine("httpContext.Request.Url.AbsolutePath = " + System.Web.HttpContext.Current.Request.Url.AbsolutePath);
        sb.AppendLine(String.Format("{0}. {1} = ", errorKey, sqlEx.ToString()));
        sb.AppendLine("commandText = " + commandText);
        WriteLog.Log<String>(sb.ToString());
    }
    #endregion void WriteError (string errorKey, ref SqlException sqlEx, ref String commandText)

    #region void WriteError (string errorKey, ref Exception ex, ref String commandText)
    private void WriteError (string errorKey, ref Exception ex, ref String commandText)
    {
        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        sb.AppendLine(String.Format("ip = {0}, path = {1}", System.Web.HttpContext.Current.Request.UserHostAddress, System.Web.HttpContext.Current.Request.Path));
        sb.AppendLine("httpContext.Request.ApplicationPath = " + System.Web.HttpContext.Current.Request.ApplicationPath);
        sb.AppendLine("httpContext.Request.Url.AbsoluteUri = " + System.Web.HttpContext.Current.Request.Url.AbsoluteUri);
        sb.AppendLine("httpContext.Request.Url.AbsolutePath = " + System.Web.HttpContext.Current.Request.Url.AbsolutePath);
        sb.AppendLine(String.Format("{0}. {1} = ", errorKey, ex.ToString()));
        sb.AppendLine("commandText = " + commandText);
        WriteLog.Log<String>(sb.ToString());
    }
    #endregion void WriteError (string errorKey, ref Exception ex, ref String commandText)

    #endregion 私有方法

    #region 靜態方法

    #region static string GetFields<T> (T fields)
    private static string GetFields<T> (T fields)
    {
        System.Text.StringBuilder sb = new System.Text.StringBuilder();

        foreach (T f in Enum.GetValues(typeof(T)))
        {
            if ((uint.Parse(Convert.ChangeType(f, typeof(uint)).ToString()) & uint.Parse(Convert.ChangeType(fields, typeof(uint)).ToString())) > 0)
            {
                if (sb.Length > 0)
                    sb.Append(", ");

                sb.Append(String.Format("[{0}]", f));
            }
        }

        return sb.ToString();
    }
    #endregion static string GetFields<T> (T fields)

    #endregion 靜態方法

    #endregion 方法/私有方法/靜態方法
}
#endregion class DataProviderBase

