﻿using System;
using System.Web;

/// <summary>
/// ResponseFilter 的摘要描述
/// </summary>
public class ResponseFilter : System.IO.Stream
{
    public ResponseFilter (System.IO.Stream sink, string _str)
    {
        _sink = sink;
        // 
        // TODO: 在此處添加構造函數邏輯 
        // 
        this.str = _str;
    }
    private string str = "";
    private System.IO.Stream _sink;
    private long _position;
    //private System.Text.Encoding encoding = System.Text.Encoding.UTF8;
    //private System.Text.StringBuilder oOutput = new System.Text.StringBuilder();
    // The following members of Stream must be overriden. 
    public override bool CanRead
    {
        get { return true; }
    }

    public override bool CanSeek
    {
        get { return true; }
    }

    public override bool CanWrite
    {
        get { return true; }
    }

    public override long Length
    {
        get { return 0; }
    }

    public override long Position
    {
        get { return _position; }
        set { _position = value; }
    }

    public override long Seek (long offset, System.IO.SeekOrigin direction)
    {
        return _sink.Seek(offset, direction);
    }

    public override void SetLength (long length)
    {
        _sink.SetLength(length);
    }

    public override void Close ()
    {
        _sink.Close();
    }

    public override void Flush ()
    {
        _sink.Flush();
    }

    public override int Read (byte[] buffer, int offset, int count)
    {
        return _sink.Read(buffer, offset, count);
    }

    // The Write method actually does the filtering. 
    public override void Write (byte[] buffer, int offset, int count)
    {
        string szBuffer = System.Text.UTF8Encoding.UTF8.GetString(buffer, offset, count);
        //WriteLog<string>.Log("szBuffer", szBuffer);
        string ap = "action=\"";
        int pos = -1;
        if ((pos = szBuffer.IndexOf(ap)) != -1)
        {
            int epos = szBuffer.IndexOf("\"", pos + ap.Length + 1);
            if (epos != -1)
            {
                szBuffer = szBuffer.Remove(pos + ap.Length, epos - pos - ap.Length);
            }
            szBuffer = szBuffer.Insert(pos + ap.Length, this.str);
            byte[] data = System.Text.UTF8Encoding.UTF8.GetBytes(szBuffer);

            _sink.Write(data, 0, data.Length);
        }
        else
        {
            //oOutput.Append(szBuffer);
            byte[] data = System.Text.UTF8Encoding.UTF8.GetBytes(szBuffer);

            _sink.Write(data, 0, data.Length);
        }
        //下面的這一段可以用來修改<Head></head>之間的內容； 
        //Regex oEndFile = new Regex("</head>", RegexOptions.IgnoreCase|RegexOptions.Compiled); 
        //if (oEndFile.IsMatch(szBuffer)) 
        //{ 
        // //Append the last buffer of data 
        // //附加上緩衝區中的最後一部分數據 
        // oOutput.Append(szBuffer); 
        // //Get back the complete response for the client 
        // //傳回完整的客戶端返回數據 
        // string szCompleteBuffer = oOutput.ToString().ToLower(); 
        // int ipos = szCompleteBuffer.IndexOf("<title>"); 
        // int epos = szCompleteBuffer.IndexOf("</title>",ipos+7); 
        // string sp = szCompleteBuffer.Substring(ipos+7, epos - ipos ); 
        // szCompleteBuffer = szCompleteBuffer.Remove(ipos+7,sp.Length-7); 
        // szCompleteBuffer = szCompleteBuffer.Insert(ipos + 7, "dhz"); 
        // // szCompleteBuffer = szCompleteBuffer.Replace(sp, "dhz"); 
        // //No match, so write out original data 
        // //沒有匹配，因此寫入源代碼 
        // byte[] data = System.Text.UTF8Encoding.UTF8.GetBytes(szCompleteBuffer); 
        // _sink.Write(data, 0, data.Length); 
        //} 
        //else 
        //{ 
        // oOutput.Append(szBuffer); 
        //} 
    }
}
