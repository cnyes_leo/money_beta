﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

#region class DataProvider
public partial class DataProvider : IDisposable
{
    #region 5_224
    public class _5_224
    {
        #region class ADSRVDB : DataProvider
        public class ADSRVDB : DataProvider
        {
            #region ReturnTable1_CommonFields : byte
            public enum ReturnTable1_CommonFields : byte
            {
                PageIndex,
                PageSize,
                TotalCount,
                TotalPage
            }
            #endregion ReturnTable1_CommonFields : byte

            #region GetTwDepositRate_ReturnTable2Fields : byte
            public enum GetTwDepositRate_ReturnTable2Fields : byte
            {
                BCode,
                BName,
                /// <summary>
                /// 活期儲蓄存款
                /// </summary>
                Rate1,
                /// <summary>
                /// 薪資轉帳活期儲蓄存款
                /// </summary>
                Rate2,
                /// <summary>
                /// 証券戶活期儲蓄存款
                /// </summary>
                Rate3,
                /// <summary>
                /// 定期存款 1月
                /// </summary>
                Rate4,
                /// <summary>
                /// 定期存款 3月
                /// </summary>
                Rate5,
                /// <summary>
                /// 定期存款 6月
                /// </summary>
                Rate6,
                /// <summary>
                /// 定期存款 9月
                /// </summary>
                Rate7,
                /// <summary>
                /// 定期存款 1年
                /// </summary>
                Rate8,
                /// <summary>
                /// 定期儲蓄存款 1年
                /// </summary>
                Rate9,
                /// <summary>
                /// 定期儲蓄存款 2年
                /// </summary>
                Rate10,
                /// <summary>
                /// 定期儲蓄存款 3年
                /// </summary>
                Rate11
            }
            #endregion GetTwDepositRate_ReturnTable2Fields : byte

            #region GetUsuryRate1_ReturnTable2Fields : byte
            public enum GetUsuryRate1_ReturnTable2Fields : byte
            {
                Index,
                BCode,
                BName,
                /// <summary>
                /// 信用卡最低利率
                /// </summary>
                Rate1_Min,
                /// <summary>
                /// 信用卡最高利率
                /// </summary>
                Rate1_Max,
                /// <summary>
                /// 現金卡最低利率
                /// </summary>
                Rate2_Min,
                /// <summary>
                /// 現金卡最高利率
                /// </summary>
                Rate2_Max,
                /// <summary>
                /// 指數型房貸
                /// </summary>
                Rate3
            }
            #endregion GetUsuryRate1_ReturnTable2Fields : byte

            #region GetBankDetail_ReturnTable1Fields : byte
            public enum GetBankDetail_ReturnTable1Fields : byte
            {
                BCode,
                BName,
                Item,
                Period,
                Amount,
                EDate,
                FRate,
                MRate,
                BankType
            }
            #endregion GetBankDetail_ReturnTable1Fields : byte

            #region GetBankDetail_ReturnTable2Fields : byte
            public enum GetBankDetail_ReturnTable2Fields : byte
            {
                /// <summary>
                /// 幣別
                /// </summary>
                currency,
                /// <summary>
                /// 項目
                /// </summary>
                bankitem,
                /// <summary>
                /// 存期
                /// </summary>
                bankperiod,
                /// <summary>
                /// 額度別
                /// </summary>
                banklimit,
                /// <summary>
                /// 利率
                /// </summary>
                bankrate
            }
            #endregion GetBankDetail_ReturnTable2Fields : byte

            #region Get各項業務利率_ReturnTable2Fields : byte
            public enum Get各項業務利率_ReturnTable2Fields : byte
            {
                MaxRate,
                MinRate,
                ARate
            }
            #endregion Get各項業務利率_ReturnTable2Fields : byte

            #region Get各項業務利率_ReturnTable3Fields : byte
            public enum Get各項業務利率_ReturnTable3Fields : byte
            {
                BCode,
                BName,
                Rate
            }
            #endregion Get各項業務利率_ReturnTable3Fields : byte

            #region void GetADs (string ads)（得到 AD 資料）
            /// <summary>
            /// 得到 AD 資料
            /// </summary>
            public void GetADs (string ads)
            {
                SqlParameter[] parameters = new SqlParameter[1];
                parameters[0] = new SqlParameter("@Tags", SqlDbType.VarChar, 100);
                parameters[0].Value = ads;

                GetDataReturnDataTable(Enums.euConnectionString._5_224_ADSRVDB, CommandType.StoredProcedure, "sp_show_text_ad_byAlex", parameters);
            }
            #endregion void GetADs (string ads)（得到 AD 資料）
        }
        #endregion class ADSRVDB : DataProvider
    }
    #endregion 5_224
}
#endregion class DataProviderBase

