﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

#region class DataProvider
public partial class DataProvider : IDisposable
{
    #region 24
    public class _24
    {
        #region class Bank : DataProvider
        public class Bank : DataProvider
        {
            #region ReturnTable1_CommonFields : byte
            public enum ReturnTable1_CommonFields : byte
            {
                PageIndex,
                PageSize,
                TotalCount,
                TotalPage
            }
            #endregion ReturnTable1_CommonFields : byte

            #region GetTwDepositRate_ReturnTable2Fields : byte
            public enum GetTwDepositRate_ReturnTable2Fields : byte
            {
                BCode,
                BName,
                /// <summary>
                /// 活期儲蓄存款
                /// </summary>
                Rate1,
                /// <summary>
                /// 薪資轉帳活期儲蓄存款
                /// </summary>
                Rate2,
                /// <summary>
                /// 証券戶活期儲蓄存款
                /// </summary>
                Rate3,
                /// <summary>
                /// 定期存款 1月
                /// </summary>
                Rate4,
                /// <summary>
                /// 定期存款 3月
                /// </summary>
                Rate5,
                /// <summary>
                /// 定期存款 6月
                /// </summary>
                Rate6,
                /// <summary>
                /// 定期存款 9月
                /// </summary>
                Rate7,
                /// <summary>
                /// 定期存款 1年
                /// </summary>
                Rate8,
                /// <summary>
                /// 定期儲蓄存款 1年
                /// </summary>
                Rate9,
                /// <summary>
                /// 定期儲蓄存款 2年
                /// </summary>
                Rate10,
                /// <summary>
                /// 定期儲蓄存款 3年
                /// </summary>
                Rate11
            }
            #endregion GetTwDepositRate_ReturnTable2Fields : byte

            #region //GetDepositRate13_14_15_ReturnTable2Fields : byte
            //public enum GetDepositRate13_14_15_ReturnTable2Fields : byte
            //{
            //    Index,
            //    BCode,
            //    BName,
            //    Rate
            //}
            #endregion //GetDepositRate13_14_15_ReturnTable2Fields : byte

            #region GetUsuryRate1_ReturnTable2Fields : byte
            public enum GetUsuryRate1_ReturnTable2Fields : byte
            {
                Index,
                BCode,
                BName,
                /// <summary>
                /// 信用卡最低利率
                /// </summary>
                Rate1_Min,
                /// <summary>
                /// 信用卡最高利率
                /// </summary>
                Rate1_Max,
                /// <summary>
                /// 現金卡最低利率
                /// </summary>
                Rate2_Min,
                /// <summary>
                /// 現金卡最高利率
                /// </summary>
                Rate2_Max,
                /// <summary>
                /// 指數型房貸
                /// </summary>
                Rate3
            }
            #endregion GetUsuryRate1_ReturnTable2Fields : byte

            #region GetBankDetail_ReturnTable1Fields : byte
            public enum GetBankDetail_ReturnTable1Fields : byte
            {
                BCode,
                BName,
                Item,
                Period,
                Amount,
                EDate,
                FRate,
                MRate,
                BankType
            }
            #endregion GetBankDetail_ReturnTable1Fields : byte

            #region GetBankDetail_ReturnTable2Fields : byte
            public enum GetBankDetail_ReturnTable2Fields : byte
            {
                /// <summary>
                /// 幣別
                /// </summary>
                currency,
                /// <summary>
                /// 項目
                /// </summary>
                bankitem,
                /// <summary>
                /// 存期
                /// </summary>
                bankperiod,
                /// <summary>
                /// 額度別
                /// </summary>
                banklimit,
                /// <summary>
                /// 利率
                /// </summary>
                bankrate
            }
            #endregion GetBankDetail_ReturnTable2Fields : byte

            #region Get各項業務利率_ReturnTable2Fields : byte
            public enum Get各項業務利率_ReturnTable2Fields : byte
            {
                MaxRate,
                MinRate,
                ARate
            }
            #endregion Get各項業務利率_ReturnTable2Fields : byte

            #region Get各項業務利率_ReturnTable3Fields : byte
            public enum Get各項業務利率_ReturnTable3Fields : byte
            {
                BCode,
                BName,
                Rate
            }
            #endregion Get各項業務利率_ReturnTable3Fields : byte

            #region void GetTwDepositRate (byte pageIndex, byte pageSize, Enums.euRateType rateType, Enums.euOrderDirection? orderDirection, GetTwDepositRate_ReturnTable2Fields? orderField)（存款利率）
            /// <summary>
            /// 得到 存款利率
            /// </summary>
            public void GetTwDepositRate (byte pageIndex, byte pageSize, Enums.euRateType rateType, Enums.euOrderDirection? orderDirection, GetTwDepositRate_ReturnTable2Fields? orderField)
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();

                #region commandText
                //sb.AppendLine("DECLARE @PageIndex INT");
                //sb.AppendLine("DECLARE @PageSize INT");

                //sb.AppendLine("SET @PageIndex = 1");
                //sb.AppendLine("SET @PageSize = 25");

                sb.AppendLine("DECLARE @ResultTable1 TABLE(BCode NVARCHAR(50),");
                sb.AppendLine("                            BName NVARCHAR(50),");
                sb.AppendLine("                            Rate1 NVARCHAR(50),");
                sb.AppendLine("                            Rate2 NVARCHAR(50),");
                sb.AppendLine("                            Rate3 NVARCHAR(50),");
                sb.AppendLine("                            Rate4 NVARCHAR(50),");
                sb.AppendLine("                            Rate5 NVARCHAR(50),");
                sb.AppendLine("                            Rate6 NVARCHAR(50),");
                sb.AppendLine("                            Rate7 NVARCHAR(50),");
                sb.AppendLine("                            Rate8 NVARCHAR(50),");
                sb.AppendLine("                            Rate9 NVARCHAR(50),");
                sb.AppendLine("                            Rate10 NVARCHAR(50),");
                sb.AppendLine("                            Rate11 NVARCHAR(50))");

                sb.AppendLine("DECLARE @BCode NVARCHAR(50)");

                sb.AppendLine("DECLARE vend_cursor CURSOR FOR");
                sb.AppendLine("    SELECT  DISTINCT BCode");
                sb.AppendLine("    FROM    Bank_Rate");
                sb.AppendLine("OPEN vend_cursor");
                sb.AppendLine("FETCH NEXT FROM vend_cursor");
                sb.AppendLine("INTO @BCode");

                sb.AppendLine("WHILE (@@FETCH_STATUS = 0)");
                sb.AppendLine("  BEGIN");
                sb.AppendLine("    INSERT INTO @ResultTable1");
                sb.AppendLine("                (BCode, BName)");
                sb.AppendLine("        SELECT DISTINCT BCode, BName");
                sb.AppendLine("        FROM Bank_Rate");
                sb.AppendLine("        WHERE BCode = @BCode");

                sb.AppendLine("    UPDATE @ResultTable1");
                sb.AppendLine("        SET Rate1 = ");
                sb.AppendLine("        (");
                sb.AppendLine(String.Format("            SELECT TOP 1 CASE WHEN {0} IS NULL THEN '--' WHEN CHARINDEX('.', {0})=1 THEN '0'+{0} ELSE {0} END", Enums.GetEnumDesc(rateType)));
                sb.AppendLine("            FROM Bank_Rate");
                sb.AppendLine("            WHERE BCode = @BCode");
                sb.AppendLine("              AND Item = '活期儲蓄存款'");
                sb.AppendLine("              AND Amount = '一般'");
                sb.AppendLine("        )");
                sb.AppendLine("    WHERE BCode = @BCode");

                sb.AppendLine("    UPDATE @ResultTable1");
                sb.AppendLine("        SET Rate2 = ");
                sb.AppendLine("        (");
                sb.AppendLine(String.Format("            SELECT TOP 1 CASE WHEN {0} IS NULL THEN '--' WHEN CHARINDEX('.', {0})=1 THEN '0'+{0} ELSE {0} END", Enums.GetEnumDesc(rateType)));
                sb.AppendLine("            FROM Bank_Rate");
                sb.AppendLine("            WHERE BCode = @BCode");
                sb.AppendLine("              AND Item = '薪資轉帳活期儲蓄存款'");
                sb.AppendLine("              AND Amount = '一般'");
                sb.AppendLine("        )");
                sb.AppendLine("    WHERE BCode = @BCode");

                sb.AppendLine("    UPDATE @ResultTable1");
                sb.AppendLine("        SET Rate3 = ");
                sb.AppendLine("        (");
                sb.AppendLine(String.Format("            SELECT TOP 1 CASE WHEN {0} IS NULL THEN '--' WHEN CHARINDEX('.', {0})=1 THEN '0'+{0} ELSE {0} END", Enums.GetEnumDesc(rateType)));
                sb.AppendLine("            FROM Bank_Rate");
                sb.AppendLine("            WHERE BCode = @BCode");
                sb.AppendLine("              AND Item = '証券戶活期儲蓄存款'");
                sb.AppendLine("              AND Amount = '一般'");
                sb.AppendLine("        )");
                sb.AppendLine("    WHERE BCode = @BCode");

                sb.AppendLine("    UPDATE @ResultTable1");
                sb.AppendLine("        SET Rate4 = ");
                sb.AppendLine("        (");
                sb.AppendLine(String.Format("            SELECT TOP 1 CASE WHEN {0} IS NULL THEN '--' WHEN CHARINDEX('.', {0})=1 THEN '0'+{0} ELSE {0} END", Enums.GetEnumDesc(rateType)));
                sb.AppendLine("            FROM Bank_Rate");
                sb.AppendLine("            WHERE BCode = @BCode");
                sb.AppendLine("              AND Item = '定期存款'");
                sb.AppendLine("              AND Period = '1月'");
                sb.AppendLine("              AND Amount = '一般'");
                sb.AppendLine("        )");
                sb.AppendLine("    WHERE BCode = @BCode");
                sb.AppendLine("        ");
                sb.AppendLine("    UPDATE @ResultTable1");
                sb.AppendLine("        SET Rate5 = ");
                sb.AppendLine("        (");
                sb.AppendLine(String.Format("            SELECT TOP 1 CASE WHEN {0} IS NULL THEN '--' WHEN CHARINDEX('.', {0})=1 THEN '0'+{0} ELSE {0} END", Enums.GetEnumDesc(rateType)));
                sb.AppendLine("            FROM Bank_Rate");
                sb.AppendLine("            WHERE BCode = @BCode");
                sb.AppendLine("              AND Item = '定期存款'");
                sb.AppendLine("              AND Period = '3月'");
                sb.AppendLine("              AND Amount = '一般'");
                sb.AppendLine("        )");
                sb.AppendLine("    WHERE BCode = @BCode");

                sb.AppendLine("    UPDATE @ResultTable1");
                sb.AppendLine("        SET Rate6 = ");
                sb.AppendLine("        (");
                sb.AppendLine(String.Format("            SELECT TOP 1 CASE WHEN {0} IS NULL THEN '--' WHEN CHARINDEX('.', {0})=1 THEN '0'+{0} ELSE {0} END", Enums.GetEnumDesc(rateType)));
                sb.AppendLine("            FROM Bank_Rate");
                sb.AppendLine("            WHERE BCode = @BCode");
                sb.AppendLine("              AND Item = '定期存款'");
                sb.AppendLine("              AND Period = '6月'");
                sb.AppendLine("              AND Amount = '一般'");
                sb.AppendLine("        )");
                sb.AppendLine("    WHERE BCode = @BCode");

                sb.AppendLine("    UPDATE @ResultTable1");
                sb.AppendLine("        SET Rate7 = ");
                sb.AppendLine("        (");
                sb.AppendLine(String.Format("            SELECT TOP 1 CASE WHEN {0} IS NULL THEN '--' WHEN CHARINDEX('.', {0})=1 THEN '0'+{0} ELSE {0} END", Enums.GetEnumDesc(rateType)));
                sb.AppendLine("            FROM Bank_Rate");
                sb.AppendLine("            WHERE BCode = @BCode");
                sb.AppendLine("              AND Item = '定期存款'");
                sb.AppendLine("              AND Period = '9月'");
                sb.AppendLine("              AND Amount = '一般'");
                sb.AppendLine("        )");
                sb.AppendLine("    WHERE BCode = @BCode");

                sb.AppendLine("    UPDATE @ResultTable1");
                sb.AppendLine("        SET Rate8 = ");
                sb.AppendLine("        (");
                sb.AppendLine(String.Format("            SELECT TOP 1 CASE WHEN {0} IS NULL THEN '--' WHEN CHARINDEX('.', {0})=1 THEN '0'+{0} ELSE {0} END", Enums.GetEnumDesc(rateType)));
                sb.AppendLine("            FROM Bank_Rate");
                sb.AppendLine("            WHERE BCode = @BCode");
                sb.AppendLine("              AND Item = '定期存款'");
                sb.AppendLine("              AND Period = '1年'");
                sb.AppendLine("              AND Amount = '一般'");
                sb.AppendLine("        )");
                sb.AppendLine("    WHERE BCode = @BCode");

                sb.AppendLine("    UPDATE @ResultTable1");
                sb.AppendLine("        SET Rate9 = ");
                sb.AppendLine("        (");
                sb.AppendLine(String.Format("            SELECT TOP 1 CASE WHEN {0} IS NULL THEN '--' WHEN CHARINDEX('.', {0})=1 THEN '0'+{0} ELSE {0} END", Enums.GetEnumDesc(rateType)));
                sb.AppendLine("            FROM Bank_Rate");
                sb.AppendLine("            WHERE BCode = @BCode");
                sb.AppendLine("              AND Item = '定期儲蓄存款'");
                sb.AppendLine("              AND Period = '1年'");
                sb.AppendLine("              AND Amount = '一般'");
                sb.AppendLine("        )");
                sb.AppendLine("    WHERE BCode = @BCode");

                sb.AppendLine("    UPDATE @ResultTable1");
                sb.AppendLine("        SET Rate10 = ");
                sb.AppendLine("        (");
                sb.AppendLine(String.Format("            SELECT TOP 1 CASE WHEN {0} IS NULL THEN '--' WHEN CHARINDEX('.', {0})=1 THEN '0'+{0} ELSE {0} END", Enums.GetEnumDesc(rateType)));
                sb.AppendLine("            FROM Bank_Rate");
                sb.AppendLine("            WHERE BCode = @BCode");
                sb.AppendLine("              AND Item = '定期儲蓄存款'");
                sb.AppendLine("              AND Period = '2年'");
                sb.AppendLine("              AND Amount = '一般'");
                sb.AppendLine("        )");
                sb.AppendLine("    WHERE BCode = @BCode");

                sb.AppendLine("    UPDATE @ResultTable1");
                sb.AppendLine("        SET Rate11 = ");
                sb.AppendLine("        (");
                sb.AppendLine(String.Format("            SELECT TOP 1 CASE WHEN {0} IS NULL THEN '--' WHEN CHARINDEX('.', {0})=1 THEN '0'+{0} ELSE {0} END", Enums.GetEnumDesc(rateType)));
                sb.AppendLine("            FROM Bank_Rate");
                sb.AppendLine("            WHERE BCode = @BCode");
                sb.AppendLine("              AND Item = '定期儲蓄存款'");
                sb.AppendLine("              AND Period = '3年'");
                sb.AppendLine("              AND Amount = '一般'");
                sb.AppendLine("        )");
                sb.AppendLine("    WHERE BCode = @BCode");

                sb.AppendLine("    FETCH NEXT FROM vend_cursor");
                sb.AppendLine("    INTO @BCode");
                sb.AppendLine("  END");

                sb.AppendLine("CLOSE vend_cursor");
                sb.AppendLine("DEALLOCATE vend_cursor");

                sb.AppendLine("DECLARE @ResultTable2 TABLE([Index] INT IDENTITY(1, 1),");
                sb.AppendLine("                            BCode NVARCHAR(50),");
                sb.AppendLine("                            BName NVARCHAR(50),");
                sb.AppendLine("                            Rate1 NVARCHAR(50),");
                sb.AppendLine("                            Rate2 NVARCHAR(50),");
                sb.AppendLine("                            Rate3 NVARCHAR(50),");
                sb.AppendLine("                            Rate4 NVARCHAR(50),");
                sb.AppendLine("                            Rate5 NVARCHAR(50),");
                sb.AppendLine("                            Rate6 NVARCHAR(50),");
                sb.AppendLine("                            Rate7 NVARCHAR(50),");
                sb.AppendLine("                            Rate8 NVARCHAR(50),");
                sb.AppendLine("                            Rate9 NVARCHAR(50),");
                sb.AppendLine("                            Rate10 NVARCHAR(50),");
                sb.AppendLine("                            Rate11 NVARCHAR(50))");

                sb.AppendLine("INSERT INTO @ResultTable2");
                sb.AppendLine("            (BCode, BName, Rate1, Rate2, Rate3, Rate4, Rate5, Rate6, Rate7, Rate8, Rate9, Rate10, Rate11)");
                sb.AppendLine("    SELECT BCode, BName, Rate1, Rate2, Rate3, Rate4, Rate5, Rate6, Rate7, Rate8, Rate9, Rate10, Rate11");
                sb.AppendLine("    FROM @ResultTable1 t");
                if (orderField == null || orderDirection == null)
                    sb.AppendLine("    ORDER BY t.BCode");
                else
                    sb.AppendLine(String.Format("    ORDER BY t.{0} {1}", ((orderField != null) ? (GetTwDepositRate_ReturnTable2Fields)orderField : GetTwDepositRate_ReturnTable2Fields.BCode), ((orderDirection != null) ? (Enums.euOrderDirection)orderDirection : Enums.euOrderDirection.ASC)));

                sb.AppendLine("DECLARE @TotalCount INT");
                sb.AppendLine("DECLARE @TotalPage INT");

                sb.AppendLine("SELECT @TotalCount = COUNT([Index])");
                sb.AppendLine("FROM   @ResultTable2");

                sb.AppendLine("SET @TotalPage = @TotalCount / @PageSize");
                sb.AppendLine("IF (@TotalPage * @PageSize <> @TotalCount)");
                sb.AppendLine("    SET @TotalPage = @TotalPage + 1");

                sb.AppendLine("SELECT  @PageIndex AS PageIndex,");
                sb.AppendLine("        @PageSize AS PageSize,");
                sb.AppendLine("        CASE WHEN @TotalCount IS NULL THEN 0 ELSE @TotalCount END AS TotalCount,");
                sb.AppendLine("        CASE WHEN @TotalPage IS NULL THEN 0 ELSE @TotalPage END AS TotalPage");

                sb.AppendLine("SELECT *");
                sb.AppendLine("FROM @ResultTable2");
                sb.AppendLine("WHERE [Index] BETWEEN (@PageIndex - 1) * @PageSize + 1 AND @PageIndex * @PageSize");
                sb.AppendLine("--新版理財 by Alex _24_Bank.cs Bank GetTwDepositRate");
                #endregion commandText
                //WriteLog.Log<string>("commandText", sb.ToString());
                SqlParameter[] parameters = new SqlParameter[2];
                parameters[0] = new SqlParameter("@PageIndex", SqlDbType.SmallInt);
                parameters[0].Value = pageIndex;
                parameters[1] = new SqlParameter("@PageSize", SqlDbType.SmallInt);
                parameters[1].Value = pageSize;

                GetDataReturnDataSet(Enums.euConnectionString._24_bank_ConnStr, sb.ToString(), parameters);
            }
            #endregion void GetTwDepositRate (byte pageIndex, byte pageSize, Enums.euRateType rateType, Enums.euOrderDirection? orderDirection, GetTwDepositRate_ReturnTable2Fields? orderField)（存款利率）

            #region //void GetDepositRate13_14_15 (byte pageIndex, byte pageSize, Enums.euOrderDirection? orderDirection, Enums.euDepositRate13_14_15_Item item)（基準利率/基準利率之指標利率/基準利率之一定比率）
            ///// <summary>
            ///// 得到 基準利率/基準利率之指標利率/基準利率之一定比率
            ///// </summary>
            //public void GetDepositRate13_14_15 (byte pageIndex, byte pageSize, Enums.euOrderDirection? orderDirection, Enums.euDepositRate13_14_15_Item item)
            //{
            //    System.Text.StringBuilder sb = new System.Text.StringBuilder();

            //    #region commandText
            //    //sb.AppendLine("DECLARE @PageIndex INT");
            //    //sb.AppendLine("DECLARE @PageSize INT");

            //    //sb.AppendLine("SET @PageIndex = 1");
            //    //sb.AppendLine("SET @PageSize = 25");

            //    sb.AppendLine("DECLARE @ResultTable TABLE ([Index] INT IDENTITY(1, 1),");
            //    sb.AppendLine("                            BCode NVARCHAR(50),");
            //    sb.AppendLine("                            BName NVARCHAR(50),");
            //    sb.AppendLine("                            Rate REAL)");

            //    sb.AppendLine("INSERT INTO @ResultTable");
            //    sb.AppendLine("            (BCode, BName, Rate)");
            //    sb.AppendLine("    SELECT *");
            //    sb.AppendLine("    FROM (");
            //    sb.AppendLine("             SELECT bcode, bname, MAX(rate) rate");
            //    sb.AppendLine("             FROM (");
            //    sb.AppendLine("                      SELECT *");
            //    sb.AppendLine("                      FROM q_crate4");
            //    sb.AppendLine("                      WHERE Amount='一般'");
            //    sb.AppendLine(String.Format("                        AND Item='{0}'", item));
            //    sb.AppendLine("                        AND Period=''");
            //    sb.AppendLine("                  )a");
            //    sb.AppendLine("              GROUP BY bcode, bname");
            //    sb.AppendLine("         ) b");
            //    sb.AppendLine(String.Format("    ORDER BY rate {0}", ((orderDirection != null && ((Enums.euOrderDirection)orderDirection) == Enums.euOrderDirection.DESC) ? "DESC" : String.Empty)));

            //    sb.AppendLine("DECLARE @TotalCount INT");
            //    sb.AppendLine("DECLARE @TotalPage INT");

            //    sb.AppendLine("SELECT @TotalCount = COUNT([Index])");
            //    sb.AppendLine("FROM   @ResultTable");

            //    sb.AppendLine("SET @TotalPage = @TotalCount / @PageSize");
            //    sb.AppendLine("IF (@TotalPage * @PageSize <> @TotalCount)");
            //    sb.AppendLine("    SET @TotalPage = @TotalPage + 1");

            //    sb.AppendLine("SELECT  @PageIndex AS PageIndex,");
            //    sb.AppendLine("        @PageSize AS PageSize,");
            //    sb.AppendLine("        CASE WHEN @TotalCount IS NULL THEN 0 ELSE @TotalCount END AS TotalCount,");
            //    sb.AppendLine("        CASE WHEN @TotalPage IS NULL THEN 0 ELSE @TotalPage END AS TotalPage");

            //    sb.AppendLine("SELECT *");
            //    sb.AppendLine("FROM @ResultTable");
            //    sb.AppendLine("WHERE [Index] BETWEEN (@PageIndex - 1) * @PageSize + 1 AND @PageIndex * @PageSize");
            //    sb.AppendLine("--新版理財 by Alex _24_Bank.cs Bank GetDepositRate13_14_15");
            //    #endregion commandText
            //    //WriteLog.Log<string>("commandText", sb.ToString());
            //    SqlParameter[] parameters = new SqlParameter[2];
            //    parameters[0] = new SqlParameter("@PageIndex", SqlDbType.SmallInt);
            //    parameters[0].Value = pageIndex;
            //    parameters[1] = new SqlParameter("@PageSize", SqlDbType.SmallInt);
            //    parameters[1].Value = pageSize;

            //    GetDataReturnDataSet(Enums.euConnectionString._24_bank_ConnStr, sb.ToString(), parameters);
            //}
            #endregion //void GetDepositRate13_14_15 (byte pageIndex, byte pageSize, Enums.euOrderDirection? orderDirection, Enums.euDepositRate13_14_15_Item item)（基準利率/基準利率之指標利率/基準利率之一定比率）

            #region void GetUsuryRate1 (byte pageIndex, byte pageSize, Enums.euOrderDirection? orderDirection, GetUsuryRate1_ReturnTable2Fields? orderField)（台灣放款利率）
            /// <summary>
            /// 得到 台灣放款利率
            /// </summary>
            public void GetUsuryRate1 (byte pageIndex, byte pageSize, Enums.euOrderDirection? orderDirection, GetUsuryRate1_ReturnTable2Fields? orderField)
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();

                #region commandText
                //sb.AppendLine("DECLARE @PageIndex INT");
                //sb.AppendLine("DECLARE @PageSize INT");

                //sb.AppendLine("SET @PageIndex = 1");
                //sb.AppendLine("SET @PageSize = 25");

                sb.AppendLine("DECLARE @ResultTable1 TABLE ( BCode NVARCHAR(50),");
                sb.AppendLine("                              BName NVARCHAR(50),");
                sb.AppendLine("                              Rate1_Min REAL,");
                sb.AppendLine("                              Rate1_Max REAL,");
                sb.AppendLine("                              Rate2_Min REAL,");
                sb.AppendLine("                              Rate2_Max REAL,");
                sb.AppendLine("                              Rate3 REAL)");

                sb.AppendLine("INSERT INTO @ResultTable1");
                sb.AppendLine("            (BCode, BName)");
                sb.AppendLine("    SELECT DISTINCT bcode, bname");
                sb.AppendLine("    FROM q_crate5");
                sb.AppendLine("    WHERE Amount='一般'");
                sb.AppendLine("      AND (Item = '信用卡最低利率'");
                sb.AppendLine("       OR  Item = '信用卡最高利率')");

                sb.AppendLine("INSERT INTO @ResultTable1");
                sb.AppendLine("            (BCode, BName)");
                sb.AppendLine("    SELECT DISTINCT bcode, bname");
                sb.AppendLine("    FROM q_crate8");
                sb.AppendLine("    WHERE Amount='一般'");
                sb.AppendLine("      AND (Item = '現金卡最低利率'");
                sb.AppendLine("       OR  Item = '現金卡最高利率')");
                sb.AppendLine("      AND bcode NOT IN");
                sb.AppendLine("          (");
                sb.AppendLine("              SELECT BCode");
                sb.AppendLine("              FROM @ResultTable1");
                sb.AppendLine("          )");

                sb.AppendLine("INSERT INTO @ResultTable1");
                sb.AppendLine("            (BCode, BName)");
                sb.AppendLine("    SELECT DISTINCT bcode, bname");
                sb.AppendLine("    FROM q_crate7");
                sb.AppendLine("    WHERE Amount='一般'");
                sb.AppendLine("      AND bcode NOT IN");
                sb.AppendLine("          (");
                sb.AppendLine("              SELECT BCode");
                sb.AppendLine("              FROM @ResultTable1");
                sb.AppendLine("          )");

                sb.AppendLine("DECLARE @BCode NVARCHAR(50)");
                sb.AppendLine("DECLARE @Rate REAL");

                sb.AppendLine("DECLARE vend_cursor CURSOR FOR");
                sb.AppendLine("    SELECT bcode, MAX(rate) rate");
                sb.AppendLine("    FROM q_crate5");
                sb.AppendLine("    WHERE Amount='一般'");
                sb.AppendLine("      AND Item = '信用卡最低利率'");
                sb.AppendLine("    GROUP BY bcode");
                sb.AppendLine("OPEN vend_cursor");
                sb.AppendLine("FETCH NEXT FROM vend_cursor");
                sb.AppendLine("INTO @BCode, @Rate");

                sb.AppendLine("WHILE (@@FETCH_STATUS = 0)");
                sb.AppendLine("  BEGIN");
                sb.AppendLine("    UPDATE @ResultTable1");
                sb.AppendLine("    SET Rate1_Min = @Rate");
                sb.AppendLine("    WHERE BCode = @BCode");

                sb.AppendLine("    FETCH NEXT FROM vend_cursor");
                sb.AppendLine("    INTO @BCode, @Rate");
                sb.AppendLine("  END");

                sb.AppendLine("CLOSE vend_cursor");
                sb.AppendLine("DEALLOCATE vend_cursor");

                sb.AppendLine("DECLARE vend_cursor CURSOR FOR");
                sb.AppendLine("    SELECT bcode, MAX(rate) rate");
                sb.AppendLine("    FROM q_crate5");
                sb.AppendLine("    WHERE Amount='一般'");
                sb.AppendLine("      AND Item = '信用卡最高利率'");
                sb.AppendLine("    GROUP BY bcode");
                sb.AppendLine("OPEN vend_cursor");
                sb.AppendLine("FETCH NEXT FROM vend_cursor");
                sb.AppendLine("INTO @BCode, @Rate");

                sb.AppendLine("WHILE (@@FETCH_STATUS = 0)");
                sb.AppendLine("  BEGIN");
                sb.AppendLine("    UPDATE @ResultTable1");
                sb.AppendLine("    SET Rate1_Max = @Rate");
                sb.AppendLine("    WHERE BCode = @BCode");

                sb.AppendLine("    FETCH NEXT FROM vend_cursor");
                sb.AppendLine("    INTO @BCode, @Rate");
                sb.AppendLine("  END");

                sb.AppendLine("CLOSE vend_cursor");
                sb.AppendLine("DEALLOCATE vend_cursor");

                sb.AppendLine("DECLARE vend_cursor CURSOR FOR");
                sb.AppendLine("    SELECT bcode, MAX(rate) rate");
                sb.AppendLine("    FROM q_crate8");
                sb.AppendLine("    WHERE Amount='一般'");
                sb.AppendLine("      AND Item = '現金卡最低利率'");
                sb.AppendLine("    GROUP BY bcode");
                sb.AppendLine("OPEN vend_cursor");
                sb.AppendLine("FETCH NEXT FROM vend_cursor");
                sb.AppendLine("INTO @BCode, @Rate");

                sb.AppendLine("WHILE (@@FETCH_STATUS = 0)");
                sb.AppendLine("  BEGIN");
                sb.AppendLine("    UPDATE @ResultTable1");
                sb.AppendLine("    SET Rate2_Min = @Rate");
                sb.AppendLine("    WHERE BCode = @BCode");

                sb.AppendLine("    FETCH NEXT FROM vend_cursor");
                sb.AppendLine("    INTO @BCode, @Rate");
                sb.AppendLine("  END");

                sb.AppendLine("CLOSE vend_cursor");
                sb.AppendLine("DEALLOCATE vend_cursor");

                sb.AppendLine("DECLARE vend_cursor CURSOR FOR");
                sb.AppendLine("    SELECT bcode, MAX(rate) rate");
                sb.AppendLine("    FROM q_crate8");
                sb.AppendLine("    WHERE Amount='一般'");
                sb.AppendLine("      AND Item = '現金卡最高利率'");
                sb.AppendLine("    GROUP BY bcode");
                sb.AppendLine("OPEN vend_cursor");
                sb.AppendLine("FETCH NEXT FROM vend_cursor");
                sb.AppendLine("INTO @BCode, @Rate");

                sb.AppendLine("WHILE (@@FETCH_STATUS = 0)");
                sb.AppendLine("  BEGIN");
                sb.AppendLine("    UPDATE @ResultTable1");
                sb.AppendLine("    SET Rate2_Max = @Rate");
                sb.AppendLine("    WHERE BCode = @BCode");

                sb.AppendLine("    FETCH NEXT FROM vend_cursor");
                sb.AppendLine("    INTO @BCode, @Rate");
                sb.AppendLine("  END");

                sb.AppendLine("CLOSE vend_cursor");
                sb.AppendLine("DEALLOCATE vend_cursor");

                sb.AppendLine("DECLARE vend_cursor CURSOR FOR");
                sb.AppendLine("    SELECT bcode, MAX(rate) rate");
                sb.AppendLine("    FROM q_crate7");
                sb.AppendLine("    WHERE Amount='一般'");
                sb.AppendLine("    GROUP BY bcode");
                sb.AppendLine("OPEN vend_cursor");
                sb.AppendLine("FETCH NEXT FROM vend_cursor");
                sb.AppendLine("INTO @BCode, @Rate");

                sb.AppendLine("WHILE (@@FETCH_STATUS = 0)");
                sb.AppendLine("  BEGIN");
                sb.AppendLine("    UPDATE @ResultTable1");
                sb.AppendLine("    SET Rate3 = @Rate");
                sb.AppendLine("    WHERE BCode = @BCode");

                sb.AppendLine("    FETCH NEXT FROM vend_cursor");
                sb.AppendLine("    INTO @BCode, @Rate");
                sb.AppendLine("  END");

                sb.AppendLine("CLOSE vend_cursor");
                sb.AppendLine("DEALLOCATE vend_cursor");

                sb.AppendLine("DECLARE @TotalCount INT");
                sb.AppendLine("DECLARE @TotalPage INT");

                sb.AppendLine("SELECT @TotalCount = COUNT(*)");
                sb.AppendLine("FROM   @ResultTable1");

                sb.AppendLine("SET @TotalPage = @TotalCount / @PageSize");
                sb.AppendLine("IF (@TotalPage * @PageSize <> @TotalCount)");
                sb.AppendLine("    SET @TotalPage = @TotalPage + 1");

                sb.AppendLine("SELECT  @PageIndex AS PageIndex,");
                sb.AppendLine("        @PageSize AS PageSize,");
                sb.AppendLine("        CASE WHEN @TotalCount IS NULL THEN 0 ELSE @TotalCount END AS TotalCount,");
                sb.AppendLine("        CASE WHEN @TotalPage IS NULL THEN 0 ELSE @TotalPage END AS TotalPage");

                sb.AppendLine("DECLARE @ResultTable2 TABLE (  [Index] INT IDENTITY(1, 1),");
                sb.AppendLine("                              BCode NVARCHAR(50),");
                sb.AppendLine("                              BName NVARCHAR(50),");
                sb.AppendLine("                              Rate1_Min REAL,");
                sb.AppendLine("                              Rate1_Max REAL,");
                sb.AppendLine("                              Rate2_Min REAL,");
                sb.AppendLine("                              Rate2_Max REAL,");
                sb.AppendLine("                              Rate3 REAL)");

                sb.AppendLine("INSERT INTO @ResultTable2");
                sb.AppendLine("        SELECT BCode, BName, Rate1_Min, Rate1_Max, Rate2_Min, Rate2_Max, Rate3");
                sb.AppendLine("        FROM @ResultTable1");
                sb.AppendLine(String.Format("        ORDER BY {0} {1}", ((orderField != null) ? (GetUsuryRate1_ReturnTable2Fields)orderField : GetUsuryRate1_ReturnTable2Fields.BCode), ((orderDirection != null) ? ((Enums.euOrderDirection)orderDirection).ToString() : String.Empty)));

                sb.AppendLine("SELECT *");
                sb.AppendLine("FROM @ResultTable2");
                sb.AppendLine("WHERE [Index] BETWEEN (@PageIndex - 1) * @PageSize + 1 AND @PageIndex * @PageSize");
                sb.AppendLine("--新版理財 by Alex _24_Bank.cs Bank GetUsuryRate1");
                #endregion commandText
                //WriteLog.Log<string>("commandText", sb.ToString());
                SqlParameter[] parameters = new SqlParameter[2];
                parameters[0] = new SqlParameter("@PageIndex", SqlDbType.SmallInt);
                parameters[0].Value = pageIndex;
                parameters[1] = new SqlParameter("@PageSize", SqlDbType.SmallInt);
                parameters[1].Value = pageSize;

                GetDataReturnDataSet(Enums.euConnectionString._24_bank_ConnStr, sb.ToString(), parameters);
            }
            #endregion void GetUsuryRate1 (byte pageIndex, byte pageSize, Enums.euOrderDirection? orderDirection, GetUsuryRate1_ReturnTable2Fields? orderField)（台灣放款利率）

            #region void GetBankDetail (string bCode)（銀行詳細資料）
            /// <summary>
            /// 得到 銀行詳細資料
            /// </summary>
            public void GetBankDetail (string bCode)
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();

                #region commandText

                #region //Mark
                //sb.AppendLine("SELECT *");
                //sb.AppendLine("FROM Bank_Rate");
                //sb.AppendLine("WHERE 1=1");
                //sb.AppendLine("  AND BCode = @BCode");
                //sb.AppendLine("ORDER BY Item");
                //sb.AppendLine("--新版理財 by Alex _24_Bank.cs GetBankDetail");
                #endregion //Mark
                //sb.AppendLine("DECLARE @BCode VARCHAR(20)");
                //sb.AppendLine("SET @BCode = '0040000'");

                sb.AppendLine("DECLARE @Exist TINYINT");
                sb.AppendLine("SELECT @Exist = COUNT(BCode)");

                sb.AppendLine("FROM Bank_Rate");
                sb.AppendLine("WHERE 1=1");
                sb.AppendLine("  AND BCode = @BCode");

                sb.AppendLine("IF (@Exist > 0)");
                sb.AppendLine("  BEGIN");
                sb.AppendLine("    SELECT 1, BName ");
                sb.AppendLine("    FROM Bank_Rate");
                sb.AppendLine("    WHERE 1=1");
                sb.AppendLine("      AND BCode = @BCode");

                sb.AppendLine("    SELECT Item, Period, Amount, EDate, FRate, MRate, BankType");
                sb.AppendLine("    FROM Bank_Rate");
                sb.AppendLine("    WHERE 1=1");
                sb.AppendLine("      AND BCode = @BCode");
                sb.AppendLine("    ORDER BY Item");

                sb.AppendLine("    SELECT currency, bankitem, bankperiod, banklimit, bankrate");
                sb.AppendLine("    FROM newBank_rate");
                sb.AppendLine("    WHERE 1=1");
                sb.AppendLine("      AND bankid = @BCode");
                sb.AppendLine("  END");
                sb.AppendLine("ELSE");
                sb.AppendLine("  BEGIN");
                sb.AppendLine("    SELECT 2, bankname AS BName");
                sb.AppendLine("    FROM newBank_rate");
                sb.AppendLine("    WHERE 1=1");
                sb.AppendLine("      AND bankid = @BCode");

                sb.AppendLine("    SELECT currency, bankitem, bankperiod, banklimit, bankrate");
                sb.AppendLine("    FROM newBank_rate");
                sb.AppendLine("    WHERE 1=1");
                sb.AppendLine("      AND bankid = @BCode");
                sb.AppendLine("  END");
                sb.AppendLine("--新版理財 by Alex _24_Bank.cs Bank GetBankDetail");
                #endregion commandText
                //WriteLog.Log<string>("commandText", sb.ToString());
                SqlParameter[] parameters = new SqlParameter[1];
                parameters[0] = new SqlParameter("@BCode", SqlDbType.NVarChar, 50);
                parameters[0].Value = bCode;

                GetDataReturnDataSet(Enums.euConnectionString._24_bank_ConnStr, sb.ToString(), parameters);
            }
            #endregion void GetBankDetail (string bCode)（銀行詳細資料）

            #region Get各項業務利率 (string tableName, bool moreWhereCondition, string item, string period)
            public void Get各項業務利率 (string tableName, bool moreWhereCondition, string item, string period)
            {
                System.Text.StringBuilder sbBase = new System.Text.StringBuilder();
                System.Text.StringBuilder sb = new System.Text.StringBuilder();

                #region CommandText
                sbBase.Append("SELECT * ");
                sbBase.Append(String.Format("FROM {0} ", tableName));
                sbBase.Append("WHERE Amount='一般' ");
                if (moreWhereCondition)
                {
                    sbBase.Append("AND Item=@Item ");
                    sbBase.Append("AND Period=@Period");
                }

                sb.AppendLine("SELECT TOP 2 Rate FROM");
                sb.AppendLine("(");
                sb.AppendLine("    SELECT BCode, BName, MAX(rate) Rate");
                sb.AppendLine("    FROM");
                sb.AppendLine("    (");
                sb.AppendLine(String.Format("        {0}", sbBase.ToString()));
                sb.AppendLine("    ) a");
                sb.AppendLine("    GROUP BY BCode, BName");
                sb.AppendLine(") b");
                sb.AppendLine("GROUP BY Rate");
                sb.AppendLine("ORDER BY COUNT(Rate) DESC");

                sb.AppendLine("SELECT MAX(Rate) MaxRate, MIN(Rate) MinRate, AVG(Rate) ARate");
                sb.AppendLine("FROM");
                sb.AppendLine("(");
                sb.AppendLine("    SELECT BCode, BName, MAX(rate) Rate");
                sb.AppendLine("    FROM");
                sb.AppendLine("    (");
                sb.AppendLine(String.Format("        {0}", sbBase.ToString()));
                sb.AppendLine("    ) a");
                sb.AppendLine("    GROUP BY BCode, BName");
                sb.AppendLine(") b ");
                
                sb.AppendLine("SELECT BCode, BName, MAX(Rate) Rate");
                sb.AppendLine("FROM");
                sb.AppendLine("(");
                sb.AppendLine(String.Format("    {0}", sbBase.ToString()));
                sb.AppendLine(") a");
                sb.AppendLine("GROUP BY BCode, BName");
                sb.AppendLine("ORDER BY Rate DESC");
                sb.AppendLine("--新版理財 by Alex _24_Bank.cs Bank Get各項業務利率");
                #endregion CommandText
                //WriteLog.Log<string>("CommandText", sb.ToString());
                SqlParameter[] parameters = null;
                if (moreWhereCondition)
                {
                    parameters = new SqlParameter[2];

                    parameters[0] = new SqlParameter("@Item", SqlDbType.NVarChar, 50);
                    parameters[0].Value = item;
                    parameters[1] = new SqlParameter("@Period", SqlDbType.NVarChar, 50);
                    parameters[1].Value = period;
                }

                GetDataReturnDataSet(Enums.euConnectionString._24_bank_ConnStr, sb.ToString(), parameters);
            }
            #endregion Get各項業務利率 (string tableName, bool moreWhereCondition, string item, string period)
        }
        #endregion class Bank : DataProvider

        #region class NewBankRate : DataProvider
        public class NewBankRate : DataProvider
        {
            #region Get外幣存款利率ByCountry_ReturnTable2Fields : byte
            public enum Get外幣存款利率ByCountry_ReturnTable2Fields : byte
            {
                Index,
                BankID,
                BankName,
                /// <summary>
                /// 美元
                /// </summary>
                [System.ComponentModel.Description("美元")]
                Rate_USD,
                /// <summary>
                /// 歐元
                /// </summary>
                [System.ComponentModel.Description("歐元")]
                Rate_EUR,
                /// <summary>
                /// 人民幣
                /// </summary>
                [System.ComponentModel.Description("人民幣")]
                Rate_CNY,
                /// <summary>
                /// 英鎊
                /// </summary>
                [System.ComponentModel.Description("英鎊")]
                Rate_GBP,
                /// <summary>
                /// 澳幣
                /// </summary>
                [System.ComponentModel.Description("澳幣")]
                Rate_AUD,
                /// <summary>
                /// 紐幣
                /// </summary>
                [System.ComponentModel.Description("紐幣")]
                Rate_NZD,
                /// <summary>
                /// 加拿大幣
                /// </summary>
                [System.ComponentModel.Description("加拿大幣")]
                Rate_CAD,
                /// <summary>
                /// 新加坡幣
                /// </summary>
                [System.ComponentModel.Description("新加坡幣")]
                Rate_SGD,
                /// <summary>
                /// 瑞士法郎
                /// </summary>
                [System.ComponentModel.Description("瑞士法郎")]
                Rate_CHF,
                /// <summary>
                /// 港幣
                [System.ComponentModel.Description("港幣")]
                /// </summary>
                Rate_HKD,
                /// <summary>
                /// 日圓
                [System.ComponentModel.Description("日圓")]
                /// </summary>
                Rate_JPY,
                /// <summary>
                /// 南非幣
                [System.ComponentModel.Description("南非幣")]
                /// </summary>
                Rate_ZAR
            }
            #endregion Get外幣存款利率ByCountry_ReturnTable2Fields : byte

            #region Get外幣存款利率ByCountry_Currency_ReturnTable2Fields : byte
            public enum Get外幣存款利率ByCountry_Currency_ReturnTable2Fields : byte
            {
                Index,
                BankID,
                BankName,
                /// <summary>
                /// 活期存款
                /// </summary>
                [System.ComponentModel.Description("活期存款")]
                Rate1,
                /// <summary>
                /// 定期存款,1個月
                /// </summary>
                [System.ComponentModel.Description("定期存款,1個月")]
                Rate2,
                /// <summary>
                /// 定期存款,3個月
                /// </summary>
                [System.ComponentModel.Description("定期存款,3個月")]
                Rate3,
                /// <summary>
                /// 定期存款,6個月
                /// </summary>
                [System.ComponentModel.Description("定期存款,6個月")]
                Rate4,
                /// <summary>
                /// 定期存款,9個月
                /// </summary>
                [System.ComponentModel.Description("定期存款,9個月")]
                Rate5,
                /// <summary>
                /// 定期存款,1年
                [System.ComponentModel.Description("定期存款,1年")]
                /// </summary>
                Rate6
            }
            #endregion Get外幣存款利率ByCountry_Currency_ReturnTable2Fields : byte

            #region Get存款利率ByCountry_Currency_ReturnTable2Fields : byte
            public enum Get存款利率ByCountry_Currency_ReturnTable2Fields : byte
            {
                Index,
                BankID,
                BankName,
                Rate0,
                Rate1,
                Rate2,
                Rate3,
                Rate6,
                Rate9,
                Rate12
            }
            #endregion Get存款利率ByCountry_Currency_ReturnTable2Fields : byte

            #region void Get外幣存款利率ByCountry (string countryCode, string period, byte pageIndex, byte pageSize, Get外幣存款利率ByCountry_ReturnTable2Fields? orderField, Enums.euOrderDirection? orderDirection)（依據國碼得到銀行資料）
            /// <summary>
            /// 依據國碼得到銀行資料
            /// </summary>
            public void Get外幣存款利率ByCountry (string countryCode, string period, byte pageIndex, byte pageSize, Get外幣存款利率ByCountry_ReturnTable2Fields? orderField, Enums.euOrderDirection? orderDirection)
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                //WriteLog.Log<string>("countryCode", countryCode); WriteLog.Log<string>("period", period); WriteLog.Log<byte>("pageIndex", pageIndex); WriteLog.Log<byte>("pageSize", pageSize); WriteLog.Log<string>("orderField", orderField.ToString()); WriteLog.Log<string>("orderDirection", orderDirection.ToString());
                #region CommandText
                sb.AppendLine("--DECLARE @PageIndex SmallInt");
                sb.AppendLine(String.Format("--SET @PageIndex = {0}", pageIndex));

                sb.AppendLine("--DECLARE @PageSize SmallInt");
                sb.AppendLine(String.Format("--SET @PageSize = {0}", pageSize));

                if (countryCode.Length > 0)
                {
                    sb.AppendLine("--DECLARE @Country VARCHAR(2)");
                    sb.AppendLine(String.Format("--SET @Country = '{0}'", countryCode));
                }

                sb.AppendLine("DECLARE @ResultTable1 TABLE (BankID VARCHAR(50),");
                sb.AppendLine("                             BankName VARCHAR(50),");
                #region //Mark
                //sb.AppendLine("                             Rate_USD DECIMAL(10, 6),");
                //sb.AppendLine("                             Rate_EUR DECIMAL(10, 6),");
                //sb.AppendLine("                             Rate_GBP DECIMAL(10, 6),");
                //sb.AppendLine("                             Rate_AUD DECIMAL(10, 6),");
                //sb.AppendLine("                             Rate_NZD DECIMAL(10, 6),");
                //sb.AppendLine("                             Rate_CAD DECIMAL(10, 6),");
                //sb.AppendLine("                             Rate_SGD DECIMAL(10, 6),");
                //sb.AppendLine("                             Rate_CHF DECIMAL(10, 6),");
                //sb.AppendLine("                             Rate_HKD DECIMAL(10, 6),");
                //sb.AppendLine("                             Rate_JPY DECIMAL(10, 6),");
                //sb.AppendLine("                             Rate_ZAR DECIMAL(10, 6))");
                #endregion //Mark
                foreach (Enums.eu外幣存款利率抓取幣別 c in Enum.GetValues(typeof(Enums.eu外幣存款利率抓取幣別)))
                {
                    sb.AppendLine(String.Format("                             Rate_{0} DECIMAL(10, 6){1}", c, ((c != Enums.eu外幣存款利率抓取幣別.ZAR) ? "," : ")")));
                }

                sb.AppendLine("DECLARE @BankID VARCHAR(50)");
                sb.AppendLine("DECLARE @BankName VARCHAR(50)");
                #region //Mark
                //sb.AppendLine("DECLARE @USD DECIMAL(10, 6)");
                //sb.AppendLine("DECLARE @EUR DECIMAL(10, 6)");
                //sb.AppendLine("DECLARE @GBP DECIMAL(10, 6)");
                //sb.AppendLine("DECLARE @AUD DECIMAL(10, 6)");
                //sb.AppendLine("DECLARE @NZD DECIMAL(10, 6)");
                //sb.AppendLine("DECLARE @CAD DECIMAL(10, 6)");
                //sb.AppendLine("DECLARE @SGD DECIMAL(10, 6)");
                //sb.AppendLine("DECLARE @CHF DECIMAL(10, 6)");
                //sb.AppendLine("DECLARE @HKD DECIMAL(10, 6)");
                //sb.AppendLine("DECLARE @JPY DECIMAL(10, 6)");
                //sb.AppendLine("DECLARE @ZAR DECIMAL(10, 6)");
                #endregion //Mark
                foreach (Enums.eu外幣存款利率抓取幣別 c in Enum.GetValues(typeof(Enums.eu外幣存款利率抓取幣別)))
                {
                    sb.AppendLine(String.Format("DECLARE @{0} DECIMAL(10, 6)", c));
                }

                sb.AppendLine("DECLARE vend_cursor CURSOR FOR");
                sb.AppendLine("    SELECT DISTINCT bankid, bankname");
                sb.AppendLine("    FROM newBank_rate");
                sb.AppendLine("    WHERE 1=1");
                if (countryCode.Length > 0)
                    sb.AppendLine("      AND Country = @Country");
                sb.AppendLine("OPEN vend_cursor");
                sb.AppendLine("FETCH NEXT FROM vend_cursor");
                sb.AppendLine("INTO @BankID, @BankName");

                sb.AppendLine("WHILE (@@FETCH_STATUS = 0)");
                sb.AppendLine("  BEGIN");
                #region //Mark
                //string[,] currentcys =
                //    {
                //        {"美元", "USD"},
                //        {"歐元", "EUR"},
                //        {"英鎊", "GBP"},
                //        {"澳幣", "AUD"},
                //        {"紐幣", "NZD"},
                //        {"加拿大幣", "CAD"},
                //        {"新加坡幣", "SGD"},
                //        {"瑞士法郎", "CHF"},
                //        {"港幣", "HKD"},
                //        {"日圓", "JPY"},
                //        {"南非幣", "ZAR"}
                //    };

                //for (int i = 0; i < currentcys.GetLength(0); i++)
                //{
                //    sb.AppendLine(String.Format("    /*{0}*/", currentcys[i, 0]));
                //    sb.AppendLine(String.Format("    SELECT @{0} = bankrate", currentcys[i, 1]));
                //    sb.AppendLine("    FROM newBank_rate");
                //    sb.AppendLine("    WHERE bankid = @BankID");
                //    sb.AppendLine(String.Format("      AND currency = '{0}'", currentcys[i, 1]));
                //    if (period.Length > 0)
                //    {
                //        sb.AppendLine("      AND bankitem LIKE '%定期%'");
                //        sb.Append("      AND");
                //        if (period.IndexOf(',') > 0)
                //            sb.Append(" (");

                //        for (int j = 0; j < period.Split(',').Length; j++)
                //            sb.Append(String.Format(" {0}bankperiod = '{1}'", ((j == 0) ? String.Empty : "OR "), period.Split(',')[j]));

                //        if (period.IndexOf(',') > 0)
                //            sb.AppendLine(" )");
                //        else
                //            sb.AppendLine(String.Empty);
                //    }
                //}
                #endregion //Mark
                foreach (Enums.eu外幣存款利率抓取幣別 c in Enum.GetValues(typeof(Enums.eu外幣存款利率抓取幣別)))
                {
                    sb.AppendLine(String.Format("    /*{0}*/", Enums.GetEnumDesc(c)));
                    sb.AppendLine(String.Format("    SELECT @{0} = bankrate", c));
                    sb.AppendLine("    FROM newBank_rate");
                    sb.AppendLine("    WHERE bankid = @BankID");
                    sb.AppendLine(String.Format("      AND currency = '{0}'", c));
                    if (period.Length > 0)
                    {
                        sb.AppendLine("      AND bankitem LIKE '%定期%'");
                        sb.Append("      AND");
                        if (period.IndexOf(',') > 0)
                            sb.Append(" (");

                        for (int j = 0; j < period.Split(',').Length; j++)
                            sb.Append(String.Format(" {0}bankperiod = '{1}'", ((j == 0) ? String.Empty : "OR "), period.Split(',')[j]));

                        if (period.IndexOf(',') > 0)
                            sb.AppendLine(" )");
                        else
                            sb.AppendLine(String.Empty);
                    }
                }

                #region //Mark
                //sb.AppendLine("    /*美元*/");
                //sb.AppendLine("    SELECT @USD = bankrate");
                //sb.AppendLine("    FROM newBank_rate");
                //sb.AppendLine("    WHERE bankid = @BankID");
                //sb.AppendLine("      AND currency = 'USD'");
                //sb.AppendLine("    /*歐元*/");
                //sb.AppendLine("    SELECT @EUR = bankrate");
                //sb.AppendLine("    FROM newBank_rate");
                //sb.AppendLine("    WHERE bankid = @BankID");
                //sb.AppendLine("      AND currency = 'EUR'");
                //sb.AppendLine("    /*英鎊*/");
                //sb.AppendLine("    SELECT @GBP = bankrate");
                //sb.AppendLine("    FROM newBank_rate");
                //sb.AppendLine("    WHERE bankid = @BankID");
                //sb.AppendLine("      AND currency = 'GBP'");
                //sb.AppendLine("    /*澳幣*/");
                //sb.AppendLine("    SELECT @AUD = bankrate");
                //sb.AppendLine("    FROM newBank_rate");
                //sb.AppendLine("    WHERE bankid = @BankID");
                //sb.AppendLine("      AND currency = 'AUD'");
                //sb.AppendLine("    /*紐幣*/");
                //sb.AppendLine("    SELECT @NZD = bankrate");
                //sb.AppendLine("    FROM newBank_rate");
                //sb.AppendLine("    WHERE bankid = @BankID");
                //sb.AppendLine("      AND currency = 'NZD'");
                //sb.AppendLine("    /*加拿大幣*/");
                //sb.AppendLine("    SELECT @CAD = bankrate");
                //sb.AppendLine("    FROM newBank_rate");
                //sb.AppendLine("    WHERE bankid = @BankID");
                //sb.AppendLine("      AND currency = 'CAD'");
                //sb.AppendLine("    /*新加坡幣*/");
                //sb.AppendLine("    SELECT @SGD = bankrate");
                //sb.AppendLine("    FROM newBank_rate");
                //sb.AppendLine("    WHERE bankid = @BankID");
                //sb.AppendLine("      AND currency = 'SGD'");
                //sb.AppendLine("    /*瑞士法郎*/");
                //sb.AppendLine("    SELECT @CHF = bankrate");
                //sb.AppendLine("    FROM newBank_rate");
                //sb.AppendLine("    WHERE bankid = @BankID");
                //sb.AppendLine("      AND currency = 'CHF'");
                //sb.AppendLine("    /*港幣*/");
                //sb.AppendLine("    SELECT @HKD = bankrate");
                //sb.AppendLine("    FROM newBank_rate");
                //sb.AppendLine("    WHERE bankid = @BankID");
                //sb.AppendLine("      AND currency = 'HKD'");
                #endregion //Mark
                sb.AppendLine("    INSERT INTO @ResultTable1");
                //sb.AppendLine("                (BankID, BankName, Rate_USD, Rate_EUR, Rate_GBP, Rate_AUD, Rate_NZD, Rate_CAD, Rate_SGD, Rate_CHF, Rate_HKD, Rate_JPY, Rate_ZAR)");
                sb.Append("                (BankID, BankName");
                foreach (Enums.eu外幣存款利率抓取幣別 c in Enum.GetValues(typeof(Enums.eu外幣存款利率抓取幣別)))
                {
                    sb.Append(String.Format(", Rate_{0}", c));
                }
                sb.AppendLine(")");

                //sb.AppendLine("        VALUES  (@BankID, @BankName, @USD, @EUR, @GBP, @AUD, @NZD, @CAD, @SGD, @CHF, @HKD, @JPY, @ZAR)");
                sb.Append("        VALUES  (@BankID, @BankName");
                foreach (Enums.eu外幣存款利率抓取幣別 c in Enum.GetValues(typeof(Enums.eu外幣存款利率抓取幣別)))
                {
                    sb.Append(String.Format(", @{0}", c));
                }
                sb.AppendLine(")");

                foreach (Enums.eu外幣存款利率抓取幣別 c in Enum.GetValues(typeof(Enums.eu外幣存款利率抓取幣別)))
                {
                    sb.AppendLine(String.Format("    SET @{0} = NULL", c));
                }

                sb.AppendLine("    FETCH NEXT FROM vend_cursor");
                sb.AppendLine("    INTO @BankID, @BankName");
                sb.AppendLine("  END");

                sb.AppendLine("CLOSE vend_cursor");
                sb.AppendLine("DEALLOCATE vend_cursor");

                //sb.AppendLine("--DECLARE @PageIndex INT");
                //sb.AppendLine("--DECLARE @PageSize INT");

                //sb.AppendLine("--SET @PageIndex = 1");
                //sb.AppendLine("--SET @PageSize = 25");

                sb.AppendLine("DECLARE @TotalCount INT");
                sb.AppendLine("DECLARE @TotalPage INT");

                sb.AppendLine("SELECT @TotalCount = COUNT(BankID)");
                sb.AppendLine("FROM   @ResultTable1");

                sb.AppendLine("SET @TotalPage = @TotalCount / @PageSize");
                sb.AppendLine("IF (@TotalPage * @PageSize <> @TotalCount)");
                sb.AppendLine("    SET @TotalPage = @TotalPage + 1");

                sb.AppendLine("SELECT  @PageIndex AS PageIndex,");
                sb.AppendLine("        @PageSize AS PageSize,");
                sb.AppendLine("        CASE WHEN @TotalCount IS NULL THEN 0 ELSE @TotalCount END AS TotalCount,");
                sb.AppendLine("        CASE WHEN @TotalPage IS NULL THEN 0 ELSE @TotalPage END AS TotalPage");

                sb.AppendLine("DECLARE @ResultTable2 TABLE ([Index] INT IDENTITY(1, 1),");
                sb.AppendLine("                             BankID VARCHAR(50),");
                sb.AppendLine("                             BankName VARCHAR(50),");
                #region //Mark
                //sb.AppendLine("                             Rate_USD DECIMAL(10, 6),");
                //sb.AppendLine("                             Rate_EUR DECIMAL(10, 6),");
                //sb.AppendLine("                             Rate_GBP DECIMAL(10, 6),");
                //sb.AppendLine("                             Rate_AUD DECIMAL(10, 6),");
                //sb.AppendLine("                             Rate_NZD DECIMAL(10, 6),");
                //sb.AppendLine("                             Rate_CAD DECIMAL(10, 6),");
                //sb.AppendLine("                             Rate_SGD DECIMAL(10, 6),");
                //sb.AppendLine("                             Rate_CHF DECIMAL(10, 6),");
                //sb.AppendLine("                             Rate_HKD DECIMAL(10, 6),");
                //sb.AppendLine("                             Rate_JPY DECIMAL(10, 6),");
                //sb.AppendLine("                             Rate_ZAR DECIMAL(10, 6))");
                #endregion //Mark
                foreach (Enums.eu外幣存款利率抓取幣別 c in Enum.GetValues(typeof(Enums.eu外幣存款利率抓取幣別)))
                {
                    sb.AppendLine(String.Format("                             Rate_{0} DECIMAL(10, 6){1}", c, ((c != Enums.eu外幣存款利率抓取幣別.ZAR) ? "," : ")")));
                }

                sb.AppendLine("INSERT INTO @ResultTable2");
                //sb.AppendLine("    SELECT BankID, BankName, Rate_USD, Rate_EUR, Rate_GBP, Rate_AUD, Rate_NZD, Rate_CAD, Rate_SGD, Rate_CHF, Rate_HKD, Rate_JPY, Rate_ZAR");
                sb.Append("    SELECT BankID, BankName");
                foreach (Enums.eu外幣存款利率抓取幣別 c in Enum.GetValues(typeof(Enums.eu外幣存款利率抓取幣別)))
                {
                    sb.Append(String.Format(", Rate_{0}", c));
                }
                sb.AppendLine(String.Empty);

                sb.AppendLine("    FROM @ResultTable1");
                if (orderField != null && orderDirection != null)
                    sb.AppendLine(String.Format("    ORDER BY {0} {1}", orderField, orderDirection));

                sb.AppendLine("SELECT *");
                sb.AppendLine("FROM @ResultTable2");
                sb.AppendLine("WHERE [Index] BETWEEN (@PageIndex - 1) * @PageSize + 1 AND @PageIndex * @PageSize");

                sb.AppendLine("--新版理財 by Alex _24_Bank.cs NewBankRate Get外幣存款利率ByCountry");
                #endregion CommandText
                //if (("dongdong05|cnyes_Alexc").IndexOf(System.Web.HttpContext.Current.Server.UrlDecode(Globals.AicLogin(System.Web.HttpContext.Current.Request))) >= 0)
                //{
                //    WriteLog.Log1<string>("_24_Bank.txt", "NewBankRate Get外幣存款利率ByCountry", sb.ToString());
                //}
                SqlParameter[] parameters = new SqlParameter[((countryCode.Length > 0) ? 3 : 2)];
                parameters[0] = new SqlParameter("@PageIndex", SqlDbType.SmallInt);
                parameters[0].Value = pageIndex;
                parameters[1] = new SqlParameter("@PageSize", SqlDbType.SmallInt);
                parameters[1].Value = pageSize;
                if (countryCode.Length > 0)
                {
                    parameters[2] = new SqlParameter("@Country", SqlDbType.VarChar, 2);
                    parameters[2].Value = countryCode;
                }

                GetDataReturnDataSet(Enums.euConnectionString._24_bank_ConnStr, sb.ToString(), parameters);
            }
            #endregion void Get外幣存款利率ByCountry (string countryCode, string period, byte pageIndex, byte pageSize, Get外幣存款利率ByCountry_ReturnTable2Fields? orderField, Enums.euOrderDirection? orderDirection)（依據國碼得到銀行資料）

            #region void Get外幣存款利率ByCountry_Currency (string countryCode, string currency, byte pageIndex, byte pageSize, Get外幣存款利率ByCountry_Currency_ReturnTable2Fields? orderField, Enums.euOrderDirection? orderDirection)（依據國碼、幣別得到銀行資料）
            /// <summary>
            /// 依據國碼、幣別得到銀行資料
            /// </summary>
            public void Get外幣存款利率ByCountry_Currency (string countryCode, string currency, byte pageIndex, byte pageSize, Get外幣存款利率ByCountry_Currency_ReturnTable2Fields? orderField, Enums.euOrderDirection? orderDirection)
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                //WriteLog.Log<string>("countryCode", countryCode); WriteLog.Log<string>("currency", currency); WriteLog.Log<byte>("pageIndex", pageIndex); WriteLog.Log<byte>("pageSize", pageSize); WriteLog.Log<string>("orderField", orderField.ToString()); WriteLog.Log<string>("orderDirection", orderDirection.ToString());
                #region CommandText
                sb.AppendLine("DECLARE @ResultTable1 TABLE (BankID VARCHAR(50),");
                sb.AppendLine("                             BankName VARCHAR(50),");
                sb.AppendLine("                             Rate1 DECIMAL(10, 6) NULL,");
                sb.AppendLine("                             Rate2 DECIMAL(10, 6) NULL,");
                sb.AppendLine("                             Rate3 DECIMAL(10, 6) NULL,");
                sb.AppendLine("                             Rate4 DECIMAL(10, 6) NULL,");
                sb.AppendLine("                             Rate5 DECIMAL(10, 6) NULL,");
                sb.AppendLine("                             Rate6 DECIMAL(10, 6) NULL)");

                sb.AppendLine("DECLARE @BankID VARCHAR(50)");
                sb.AppendLine("DECLARE @BankName VARCHAR(50)");
                sb.AppendLine("DECLARE @Rate1 DECIMAL(10, 6)");
                sb.AppendLine("DECLARE @Rate2 DECIMAL(10, 6)");
                sb.AppendLine("DECLARE @Rate3 DECIMAL(10, 6)");
                sb.AppendLine("DECLARE @Rate4 DECIMAL(10, 6)");
                sb.AppendLine("DECLARE @Rate5 DECIMAL(10, 6)");
                sb.AppendLine("DECLARE @Rate6 DECIMAL(10, 6)");

                sb.AppendLine("DECLARE vend_cursor CURSOR FOR");
                sb.AppendLine("    SELECT DISTINCT bankid, bankname");
                sb.AppendLine("    FROM newBank_rate");
                sb.AppendLine("    WHERE 1=1");
                if (countryCode.Length > 0)
                    sb.AppendLine("      AND Country = @Country");
                sb.AppendLine("OPEN vend_cursor");
                sb.AppendLine("FETCH NEXT FROM vend_cursor");
                sb.AppendLine("INTO @BankID, @BankName");

                sb.AppendLine("WHILE (@@FETCH_STATUS = 0)");
                sb.AppendLine("  BEGIN");

                string[] itemPeriods =
                    {
                        "活期存款", "定期存款,1個月", "定期存款,3個月", "定期存款,6個月", "定期存款,9個月", "定期存款,1年"
                    };

                for (int i = 0; i < itemPeriods.Length; i++)
                {
                    sb.AppendLine(String.Format("    /*{0}*/", itemPeriods[i].Replace(",", " - ")));
                    sb.AppendLine(String.Format("    SELECT @Rate{0} = bankrate", i + 1));
                    sb.AppendLine("    FROM newBank_rate");
                    sb.AppendLine("    WHERE bankid = @BankID");
                    sb.AppendLine("      AND currency = @Currency");
                    sb.AppendLine(String.Format("      AND bankitem LIKE '%{0}%'", itemPeriods[i].Split(',')[0].Substring(0, 2)));
                    if (itemPeriods[i].Split(',').Length > 1)
                        sb.AppendLine(String.Format("      AND bankperiod = '{0}'", itemPeriods[i].Split(',')[1]));
                }

                sb.AppendLine("    INSERT INTO @ResultTable1");
                sb.AppendLine("                (BankID, BankName, Rate1, Rate2, Rate3, Rate4, Rate5, Rate6)");
                sb.AppendLine("        VALUES  (@BankID, @BankName, @Rate1, @Rate2, @Rate3, @Rate4, @Rate5, @Rate6)");

                sb.AppendLine("         SET @Rate1 = NULL");
                sb.AppendLine("         SET @Rate2 = NULL");
                sb.AppendLine("         SET @Rate3 = NULL");
                sb.AppendLine("         SET @Rate4 = NULL");
                sb.AppendLine("         SET @Rate5 = NULL");
                sb.AppendLine("         SET @Rate6 = NULL");

                sb.AppendLine("    FETCH NEXT FROM vend_cursor");
                sb.AppendLine("    INTO @BankID, @BankName");
                sb.AppendLine("  END");

                sb.AppendLine("CLOSE vend_cursor");
                sb.AppendLine("DEALLOCATE vend_cursor");

                //sb.AppendLine("--DECLARE @PageIndex INT");
                //sb.AppendLine("--DECLARE @PageSize INT");
                //sb.AppendLine("--SET @PageIndex = 1");
                //sb.AppendLine("--SET @PageSize = 25");

                sb.AppendLine("DECLARE @TotalCount INT");
                sb.AppendLine("DECLARE @TotalPage INT");

                sb.AppendLine("SELECT @TotalCount = COUNT(BankID)");
                sb.AppendLine("FROM   @ResultTable1");
                sb.AppendLine("SET @TotalPage = @TotalCount / @PageSize");
                sb.AppendLine("IF (@TotalPage * @PageSize <> @TotalCount)");
                sb.AppendLine("    SET @TotalPage = @TotalPage + 1");

                sb.AppendLine("SELECT  @PageIndex AS PageIndex,");
                sb.AppendLine("        @PageSize AS PageSize,");
                sb.AppendLine("        CASE WHEN @TotalCount IS NULL THEN 0 ELSE @TotalCount END AS TotalCount,");
                sb.AppendLine("        CASE WHEN @TotalPage IS NULL THEN 0 ELSE @TotalPage END AS TotalPage");

                sb.AppendLine("DECLARE @ResultTable2 TABLE ([Index] INT IDENTITY(1, 1),");
                sb.AppendLine("                             BankID VARCHAR(50),");
                sb.AppendLine("                             BankName VARCHAR(50),");
                sb.AppendLine("                             Rate1 DECIMAL(10, 6),");
                sb.AppendLine("                             Rate2 DECIMAL(10, 6),");
                sb.AppendLine("                             Rate3 DECIMAL(10, 6),");
                sb.AppendLine("                             Rate4 DECIMAL(10, 6),");
                sb.AppendLine("                             Rate5 DECIMAL(10, 6),");
                sb.AppendLine("                             Rate6 DECIMAL(10, 6))");

                sb.AppendLine("INSERT INTO @ResultTable2");
                sb.AppendLine("    SELECT BankID, BankName, Rate1, Rate2, Rate3, Rate4, Rate5, Rate6");
                sb.AppendLine("    FROM @ResultTable1");
                if (orderField != null && orderDirection != null)
                    sb.AppendLine(String.Format("    ORDER BY {0} {1}", orderField, orderDirection));

                sb.AppendLine("SELECT *");
                sb.AppendLine("FROM @ResultTable2");
                sb.AppendLine("WHERE [Index] BETWEEN (@PageIndex - 1) * @PageSize + 1 AND @PageIndex * @PageSize");

                sb.AppendLine("--新版理財 by Alex _24_Bank.cs NewBankRate Get外幣存款利率ByCountry_Currency");
                #endregion CommandText
                //if (("dongdong05|cnyes_Alexc").IndexOf(System.Web.HttpContext.Current.Server.UrlDecode(Globals.AicLogin(System.Web.HttpContext.Current.Request))) >= 0)
                //{
                //    WriteLog.Log1<string>("_24_Bank.txt", "NewBankRate Get外幣存款利率ByCountry_Currency", sb.ToString());
                //}
                SqlParameter[] parameters = new SqlParameter[((countryCode.Length > 0) ? 4 : 3)];
                parameters[0] = new SqlParameter("@PageIndex", SqlDbType.SmallInt);
                parameters[0].Value = pageIndex;
                parameters[1] = new SqlParameter("@PageSize", SqlDbType.SmallInt);
                parameters[1].Value = pageSize;
                parameters[2] = new SqlParameter("@Currency", SqlDbType.VarChar, 3);
                parameters[2].Value = currency;
                if (countryCode.Length > 0)
                {
                    parameters[3] = new SqlParameter("@Country", SqlDbType.VarChar, 2);
                    parameters[3].Value = countryCode;
                }

                GetDataReturnDataSet(Enums.euConnectionString._24_bank_ConnStr, sb.ToString(), parameters);
                //WriteLog.LogDataSet(this.ResultDataSet);
            }
            #endregion void Get外幣存款利率ByCountry_Currency (string countryCode, string currency, byte pageIndex, byte pageSize, Get外幣存款利率ByCountry_Currency_ReturnTable2Fields? orderField, Enums.euOrderDirection? orderDirection)（依據國碼、幣別得到銀行資料）

            #region void Get存款利率ByCountry (string countryCode, string currency, Get存款利率ByCountry_Currency_ReturnTable2Fields? orderField, Enums.euOrderDirection? orderDirection)（依據國碼得到銀行資料）
            /// <summary>
            /// 依據國碼得到銀行資料
            /// </summary>
            public void Get存款利率ByCountry (string countryCode, string currency, Get存款利率ByCountry_Currency_ReturnTable2Fields? orderField, Enums.euOrderDirection? orderDirection)
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                //WriteLog.Log<string>("countryCode", countryCode); WriteLog.Log<string>("period", period); WriteLog.Log<byte>("pageIndex", pageIndex); WriteLog.Log<byte>("pageSize", pageSize); WriteLog.Log<string>("orderField", orderField.ToString()); WriteLog.Log<string>("orderDirection", orderDirection.ToString());
                #region CommandText
                sb.AppendLine("DECLARE @ResultTable1 TABLE (BankID VARCHAR(50),");
                sb.AppendLine("                             BankName VARCHAR(50),");
                sb.AppendLine("                             Rate0 DECIMAL(10, 6),");
                sb.AppendLine("                             Rate1 DECIMAL(10, 6),");
                sb.AppendLine("                             Rate2 DECIMAL(10, 6),");
                sb.AppendLine("                             Rate3 DECIMAL(10, 6),");
                sb.AppendLine("                             Rate6 DECIMAL(10, 6),");
                sb.AppendLine("                             Rate9 DECIMAL(10, 6),");
                sb.AppendLine("                             Rate12 DECIMAL(10, 6))");

                sb.AppendLine("DECLARE @BankID VARCHAR(50)");
                sb.AppendLine("DECLARE @BankName VARCHAR(50)");
                sb.AppendLine("DECLARE @Rate DECIMAL(10, 6)");
                sb.AppendLine("DECLARE @Rate1 DECIMAL(10, 6)");
                sb.AppendLine("DECLARE @Rate2 DECIMAL(10, 6)");
                sb.AppendLine("DECLARE @Rate3 DECIMAL(10, 6)");
                sb.AppendLine("DECLARE @Rate6 DECIMAL(10, 6)");
                sb.AppendLine("DECLARE @Rate9 DECIMAL(10, 6)");
                sb.AppendLine("DECLARE @Rate12 DECIMAL(10, 6)");

                sb.AppendLine("DECLARE vend_cursor CURSOR FOR");
                sb.AppendLine("    SELECT DISTINCT bankid, bankname");
                sb.AppendLine("    FROM newBank_rate");
                sb.AppendLine("    WHERE 1=1");
                sb.AppendLine("      AND Country = @Country");
                sb.AppendLine("      AND currency = @Currency");
                sb.AppendLine("OPEN vend_cursor");
                sb.AppendLine("FETCH NEXT FROM vend_cursor");
                sb.AppendLine("INTO @BankID, @BankName");

                sb.AppendLine("WHILE (@@FETCH_STATUS = 0)");
                sb.AppendLine("  BEGIN");

                string[,] itemPeriods = 
                    {
                        {"活期", ""},
                        {"定期", "1個月"},
                        {"定期", "2個月"},
                        {"定期", "3個月"},
                        {"定期", "6個月"},
                        {"定期", "9個月"},
                        {"定期", "12個月"}
                    };

                for (int i = 0; i < itemPeriods.GetLength(0); i++)
                {
                    sb.AppendLine(String.Format("    /*{0} {1}*/", itemPeriods[i, 0], itemPeriods[i, 1]));
                    sb.AppendLine(String.Format("    SELECT @Rate{0} = bankrate", itemPeriods[i, 1].Replace("個月", String.Empty)));
                    sb.AppendLine("    FROM newBank_rate");
                    sb.AppendLine("    WHERE 1=1");
                    sb.AppendLine("      AND bankid = @BankID");
                    sb.AppendLine("      AND country = @Country");
                    sb.AppendLine("      AND currency = @Currency");
                    sb.Append(String.Format("      AND {0}bankitem LIKE '{1}%'", ((itemPeriods[i, 1].Length > 0) ? "(" : String.Empty), itemPeriods[i, 0]));
                    if (itemPeriods[i, 1].Length > 0)
                    {
                        sb.Append(Environment.NewLine + String.Format("        AND {0}bankperiod = '{1}'", ((i == itemPeriods.GetLength(0) - 1) ? "(" : String.Empty), itemPeriods[i, 1]));

                        if (i == itemPeriods.GetLength(0) - 1)
                        {
                            sb.Append(String.Format(" OR bankperiod = '{0}'", "1年"));

                            sb.AppendLine(" )");
                        }

                        sb.AppendLine(" )");
                    }
                    else
                        sb.Append(Environment.NewLine);
                }

                sb.AppendLine("    IF (@Rate IS NOT NULL OR @Rate2 IS NOT NULL OR @Rate3 IS NOT NULL OR @Rate6 IS NOT NULL OR @Rate9 IS NOT NULL OR @Rate12 IS NOT NULL)");
                sb.AppendLine("      BEGIN");
                sb.AppendLine("        INSERT INTO @ResultTable1");
                sb.AppendLine("                    (BankID, BankName, Rate0, Rate1, Rate2, Rate3, Rate6, Rate9, Rate12)");
                sb.AppendLine("            VALUES  (@BankID, @BankName, @Rate, @Rate1, @Rate2, @Rate3, @Rate6, @Rate9, @Rate12)");
                sb.AppendLine("      END");
                sb.AppendLine("    FETCH NEXT FROM vend_cursor");
                sb.AppendLine("    INTO @BankID, @BankName");
                sb.AppendLine("  END");

                sb.AppendLine("CLOSE vend_cursor");
                sb.AppendLine("DEALLOCATE vend_cursor");

                sb.AppendLine("DECLARE @ResultTable2 TABLE ([Index] INT IDENTITY(1, 1),");
                sb.AppendLine("                             BankID VARCHAR(50),");
                sb.AppendLine("                             BankName VARCHAR(50),");
                sb.AppendLine("                             Rate0 DECIMAL(10, 6),");
                sb.AppendLine("                             Rate1 DECIMAL(10, 6),");
                sb.AppendLine("                             Rate2 DECIMAL(10, 6),");
                sb.AppendLine("                             Rate3 DECIMAL(10, 6),");
                sb.AppendLine("                             Rate6 DECIMAL(10, 6),");
                sb.AppendLine("                             Rate9 DECIMAL(10, 6),");
                sb.AppendLine("                             Rate12 DECIMAL(10, 6))");

                sb.AppendLine("INSERT INTO @ResultTable2");
                sb.AppendLine("    SELECT BankID, BankName, Rate0, Rate1, Rate2, Rate3, Rate6, Rate9, Rate12");
                sb.AppendLine("    FROM @ResultTable1");
                if (orderField != null && orderDirection != null)
                    sb.AppendLine(String.Format("    ORDER BY {0} {1}", orderField, orderDirection));

                sb.AppendLine("SELECT *");
                sb.AppendLine("FROM @ResultTable2");

                sb.AppendLine("--新版理財 by Alex _24_Bank.cs NewBankRate Get存款利率ByCountry");
                #endregion CommandText
                //if (("dongdong05|cnyes_Alexc").IndexOf(System.Web.HttpContext.Current.Server.UrlDecode(Globals.AicLogin(System.Web.HttpContext.Current.Request))) >= 0)
                //{
                //    WriteLog.Log1<string>("_24_Bank.txt", "NewBankRate Get存款利率ByCountry", sb.ToString());
                //}
                SqlParameter[] parameters = new SqlParameter[2];
                parameters[0] = new SqlParameter("@Country", SqlDbType.VarChar, 2);
                parameters[0].Value = countryCode;
                parameters[1] = new SqlParameter("@Currency", SqlDbType.VarChar, 3);
                parameters[1].Value = currency;

                GetDataReturnDataTable(Enums.euConnectionString._24_bank_ConnStr, sb.ToString(), parameters);
            }
            #endregion void Get存款利率ByCountry (string countryCode, string currency, Get存款利率ByCountry_Currency_ReturnTable2Fields? orderField, Enums.euOrderDirection? orderDirection)（依據國碼得到銀行資料）
        }
        #endregion class NewBankRate : DataProvider

        #region class Bank_NewBankRate : DataProvider
        public class Bank_NewBankRate : DataProvider
        {
            #region GetAllBanks_ReturnTableFields : byte
            public enum GetAllBanks_ReturnTableFields : byte
            {
                BCode,
                BName,
                BankType
            }
            #endregion GetAllBanks_ReturnTableFields : byte

            #region void GetAllBanks ()（所有銀行、郵局）
            /// <summary>
            /// 得到 所有銀行、郵局
            /// </summary>
            public void GetAllBanks ()
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();

                #region CommandText
                sb.AppendLine("SELECT DISTINCT BCode, BName, BankType");
                sb.AppendLine("FROM Bank_Rate where BName !='大眾銀行' ");

                sb.AppendLine("SELECT DISTINCT bankid AS BCode, bankname AS BName, null AS BankType ");
                sb.AppendLine("FROM newBank_rate");
                sb.AppendLine("WHERE 1=1");
                sb.AppendLine("  AND country = 'HK'");

                sb.AppendLine("SELECT DISTINCT bankid AS BCode, bankname AS BName, null AS BankType");
                sb.AppendLine("FROM newBank_rate");
                sb.AppendLine("WHERE 1=1");
                sb.AppendLine("  AND country = 'CN'  ");

                sb.AppendLine("--新版理財 by Alex _24_Bank.cs Bank_NewBankRate GetAllBanks");
                #endregion CommandText

                GetDataReturnDataSet(Enums.euConnectionString._24_bank_ConnStr, sb.ToString(), null);
            }
            #endregion void GetAllBanks ()（所有銀行、郵局）
        }
        #endregion class Bank_NewBankRate : DataProvider

        #region class Bank_all : DataProvider
        public class Bank_all : DataProvider
        {
            #region GetAllBanksCodeNameCash_ReturnTable1Fields : byte
            public enum GetAllBanksCodeNameCash_ReturnTable1Fields : byte
            {
                BCode,
                BName
            }
            #endregion GetAllBanksCodeNameCash_ReturnTable1Fields : byte

            #region GetAllBanksCodeNameCash_ReturnTable2Fields : byte
            public enum GetAllBanksCodeNameCash_ReturnTable2Fields : byte
            {
                Cash
            }
            #endregion GetAllBanksCodeNameCash_ReturnTable2Fields : byte

            #region GetByBCodeCash_ReturnTableFields : byte
            public enum GetByBCodeCash_ReturnTableFields : byte
            {
                jbid,
                Bank,
                DateTime,
                Cash,
                B,
                S,
                B1,
                S1,
                bcode
            }
            #endregion GetByBCodeCash_ReturnTableFields : byte

            #region Get銀行換匯對照表_ReturnTableFields : byte
            public enum Get銀行換匯對照表_ReturnTableFields : byte
            {
                BCode,
                BankName,
                Rate_USD_B,
                Rate_USD_S,
                Rate_EUR_B,
                Rate_EUR_S,
                Rate_JPY_B,
                Rate_JPY_S,
                Rate_HKD_B,
                Rate_HKD_S,
                Rate_CNY_B,
                Rate_CNY_S,
                Rate_AUD_B,
                Rate_AUD_S
            }
            #endregion Get銀行換匯對照表_ReturnTableFields : byte

            #region euGet銀行換匯對照表_GetType : byte
            public enum euGet銀行換匯對照表_GetType : byte
            {
                現金,
                即期
            }
            #endregion euGet銀行換匯對照表_GetType : byte

            #region void GetAllBanksCodeNameCash (string bcode, string currency)（所有銀行、幣別）
            /// <summary>
            /// 得到 所有銀行、幣別
            /// </summary>
            public void GetAllBanksCodeNameCash (string bcode, string currency)
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder(); 

                #region CommandText
                sb.AppendLine("SELECT DISTINCT BCode, Bank AS BName");
                sb.AppendLine("FROM bank_all");

                sb.AppendLine("SELECT DISTINCT Cash");
                sb.AppendLine("FROM bank_all");

                if (bcode != null && bcode.Length > 0)
                    sb.AppendLine("SELECT [DateTime] FROM bank_all WHERE BCode = @Key");
                if (currency != null && currency.Length > 0)
                    sb.AppendLine("SELECT TOP 1 [DateTime] FROM bank_all WHERE Cash = @Key");

                sb.AppendLine("--新版理財 by Alex _24_Bank.cs Bank_all GetAllBanksCodeNameCash");
                #endregion CommandText

                SqlParameter[] parameters = null;

                if ((bcode != null && bcode.Length > 0) || (currency != null && currency.Length > 0))
                {
                    parameters = new SqlParameter[1];

                    parameters[0] = new SqlParameter("@Key", SqlDbType.VarChar, 10);
                    parameters[0].Value = ((bcode != null && bcode.Length > 0) ? bcode : currency);
                }

                GetDataReturnDataSet(Enums.euConnectionString._24_bank_ConnStr, sb.ToString(), parameters);
            }
            #endregion void GetAllBanksCodeNameCash (string bcode, string currency)（所有銀行、幣別）

            #region void GetByBCodeCash (string bcode, string currency, GetByBCodeCash_ReturnTableFields? orderField, Enums.euOrderDirection? orderDirection)（依銀行代碼或幣別得到資料）
            /// <summary>
            /// 依銀行代碼或幣別得到資料
            /// </summary>
            public void GetByBCodeCash (string bcode, string currency, GetByBCodeCash_ReturnTableFields? orderField, Enums.euOrderDirection? orderDirection)
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                //WriteLog.Log<string>("bcode", bcode); WriteLog.Log<string>("currency", currency); WriteLog.Log<string>("orderField", orderField.ToString()); WriteLog.Log<string>("orderDirection", orderDirection.ToString());
                #region CommandText
                sb.AppendLine(String.Format("SELECT * FROM bank_all WHERE {0} = @Key", ((bcode != null && bcode.Length > 0) ? GetByBCodeCash_ReturnTableFields.bcode : GetByBCodeCash_ReturnTableFields.Cash)));
                if (orderDirection != null && orderField != null)
                    sb.AppendLine(String.Format("ORDER BY [{0}] {1}", orderField, orderDirection));

                sb.AppendLine("--新版理財 by Alex _24_Bank.cs Bank_all GetByBCodeCash");
                #endregion CommandText

                SqlParameter[] parameters = new SqlParameter[1];

                parameters[0] = new SqlParameter("@Key", SqlDbType.VarChar, 10);
                parameters[0].Value = ((bcode != null && bcode.Length > 0) ? bcode : currency);

                GetDataReturnDataTable(Enums.euConnectionString._24_bank_ConnStr, sb.ToString(), parameters);
            }
            #endregion void GetByBCodeCash (string bcode, string currency, GetByBCodeCash_ReturnTableFields? orderField, Enums.euOrderDirection? orderDirection)（依銀行代碼或幣別得到資料）

            #region void Get銀行換匯對照表 (euGet銀行換匯對照表_GetType getType, Get銀行換匯對照表_ReturnTableFields? orderField, Enums.euOrderDirection? orderDirection)（Get銀行換匯對照表）
            /// <summary>
            /// Get銀行換匯對照表
            /// </summary>
            public void Get銀行換匯對照表 (euGet銀行換匯對照表_GetType getType, Get銀行換匯對照表_ReturnTableFields? orderField, Enums.euOrderDirection? orderDirection)
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                //WriteLog.Log<string>("bcode", bcode); WriteLog.Log<string>("currency", currency); WriteLog.Log<string>("orderField", orderField.ToString()); WriteLog.Log<string>("orderDirection", orderDirection.ToString());
                #region CommandText
                Enums.euCurrency[] currencys = 
                    {
                        Enums.euCurrency.USD,
                        Enums.euCurrency.EUR,
                        Enums.euCurrency.JPY,
                        Enums.euCurrency.HKD,
                        Enums.euCurrency.CNY,
                        Enums.euCurrency.AUD
                    };

                sb.AppendLine("DECLARE @ResultTable1 TABLE (BCode VARCHAR(50),");
                sb.AppendLine("                             BankName VARCHAR(50),");
                #region //Mark
                //sb.AppendLine("                             Rate_USD_B DECIMAL(10, 6),");
                //sb.AppendLine("                             Rate_USD_S DECIMAL(10, 6),");
                //sb.AppendLine("                             Rate_EUR_B DECIMAL(10, 6),");
                //sb.AppendLine("                             Rate_EUR_S DECIMAL(10, 6),");
                //sb.AppendLine("                             Rate_JPY_B DECIMAL(10, 6),");
                //sb.AppendLine("                             Rate_JPY_S DECIMAL(10, 6),");
                //sb.AppendLine("                             Rate_HKD_B DECIMAL(10, 6),");
                //sb.AppendLine("                             Rate_HKD_S DECIMAL(10, 6),");
                //sb.AppendLine("                             Rate_CNY_B DECIMAL(10, 6),");
                //sb.AppendLine("                             Rate_CNY_S DECIMAL(10, 6),");
                //sb.AppendLine("                             Rate_AUD_B DECIMAL(10, 6),");
                //sb.AppendLine("                             Rate_AUD_S DECIMAL(10, 6))");
                #endregion //Mark
                foreach (Enums.euCurrency c in currencys)
                {
                    sb.AppendLine(String.Format("                             Rate_{0}_B REAL,", c));
                    sb.AppendLine(String.Format("                             Rate_{0}_S REAL{1}", c, ((c == Enums.euCurrency.AUD) ? ")" : ",")));
                }

                sb.AppendLine("DECLARE @BCode VARCHAR(50)");
                sb.AppendLine("DECLARE @BankName VARCHAR(50)");
                #region //Mark
                //sb.AppendLine("DECLARE @Rate_USD_B DECIMAL(10, 6)");
                //sb.AppendLine("DECLARE @Rate_USD_S DECIMAL(10, 6)");
                //sb.AppendLine("DECLARE @Rate_EUR_B DECIMAL(10, 6)");
                //sb.AppendLine("DECLARE @Rate_EUR_S DECIMAL(10, 6)");
                //sb.AppendLine("DECLARE @Rate_JPY_B DECIMAL(10, 6)");
                //sb.AppendLine("DECLARE @Rate_JPY_S DECIMAL(10, 6)");
                //sb.AppendLine("DECLARE @Rate_HKD_B DECIMAL(10, 6)");
                //sb.AppendLine("DECLARE @Rate_HKD_S DECIMAL(10, 6)");
                //sb.AppendLine("DECLARE @Rate_CNY_B DECIMAL(10, 6)");
                //sb.AppendLine("DECLARE @Rate_CNY_S DECIMAL(10, 6)");
                //sb.AppendLine("DECLARE @Rate_AUD_B DECIMAL(10, 6)");
                //sb.AppendLine("DECLARE @Rate_AUD_S DECIMAL(10, 6)");
                #endregion //Mark
                foreach (Enums.euCurrency c in currencys)
                {
                    sb.AppendLine(String.Format("DECLARE @Rate_{0}_B REAL", c));
                    sb.AppendLine(String.Format("DECLARE @Rate_{0}_S REAL", c));
                }

                sb.AppendLine("DECLARE vend_cursor CURSOR FOR");
                sb.AppendLine("    SELECT DISTINCT bcode, bank");
                sb.AppendLine("    FROM bank_all");
                sb.AppendLine("    WHERE 1=1");
                sb.AppendLine("OPEN vend_cursor");
                sb.AppendLine("FETCH NEXT FROM vend_cursor");
                sb.AppendLine("INTO @BCode, @BankName");

                sb.AppendLine("WHILE (@@FETCH_STATUS = 0)");
                sb.AppendLine("  BEGIN");
                #region //Mark
                //sb.AppendLine("    /*美元-現金買進*/");
                //sb.AppendLine("    SELECT @Rate_USD_B = B");
                //sb.AppendLine("    FROM bank_all");
                //sb.AppendLine("    WHERE bcode = @BCode");
                //sb.AppendLine("      AND Cash = 'USD'");
                //sb.AppendLine("    /*美元-現金買出*/");
                //sb.AppendLine("    SELECT @Rate_USD_S = S");
                //sb.AppendLine("    FROM bank_all");
                //sb.AppendLine("    WHERE bcode = @BCode");
                //sb.AppendLine("      AND Cash = 'USD'");
                //sb.AppendLine("    /*歐元-現金買進*/");
                //sb.AppendLine("    SELECT @Rate_EUR_B = B");
                //sb.AppendLine("    FROM bank_all");
                //sb.AppendLine("    WHERE bcode = @BCode");
                //sb.AppendLine("      AND Cash = 'EUR'");
                //sb.AppendLine("    /*歐元-現金買出*/");
                //sb.AppendLine("    SELECT @Rate_EUR_S = S");
                //sb.AppendLine("    FROM bank_all");
                //sb.AppendLine("    WHERE bcode = @BCode");
                //sb.AppendLine("      AND Cash = 'EUR'");
                //sb.AppendLine("    /*日元-現金買進*/");
                //sb.AppendLine("    SELECT @Rate_JPY_B = B");
                //sb.AppendLine("    FROM bank_all");
                //sb.AppendLine("    WHERE bcode = @BCode");
                //sb.AppendLine("      AND Cash = 'JPY'");
                //sb.AppendLine("    /*日元-現金買出*/");
                //sb.AppendLine("    SELECT @Rate_JPY_S = S");
                //sb.AppendLine("    FROM bank_all");
                //sb.AppendLine("    WHERE bcode = @BCode");
                //sb.AppendLine("      AND Cash = 'JPY'");
                //sb.AppendLine("    /*港幣-現金買進*/");
                //sb.AppendLine("    SELECT @Rate_HKD_B = B");
                //sb.AppendLine("    FROM bank_all");
                //sb.AppendLine("    WHERE bcode = @BCode");
                //sb.AppendLine("      AND Cash = 'HKD'");
                //sb.AppendLine("    /*港幣-現金買出*/");
                //sb.AppendLine("    SELECT @Rate_HKD_S = S");
                //sb.AppendLine("    FROM bank_all");
                //sb.AppendLine("    WHERE bcode = @BCode");
                //sb.AppendLine("      AND Cash = 'HKD'");
                //sb.AppendLine("    /*人民幣-現金買進*/");
                //sb.AppendLine("    SELECT @Rate_CNY_B = B");
                //sb.AppendLine("    FROM bank_all");
                //sb.AppendLine("    WHERE bcode = @BCode");
                //sb.AppendLine("      AND Cash = 'CNY'");
                //sb.AppendLine("    /*人民幣-現金買出*/");
                //sb.AppendLine("    SELECT @Rate_CNY_S = S");
                //sb.AppendLine("    FROM bank_all");
                //sb.AppendLine("    WHERE bcode = @BCode");
                //sb.AppendLine("      AND Cash = 'CNY'");
                //sb.AppendLine("    /*澳幣-現金買進*/");
                //sb.AppendLine("    SELECT @Rate_AUD_B = B");
                //sb.AppendLine("    FROM bank_all");
                //sb.AppendLine("    WHERE bcode = @BCode");
                //sb.AppendLine("      AND Cash = 'AUD'");
                //sb.AppendLine("    /*澳幣-現金買出*/");
                //sb.AppendLine("    SELECT @Rate_AUD_S = S");
                //sb.AppendLine("    FROM bank_all");
                //sb.AppendLine("    WHERE bcode = @BCode");
                //sb.AppendLine("      AND Cash = 'AUD'");
                #endregion //Mark
                foreach (Enums.euCurrency c in currencys)
                {
                    sb.AppendLine(String.Format("    /*{0}-{1}買進*/", Enums.GetEnumDesc(c), getType));
                    sb.AppendLine(String.Format("    SELECT @Rate_{0}_B = B{1}", c, ((getType == euGet銀行換匯對照表_GetType.即期) ? "1" : String.Empty)));
                    sb.AppendLine("    FROM bank_all");
                    sb.AppendLine("    WHERE bcode = @BCode");
                    sb.AppendLine(String.Format("      AND Cash = '{0}'", c));
                    sb.AppendLine(String.Format("    /*{0}-{1}賣出*/", Enums.GetEnumDesc(c), getType));
                    sb.AppendLine(String.Format("    SELECT @Rate_{0}_S = S{1}", c, ((getType == euGet銀行換匯對照表_GetType.即期) ? "1" : String.Empty)));
                    sb.AppendLine("    FROM bank_all");
                    sb.AppendLine("    WHERE bcode = @BCode");
                    sb.AppendLine(String.Format("      AND Cash = '{0}'", c));
                }

                sb.AppendLine("    INSERT INTO @ResultTable1");
                #region //Mark
                //sb.AppendLine("                (BCode, BankName, Rate_USD_B, Rate_USD_S, Rate_EUR_B, Rate_EUR_S, Rate_JPY_B, Rate_JPY_S, Rate_HKD_B, Rate_HKD_S, Rate_CNY_B, Rate_CNY_S, Rate_AUD_B, Rate_AUD_S)");
                //sb.AppendLine("        VALUES  (@BCode, @BankName, @Rate_USD_B, @Rate_USD_S, @Rate_EUR_B, @Rate_EUR_S, @Rate_JPY_B, @Rate_JPY_S, @Rate_HKD_B, @Rate_HKD_S, @Rate_CNY_B, @Rate_CNY_S, @Rate_AUD_B, @Rate_AUD_S)");
                #endregion //Mark
                sb.AppendLine(String.Format("                (BCode, BankName, Rate_{0}_B, Rate_{0}_S, Rate_{1}_B, Rate_{1}_S, Rate_{2}_B, Rate_{2}_S, Rate_{3}_B, Rate_{3}_S, Rate_{4}_B, Rate_{4}_S, Rate_{5}_B, Rate_{5}_S)", currencys[0], currencys[1], currencys[2], currencys[3], currencys[4], currencys[5]));
                sb.AppendLine(String.Format("        VALUES  (@BCode, @BankName, @Rate_{0}_B, @Rate_{0}_S, @Rate_{1}_B, @Rate_{1}_S, @Rate_{2}_B, @Rate_{2}_S, @Rate_{3}_B, @Rate_{3}_S, @Rate_{4}_B, @Rate_{4}_S, @Rate_{5}_B, @Rate_{5}_S)", currencys[0], currencys[1], currencys[2], currencys[3], currencys[4], currencys[5]));

                foreach (Enums.euCurrency c in currencys)
                {
                    sb.AppendLine(String.Format("    SET @Rate_{0}_B = NULL", c));
                    sb.AppendLine(String.Format("    SET @Rate_{0}_S = NULL", c));
                }

                sb.AppendLine("    FETCH NEXT FROM vend_cursor");
                sb.AppendLine("    INTO @BCode, @BankName");
                sb.AppendLine("  END");
                sb.AppendLine("CLOSE vend_cursor");
                sb.AppendLine("DEALLOCATE vend_cursor");

                sb.AppendLine("SELECT *");
                sb.AppendLine("FROM @ResultTable1");
                if (orderField != null && orderDirection != null)
                    sb.AppendLine(String.Format("ORDER BY {0} {1}", orderField, orderDirection));

                sb.AppendLine("--新版理財 by Alex _24_Bank.cs Bank_all Get銀行換匯對照表");
                #endregion CommandText
                //if (("dongdong05|cnyes_Alexc").IndexOf(System.Web.HttpContext.Current.Server.UrlDecode(Globals.AicLogin(System.Web.HttpContext.Current.Request))) >= 0)
                //{
                //    WriteLog.Log1<string>("__d.txt", "CommandText", sb.ToString());
                //}
                GetDataReturnDataTable(Enums.euConnectionString._24_bank_ConnStr, sb.ToString(), null);
            }
            #endregion void Get銀行換匯對照表 (euGet銀行換匯對照表_GetType getType, Get銀行換匯對照表_ReturnTableFields? orderField, Enums.euOrderDirection? orderDirection)（Get銀行換匯對照表）
        }
        #endregion class Bank_all__Bank_Rate : DataProvider
    }
    #endregion 24
}
#endregion class DataProviderBase

