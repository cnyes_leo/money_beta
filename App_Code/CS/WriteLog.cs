﻿using System;
using System.Data;
using System.Configuration;
using System.IO;
using System.Text;

/// <summary>
/// WriteLog 的摘要描述
/// </summary>
public class WriteLog
{
    public WriteLog ()
    {
        //
        // TODO: 在此加入建構函式的程式碼
        //
    }

    #region void Log<T> (T msg)
    public static void Log<T> (T msg)
    {
        try
        {
            string filename = String.Format("{0:yyyyMMdd}.txt", DateTime.Today);

            using (FileStream fs = new FileStream(System.Web.HttpContext.Current.Server.MapPath("~/Log/" + filename), FileMode.Append, FileAccess.Write, FileShare.Write))
            {
                using (StreamWriter sw = new StreamWriter(fs, Encoding.UTF8))
                {
                    sw.WriteLine(String.Format("{0}", msg));
                }
            }
        }
        finally
        {
            //Response.End (); 
        }
    }
    #endregion void Log<T> (T msg)

    #region void Log<T> (T msg, bool isNewLine)
    public static void Log<T> (T msg, bool isNewLine)
    {
        Log<String>(msg.ToString() + Environment.NewLine);
    }
    #endregion void Log<T> (T msg, bool isNewLine)

    #region void Log<T> (string txt, T msg)
    public static void Log<T> (string txt, T msg)
    {
        if (msg != null)
        {
            Log<String>(txt + " = " + msg.ToString());
        }
        else
            Log<String>(txt + " = NULL！");
    }
    #endregion void Log<T> (string txt, T msg)

    #region void Log<T> (string txt, T msg, bool isNewLine)
    public static void Log<T> (string txt, T msg, bool isNewLine)
    {
        Log<String>(txt + " = " + msg.ToString() + Environment.NewLine);
    }
    #endregion void Log<T> (string txt, T msg, bool isNewLine)

    #region void Log (string txt, int msg)
    public static void Log (string txt, int msg)
    {
        Log<int>(txt, msg);
    }
    #endregion void Log (string txt, int msg)

    #region void Log (string txt, uint msg)
    public static void Log (string txt, uint msg)
    {
        Log<uint>(txt, msg);
    }
    #endregion void Log (string txt, uint msg)

    #region void Log (string txt, byte msg)
    public static void Log (string txt, byte msg)
    {
        Log<byte>(txt, msg);
    }
    #endregion void Log (string txt, byte msg)

    #region void Log (string txt, short msg)
    public static void Log (string txt, short msg)
    {
        Log<short>(txt, msg);
    }
    #endregion void Log (string txt, short msg)

    #region void Log (string txt, ushort msg)
    public static void Log (string txt, ushort msg)
    {
        Log<ushort>(txt, msg);
    }
    #endregion void Log (string txt, ushort msg)

    #region void Log (string txt, float msg)
    public static void Log (string txt, float msg)
    {
        Log<float>(txt, msg);
    }
    #endregion void Log (string txt, float msg)

    #region void Log (string txt, double msg)
    public static void Log (string txt, double msg)
    {
        Log<double>(txt, msg);
    }
    #endregion void Log (string txt, double msg)

    #region void Log (string txt, long msg)
    public static void Log (string txt, long msg)
    {
        Log<long>(txt, msg);
    }
    #endregion void Log (string txt, long msg)

    #region void Log (string txt, ulong msg)
    public static void Log (string txt, ulong msg)
    {
        Log<ulong>(txt, msg);
    }
    #endregion void Log (string txt, ulong msg)

    #region void Log (string txt, string msg)
    public static void Log (string txt, string msg)
    {
        Log<string>(txt, msg);
    }
    #endregion void Log (string txt, string msg)

    #region void Log (string txt, string[] msg)
    public static void Log (string txt, string[] msg)
    {
        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        for (int i = 0; i < msg.Length; i++)
            sb.AppendLine(String.Format("{0}[{1}] = {2}", txt, i, msg[i]));

        Log<string>(sb.ToString());
    }
    #endregion void Log (string txt, string[] msg)

    #region void Log (string txt, DateTime msg)
    public static void Log (string txt, DateTime msg)
    {
        Log<DateTime>(txt, msg);
    }
    #endregion void Log (string txt, DateTime msg)

    #region void Log (string txt, Exception msg)
    public static void Log (string txt, Exception msg)
    {
        Log<Exception>(txt, msg);
    }
    #endregion void Log (string txt, Exception msg)

    #region void Log (string txt, System.Data.SqlClient.SqlException msg)
    public static void Log (string txt, System.Data.SqlClient.SqlException msg)
    {
        Log<System.Data.SqlClient.SqlException>(txt, msg);
    }
    #endregion void Log (string txt, System.Data.SqlClient.SqlException msg)

    #region void Log (string txt, System.Text.StringBuilder msg)
    public static void Log (string txt, System.Text.StringBuilder msg)
    {
        Log<System.Text.StringBuilder>(txt, msg);
    }
    #endregion void Log (string txt, System.Text.StringBuilder msg)


    #region void Log1<T> (string filename, string msg)
    public static void Log1<T> (string filename, string msg)
    {
        try
        {
            using (FileStream fs = new FileStream(System.Web.HttpContext.Current.Server.MapPath("~/Log/" + filename), FileMode.Append, FileAccess.Write, FileShare.Write))
            {
                using (StreamWriter sw = new StreamWriter(fs, Encoding.UTF8))
                {
                    sw.WriteLine(String.Format("{0}", msg));
                }
            }
        }
        finally
        {
            //Response.End (); 
        }
    }
    #endregion void Log1<T> (string filename, string msg)

    #region void Log1<T> (string fileName, string txt, T msg)
    public static void Log1<T> (string fileName, string txt, T msg)
    {
        if (msg != null)
            Log1<T>(fileName, txt + " = " + msg.ToString());
        else
            Log1<T>(fileName, txt + " IS NULL ！");
    }
    #endregion void Log1<T> (string fileName, string txt, T msg)

    #region void Log1<T> (string fileName, string txt, T msg, bool isNewLine)
    public static void Log1<T> (string fileName, string txt, T msg, bool isNewLine)
    {
        Log1<String>(fileName, txt + " = " + msg.ToString() + Environment.NewLine);
    }
    #endregion void Log1<T> (string fileName, string txt, T msg, bool isNewLine)

    #region void Log1 (string fileName, string txt, int msg)
    public static void Log1 (string fileName, string txt, int msg)
    {
        Log1<int>(fileName, txt, msg);
    }
    #endregion void Log1 (string fileName, string txt, int msg)

    #region void Log1 (string fileName, string txt, uint msg)
    public static void Log1 (string fileName, string txt, uint msg)
    {
        Log1<uint>(fileName, txt, msg);
    }
    #endregion void Log1 (string fileName, string txt, uint msg)

    #region void Log1 (string fileName, string txt, byte msg)
    public static void Log1 (string fileName, string txt, byte msg)
    {
        Log1<byte>(fileName, txt, msg);
    }
    #endregion void Log1 (string fileName, string txt, byte msg)

    #region void Log1 (string fileName, string txt, short msg)
    public static void Log1 (string fileName, string txt, short msg)
    {
        Log1<short>(fileName, txt, msg);
    }
    #endregion void Log1 (string fileName, string txt, short msg)

    #region void Log1 (string fileName, string txt, ushort msg)
    public static void Log1 (string fileName, string txt, ushort msg)
    {
        Log1<ushort>(fileName, txt, msg);
    }
    #endregion void Log1 (string fileName, string txt, ushort msg)

    #region void Log1 (string fileName, string txt, float msg)
    public static void Log1 (string fileName, string txt, float msg)
    {
        Log1<float>(fileName, txt, msg);
    }
    #endregion void Log1 (string fileName, string txt, float msg)

    #region void Log1 (string fileName, string txt, double msg)
    public static void Log1 (string fileName, string txt, double msg)
    {
        Log1<double>(fileName, txt, msg);
    }
    #endregion void Log1 (string fileName, string txt, double msg)

    #region void Log1 (string fileName, string txt, long msg)
    public static void Log1 (string fileName, string txt, long msg)
    {
        Log1<long>(fileName, txt, msg);
    }
    #endregion void Log1 (string fileName, string txt, long msg)

    #region void Log1 (string fileName, string txt, ulong msg)
    public static void Log1 (string fileName, string txt, ulong msg)
    {
        Log1<ulong>(fileName, txt, msg);
    }
    #endregion void Log1 (string fileName, string txt, ulong msg)

    #region void Log1 (string fileName, string txt, string msg)
    public static void Log1 (string fileName, string txt, string msg)
    {
        Log1<string>(fileName, txt, msg);
    }
    #endregion void Log1 (string fileName, string txt, string msg)

    #region void Log1 (string fileName, string txt, string[] msg)
    public static void Log1 (string fileName, string txt, string[] msg)
    {
        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        for (int i = 0; i < msg.Length; i++)
            sb.AppendLine(String.Format("{0}[{1}] = {2}", txt, i, msg[i]));

        Log1<string>(fileName, sb.ToString());
    }
    #endregion void Log1 (string fileName, string txt, string[] msg)

    #region void Log1 (string fileName, string txt, DateTime msg)
    public static void Log1 (string fileName, string txt, DateTime msg)
    {
        Log1<DateTime>(fileName, txt, msg);
    }
    #endregion void Log1<T> (string fileName, string txt, DateTime msg)

    #region void Log1 (string fileName, string txt, Exception msg)
    public static void Log1 (string fileName, string txt, Exception msg)
    {
        Log1<Exception>(fileName, txt, msg);
    }
    #endregion void Log1<T> (string fileName, string txt, Exception msg)

    #region void Log1 (string fileName, string txt, System.Data.SqlClient.SqlException msg)
    public static void Log1 (string fileName, string txt, System.Data.SqlClient.SqlException msg)
    {
        Log1<System.Data.SqlClient.SqlException>(fileName, txt, msg);
    }
    #endregion void Log1<T> (string fileName, string txt, System.Data.SqlClient.SqlException msg)

    #region void Log1 (string fileName, string txt, System.Text.StringBuilder msg)
    public static void Log1 (string fileName, string txt, System.Text.StringBuilder msg)
    {
        Log1<System.Text.StringBuilder>(fileName, txt, msg);
    }
    #endregion void Log1<T> (string fileName, string txt, System.Text.StringBuilder msg)

    #region LogDataView (ref DataView dv)
    public static void LogDataView (ref DataView dv)
    {
        if (dv != null)
        {
            DataTable dt = dv.ToTable();
            LogDataTable(ref dt);
        }
        else
            Log<String>("[LogDataView (ref DataView dv)] dv is NULL！" + Environment.NewLine);
    }
    #endregion LogDataView (ref DataView dv)

    #region LogDataView (String filename, ref DataView dv)
    public static void LogDataView (String filename, ref DataView dv)
    {
        if (dv != null)
        {
            DataTable dt = dv.ToTable();
            LogDataTable(filename, ref dt);
        }
        else
            Log1<String>(filename, "[LogDataView (ref DataView dv)] dv is NULL！" + Environment.NewLine);
    }
    #endregion LogDataView (String filename, ref DataView dv)

    #region LogDataTable(ref DataTable dt)
    public static void LogDataTable (ref DataTable dt)
    {
        LogDataTable(ref dt, -1);
    }
    #endregion LogDataTable(ref DataTable dt)

    #region LogDataTable(ref DataTable dt, int logCount)
    public static void LogDataTable (ref DataTable dt, int logCount)
    {
        if (dt != null)
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (logCount != -1 && i == logCount)
                        break;

                    for (int k = 0; k < dt.Columns.Count; k++)
                    {
                        string columnName = dt.Columns[k].ColumnName;
                        sb.AppendLine(String.Format("{0} = {1}", columnName, dt.Rows[i][columnName].ToString()));
                    }
                    sb.AppendLine(String.Empty);
                }
            }
            else
            {
                for (int k = 0; k < dt.Columns.Count; k++)
                {
                    string columnName = dt.Columns[k].ColumnName;
                    sb.AppendLine(String.Format("dt.Columns[{0}] = {1}", k, columnName));
                    sb.AppendLine(String.Empty);
                }
            }

            Log<String>(sb.ToString());
        }
        else
            Log<String>("[LogDataTable (ref DataTable dt, int logCount)] dt is NULL！" + Environment.NewLine);
    }
    #endregion LogDataTable(ref DataTable dt, int logCount)

    #region LogDataSet(ref DataSet ds)
    public static void LogDataSet (ref DataSet ds)
    {
        if (ds != null)
        {
            for (int i = 0; i < ds.Tables.Count; i++)
            {
                Log<String>(String.Format("Table：{0}", i + 1));
                DataTable dt = ds.Tables[i];
                LogDataTable(ref dt);
            }
        }
        else
            Log<String>("[LogDataSet (ref DataSet ds)] ds is NULL！" + Environment.NewLine);
    }
    #endregion LogDataSet(ref DataSet ds)

    #region LogDataSet(ref DataSet ds, int logCount)
    public static void LogDataSet (ref DataSet ds, int logCount)
    {
        if (ds != null)
        {
            for (int i = 0; i < ds.Tables.Count; i++)
            {
                Log<String>(String.Format("Table：{0}", i + 1));
                DataTable dt = ds.Tables[i];
                LogDataTable(ref dt, logCount);
            }
        }
        else
            Log<String>("[LogDataSet (ref DataSet ds, int logCount)] ds is NULL！" + Environment.NewLine);
    }
    #endregion LogDataSet(ref DataSet ds, int logCount)

    #region LogDataView (DataView dv)
    public static void LogDataView (DataView dv)
    {
        if (dv != null)
            LogDataTable(dv.ToTable());
        else
            Log<String>("[LogDataView (DataView dv)] dv is NULL！" + Environment.NewLine);
    }
    #endregion LogDataView (DataView dv)

    #region LogDataTable(DataTable dt)
    public static void LogDataTable (DataTable dt)
    {
        LogDataTable(dt, -1);
    }
    #endregion LogDataTable(DataTable dt)

    #region LogDataTable(DataTable dt, int logCount)
    public static void LogDataTable (DataTable dt, int logCount)
    {
        if (dt != null)
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();

            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (logCount != -1 && i == logCount)
                        break;

                    for (int k = 0; k < dt.Columns.Count; k++)
                    {
                        string columnName = dt.Columns[k].ColumnName;
                        sb.AppendLine(String.Format("{0} = {1}", columnName, dt.Rows[i][columnName]));
                    }
                    sb.AppendLine(String.Empty);
                }
            }
            else
            {
                for (int k = 0; k < dt.Columns.Count; k++)
                {
                    string columnName = dt.Columns[k].ColumnName;
                    sb.AppendLine(String.Format("dt.Columns[{0}] = {1}", k, columnName));
                    sb.AppendLine(String.Empty);
                }
            }

            Log<String>(sb.ToString());
        }
        else
            Log<String>("[LogDataTable (DataTable dt, int logCount)] dt is NULL！" + Environment.NewLine);
    }
    #endregion LogDataTable(DataTable dt, int logCount)

    #region LogDataSet(DataSet ds)
    public static void LogDataSet (DataSet ds)
    {
        if (ds != null)
        {
            for (int i = 0; i < ds.Tables.Count; i++)
            {
                Log<String>(String.Format("Table：{0}", i + 1));

                LogDataTable(ds.Tables[i]);
            }
        }
        else
            Log<String>("[LogDataSet (DataSet ds)] ds is NULL！" + Environment.NewLine);
    }
    #endregion LogDataSet(DataSet ds)

    #region LogDataSet(DataSet ds, int logCount)
    public static void LogDataSet (DataSet ds, int logCount)
    {
        if (ds != null)
        {
            for (int i = 0; i < ds.Tables.Count; i++)
            {
                Log<String>(String.Format("Table：{0}", i + 1));

                LogDataTable(ds.Tables[i], logCount);
            }
        }
        else
            Log<String>("[LogDataSet (DataSet ds, int logCount)] ds is NULL！" + Environment.NewLine);
    }
    #endregion LogDataSet(DataSet ds, int logCount)



    #region LogDataTable(String filename, ref DataTable dt)
    public static void LogDataTable (String filename, ref DataTable dt)
    {
        LogDataTable(filename, ref dt, -1);
    }
    #endregion LogDataTable(String filename, ref DataTable dt)

    #region LogDataTable(String filename, ref DataTable dt, int logCount)
    public static void LogDataTable (String filename, ref DataTable dt, int logCount)
    {
        if (dt != null)
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (logCount != -1 && i == logCount)
                        break;

                    for (int k = 0; k < dt.Columns.Count; k++)
                    {
                        string columnName = dt.Columns[k].ColumnName;
                        sb.AppendLine(String.Format("{0} = {1}", columnName, dt.Rows[i][columnName].ToString()));
                    }
                    sb.AppendLine(String.Empty);
                }
            }
            else
            {
                for (int k = 0; k < dt.Columns.Count; k++)
                {
                    string columnName = dt.Columns[k].ColumnName;
                    sb.AppendLine(String.Format("dt.Columns[{0}] = {1}", k, columnName));
                    sb.AppendLine(String.Empty);
                }
            }

            Log1<String>(filename, sb.ToString());
        }
        else
            Log1<String>(filename, "[LogDataTable (ref DataTable dt, int logCount)] dt is NULL！" + Environment.NewLine);
    }
    #endregion LogDataTable(String filename, ref DataTable dt, int logCount)

    #region LogDataSet(String filename, ref DataSet ds)
    public static void LogDataSet (String filename, ref DataSet ds)
    {
        if (ds != null)
        {
            for (int i = 0; i < ds.Tables.Count; i++)
            {
                Log1<String>(filename, String.Format("Table：{0}", i + 1));
                DataTable dt = ds.Tables[i];
                LogDataTable(filename, ref dt);
            }
        }
        else
            Log1<String>(filename, "[LogDataSet (ref DataSet ds)] ds is NULL！" + Environment.NewLine);
    }
    #endregion LogDataSet(String filename, ref DataSet ds)

    #region LogDataSet(String filename, ref DataSet ds, int logCount)
    public static void LogDataSet (String filename, ref DataSet ds, int logCount)
    {
        if (ds != null)
        {
            for (int i = 0; i < ds.Tables.Count; i++)
            {
                Log1<String>(filename, String.Format("Table：{0}", i + 1));
                DataTable dt = ds.Tables[i];
                LogDataTable(filename, ref dt, logCount);
            }
        }
        else
            Log1<String>(filename, "[LogDataSet (ref DataSet ds, int logCount)] ds is NULL！" + Environment.NewLine);
    }
    #endregion LogDataSet(String filename, ref DataSet ds, int logCount)

    #region LogDataView (String filename, DataView dv)
    public static void LogDataView (String filename, DataView dv)
    {
        if (dv != null)
            LogDataTable(filename, dv.ToTable());
        else
            Log1<String>(filename, "[LogDataView (DataView dv)] dv is NULL！" + Environment.NewLine);
    }
    #endregion LogDataView (String filename, DataView dv)

    #region LogDataTable(String filename, DataTable dt)
    public static void LogDataTable (String filename, DataTable dt)
    {
        LogDataTable(filename, dt, -1);
    }
    #endregion LogDataTable(String filename, DataTable dt)

    #region LogDataTable(String filename, DataTable dt, int logCount)
    public static void LogDataTable (String filename, DataTable dt, int logCount)
    {
        if (dt != null)
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();

            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (logCount != -1 && i == logCount)
                        break;

                    for (int k = 0; k < dt.Columns.Count; k++)
                    {
                        string columnName = dt.Columns[k].ColumnName;
                        sb.AppendLine(String.Format("{0} = {1}", columnName, dt.Rows[i][columnName]));
                    }
                    sb.AppendLine(String.Empty);
                }
            }
            else
            {
                for (int k = 0; k < dt.Columns.Count; k++)
                {
                    string columnName = dt.Columns[k].ColumnName;
                    sb.AppendLine(String.Format("dt.Columns[{0}] = {1}", k, columnName));
                    sb.AppendLine(String.Empty);
                }
            }

            Log1<String>(filename, sb.ToString());
        }
        else
            Log1<String>(filename, "[LogDataTable (DataTable dt, int logCount)] dt is NULL！" + Environment.NewLine);
    }
    #endregion LogDataTable(String filename, DataTable dt, int logCount)

    #region LogDataSet(String filename, DataSet ds)
    public static void LogDataSet (String filename, DataSet ds)
    {
        if (ds != null)
        {
            for (int i = 0; i < ds.Tables.Count; i++)
            {
                Log1<String>(filename, String.Format("Table：{0}", i + 1));

                LogDataTable(filename, ds.Tables[i]);
            }
        }
        else
            Log1<String>(filename, "[LogDataSet (DataSet ds)] ds is NULL！" + Environment.NewLine);
    }
    #endregion LogDataSet(String filename, DataSet ds)

    #region LogDataSet(String filename, DataSet ds, int logCount)
    public static void LogDataSet (String filename, DataSet ds, int logCount)
    {
        if (ds != null)
        {
            for (int i = 0; i < ds.Tables.Count; i++)
            {
                Log1<String>(filename, String.Format("Table：{0}", i + 1));

                LogDataTable(filename, ds.Tables[i], logCount);
            }
        }
        else
            Log1<String>(filename, "[LogDataSet (DataSet ds, int logCount)] ds is NULL！" + Environment.NewLine);
    }
    #endregion LogDataSet(String filename, DataSet ds, int logCount)
}