﻿using System;
using System.Data;
using System.Configuration;
using System.Xml;
using System.Text.RegularExpressions;

/// <summary>
/// UrlMapping 的摘要描述
/// </summary>
public class UrlMapping : IDisposable
{
    #region 變數
    private string _RuleFile = "Rule.xml";

    private string _FakeUrl = String.Empty;

    private string _RightUrl = String.Empty;

    private bool _Disposed;
    #endregion 變數

    #region 屬性

    #region string RuleFile
    public string RuleFile
    {
        set
        {
            _RuleFile = value;
        }
    }
    #endregion string RuleFile

    #region string FakeUrl
    public string FakeUrl
    {
        set
        {
            _FakeUrl = value;

            GetUrl();
        }
    }
    #endregion FakeUrl SourceUrl

    #region string RightUrl
    public string RightUrl
    {
        get
        {
            return _RightUrl;
        }
    }
    #endregion string RightUrl

    #endregion 屬性

    #region 建構子
    public UrlMapping()
	{
		//
		// TODO: 在此加入建構函式的程式碼
		//
	}

    public UrlMapping (string fakeUrl, string ruleFile)
    {
        _RuleFile = ruleFile;
        _FakeUrl = fakeUrl;

        GetUrl();
    }
    #endregion 建構子

    #region 解構子
    ~UrlMapping ()
    {
        Dispose(false);
    }
    #endregion 解構子

    #region Dispose()
    void IDisposable.Dispose()
    {
        Dispose(true);
        GC.SuppressFinalize(this);
    }
    #endregion Dispose()

    #region Dispose(bool isDisposing)
    protected virtual void Dispose(bool isDisposing)
    {
        if (!_Disposed)
        {
            _Disposed = true;
        }
    }
    #endregion Dispose(bool isDisposing)

    #region 方法

    #region Close()
    public void Close()
    {
        ((IDisposable)this).Dispose();
    }
    #endregion Close()

    #region void GetUrl ()
    private void GetUrl ()
    {
        XmlDocument xd = new XmlDocument();
        xd.Load(_RuleFile);

        XmlNodeList lst = xd.GetElementsByTagName("RewriterRule");
        //WriteLog.Log<string>("_FakeUrl", _FakeUrl);

        foreach (XmlNode nd in lst)
        {
            foreach (XmlNode chk in nd.ChildNodes[0].ChildNodes)
            {
                string rule = chk.InnerText;
                if (rule[0] != '^')
                    rule = "^" + rule;
                if (rule[rule.Length - 1] != '$')
                    rule = rule + "$";

                //WriteLog.Log<string>("rule", rule);

                Regex reg = new Regex(rule, RegexOptions.IgnoreCase);
                if (reg.IsMatch(_FakeUrl))
                {
                    _RightUrl = reg.Replace(_FakeUrl, nd.ChildNodes[1].InnerText);
                    break;
                }
            }

            if (_RightUrl != null && _RightUrl.Length > 0)
            {
                while (_RightUrl[_RightUrl.Length - 1] == '&')
                    _RightUrl = _RightUrl.Substring(0, _RightUrl.Length - 1);

                break;
            }
        }
    }
    #endregion void GetUrl ()

    #endregion 方法
}
