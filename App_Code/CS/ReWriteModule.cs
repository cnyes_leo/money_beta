﻿using System;
using System.Web;

/// <summary>
/// ReWriteModule 的摘要描述
/// </summary>
public class ReWriteModule : IHttpModule
{
    public ReWriteModule ()
    {
    }

    public override string ToString ()
    {
        return this.GetType().ToString();
    }

    void IHttpModule.Dispose ()
    {
    }

    void IHttpModule.Init (HttpApplication context)
    {
        context.BeginRequest += delegate(object sender, EventArgs e)
        {
            System.Web.HttpContext httpContext = context.Context;

            if (httpContext.Request.RawUrl.Equals(String.Format("{0}{1}", httpContext.Request.Path.Substring(0, httpContext.Request.Path.LastIndexOf('/') + 1), Enums.GetEnumDesc(Enums.euNavigationCategory.銀行服務)), StringComparison.InvariantCultureIgnoreCase))
            {
                if (System.IO.File.Exists(httpContext.Server.MapPath(Enums.GetEnumDesc(Enums.euBankServiceNavigations.牌告利率))))
                {
                    httpContext.Server.Transfer(Enums.GetEnumDesc(Enums.euBankServiceNavigations.牌告利率));
                    return;
                }
            }

            if (!(httpContext.Request.Url.AbsolutePath.StartsWith("/ajax/", StringComparison.InvariantCultureIgnoreCase) ||
                httpContext.Request.ContentType.Equals("image/pjpeg", StringComparison.InvariantCulture) ||
                httpContext.Request.Url.AbsolutePath.Equals("/trace.axd", StringComparison.InvariantCultureIgnoreCase) ||
                httpContext.Request.ContentType.Equals("/ChartAxd.axd", StringComparison.InvariantCulture) ||
                httpContext.Request.Url.AbsolutePath.EndsWith(".asmx", StringComparison.InvariantCultureIgnoreCase) ||
                httpContext.Request.Url.AbsolutePath.EndsWith("ScriptResource.axd", StringComparison.InvariantCultureIgnoreCase) ||
                httpContext.Request.Url.AbsolutePath.EndsWith("WebResource.axd", StringComparison.InvariantCultureIgnoreCase) ||
                System.IO.File.Exists(httpContext.Server.MapPath(httpContext.Request.Url.AbsolutePath))))
            {
                string type = httpContext.Request.ContentType.ToLower();
                string path = httpContext.Request.Path;
                string queryString = httpContext.Request.QueryString.ToString();

                if (queryString != null && queryString.Length > 0 && queryString[queryString.Length - 1] == '&')
                    queryString = queryString.Substring(0, queryString.Length - 1);

                string apppath = httpContext.Request.ApplicationPath;

                if (apppath != "/")
                    path = path.Remove(0, apppath.Length);

                path = String.Format("~{0}{1}", path, ((queryString.Length > 0) ? String.Format("?{0}", queryString) : String.Empty));

                UrlMapping um = new UrlMapping(path.Trim(), httpContext.Server.MapPath("~/Rule.xml"));
                string newUrl = um.RightUrl;
                //WriteLog.Log<string>("newUrl", newUrl);
                um.Close();
                um = null;

                if (newUrl != null && newUrl.Length > 0)
                {
                    httpContext.Response.Filter = new ResponseFilter(httpContext.Response.Filter, httpContext.Request.Path);

                    httpContext.RewritePath(newUrl);

                }//如果要求處理所有的請求時用到 
                else 
                {
                    System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    sb.AppendLine(String.Format("{0:yyyy/MM/dd HH:mm:ss}  {1}  {2}  {3}", DateTime.Now, System.Web.HttpContext.Current.Request.Url.LocalPath,
                                                                                         System.Web.HttpContext.Current.Request.ServerVariables["LOCAL_ADDR"],
                                                                                         System.Web.HttpContext.Current.Request.UserHostAddress));
                    sb.AppendLine(System.Web.HttpContext.Current.Request.Url.AbsoluteUri);
                    if (System.Web.HttpContext.Current.Request.UrlReferrer != null)
                        sb.AppendLine("UrlReferrer = " + System.Web.HttpContext.Current.Request.UrlReferrer.AbsoluteUri);
                    sb.AppendLine("path = " + path);
                    sb.AppendLine("httpContext.Request.Url.AbsoluteUri = " + httpContext.Request.Url.AbsoluteUri);
                    sb.AppendLine("httpContext.Request.Url.AbsolutePath = " + httpContext.Request.Url.AbsolutePath);
                    sb.AppendLine("httpContext.Request.ContentType = " + httpContext.Request.ContentType);
                    WriteLog.Log<string>(sb.ToString());
                    httpContext.Response.Redirect("~/", true);
                    httpContext.Response.End();
                } 
            }
        };
    }
}
