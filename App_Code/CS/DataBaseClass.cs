﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Drawing;



/// <summary>
/// DataBaseClass 的摘要描述
/// </summary>
public class DataBaseClass
{
    #region DataBaseClass

    #region ExecSQL
    //ExexSQL For Insert_Update_Delete
    public Boolean ExecSQL(string connectionString, string Insert_Update_Delete_SqlStr)
    {
        SqlConnection con = new SqlConnection(connectionString);
        con.Open();
        SqlCommand dbCommand = new SqlCommand(Insert_Update_Delete_SqlStr, con);
        try
        {
            dbCommand.ExecuteNonQuery();
            con.Close();
        }
        catch
        {
            con.Close();
            return false;
        }
        return true;
    }
    #endregion

    #region Select_DataReader
    //Select String DataTable == DataReader
    public DataTable Select_DataReader(string connectionString, string SqlStr)
    {
        DataTable tb1 = new DataTable();
        using (SqlConnection con = new SqlConnection(connectionString))
        {
            try
            {
                con.Open();

                SqlCommand Com = new SqlCommand(SqlStr, con);
                SqlDataReader dr = Com.ExecuteReader();
                tb1.Load(dr);
                dr.Dispose();
                dr = null;
            }
            catch// (Exception ex) 
            {
                // throw ex; 
            }
        }

        return tb1;

    }
    #endregion

    #region Procedure_DataReader_Parameters
    //Pro DataTable == DataReader Add Parameters
    public DataTable Procedure_DataReader_Parameters(string connectionString, string ProName, SqlParameter[] Param1)
    {

        DataTable tb1 = new DataTable();
        using (SqlConnection con = new SqlConnection(connectionString))
        {
            try
            {
                con.Open();

                SqlCommand Com = new SqlCommand(ProName, con);
                Com.CommandType = CommandType.StoredProcedure;
                //Add Parameters
                foreach (SqlParameter parameter in Param1)
                {
                    Com.Parameters.Add(parameter);
                }

                SqlDataReader dr = Com.ExecuteReader();
                tb1.Load(dr);
                dr.Dispose();
                dr = null;
            }
            catch// (Exception ex)
            {
                // throw ex; 
            }

        }
        return tb1;
    }
    #endregion

    #region Procedure_DataSet_Parameters
    public DataSet Procedure_DataSet_Parameters(string connectionString, string ProName, SqlParameter[] Param1)
    {
        DataSet dataSet = new DataSet();
        using (SqlConnection con = new SqlConnection(connectionString))
        {
            try
            {
                con.Open();
                SqlDataAdapter sqlDA = new SqlDataAdapter();
                SqlCommand command = new SqlCommand(ProName, con);
                command.CommandType = CommandType.StoredProcedure;
                foreach (SqlParameter parameter in Param1)
                {
                    if (parameter != null)
                    {
                        // 檢查未分配值的輸出參數,將其分配以DBNull.Value.
                        if ((parameter.Direction == ParameterDirection.InputOutput || parameter.Direction == ParameterDirection.Input) &&
                            (parameter.Value == null))
                        {
                            parameter.Value = DBNull.Value;
                        }
                        command.Parameters.Add(parameter);
                    }
                }

                sqlDA.SelectCommand = command;
                sqlDA.Fill(dataSet, "Ds");
                con.Close();
            }
            catch// (Exception ex)
            {
                // throw ex; 
            }

        }
        return dataSet;
    }
    public DataSet Procedure_DataSet_Parameters(string connectionString, string ProName, SqlParameter[] Param1, string TableName)
    {
        DataSet dataSet = new DataSet();
        using (SqlConnection con = new SqlConnection(connectionString))
        {
            try
            {
                con.Open();
                SqlDataAdapter sqlDA = new SqlDataAdapter();
                SqlCommand command = new SqlCommand(ProName, con);
                command.CommandType = CommandType.StoredProcedure;
                foreach (SqlParameter parameter in Param1)
                {
                    if (parameter != null)
                    {
                        // 檢查未分配值的輸出參數,將其分配以DBNull.Value.
                        if ((parameter.Direction == ParameterDirection.InputOutput || parameter.Direction == ParameterDirection.Input) &&
                            (parameter.Value == null))
                        {
                            parameter.Value = DBNull.Value;
                        }
                        command.Parameters.Add(parameter);
                    }
                }

                sqlDA.SelectCommand = command;
                sqlDA.Fill(dataSet, TableName);
                con.Close();
            }
            catch// (Exception ex)
            {
                // throw ex; 
            }

        }
        return dataSet;
    }
    #endregion

    #region//Procedure_DataReader
    //Procedure Retrun Table == DataReader
    public DataTable Procedure_DataReader(string connectionString, string pro)
    {
        DataTable tb1 = new DataTable();
        using (SqlConnection con = new SqlConnection(connectionString))
        {
            try
            {
                con.Open();

                SqlCommand cmd1 = new SqlCommand(pro, con);
                cmd1.CommandType = CommandType.StoredProcedure;
                SqlDataReader dr = cmd1.ExecuteReader();

                tb1.Load(dr);

                cmd1.Dispose();
                cmd1 = null;
            }
            catch// (Exception ex)
            {
                // throw ex; 
            }


        }
        return tb1;
    }
    #endregion

    #region//Select_DataReader_Parameters
    //Select String DataTable == DataReader Add Parameters
    public DataTable Select_DataReader_Parameters(string connectionString, string SqlStr, SqlParameter[] Param1)
    {
        DataTable tb1 = new DataTable();
        using (SqlConnection con = new SqlConnection(connectionString))
        {
            try
            {
                con.Open();

                SqlCommand Com = new SqlCommand(SqlStr, con);
                //Add Parameters
                foreach (SqlParameter parameter in Param1)
                {
                    Com.Parameters.Add(parameter);
                }

                SqlDataReader dr = Com.ExecuteReader();
                tb1.Load(dr);
                dr.Dispose();
                dr = null;
            }
            catch// (Exception ex)
            {
                // throw ex; 
            }

        }
        return tb1;
    }
    #endregion

    #region//Select_DataSet_RetrunManyTable
    //This Function is Return Many Table 
    public DataSet General_Select_DataSet_RetrunManyTable(string connectionString, string SqlStr)
    {
        DataSet ds = new DataSet();
        using (SqlConnection con = new SqlConnection(connectionString))
        {
            try
            {
                con.Open();

                //original
                string CommandText = SqlStr;
                SqlDataAdapter da = new SqlDataAdapter(CommandText, con);

                ////Another
                //SqlCommand testCommand = con.CreateCommand();
                //testCommand.CommandText = SqlStr_RetrunManyTable;
                //SqlDataAdapter da = new SqlDataAdapter(testCommand);

                da.Fill(ds);

                da.Dispose();
                da = null;
            }
            catch// (Exception ex)
            {
                // throw ex; 
            }
        }
        return ds;
    }
    #endregion

    #endregion
}