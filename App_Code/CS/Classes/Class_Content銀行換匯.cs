﻿using System;
using System.Data;
using System.Configuration;

/// <summary>
/// Class_Content銀行換匯 的摘要描述
/// </summary>
public class Class_Content銀行換匯
{
    #region 變數
    private string _BCode = String.Empty;

    private string _Currency = String.Empty;

    private Enums.euOrderDirection? _OrderDirection = null;

    private byte? _OrderIndex = null;

    private Enums.euBankMoneyExchangeType _Type = Enums.euBankMoneyExchangeType.Bank;
    #endregion 變數

    #region 屬性

    #region string BCode
    public string BCode
    {
        set
        {
            _BCode = value;
        }
        get
        {
            return _BCode;
        }
    }
    #endregion string BCode

    #region string Currency
    public string Currency
    {
        set
        {
            _Currency = value;
        }
    }
    #endregion string Currency

    #region Enums.euOrderDirection? OrderDirection
    public Enums.euOrderDirection? OrderDirection
    {
        set
        {
            _OrderDirection = value;
        }
        get
        {
            return _OrderDirection;
        }
    }
    #endregion Enums.euOrderDirection? OrderDirection

    #region byte? OrderIndex
    public byte? OrderIndex
    {
        set
        {
            _OrderIndex = value;
        }
        get
        {
            return _OrderIndex;
        }
    }
    #endregion byte? OrderIndex

    #region Enums.euBankMoneyExchangeType Type
    public Enums.euBankMoneyExchangeType Type
    {
        set
        {
            _Type = value;
        }
        get
        {
            return _Type;
        }
    }
    #endregion Enums.euBankMoneyExchangeType Type

    #region string FootJs
    public string FootJs
    {
        get
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();

            sb.AppendLine("<script type='text/javascript'>");
            sb.AppendLine("<!--");
            sb.AppendLine("$('select').change(function(){");
            sb.AppendLine("    if ($(this)[0].selectedIndex == 0)");
            sb.AppendLine(String.Format("        window.location.href='{0}{1}/{2}.aspx';", System.Web.HttpContext.Current.Request.Path.Substring(0, System.Web.HttpContext.Current.Request.Path.LastIndexOf('/') + 1), System.IO.Path.GetFileNameWithoutExtension(Enums.GetEnumDesc(Enums.euBankServiceNavigations.銀行換匯)), _Type));
            sb.AppendLine("    else");
            sb.AppendLine("    {");
            sb.AppendLine(String.Format("        window.location.href='{0}{1}/' + $(this).attr('id').substring(0, $(this).attr('id').length - 1) + '/' + $('option:selected', this).val() + '.aspx';", System.Web.HttpContext.Current.Request.Path.Substring(0, System.Web.HttpContext.Current.Request.Path.LastIndexOf('/') + 1), System.IO.Path.GetFileNameWithoutExtension(Enums.GetEnumDesc(Enums.euBankServiceNavigations.銀行換匯))));
            sb.AppendLine("    }");
            sb.AppendLine("});");
            sb.AppendLine("//-->");
            sb.AppendLine("</script>");

            return sb.ToString();
        }
    }
    #endregion string FootJs

    #endregion 屬性

	public Class_Content銀行換匯()
	{
		//
		// TODO: 在此加入建構函式的程式碼
		//
    }

    #region string Create ()
    public string Create ()
    {
        System.Text.StringBuilder sb = new System.Text.StringBuilder();

        DataProvider._24.Bank_all _24_Bank_all_Dp = new DataProvider._24.Bank_all();

        DataProvider._24.Bank_all.GetByBCodeCash_ReturnTableFields? orderField = null;
        if (_OrderIndex != null)
        {
            if ((byte)_OrderIndex == 1)
            {
                if (_Currency != null && _Currency.Length > 0)
                    orderField = DataProvider._24.Bank_all.GetByBCodeCash_ReturnTableFields.Bank;
                else
                    orderField = DataProvider._24.Bank_all.GetByBCodeCash_ReturnTableFields.Cash;
            }
            else
                orderField = (DataProvider._24.Bank_all.GetByBCodeCash_ReturnTableFields)((byte)_OrderIndex + 2);
        }

        _24_Bank_all_Dp.GetByBCodeCash(_BCode, _Currency, orderField, _OrderDirection);

        sb.Append("<div class='fmTb'>");
        sb.Append("<table border='0' cellspacing='0' cellpadding='0'>");
        sb.Append("<thead>");
        sb.Append("<tr>");

        string orderMark = (_OrderDirection == null || ((Enums.euOrderDirection)_OrderDirection) == Enums.euOrderDirection.ASC) ? "▲" : "▼";
        string a_d = ((orderMark == "▼") ? "a" : "d");

        string row1 = "幣別";
        string cashRemit1 = "匯";
        string cashRemit2 = "匯";
        if (_Currency != null && _Currency.Length > 0)
        {
            row1 = "銀行";
            cashRemit1 = "進";
            cashRemit2 = "出";
        }

        string[] classes = new string[5];
        if (_OrderIndex != null)
            classes[(byte)_OrderIndex - 1] = " class='bgc01'";

        byte classesIndex = 1;

        sb.Append(String.Format("<th class='nthBg'><a href='?{0}={1}' title='鉅亨理財'>{2}{3}</a></th>", a_d, classesIndex, row1, orderMark));
        sb.Append(String.Format("<th width='18%'{0}><a href='?{1}={2}' title='鉅亨理財'>現鈔買{3}{4}</a></th>", classes[classesIndex++], a_d, classesIndex, cashRemit1, orderMark));
        sb.Append(String.Format("<th width='18%'{0}><a href='?{1}={2}' title='鉅亨理財'>現鈔賣{3}{4}</a></th>", classes[classesIndex++], a_d, classesIndex, cashRemit2, orderMark));
        sb.Append(String.Format("<th width='18%'{0}><a href='?{1}={2}' title='鉅亨理財'>即期買{3}{4}</a></th>", classes[classesIndex++], a_d, classesIndex, cashRemit1, orderMark));
        sb.Append(String.Format("<th width='18%'{0}><a href='?{1}={2}' title='鉅亨理財'>即期賣{3}{4}</a></th>", classes[classesIndex++], a_d, classesIndex, cashRemit2, orderMark));

        sb.Append("</tr>");
        sb.Append("</thead>");
        sb.Append("<tbody>");

        Globals g = new Globals();

        foreach (DataRow dr in _24_Bank_all_Dp.ResultDataTable.Rows)
        {
            classesIndex = 1;
            sb.Append("<tr>");
            sb.Append(String.Format("<td class='cnlt'>{0}{1}{2}{3}</td>", ((_Type == Enums.euBankMoneyExchangeType.Currency) ? String.Format("<a href='{0}BankMoneyExchange/Bank/{1}.aspx' title='鉅亨理財'>", System.Web.HttpContext.Current.Request.Path.Substring(0, System.Web.HttpContext.Current.Request.Path.LastIndexOf('/') + 1), dr[DataProvider._24.Bank_all.GetByBCodeCash_ReturnTableFields.bcode.ToString()]) : String.Empty), ((_Type == Enums.euBankMoneyExchangeType.Bank && Enum.IsDefined(typeof(Enums.euCurrency), dr[DataProvider._24.Bank_all.GetByBCodeCash_ReturnTableFields.Cash.ToString()])) ? String.Format("{0} / ", Enums.GetEnumDesc((Enums.euCurrency)Enum.Parse(typeof(Enums.euCurrency), dr[DataProvider._24.Bank_all.GetByBCodeCash_ReturnTableFields.Cash.ToString()].ToString()))) : String.Empty), dr[((_Type == Enums.euBankMoneyExchangeType.Currency) ? DataProvider._24.Bank_all.GetByBCodeCash_ReturnTableFields.Bank.ToString() : DataProvider._24.Bank_all.GetByBCodeCash_ReturnTableFields.Cash.ToString())], ((_Type == Enums.euBankMoneyExchangeType.Currency) ? "</a>" : String.Empty)));
            sb.Append(String.Format("<td{0}>{1}</td>", classes[classesIndex++], g.MyFloatConvert(dr[DataProvider._24.Bank_all.GetByBCodeCash_ReturnTableFields.B.ToString()].ToString(), "F4", "--", String.Empty)));
            sb.Append(String.Format("<td{0}>{1}</td>", classes[classesIndex++], g.MyFloatConvert(dr[DataProvider._24.Bank_all.GetByBCodeCash_ReturnTableFields.S.ToString()].ToString(), "F4", "--", String.Empty)));
            sb.Append(String.Format("<td{0}>{1}</td>", classes[classesIndex++], g.MyFloatConvert(dr[DataProvider._24.Bank_all.GetByBCodeCash_ReturnTableFields.B1.ToString()].ToString(), "F4", "--", String.Empty)));
            sb.Append(String.Format("<td{0}>{1}</td>", classes[classesIndex++], g.MyFloatConvert(dr[DataProvider._24.Bank_all.GetByBCodeCash_ReturnTableFields.S1.ToString()].ToString(), "F4", "--", String.Empty)));
            sb.Append("</tr>");
        }

        sb.Append("</tbody>");
        sb.Append("</table>");
        sb.Append("</div><!-- fmTb:end -->");

        return sb.ToString();
    }
    #endregion string Create ()
}
