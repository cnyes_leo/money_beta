﻿using System;
using System.Data;
using System.Configuration;

/// <summary>
/// Class_Content放款利率1 的摘要描述
/// </summary>
public class Class_Content放款利率1
{
    #region 變數
    private byte _PageIndex = 1;

    private byte _PageSize = 25;

    private Enums.euOrderDirection? _OrderDirection = null;

    private byte? _OrderIndex = null;
    #endregion 變數

    #region 屬性

    #region byte PageIndex
    public byte PageIndex
    {
        set
        {
            _PageIndex = value;
        }
    }
    #endregion byte PageIndex

    #region byte PageSize
    public byte PageSize
    {
        set
        {
            _PageSize = value;
        }
    }
    #endregion byte PageSize

    #region Enums.euOrderDirection? OrderDirection
    public Enums.euOrderDirection? OrderDirection
    {
        set
        {
            _OrderDirection = value;
        }
        get
        {
            return _OrderDirection;
        }
    }
    #endregion Enums.euOrderDirection? OrderDirection

    #region byte? OrderIndex
    public byte? OrderIndex
    {
        set
        {
            _OrderIndex = value;
        }
        get
        {
            return _OrderIndex;
        }
    }
    #endregion byte? OrderIndex

    #endregion 屬性

	public Class_Content放款利率1()
	{
		//
		// TODO: 在此加入建構函式的程式碼
		//
    }

    #region string Create (byte pageIndex, byte pageSize, Enums.euOrderDirection? orderDirection, byte? orderIndex)
    public string Create (byte pageIndex, byte pageSize, Enums.euOrderDirection? orderDirection, byte? orderIndex)
    {
        _PageIndex = pageIndex;
        _PageSize = pageSize;
        _OrderDirection = orderDirection;
        _OrderIndex = orderIndex;

        return Create();
    }
    #endregion string Create (byte pageIndex, byte pageSize, Enums.euOrderDirection? orderDirection, byte? orderIndex)

    #region string Create ()
    public string Create ()
    {
        System.Text.StringBuilder sb = new System.Text.StringBuilder();

        sb.Append("<div class='fmTb'>");
        sb.Append("<div class='hd'>");
        sb.Append("<span class='dataInfo'><cite>單位：年息%</cite></span>");
        sb.Append("</div><!-- hd:end -->");
        sb.Append("<table border='0' cellspacing='0' cellpadding='0' width='100%' class='tabType'>");
        sb.Append("<tr>");
        sb.Append("<th width='25%' rowspan='2'>銀行名稱</th>");
        sb.Append("<th colspan='2'>信用卡利率</th>");
        sb.Append("<th colspan='2'>現金卡利率</th>");
        sb.Append("<th>指數房貸利率</th>");
        sb.Append("</tr>");
        sb.Append("<tr>");

        string orderMark = (_OrderDirection == null || ((Enums.euOrderDirection)_OrderDirection) == Enums.euOrderDirection.ASC) ? "▲" : "▼";
        string a_d = ((orderMark == "▼") ? "a" : "d");

        string[] classes = new string[5];
        if (_OrderIndex != null)
            classes[(byte)_OrderIndex - 1] = " class='bgc01'";

        byte classesIndex = 0;

        sb.Append(String.Format("<td width='15%'{0}><a href='{1}.aspx?{2}={3}' title='鉅亨理財'>最低{4}</a></td>", classes[classesIndex++], System.IO.Path.GetFileNameWithoutExtension(System.Web.HttpContext.Current.Request.RawUrl), a_d, classesIndex, orderMark));
        sb.Append(String.Format("<td width='15%'{0}><a href='{1}.aspx?{2}={3}' title='鉅亨理財'>最高{4}</a></td>", classes[classesIndex++], System.IO.Path.GetFileNameWithoutExtension(System.Web.HttpContext.Current.Request.RawUrl), a_d, classesIndex, orderMark));
        sb.Append(String.Format("<td width='15%'{0}><a href='{1}.aspx?{2}={3}' title='鉅亨理財'>最低{4}</a></td>", classes[classesIndex++], System.IO.Path.GetFileNameWithoutExtension(System.Web.HttpContext.Current.Request.RawUrl), a_d, classesIndex, orderMark));
        sb.Append(String.Format("<td width='15%'{0}><a href='{1}.aspx?{2}={3}' title='鉅亨理財'>最高{4}</a></td>", classes[classesIndex++], System.IO.Path.GetFileNameWithoutExtension(System.Web.HttpContext.Current.Request.RawUrl), a_d, classesIndex, orderMark));
        sb.Append(String.Format("<td width='15%'{0}><a href='{1}.aspx?{2}={3}' title='鉅亨理財'>指標利率{4}</a></td>", classes[classesIndex++], System.IO.Path.GetFileNameWithoutExtension(System.Web.HttpContext.Current.Request.RawUrl), a_d, classesIndex, orderMark));
        sb.Append("</tr>");

        // 內容
        DataProvider._24.Bank _24BankProvider = new DataProvider._24.Bank();

        DataProvider._24.Bank.GetUsuryRate1_ReturnTable2Fields? orderField = null;
        if (_OrderIndex != null)
            orderField = (DataProvider._24.Bank.GetUsuryRate1_ReturnTable2Fields)((byte)_OrderIndex + 2);

        _24BankProvider.GetUsuryRate1(_PageIndex, _PageSize, _OrderDirection, orderField);

        Globals g = new Globals();

        if (_24BankProvider.ResultDataSet.Tables.Count > 1)
        {
            foreach (DataRow dr in _24BankProvider.ResultDataSet.Tables[1].Rows)
            {
                classesIndex = 0;

                sb.Append("<tr>");
                sb.Append(String.Format("<td class='cnlt'><a href='{0}BankDetail/{1}.aspx' title='{2}'>{2}</a></td>", System.Web.HttpContext.Current.Request.Path.Substring(0, System.Web.HttpContext.Current.Request.Path.LastIndexOf('/') + 1), dr[DataProvider._24.Bank.GetUsuryRate1_ReturnTable2Fields.BCode.ToString()], dr[DataProvider._24.Bank.GetUsuryRate1_ReturnTable2Fields.BName.ToString()]));
                sb.Append(String.Format("<td{0}>{1}</td>", classes[classesIndex++], g.MyFloatConvert(dr[DataProvider._24.Bank.GetUsuryRate1_ReturnTable2Fields.Rate1_Min.ToString()].ToString(), "F2", "--", String.Empty)));
                sb.Append(String.Format("<td{0}>{1}</td>", classes[classesIndex++], g.MyFloatConvert(dr[DataProvider._24.Bank.GetUsuryRate1_ReturnTable2Fields.Rate1_Max.ToString()].ToString(), "F2", "--", String.Empty)));
                sb.Append(String.Format("<td{0}>{1}</td>", classes[classesIndex++], g.MyFloatConvert(dr[DataProvider._24.Bank.GetUsuryRate1_ReturnTable2Fields.Rate2_Min.ToString()].ToString(), "F2", "--", String.Empty)));
                sb.Append(String.Format("<td{0}>{1}</td>", classes[classesIndex++], g.MyFloatConvert(dr[DataProvider._24.Bank.GetUsuryRate1_ReturnTable2Fields.Rate2_Max.ToString()].ToString(), "F2", "--", String.Empty)));
                sb.Append(String.Format("<td{0}>{1}</td>", classes[classesIndex++], g.MyFloatConvert(dr[DataProvider._24.Bank.GetUsuryRate1_ReturnTable2Fields.Rate3.ToString()].ToString(), "F4", "--", String.Empty)));
                sb.Append("</tr>");
            }
        }

        sb.Append("</table>");

        if (_24BankProvider.ResultDataSet.Tables.Count > 0)
        {
            // 分頁
            int totalPage = 1;
            int.TryParse(_24BankProvider.ResultDataSet.Tables[0].Rows[0][DataProvider._24.Bank.ReturnTable1_CommonFields.TotalPage.ToString()].ToString(), out totalPage);

            string baseURL = String.Format("?{0}{1}", ((_OrderDirection != null && _OrderIndex != null) ? ((((Enums.euOrderDirection)_OrderDirection) == Enums.euOrderDirection.ASC) ? String.Format("a={0}", _OrderIndex) : String.Format("d={0}", _OrderIndex)) : String.Empty), ((_OrderDirection == null || _OrderIndex == null) ? String.Empty : "&")) + "p={0}";
            sb.Append(g.BankServicePager(totalPage, _PageIndex, 10, baseURL));
        }

        sb.Append("</div><!-- fmTb:end -->");

        return sb.ToString();
    }
    #endregion string Create ()
}
