﻿using System;
using System.Data;
using System.Configuration;

/// <summary>
/// Class_Content存款利率2_3 的摘要描述
/// </summary>
public class Class_Content存款利率2_3
{
    #region 變數
    private Enums.eu存款利率Navigations _存款利率Nav = Enums.eu存款利率Navigations.None;

    private byte _PageIndex = 1;

    private byte _PageSize = 25;

    private Enums.euOrderDirection? _OrderDirection = null;

    private byte? _OrderIndex = null;
    #endregion 變數

    #region 屬性

    #region Enums.eu存款利率Navigations 存款利率Nav
    public Enums.eu存款利率Navigations 存款利率Nav
    {
        set
        {
            _存款利率Nav = value;
        }
        get
        {
            return _存款利率Nav;
        }
    }
    #endregion Enums.eu存款利率Navigations 存款利率Nav

    #region byte PageIndex
    public byte PageIndex
    {
        set
        {
            _PageIndex = value;
        }
    }
    #endregion byte PageIndex

    #region byte PageSize
    public byte PageSize
    {
        set
        {
            _PageSize = value;
        }
    }
    #endregion byte PageSize

    #region Enums.euOrderDirection? OrderDirection
    public Enums.euOrderDirection? OrderDirection
    {
        set
        {
            _OrderDirection = value;
        }
        get
        {
            return _OrderDirection;
        }
    }
    #endregion Enums.euOrderDirection? OrderDirection

    #region byte? OrderIndex
    public byte? OrderIndex
    {
        set
        {
            _OrderIndex = value;
        }
        get
        {
            return _OrderIndex;
        }
    }
    #endregion byte? OrderIndex

    #endregion 屬性

	public Class_Content存款利率2_3()
	{
		//
		// TODO: 在此加入建構函式的程式碼
		//
    }

    #region string Create (Enums.eu存款利率Navigations nav, byte pageIndex, byte pageSize, Enums.euOrderDirection? orderDirection, byte? orderIndex)
    public string Create (Enums.eu存款利率Navigations nav, byte pageIndex, byte pageSize, Enums.euOrderDirection? orderDirection, byte? orderIndex)
    {
        _存款利率Nav = nav;
        _PageIndex = pageIndex;
        _PageSize = pageSize;
        _OrderDirection = orderDirection;
        _OrderIndex = orderIndex;

        return Create();
    }
    #endregion string Create (Enums.eu存款利率Navigations nav, byte pageIndex, byte pageSize, Enums.euOrderDirection? orderDirection, byte? orderIndex)

    #region string Create ()
    public string Create ()
    {
        System.Text.StringBuilder sb = new System.Text.StringBuilder();

        sb.Append("<div class='fmTb'>");
        sb.Append("<div class='hd'>");

        string orderMark = (_OrderDirection == null || ((Enums.euOrderDirection)_OrderDirection) == Enums.euOrderDirection.ASC) ? "▲" : "▼";
        string a_d = ((orderMark == "▼") ? "a" : "d");

        string p = String.Empty;
        if (_PageIndex > 1)
            p = "&p=" + _PageIndex.ToString();

        string[] classes = new string[7];
        if (_OrderIndex != null)
            classes[(byte)_OrderIndex - 1] = " class='bgc01'";

        byte classesIndex = 0;

        sb.Append(String.Format("<span class='dataInfo'> 單位：年息 ％<cite>{0:yyyy-MM-dd}</cite></span>", DateTime.Today));
        sb.Append("</div><!-- hd:end -->");
        sb.Append("<table border='0' cellspacing='0' cellpadding='0' width='100%' class='tabType'>");
        sb.Append("<tr>");
        sb.Append("<th width='13%' rowspan='2' class='nthBg'>銀行名稱</th>");
        sb.Append(String.Format("<th width='12%' rowspan='2'{0}><a href='{1}.aspx?{2}=1{3}' title='鉅亨理財'>活期儲蓄存款{4}</th>", classes[classesIndex++], System.IO.Path.GetFileNameWithoutExtension(System.Web.HttpContext.Current.Request.RawUrl), a_d, p, orderMark));
        sb.Append("<th colspan='6'>定期存款</th>");
        sb.Append("</tr>");

        sb.Append("<tr>");
        sb.Append(String.Format("<td width='13%'{0}><a href='{1}.aspx?{2}=2{3}' title='鉅亨理財'>1個月{4}</a></td>", classes[classesIndex++], System.IO.Path.GetFileNameWithoutExtension(System.Web.HttpContext.Current.Request.RawUrl), a_d, p, orderMark));
        sb.Append(String.Format("<td width='12%'{0}><a href='{1}.aspx?{2}=3{3}' title='鉅亨理財'>2個月{4}</a></td>", classes[classesIndex++], System.IO.Path.GetFileNameWithoutExtension(System.Web.HttpContext.Current.Request.RawUrl), a_d, p, orderMark));
        sb.Append(String.Format("<td width='12%'{0}><a href='{1}.aspx?{2}=4{3}' title='鉅亨理財'>3個月{4}</a></td>", classes[classesIndex++], System.IO.Path.GetFileNameWithoutExtension(System.Web.HttpContext.Current.Request.RawUrl), a_d, p, orderMark));
        sb.Append(String.Format("<td width='13%'{0}><a href='{1}.aspx?{2}=5{3}' title='鉅亨理財'>6個月{4}</a></td>", classes[classesIndex++], System.IO.Path.GetFileNameWithoutExtension(System.Web.HttpContext.Current.Request.RawUrl), a_d, p, orderMark));
        sb.Append(String.Format("<td width='13%'{0}><a href='{1}.aspx?{2}=6{3}' title='鉅亨理財'>9個月{4}</a></td>", classes[classesIndex++], System.IO.Path.GetFileNameWithoutExtension(System.Web.HttpContext.Current.Request.RawUrl), a_d, p, orderMark));
        sb.Append(String.Format("<td width='12%'{0}><a href='{1}.aspx?{2}=7{3}' title='鉅亨理財'>12個月{4}</a></td>", classes[classesIndex++], System.IO.Path.GetFileNameWithoutExtension(System.Web.HttpContext.Current.Request.RawUrl), a_d, p, orderMark));
        sb.Append("</tr>");

        // 內容
        DataProvider._24.NewBankRate _24_NewBankRate_Provider = new DataProvider._24.NewBankRate();

        DataProvider._24.NewBankRate.Get存款利率ByCountry_Currency_ReturnTable2Fields? orderField = null;
        if (_OrderIndex != null)
            orderField = (DataProvider._24.NewBankRate.Get存款利率ByCountry_Currency_ReturnTable2Fields)((byte)_OrderIndex + 1);

        _24_NewBankRate_Provider.Get存款利率ByCountry(((_存款利率Nav == Enums.eu存款利率Navigations.中國利率排行) ? "CN" : "HK"), ((_存款利率Nav == Enums.eu存款利率Navigations.中國利率排行) ? "CNY" : "HKD"), orderField, _OrderDirection);

        Globals g = new Globals();

        foreach (DataRow dr in _24_NewBankRate_Provider.ResultDataTable.Rows)
        {
            classesIndex = 0;
            sb.Append("<tr>");
            sb.Append(String.Format("<td class='cnlt'><a href='{0}BankDetail/{1}.aspx' title='{2}'>{2}</a></td>", System.Web.HttpContext.Current.Request.Path.Substring(0, System.Web.HttpContext.Current.Request.Path.LastIndexOf('/') + 1), dr[DataProvider._24.NewBankRate.Get存款利率ByCountry_Currency_ReturnTable2Fields.BankID.ToString()], dr[DataProvider._24.NewBankRate.Get存款利率ByCountry_Currency_ReturnTable2Fields.BankName.ToString()]));
            sb.Append(String.Format("<td{0}>{1}</td>", classes[classesIndex++], g.MyFloatConvert(dr[DataProvider._24.NewBankRate.Get存款利率ByCountry_Currency_ReturnTable2Fields.Rate0.ToString()].ToString(), String.Empty, "--", String.Empty, 100)));
            sb.Append(String.Format("<td{0}>{1}</td>", classes[classesIndex++], g.MyFloatConvert(dr[DataProvider._24.NewBankRate.Get存款利率ByCountry_Currency_ReturnTable2Fields.Rate1.ToString()].ToString(), String.Empty, "--", String.Empty, 100)));
            sb.Append(String.Format("<td{0}>{1}</td>", classes[classesIndex++], g.MyFloatConvert(dr[DataProvider._24.NewBankRate.Get存款利率ByCountry_Currency_ReturnTable2Fields.Rate2.ToString()].ToString(), String.Empty, "--", String.Empty, 100)));
            sb.Append(String.Format("<td{0}>{1}</td>", classes[classesIndex++], g.MyFloatConvert(dr[DataProvider._24.NewBankRate.Get存款利率ByCountry_Currency_ReturnTable2Fields.Rate3.ToString()].ToString(), String.Empty, "--", String.Empty, 100)));
            sb.Append(String.Format("<td{0}>{1}</td>", classes[classesIndex++], g.MyFloatConvert(dr[DataProvider._24.NewBankRate.Get存款利率ByCountry_Currency_ReturnTable2Fields.Rate6.ToString()].ToString(), String.Empty, "--", String.Empty, 100)));
            sb.Append(String.Format("<td{0}>{1}</td>", classes[classesIndex++], g.MyFloatConvert(dr[DataProvider._24.NewBankRate.Get存款利率ByCountry_Currency_ReturnTable2Fields.Rate9.ToString()].ToString(), String.Empty, "--", String.Empty, 100)));
            sb.Append(String.Format("<td{0}>{1}</td>", classes[classesIndex++], g.MyFloatConvert(dr[DataProvider._24.NewBankRate.Get存款利率ByCountry_Currency_ReturnTable2Fields.Rate12.ToString()].ToString(), String.Empty, "--", String.Empty, 100)));
            sb.Append("</tr>");
        }

        sb.Append("</table>");

        sb.Append("</div><!-- fmTb:end -->");

        sb.Append("<p class='comt'><span>備註:</span>1.僅供參考，以銀行實際利率為準。 2.有些銀行會有最低存款下限。</p>");

        return sb.ToString();
    }
    #endregion string Create ()
}
