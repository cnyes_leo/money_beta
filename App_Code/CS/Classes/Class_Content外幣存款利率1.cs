﻿using System;
using System.Data;
using System.Configuration;

/// <summary>
/// Class_Content外幣存款利率1 的摘要描述
/// </summary>
public class Class_Content外幣存款利率1
{
    #region 變數
    //private byte _SubIndex = 1;

    private Enums.eu外幣存款利率Navigations _外幣存款利率Nav = Enums.eu外幣存款利率Navigations.台灣利率排行;

    private Enums.eu外幣存款利率SubNavigations _外幣存款利率SubNav = Enums.eu外幣存款利率SubNavigations.All;

    private string _Currency = String.Empty;

    private byte? _ItemPeriod = null;

    private byte _PageIndex = 1;

    private byte _PageSize = 25;

    private Enums.euOrderDirection? _OrderDirection = null;

    private byte? _OrderIndex = null;

    //private System.Text.StringBuilder _FootJs = new System.Text.StringBuilder();
    #endregion 變數

    #region 屬性

    #region //byte SubIndex
    //public byte SubIndex
    //{
    //    set
    //    {
    //        _SubIndex = value;
    //    }
    //}
    #endregion //byte SubIndex

    #region Enums.eu外幣存款利率Navigations 外幣存款利率Nav
    public Enums.eu外幣存款利率Navigations 外幣存款利率Nav
    {
        set
        {
            _外幣存款利率Nav = value;
        }
        get
        {
            return _外幣存款利率Nav;
        }
    }
    #endregion Enums.eu外幣存款利率Navigations 外幣存款利率Nav

    #region Enums.eu外幣存款利率SubNavigations 外幣存款利率SubNav
    public Enums.eu外幣存款利率SubNavigations 外幣存款利率SubNav
    {
        set
        {
            _外幣存款利率SubNav = value;
        }
        get
        {
            return _外幣存款利率SubNav;
        }
    }
    #endregion Enums.eu外幣存款利率SubNavigations 外幣存款利率SubNav

    #region string Currency
    public string Currency
    {
        set
        {
            _Currency = value;
        }
    }
    #endregion string Currency

    #region byte? ItemPeriod
    public byte? ItemPeriod
    {
        set
        {
            _ItemPeriod = value;
        }
        get
        {
            return _ItemPeriod;
        }
    }
    #endregion byte? ItemPeriod

    #region byte PageIndex
    public byte PageIndex
    {
        set
        {
            _PageIndex = value;
        }
    }
    #endregion byte PageIndex

    #region byte PageSize
    public byte PageSize
    {
        set
        {
            _PageSize = value;
        }
    }
    #endregion byte PageSize

    #region Enums.euOrderDirection? OrderDirection
    public Enums.euOrderDirection? OrderDirection
    {
        set
        {
            _OrderDirection = value;
        }
        get
        {
            return _OrderDirection;
        }
    }
    #endregion Enums.euOrderDirection? OrderDirection

    #region byte? OrderIndex
    public byte? OrderIndex
    {
        set
        {
            _OrderIndex = value;
        }
        get
        {
            return _OrderIndex;
        }
    }
    #endregion byte? OrderIndex

    #region string FootJs
    public string FootJs
    {
        get
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();

            sb.AppendLine("<script type='text/javascript'>");
            sb.AppendLine("<!--");
            if (_外幣存款利率SubNav == Enums.eu外幣存款利率SubNavigations.Currency)
            {
                sb.AppendLine("$(\"input:radio[id^='Currency']\").click(function(){");
                sb.AppendLine(String.Format("    window.location.href='{0}{1}{2}/{3}/' + $(this).attr('id').split('_')[1] + '.aspx'", System.Web.HttpContext.Current.Request.Path.Substring(0, System.Web.HttpContext.Current.Request.Path.LastIndexOf('/') + 1), System.IO.Path.GetFileNameWithoutExtension(Enums.GetEnumDesc(Enums.euBankServiceNavigations.外幣存款利率)), (byte)_外幣存款利率Nav, _外幣存款利率SubNav));
            }
            else
            {
                sb.AppendLine("$(\"select[name='dataym']\").change(function(){");
                sb.AppendLine("    if ($(this)[0].selectedIndex > 0)");
                sb.AppendLine("    {");
                sb.AppendLine(String.Format("        window.location.href='{0}{1}ip=' + $('option:selected', this).val();", /*0*/System.Text.RegularExpressions.Regex.Replace(System.Web.HttpContext.Current.Request.RawUrl.ToString(), "&?ip=\\d", String.Empty).Replace("?&", "&").Replace(".aspx&", ".aspx?"), /*1*/((System.Web.HttpContext.Current.Request.RawUrl.IndexOf('?') > 0) ? "&" : "?")));
                sb.AppendLine("    }");
                sb.AppendLine("    else");
                sb.AppendLine(String.Format("        window.location.href='{0}ValutaDepositRate.aspx';", System.Web.HttpContext.Current.Request.Path.Substring(0, System.Web.HttpContext.Current.Request.Path.LastIndexOf('/') + 1)));
            }
            sb.AppendLine("});");
            sb.AppendLine("//-->");
            sb.AppendLine("</script>");

            return sb.ToString();
        }
    }
    #endregion string FootJs

    #endregion 屬性

    #region 建構子
    public Class_Content外幣存款利率1()
	{
		//
		// TODO: 在此加入建構函式的程式碼
		//
	}

    //public Class_Content外幣存款利率1 (Enums.eu外幣存款利率Navigations nav, Enums.eu外幣存款利率SubNavigations subNav, string currency, byte pageIndex, byte pageSize)
    //{
    //    _外幣存款利率Nav = nav;
    //    _外幣存款利率SubNav = subNav;
    //    _Currency = currency;
    //    _PageIndex = pageIndex;
    //    _PageSize = pageSize;
    //}
    #endregion 建構子

    #region string Create ()
    public string Create ()
    {
        System.Text.StringBuilder sb = new System.Text.StringBuilder();

        DataProvider._24.NewBankRate _24_NewBankRateProvider = new DataProvider._24.NewBankRate();

        sb.Append("<div class='fmTb'>");
        sb.Append("<div class='hd'>");
        sb.Append("<span class='hdInp'>");

        #region 全部幣別
        if (_外幣存款利率SubNav == Enums.eu外幣存款利率SubNavigations.All)
        {
            string[] optionsSelected = new string[7];
            if (_ItemPeriod == null)
                optionsSelected[0] = " selected='selected'";
            else
                optionsSelected[(byte)_ItemPeriod] = " selected='selected'";
            sb.Append("<select name='dataym'>");
            sb.Append(String.Format("<option value='0'{0}>選擇存款期間</option>", optionsSelected[0]));
            sb.Append(String.Format("<option value='1'{0}>定存3個月</option>", optionsSelected[1]));
            sb.Append(String.Format("<option value='2'{0}>定存1星期</option>", optionsSelected[2]));
            sb.Append(String.Format("<option value='3'{0}>定存1個月</option>", optionsSelected[3]));
            sb.Append(String.Format("<option value='4'{0}>定存6個月</option>", optionsSelected[4]));
            sb.Append(String.Format("<option value='5'{0}>定存9個月</option>", optionsSelected[5]));
            sb.Append(String.Format("<option value='6'{0}>定存1年</option>", optionsSelected[6]));
            sb.Append("</select>");
        }
        #endregion 全部幣別
        #region 各幣別
        else
        {
            if (_Currency.Length == 0)
                _Currency = DataProvider._24.NewBankRate.Get外幣存款利率ByCountry_ReturnTable2Fields.Rate_USD.ToString().Split('_')[1];

            string[] selected = new string[11];

            foreach (DataProvider._24.NewBankRate.Get外幣存款利率ByCountry_ReturnTable2Fields f in Enum.GetValues(typeof(DataProvider._24.NewBankRate.Get外幣存款利率ByCountry_ReturnTable2Fields)))
            {
                if (f == DataProvider._24.NewBankRate.Get外幣存款利率ByCountry_ReturnTable2Fields.Index || f == DataProvider._24.NewBankRate.Get外幣存款利率ByCountry_ReturnTable2Fields.BankID || f == DataProvider._24.NewBankRate.Get外幣存款利率ByCountry_ReturnTable2Fields.BankName)
                    continue;

                sb.Append(String.Format("<input name='choice' type='radio' value='{0}' id='Currency_{0}'{1} />{2}", f.ToString().Split('_')[1], ((f.ToString().EndsWith(_Currency, StringComparison.InvariantCultureIgnoreCase)) ? " checked='checked'" : String.Empty), Enums.GetEnumDesc(f)));
            }
            #region //Mark
            //sb.Append("<input name='choice' type='radio' value='' />美元");
            //sb.Append("<input name='choice' type='radio' value='' />歐元");
            //sb.Append("<input name='choice' type='radio' value='' />英鎊");
            //sb.Append("<input name='choice' type='radio' value='' />澳幣");
            //sb.Append("<input name='choice' type='radio' value='' />紐幣");
            //sb.Append("<input name='choice' type='radio' value='' />加拿大幣");
            //sb.Append("<input name='choice' type='radio' value='' />新加坡幣");
            //sb.Append("<input name='choice' type='radio' value='' />瑞士法郎");
            //sb.Append("<input name='choice' type='radio' value='' />港幣");
            #endregion //Mark
        }
        #endregion 各幣別
        sb.Append("</span><!-- hdInp:end -->");
        sb.Append(String.Format("<span class='dataInfo'>單位：年息 ％<cite>{0:yyyy-MM-dd}</cite></span>", DateTime.Today));
        sb.Append("</div>");
        sb.Append("<!-- hd:end -->");

        string orderMark = (_OrderDirection == null || ((Enums.euOrderDirection)_OrderDirection) == Enums.euOrderDirection.ASC) ? "▲" : "▼";
        string a_d = ((orderMark == "▼") ? "a" : "d");

        string p = String.Empty;
        if (_PageIndex > 1)
            p = "&p=" + _PageIndex.ToString();

        string[] classes = new string[Enum.GetNames(typeof(Enums.eu外幣存款利率抓取幣別)).Length + 1];
        // bob 說 預設為 美元（_OrderIndex == 2)
        if (_外幣存款利率SubNav == Enums.eu外幣存款利率SubNavigations.All && _OrderIndex == null)
            _OrderIndex = 2;

        if (_OrderIndex != null)
            classes[(byte)_OrderIndex - 1] = " class='bgc01'";

        byte classesIndex = 0;

        sb.Append("<table border='0' cellspacing='0' cellpadding='0' width='100%'>");
        sb.Append("<thead>");
        sb.Append("<tr>");

        Globals g = new Globals();

        #region _外幣存款利率SubNav == Enums.eu外幣存款利率SubNavigations.All
        if (_外幣存款利率SubNav == Enums.eu外幣存款利率SubNavigations.All)
        {
            sb.Append(String.Format("<th width='10%'{0}><a href='{1}.aspx?{2}=1{3}{4}' title='鉅亨理財'>銀行名稱{5}</a></th>", classes[classesIndex++], System.IO.Path.GetFileNameWithoutExtension(System.Web.HttpContext.Current.Request.RawUrl), a_d, p, ((_ItemPeriod != null && (byte)_ItemPeriod > 0) ? String.Format("&ip={0}", _ItemPeriod) : String.Empty), orderMark));
            #region //Mark
            //sb.Append(String.Format("<th width='8%'{0}><a href='{1}.aspx?{2}=2{3}{4}' title='鉅亨理財'>美元{5}</a></th>", classes[classesIndex++], System.IO.Path.GetFileNameWithoutExtension(System.Web.HttpContext.Current.Request.RawUrl), a_d, p, ((_ItemPeriod != null && (byte)_ItemPeriod > 0) ? String.Format("&ip={0}", _ItemPeriod) : String.Empty), orderMark));
            //sb.Append(String.Format("<th width='8%'{0}><a href='{1}.aspx?{2}=3{3}{4}' title='鉅亨理財'>歐元{5}</a></th>", classes[classesIndex++], System.IO.Path.GetFileNameWithoutExtension(System.Web.HttpContext.Current.Request.RawUrl), a_d, p, ((_ItemPeriod != null && (byte)_ItemPeriod > 0) ? String.Format("&ip={0}", _ItemPeriod) : String.Empty), orderMark));
            //sb.Append(String.Format("<th width='8%'{0}><a href='{1}.aspx?{2}=4{3}{4}' title='鉅亨理財'>英鎊{5}</a></th>", classes[classesIndex++], System.IO.Path.GetFileNameWithoutExtension(System.Web.HttpContext.Current.Request.RawUrl), a_d, p, ((_ItemPeriod != null && (byte)_ItemPeriod > 0) ? String.Format("&ip={0}", _ItemPeriod) : String.Empty), orderMark));
            //sb.Append(String.Format("<th width='8%'{0}><a href='{1}.aspx?{2}=5{3}{4}' title='鉅亨理財'>澳幣{5}</a></th>", classes[classesIndex++], System.IO.Path.GetFileNameWithoutExtension(System.Web.HttpContext.Current.Request.RawUrl), a_d, p, ((_ItemPeriod != null && (byte)_ItemPeriod > 0) ? String.Format("&ip={0}", _ItemPeriod) : String.Empty), orderMark));
            //sb.Append(String.Format("<th width='8%'{0}><a href='{1}.aspx?{2}=6{3}{4}' title='鉅亨理財'>紐幣{5}</a></th>", classes[classesIndex++], System.IO.Path.GetFileNameWithoutExtension(System.Web.HttpContext.Current.Request.RawUrl), a_d, p, ((_ItemPeriod != null && (byte)_ItemPeriod > 0) ? String.Format("&ip={0}", _ItemPeriod) : String.Empty), orderMark));
            //sb.Append(String.Format("<th width='8%'{0}><a href='{1}.aspx?{2}=7{3}{4}' title='鉅亨理財'>加拿大幣{5}</a></th>", classes[classesIndex++], System.IO.Path.GetFileNameWithoutExtension(System.Web.HttpContext.Current.Request.RawUrl), a_d, p, ((_ItemPeriod != null && (byte)_ItemPeriod > 0) ? String.Format("&ip={0}", _ItemPeriod) : String.Empty), orderMark));
            //sb.Append(String.Format("<th width='8%'{0}><a href='{1}.aspx?{2}=8{3}{4}' title='鉅亨理財'>新加坡幣{5}</a></th>", classes[classesIndex++], System.IO.Path.GetFileNameWithoutExtension(System.Web.HttpContext.Current.Request.RawUrl), a_d, p, ((_ItemPeriod != null && (byte)_ItemPeriod > 0) ? String.Format("&ip={0}", _ItemPeriod) : String.Empty), orderMark));
            //sb.Append(String.Format("<th width='8%'{0}><a href='{1}.aspx?{2}=9{3}{4}' title='鉅亨理財'>瑞士法郎{5}</a></th>", classes[classesIndex++], System.IO.Path.GetFileNameWithoutExtension(System.Web.HttpContext.Current.Request.RawUrl), a_d, p, ((_ItemPeriod != null && (byte)_ItemPeriod > 0) ? String.Format("&ip={0}", _ItemPeriod) : String.Empty), orderMark));
            //sb.Append(String.Format("<th width='8%'{0}><a href='{1}.aspx?{2}=10{3}{4}' title='鉅亨理財'>港幣{5}</a></th>", classes[classesIndex++], System.IO.Path.GetFileNameWithoutExtension(System.Web.HttpContext.Current.Request.RawUrl), a_d, p, ((_ItemPeriod != null && (byte)_ItemPeriod > 0) ? String.Format("&ip={0}", _ItemPeriod) : String.Empty), orderMark));
            //sb.Append(String.Format("<th width='8%'{0}><a href='{1}.aspx?{2}=10{3}{4}' title='鉅亨理財'>日圓{5}</a></th>", classes[classesIndex++], System.IO.Path.GetFileNameWithoutExtension(System.Web.HttpContext.Current.Request.RawUrl), a_d, p, ((_ItemPeriod != null && (byte)_ItemPeriod > 0) ? String.Format("&ip={0}", _ItemPeriod) : String.Empty), orderMark));
            //sb.Append(String.Format("<th width='8%'{0}><a href='{1}.aspx?{2}=10{3}{4}' title='鉅亨理財'>南非幣{5}</a></th>", classes[classesIndex++], System.IO.Path.GetFileNameWithoutExtension(System.Web.HttpContext.Current.Request.RawUrl), a_d, p, ((_ItemPeriod != null && (byte)_ItemPeriod > 0) ? String.Format("&ip={0}", _ItemPeriod) : String.Empty), orderMark));
            #endregion //Mark
            foreach (Enums.eu外幣存款利率抓取幣別 c in Enum.GetValues(typeof(Enums.eu外幣存款利率抓取幣別)))
            {
                sb.Append(String.Format("<th width='{0}%'{1}><a href='{2}.aspx?{3}={4}{5}{6}' title='鉅亨理財'>{7}{8}</a></th>", /*0*/((!(c == Enums.eu外幣存款利率抓取幣別.CAD || c == Enums.eu外幣存款利率抓取幣別.SGD || c == Enums.eu外幣存款利率抓取幣別.CHF)) ? 7 : 9), /*1*/classes[classesIndex++], /*2*/System.IO.Path.GetFileNameWithoutExtension(System.Web.HttpContext.Current.Request.RawUrl), /*3*/a_d, /*4*/((byte)c) + 2, /*5*/p, /*6*/((_ItemPeriod != null && (byte)_ItemPeriod > 0) ? String.Format("&ip={0}", _ItemPeriod) : String.Empty), /*7*/Enums.GetEnumDesc(c), /*8*/orderMark));
            }
            sb.Append("</tr>");
            sb.Append("</thead>");
            sb.Append("<tbody>");

            // 表格內容
            DataProvider._24.NewBankRate.Get外幣存款利率ByCountry_ReturnTable2Fields? orderField = null;
            if (_OrderIndex != null)
                orderField = (DataProvider._24.NewBankRate.Get外幣存款利率ByCountry_ReturnTable2Fields)((byte)_OrderIndex + 1);

            string[] periods = { "3個月", "1星期,7天", "1個月", "6個月", "9個月", "1年" };
            _24_NewBankRateProvider.Get外幣存款利率ByCountry(((_外幣存款利率Nav != Enums.eu外幣存款利率Navigations.綜合利率排行) ? Enums.GetEnumDesc(_外幣存款利率Nav) : String.Empty), ((_ItemPeriod != null) ? periods[(byte)_ItemPeriod - 1] : String.Empty), _PageIndex, _PageSize, orderField, _OrderDirection);

            if (_24_NewBankRateProvider.ResultDataSet.Tables.Count > 1)
            {
                foreach (DataRow dr in _24_NewBankRateProvider.ResultDataSet.Tables[1].Rows)
                {
                    classesIndex = 1;
                    sb.Append("<tr>");
                    sb.Append(String.Format("<td class='cnlt'><a href='{0}BankDetail/{1}.aspx' title='{2}'>{2}</a></td>", System.Web.HttpContext.Current.Request.Path.Substring(0, System.Web.HttpContext.Current.Request.Path.LastIndexOf('/') + 1), dr[DataProvider._24.NewBankRate.Get外幣存款利率ByCountry_ReturnTable2Fields.BankID.ToString()], dr[DataProvider._24.NewBankRate.Get外幣存款利率ByCountry_ReturnTable2Fields.BankName.ToString()]));
                    #region //Mark
                    //sb.Append(String.Format("<td{0}>{1}</td>", classes[classesIndex++], g.MyFloatConvert(dr[DataProvider._24.NewBankRate.Get外幣存款利率ByCountry_ReturnTable2Fields.Rate_USD.ToString()].ToString(), String.Empty, "--", String.Empty, 100)));
                    //sb.Append(String.Format("<td{0}>{1}</td>", classes[classesIndex++], g.MyFloatConvert(dr[DataProvider._24.NewBankRate.Get外幣存款利率ByCountry_ReturnTable2Fields.Rate_EUR.ToString()].ToString(), String.Empty, "--", String.Empty, 100)));
                    //sb.Append(String.Format("<td{0}>{1}</td>", classes[classesIndex++], g.MyFloatConvert(dr[DataProvider._24.NewBankRate.Get外幣存款利率ByCountry_ReturnTable2Fields.Rate_GBP.ToString()].ToString(), String.Empty, "--", String.Empty, 100)));
                    //sb.Append(String.Format("<td{0}>{1}</td>", classes[classesIndex++], g.MyFloatConvert(dr[DataProvider._24.NewBankRate.Get外幣存款利率ByCountry_ReturnTable2Fields.Rate_AUD.ToString()].ToString(), String.Empty, "--", String.Empty, 100)));
                    //sb.Append(String.Format("<td{0}>{1}</td>", classes[classesIndex++], g.MyFloatConvert(dr[DataProvider._24.NewBankRate.Get外幣存款利率ByCountry_ReturnTable2Fields.Rate_NZD.ToString()].ToString(), String.Empty, "--", String.Empty, 100)));
                    //sb.Append(String.Format("<td{0}>{1}</td>", classes[classesIndex++], g.MyFloatConvert(dr[DataProvider._24.NewBankRate.Get外幣存款利率ByCountry_ReturnTable2Fields.Rate_CAD.ToString()].ToString(), String.Empty, "--", String.Empty, 100)));
                    //sb.Append(String.Format("<td{0}>{1}</td>", classes[classesIndex++], g.MyFloatConvert(dr[DataProvider._24.NewBankRate.Get外幣存款利率ByCountry_ReturnTable2Fields.Rate_SGD.ToString()].ToString(), String.Empty, "--", String.Empty, 100)));
                    //sb.Append(String.Format("<td{0}>{1}</td>", classes[classesIndex++], g.MyFloatConvert(dr[DataProvider._24.NewBankRate.Get外幣存款利率ByCountry_ReturnTable2Fields.Rate_CHF.ToString()].ToString(), String.Empty, "--", String.Empty, 100)));
                    //sb.Append(String.Format("<td{0}>{1}</td>", classes[classesIndex++], g.MyFloatConvert(dr[DataProvider._24.NewBankRate.Get外幣存款利率ByCountry_ReturnTable2Fields.Rate_HKD.ToString()].ToString(), String.Empty, "--", String.Empty, 100)));
                    //sb.Append(String.Format("<td{0}>{1}</td>", classes[classesIndex++], g.MyFloatConvert(dr[DataProvider._24.NewBankRate.Get外幣存款利率ByCountry_ReturnTable2Fields.Rate_JPY.ToString()].ToString(), String.Empty, "--", String.Empty, 100)));
                    //sb.Append(String.Format("<td{0}>{1}</td>", classes[classesIndex++], g.MyFloatConvert(dr[DataProvider._24.NewBankRate.Get外幣存款利率ByCountry_ReturnTable2Fields.Rate_ZAR.ToString()].ToString(), String.Empty, "--", String.Empty, 100)));
                    foreach (Enums.eu外幣存款利率抓取幣別 c in Enum.GetValues(typeof(Enums.eu外幣存款利率抓取幣別)))
                    {
                        DataProvider._24.NewBankRate.Get外幣存款利率ByCountry_ReturnTable2Fields field = (DataProvider._24.NewBankRate.Get外幣存款利率ByCountry_ReturnTable2Fields)Enum.Parse(typeof(DataProvider._24.NewBankRate.Get外幣存款利率ByCountry_ReturnTable2Fields), String.Format("Rate_{0}", c));
                        sb.Append(String.Format("<td{0}>{1}</td>", classes[classesIndex++], g.MyFloatConvert(dr[field.ToString()].ToString(), String.Empty, "--", String.Empty, 100)));
                    }
                    #endregion //Mark
                    sb.Append("</tr>");
                }
            }
        }
        #endregion _外幣存款利率SubNav == Enums.eu外幣存款利率SubNavigations.All
        #region _外幣存款利率SubNav == Enums.eu外幣存款利率SubNavigations.Currency
        else
        {
            string url = String.Format("/Money/{0}{1}/{2}/{3}", System.IO.Path.GetFileNameWithoutExtension(Enums.GetEnumDesc(Enums.euBankServiceNavigations.外幣存款利率)), (byte)_外幣存款利率Nav, _外幣存款利率SubNav, _Currency);
            sb.Append(String.Format("<th width='20%'{0}><a href='{1}.aspx?{2}=1{3}' title='鉅亨理財'>銀行名稱{4}</a></th>", classes[classesIndex++], url, a_d, p, orderMark));
            sb.Append(String.Format("<th width='14%'{0}><a href='{1}.aspx?{2}=2{3}' title='鉅亨理財'>活期存款{4}</a></th>", classes[classesIndex++], url, a_d, p, orderMark));
            sb.Append(String.Format("<th width='14%'{0}><a href='{1}.aspx?{2}=3{3}' title='鉅亨理財'>1個月{4}</a></th>", classes[classesIndex++], url, a_d, p, orderMark));
            sb.Append(String.Format("<th width='13%'{0}><a href='{1}.aspx?{2}=4{3}' title='鉅亨理財'>3個月{4}</a></th>", classes[classesIndex++], url, a_d, p, orderMark));
            sb.Append(String.Format("<th width='13%'{0}><a href='{1}.aspx?{2}=5{3}' title='鉅亨理財'>6個月{4}</a></th>", classes[classesIndex++], url, a_d, p, orderMark));
            sb.Append(String.Format("<th width='13%'{0}><a href='{1}.aspx?{2}=6{3}' title='鉅亨理財'>9個月{4}</a></th>", classes[classesIndex++], url, a_d, p, orderMark));
            sb.Append(String.Format("<th width='13%'{0}><a href='{1}.aspx?{2}=7{3}' title='鉅亨理財'>1年{4}</a></th>", classes[classesIndex++], url, a_d, p, orderMark));
            sb.Append("</tr>");
            sb.Append("</thead>");
            sb.Append("<tbody>");

            // 表格內容
            DataProvider._24.NewBankRate.Get外幣存款利率ByCountry_Currency_ReturnTable2Fields? orderField = null;
            if (_OrderIndex != null)
                orderField = (DataProvider._24.NewBankRate.Get外幣存款利率ByCountry_Currency_ReturnTable2Fields)((byte)_OrderIndex + 1);

            _24_NewBankRateProvider.Get外幣存款利率ByCountry_Currency(((_外幣存款利率Nav != Enums.eu外幣存款利率Navigations.綜合利率排行) ? Enums.GetEnumDesc(_外幣存款利率Nav) : String.Empty), _Currency, _PageIndex, _PageSize, orderField, _OrderDirection);

            if (_24_NewBankRateProvider.ResultDataSet.Tables.Count > 1)
            {
                foreach (DataRow dr in _24_NewBankRateProvider.ResultDataSet.Tables[1].Rows)
                {
                    classesIndex = 1;
                    sb.Append("<tr>");
                    sb.Append(String.Format("<td class='cnlt'><a href='{0}BankDetail/{1}.aspx' title='{2}'>{2}</a></td>", System.Web.HttpContext.Current.Request.Path.Substring(0, System.Web.HttpContext.Current.Request.Path.LastIndexOf('/') + 1), dr[DataProvider._24.NewBankRate.Get外幣存款利率ByCountry_ReturnTable2Fields.BankID.ToString()], dr[DataProvider._24.NewBankRate.Get外幣存款利率ByCountry_ReturnTable2Fields.BankName.ToString()]));
                    sb.Append(String.Format("<td{0}>{1}</td>", classes[classesIndex++], g.MyFloatConvert(dr[DataProvider._24.NewBankRate.Get外幣存款利率ByCountry_Currency_ReturnTable2Fields.Rate1.ToString()].ToString(), String.Empty, "--", String.Empty, 100)));
                    sb.Append(String.Format("<td{0}>{1}</td>", classes[classesIndex++], g.MyFloatConvert(dr[DataProvider._24.NewBankRate.Get外幣存款利率ByCountry_Currency_ReturnTable2Fields.Rate2.ToString()].ToString(), String.Empty, "--", String.Empty, 100)));
                    sb.Append(String.Format("<td{0}>{1}</td>", classes[classesIndex++], g.MyFloatConvert(dr[DataProvider._24.NewBankRate.Get外幣存款利率ByCountry_Currency_ReturnTable2Fields.Rate3.ToString()].ToString(), String.Empty, "--", String.Empty, 100)));
                    sb.Append(String.Format("<td{0}>{1}</td>", classes[classesIndex++], g.MyFloatConvert(dr[DataProvider._24.NewBankRate.Get外幣存款利率ByCountry_Currency_ReturnTable2Fields.Rate4.ToString()].ToString(), String.Empty, "--", String.Empty, 100)));
                    sb.Append(String.Format("<td{0}>{1}</td>", classes[classesIndex++], g.MyFloatConvert(dr[DataProvider._24.NewBankRate.Get外幣存款利率ByCountry_Currency_ReturnTable2Fields.Rate5.ToString()].ToString(), String.Empty, "--", String.Empty, 100)));
                    sb.Append(String.Format("<td{0}>{1}</td>", classes[classesIndex++], g.MyFloatConvert(dr[DataProvider._24.NewBankRate.Get外幣存款利率ByCountry_Currency_ReturnTable2Fields.Rate6.ToString()].ToString(), String.Empty, "--", String.Empty, 100)));
                    sb.Append("</tr>");
                }
            }
        }
        #endregion _外幣存款利率SubNav == Enums.eu外幣存款利率SubNavigations.Currency

        sb.Append("</tbody>");
        sb.Append("</table>");

        // 分頁
        if (_24_NewBankRateProvider.ResultDataSet.Tables.Count > 0)
        {
            int totalPage = 1;
            int.TryParse(_24_NewBankRateProvider.ResultDataSet.Tables[0].Rows[0][DataProvider._24.Bank.ReturnTable1_CommonFields.TotalPage.ToString()].ToString(), out totalPage);

            string baseURL = String.Format("?{0}{1}", ((_OrderDirection != null && _OrderIndex != null) ? ((((Enums.euOrderDirection)_OrderDirection) == Enums.euOrderDirection.ASC) ? String.Format("a={0}", _OrderIndex) : String.Format("d={0}", _OrderIndex)) : String.Empty), ((_ItemPeriod != null && (byte)_ItemPeriod > 0) ? String.Format("&ip={0}", _ItemPeriod) : String.Empty)) + "&p={0}";
            sb.Append(g.BankServicePager(totalPage, _PageIndex, 10, baseURL.Replace("?&", "?")));
        }
        sb.Append("</div><!-- fmTb:end -->");

        return sb.ToString();
    }
    #endregion string Create ()
}
