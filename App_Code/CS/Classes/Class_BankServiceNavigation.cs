﻿using System;
using System.Data;
using System.Configuration;

/// <summary>
/// Class_BankServiceNavigation 的摘要描述
/// </summary>
public class Class_BankServiceNavigation
{
    #region 變數
    private Enums.euBankServiceNavigations _BankServiceNav = Enums.euBankServiceNavigations.牌告利率;

    private Enums.eu牌告利率Navigations _牌告利率Nav = Enums.eu牌告利率Navigations.None;

    private Enums.eu外幣存款利率Navigations _外幣存款利率Nav = Enums.eu外幣存款利率Navigations.None;

    private Enums.eu外幣存款利率SubNavigations _外幣存款利率SubNav = Enums.eu外幣存款利率SubNavigations.All;

    private Enums.eu存款利率Navigations _存款利率Nav = Enums.eu存款利率Navigations.None;

    private Enums.eu銀行換匯比較表Navigations _銀行換匯比較表Nav = Enums.eu銀行換匯比較表Navigations.None;

    private byte _SubIndex = 1;

    private Enums.euBankServiceDetail台灣地區 _BankServiceDetail台灣地區 = Enums.euBankServiceDetail台灣地區.是;

    private Enums.euBankMoneyExchangeType _BankMoneyExchangeType = Enums.euBankMoneyExchangeType.Bank;

    private string _BCode = String.Empty;

    private string _Currency = String.Empty;

    private string _BName = String.Empty;

    private string _CurrencyName = String.Empty;
    #endregion 變數

    #region 屬性

    #region Enums.euBankServiceNavigations BankServiceNav
    public Enums.euBankServiceNavigations BankServiceNav
    {
        set
        {
            _BankServiceNav = value;
        }
        get
        {
            return _BankServiceNav;
        }
    }
    #endregion euBankServiceNavigations BankServiceNav

    #region Enums.eu牌告利率Navigations 牌告利率Nav
    public Enums.eu牌告利率Navigations 牌告利率Nav
    {
        set
        {
            _牌告利率Nav = value;
        }
        get
        {
            return _牌告利率Nav;
        }
    }
    #endregion eu牌告利率Navigations 牌告利率Nav

    #region Enums.eu外幣存款利率Navigations 外幣存款利率Nav
    public Enums.eu外幣存款利率Navigations 外幣存款利率Nav
    {
        set
        {
            _外幣存款利率Nav = value;
        }
        get
        {
            return _外幣存款利率Nav;
        }
    }
    #endregion Enums.eu外幣存款利率Navigations 外幣存款利率Nav

    #region Enums.eu外幣存款利率SubNavigations 外幣存款利率SubNav
    public Enums.eu外幣存款利率SubNavigations 外幣存款利率SubNav
    {
        set
        {
            _外幣存款利率SubNav = value;
        }
        get
        {
            return _外幣存款利率SubNav;
        }
    }
    #endregion Enums.eu外幣存款利率SubNavigations 外幣存款利率SubNav

    #region Enums.eu存款利率Navigations 存款利率Nav
    public Enums.eu存款利率Navigations 存款利率Nav
    {
        set
        {
            _存款利率Nav = value;
        }
        get
        {
            return _存款利率Nav;
        }
    }
    #endregion Enums.eu存款利率Navigations 存款利率Nav

    #region Enums.eu銀行換匯比較表Navigations 銀行換匯比較表Nav
    public Enums.eu銀行換匯比較表Navigations 銀行換匯比較表Nav
    {
        set
        {
            _銀行換匯比較表Nav = value;
        }
        get
        {
            return _銀行換匯比較表Nav;
        }
    }
    #endregion Enums.eu銀行換匯比較表Navigations 銀行換匯比較表Nav

    #region byte SubIndex
    public byte SubIndex
    {
        set
        {
            _SubIndex = value;
        }
        get
        {
            return _SubIndex;
        }
    }
    #endregion byte SubIndex

    #region Enums.euBankServiceDetail台灣地區 IsBankServiceDetail台灣地區
    public Enums.euBankServiceDetail台灣地區 IsBankServiceDetail台灣地區
    {
        set
        {
            _BankServiceDetail台灣地區 = value;
        }
        get
        {
            return _BankServiceDetail台灣地區;
        }
    }
    #endregion Enums.euBankServiceDetail台灣地區 IsBankServiceDetail台灣地區

    #region Enums.euBankMoneyExchangeType BankMoneyExchangeType
    public Enums.euBankMoneyExchangeType BankMoneyExchangeType
    {
        set
        {
            _BankMoneyExchangeType = value;
        }
        get
        {
            return _BankMoneyExchangeType;
        }
    }
    #endregion Enums.euBankMoneyExchangeType BankMoneyExchangeType

    #region string BCode
    public string BCode
    {
        set
        {
            _BCode = value;
        }
        get
        {
            return _BCode;
        }
    }
    #endregion string BCode

    #region string Currency
    public string Currency
    {
        set
        {
            _Currency = value;
        }
    }
    #endregion string Currency

    #region string BName
    public string BName
    {
        get
        {
            return _BName;
        }
    }
    #endregion string BName

    #region string CurrencyName
    public string CurrencyName
    {
        get
        {
            return _CurrencyName;
        }
    }
    #endregion string CurrencyName

    #endregion 屬性

    public Class_BankServiceNavigation()
	{
		//
		// TODO: 在此加入建構函式的程式碼
		//
    }

    #region string Create (Enums.euBankServiceNavigations bankServiceNav, Enums.eu牌告利率Navigations __牌告利率Nav, Enums.eu外幣存款利率Navigations __外幣存款利率Nav, Enums.eu存款利率Navigations __存款利率Nav, Enums.eu銀行換匯比較表Navigations __銀行換匯比較表Nav, byte subIndex)
    public string Create (Enums.euBankServiceNavigations bankServiceNav, Enums.eu牌告利率Navigations __牌告利率Nav, Enums.eu外幣存款利率Navigations __外幣存款利率Nav, Enums.eu存款利率Navigations __存款利率Nav, Enums.eu銀行換匯比較表Navigations __銀行換匯比較表Nav, byte subIndex)
    {
        _BankServiceNav = bankServiceNav;
        _牌告利率Nav = __牌告利率Nav;
        _外幣存款利率Nav = __外幣存款利率Nav;
        _存款利率Nav = __存款利率Nav;
        _銀行換匯比較表Nav = __銀行換匯比較表Nav;
        _SubIndex = subIndex;

        return Create();
    }
    #endregion string Create (Enums.euBankServiceNavigations bankServiceNav, Enums.eu牌告利率Navigations __牌告利率Nav, Enums.eu外幣存款利率Navigations __外幣存款利率Nav, Enums.eu存款利率Navigations __存款利率Nav, Enums.eu銀行換匯比較表Navigations __銀行換匯比較表Nav, byte subIndex)

    #region string Create ()
    public string Create ()
    {
        System.Text.StringBuilder sb = new System.Text.StringBuilder();

        if (_牌告利率Nav == Enums.eu牌告利率Navigations.None && _外幣存款利率Nav == Enums.eu外幣存款利率Navigations.None && _存款利率Nav == Enums.eu存款利率Navigations.None && _銀行換匯比較表Nav == Enums.eu銀行換匯比較表Navigations.None)
            _牌告利率Nav = Enums.eu牌告利率Navigations.銀行牌告利率;

        #region 頁籤

        #region _BankServiceNav != Enums.euBankServiceNavigations.銀行換匯
        if (_BankServiceNav != Enums.euBankServiceNavigations.銀行換匯)
        {
            sb.Append(String.Format("<ul class='hdNavs wd{0}'>", ((_BankServiceNav != Enums.euBankServiceNavigations.Detail) ? "2in" : "pd")));

            if (_BankServiceNav != Enums.euBankServiceNavigations.放款利率)
            {
                byte count = 0;

                if (Enums.euBankServiceNavigations.Detail == _BankServiceNav)
                {
                    if (_BankServiceDetail台灣地區 == Enums.euBankServiceDetail台灣地區.是)
                    {
                        sb.Append("<li><a href='javascript:;'>台幣</a></li>");
                        sb.Append("<li><a href='javascript:;'>外幣</a></li>");
                    }
                }
                else if (Enums.eu牌告利率Navigations.None != _牌告利率Nav)
                {
                    foreach (Enums.eu牌告利率Navigations bnk in Enum.GetValues(typeof(Enums.eu牌告利率Navigations)))
                    {
                        if (bnk == Enums.eu牌告利率Navigations.None)
                            continue;

                        if (bnk == Enums.eu牌告利率Navigations.基準利率)
                            break;
                        sb.Append(String.Format("<li{0}><a href='{1}{2}{3}.htm'>{4}</a></li>", ((((byte)1 == _SubIndex && bnk == Enums.eu牌告利率Navigations.銀行牌告利率) || (_SubIndex >= (byte)Enums.eu牌告利率Navigations.各項業務利率 && bnk != Enums.eu牌告利率Navigations.銀行牌告利率)) ? " class='current2'" : String.Empty), System.Web.HttpContext.Current.Request.Path.Substring(0, System.Web.HttpContext.Current.Request.Path.LastIndexOf('/') + 1), System.IO.Path.GetFileNameWithoutExtension(Enums.GetEnumDesc(Enums.euBankServiceNavigations.牌告利率)), ++count, bnk));
                    }
                }
                else if (_外幣存款利率Nav != Enums.eu外幣存款利率Navigations.None)
                {
                    foreach (Enums.eu外幣存款利率Navigations bnk in Enum.GetValues(typeof(Enums.eu外幣存款利率Navigations)))
                    {
                        if (bnk == Enums.eu外幣存款利率Navigations.None)
                            continue;
                        sb.Append(String.Format("<li{0}><a href='{1}{2}{3}.htm'>{4}</a></li>", ((_外幣存款利率Nav == bnk) ? " class='current2'" : String.Empty), System.Web.HttpContext.Current.Request.Path.Substring(0, System.Web.HttpContext.Current.Request.Path.LastIndexOf('/') + 1), System.IO.Path.GetFileNameWithoutExtension(Enums.GetEnumDesc(Enums.euBankServiceNavigations.外幣存款利率)), ++count, bnk));
                    }
                }
                else if (_存款利率Nav != Enums.eu存款利率Navigations.None)
                {
                    foreach (Enums.eu存款利率Navigations bnk in Enum.GetValues(typeof(Enums.eu存款利率Navigations)))
                    {
                        if (bnk == Enums.eu存款利率Navigations.None)
                            continue;
                        sb.Append(String.Format("<li{0}><a href='{1}{2}{3}.htm'>{4}</a></li>", ((_存款利率Nav == bnk) ? " class='current2'" : String.Empty), System.Web.HttpContext.Current.Request.Path.Substring(0, System.Web.HttpContext.Current.Request.Path.LastIndexOf('/') + 1), System.IO.Path.GetFileNameWithoutExtension(Enums.GetEnumDesc(Enums.euBankServiceNavigations.存款利率)), ++count, bnk));
                    }
                }
                else if (_銀行換匯比較表Nav != Enums.eu銀行換匯比較表Navigations.None)
                {
                    foreach (Enums.eu銀行換匯比較表Navigations bnk in Enum.GetValues(typeof(Enums.eu銀行換匯比較表Navigations)))
                    {
                        if (bnk == Enums.eu銀行換匯比較表Navigations.None)
                            continue;
                        sb.Append(String.Format("<li{0}><a href='{1}{2}{3}.htm'>{4}</a></li>", ((_銀行換匯比較表Nav == bnk) ? " class='current2'" : String.Empty), System.Web.HttpContext.Current.Request.Path.Substring(0, System.Web.HttpContext.Current.Request.Path.LastIndexOf('/') + 1), System.IO.Path.GetFileNameWithoutExtension(Enums.GetEnumDesc(Enums.euBankServiceNavigations.銀行換匯比較表)), ++count, bnk));
                    }
                }
            }
            sb.Append("</ul><!-- hdNavs wd2in:end -->");

            if (_BankServiceNav == Enums.euBankServiceNavigations.牌告利率 && _SubIndex == 1)
                sb.Append("<div class='blank'></div>");
        }
        #endregion _BankServiceNav != Enums.euBankServiceNavigations.銀行換匯
        #region _BankServiceNav == Enums.euBankServiceNavigations.銀行換匯
        else
        {
            sb.Append(String.Format("<div class='banksrch'><select id='Banks'{0}><option value=''>依銀行名稱查詢</option>", ((_BankMoneyExchangeType == Enums.euBankMoneyExchangeType.Bank) ? " class='onr'" : String.Empty)));

            DataProvider._24.Bank_all _24_Bank_all_Dp = new DataProvider._24.Bank_all();
            _24_Bank_all_Dp.GetAllBanksCodeNameCash(_BCode, _Currency);

            //string tmp = String.Empty;

            foreach (DataRow dr in _24_Bank_all_Dp.ResultDataSet.Tables[0].Rows)
            {
                //if (_BCode != null && _BCode.Length != 0 && _BCode.Equals(dr[DataProvider._24.Bank_all.GetAllBanksCodeNameCash_ReturnTable1Fields.BCode.ToString()].ToString(), StringComparison.InvariantCultureIgnoreCase))
                //    tmp = dr[DataProvider._24.Bank_all.GetAllBanksCodeNameCash_ReturnTable1Fields.BName.ToString()].ToString();

                sb.Append(String.Format("<option value='{0}'{1}>{2}</option>", dr[DataProvider._24.Bank_all.GetAllBanksCodeNameCash_ReturnTable1Fields.BCode.ToString()], ((_BCode != null && _BCode.Length != 0 && _BCode.Equals(dr[DataProvider._24.Bank_all.GetAllBanksCodeNameCash_ReturnTable1Fields.BCode.ToString()].ToString(), StringComparison.InvariantCultureIgnoreCase)) ? " selected='selected'" : String.Empty), dr[DataProvider._24.Bank_all.GetAllBanksCodeNameCash_ReturnTable1Fields.BName.ToString()]));

                if (_BCode != null && _BCode.Length != 0 && _BCode.Equals(dr[DataProvider._24.Bank_all.GetAllBanksCodeNameCash_ReturnTable1Fields.BCode.ToString()].ToString(), StringComparison.InvariantCultureIgnoreCase))
                    _BName = dr[DataProvider._24.Bank_all.GetAllBanksCodeNameCash_ReturnTable1Fields.BName.ToString()].ToString();
            }

            sb.Append(String.Format("</select><select id='Currencys'{0}><option value=''>依幣別查詢</option>", ((_BankMoneyExchangeType == Enums.euBankMoneyExchangeType.Currency) ? " class='onr'" : String.Empty)));

            foreach (DataRow dr in _24_Bank_all_Dp.ResultDataSet.Tables[1].Rows)
            {
                //if (_Currency != null && _Currency.Length != 0 && _Currency.Equals(dr[DataProvider._24.Bank_all.GetAllBanksCodeNameCash_ReturnTable2Fields.Cash.ToString()].ToString(), StringComparison.InvariantCultureIgnoreCase))
                //    tmp = _Currency + "-" + Enums.GetEnumDesc((Enums.euCurrency)Enum.Parse(typeof(Enums.euCurrency), dr[DataProvider._24.Bank_all.GetAllBanksCodeNameCash_ReturnTable2Fields.Cash.ToString()].ToString()));

                sb.Append(String.Format("<option value='{0}'{1}>{2}</option>", dr[DataProvider._24.Bank_all.GetAllBanksCodeNameCash_ReturnTable2Fields.Cash.ToString()], ((_Currency != null && _Currency.Length != 0 && _Currency.Equals(dr[DataProvider._24.Bank_all.GetAllBanksCodeNameCash_ReturnTable2Fields.Cash.ToString()].ToString(), StringComparison.InvariantCultureIgnoreCase)) ? " selected='selected'" : String.Empty), ((Enum.IsDefined(typeof(Enums.euCurrency), dr[DataProvider._24.Bank_all.GetAllBanksCodeNameCash_ReturnTable2Fields.Cash.ToString()].ToString())) ? Enums.GetEnumDesc((Enums.euCurrency)Enum.Parse(typeof(Enums.euCurrency), dr[DataProvider._24.Bank_all.GetAllBanksCodeNameCash_ReturnTable2Fields.Cash.ToString()].ToString())) : dr[DataProvider._24.Bank_all.GetAllBanksCodeNameCash_ReturnTable2Fields.Cash.ToString()])));

                if (_Currency != null && _Currency.Length != 0 && _Currency.Equals(dr[DataProvider._24.Bank_all.GetAllBanksCodeNameCash_ReturnTable2Fields.Cash.ToString()].ToString(), StringComparison.InvariantCultureIgnoreCase))
                    _CurrencyName = ((Enum.IsDefined(typeof(Enums.euCurrency), dr[DataProvider._24.Bank_all.GetAllBanksCodeNameCash_ReturnTable2Fields.Cash.ToString()].ToString())) ? Enums.GetEnumDesc((Enums.euCurrency)Enum.Parse(typeof(Enums.euCurrency), dr[DataProvider._24.Bank_all.GetAllBanksCodeNameCash_ReturnTable2Fields.Cash.ToString()].ToString())) : dr[DataProvider._24.Bank_all.GetAllBanksCodeNameCash_ReturnTable2Fields.Cash.ToString()].ToString());
            }

            sb.Append("</select></div><!-- banksrch:end -->");

            if (_24_Bank_all_Dp.ResultDataSet.Tables.Count > 2 && _24_Bank_all_Dp.ResultDataSet.Tables[2].Rows.Count == 1)
                sb.Append(String.Format("<div class='hd'><span class='dataInfo'>{0:yyyy-MM-dd HH:mm}</span></div>", Convert.ToDateTime(_24_Bank_all_Dp.ResultDataSet.Tables[2].Rows[0][0].ToString())));
        }
        #endregion _BankServiceNav == Enums.euBankServiceNavigations.銀行換匯

        #endregion 頁籤

        #region 頁籤下子選單
        if (_BankServiceNav == Enums.euBankServiceNavigations.外幣存款利率)
        {
            sb.Append("<div class='hdInNavs'>");
            foreach (Enums.eu外幣存款利率SubNavigations subNav in Enum.GetValues(typeof(Enums.eu外幣存款利率SubNavigations)))
            {
                if (subNav == Enums.eu外幣存款利率SubNavigations.Currency)
                    sb.Append("| ");

                sb.Append(String.Format("<a href='{0}{1}{2}/{3}.aspx'{4}>{5}</a>", System.Web.HttpContext.Current.Request.Path.Substring(0, System.Web.HttpContext.Current.Request.Path.LastIndexOf('/') + 1), System.IO.Path.GetFileNameWithoutExtension(Enums.GetEnumDesc(_BankServiceNav)), (byte)_外幣存款利率Nav, subNav, ((subNav == _外幣存款利率SubNav) ? " class='hinBtn'" : String.Empty), Enums.GetEnumDesc(subNav)));
            }
            sb.Append("</div><!-- hdInNavs:end -->");
        }
        #endregion 頁籤下子選單

        if (_BankServiceNav == Enums.euBankServiceNavigations.Detail || _BankServiceNav == Enums.euBankServiceNavigations.銀行換匯比較表)
            sb.Append(String.Format("<span class='dataInfo mRt'>{0}</span>", ((_BankServiceNav == Enums.euBankServiceNavigations.Detail) ? String.Format("<a href='../{0}'>回上一頁</a>", Enums.GetEnumDesc(Enums.euBankServiceNavigations.牌告利率)) : "單位：元")));
            //sb.Append(String.Format("<span class='dataInfo mRt'><a href='../{0}'>回上一頁</a></span>", Enums.GetEnumDesc(Enums.euBankServiceNavigations.牌告利率)));

        return sb.ToString();
    }
    #endregion string Create ()
}
