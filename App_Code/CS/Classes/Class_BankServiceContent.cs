﻿using System;
using System.Data;
using System.Configuration;

/// <summary>
/// Class_BankServiceContent 的摘要描述
/// </summary>
public class Class_BankServiceContent
{
    #region 變數
    private Enums.euBankServiceNavigations _BankServiceNav = Enums.euBankServiceNavigations.外幣存款利率;

    private Enums.eu外幣存款利率Navigations _外幣存款利率Nav = Enums.eu外幣存款利率Navigations.None;

    private Enums.eu外幣存款利率SubNavigations _外幣存款利率SubNav = Enums.eu外幣存款利率SubNavigations.All;

    private string _Currency = String.Empty;

    private byte? _ItemPeriod = null;

    private Enums.eu存款利率Navigations _存款利率Nav = Enums.eu存款利率Navigations.None;

    private Enums.eu銀行換匯比較表Navigations _銀行換匯比較表Nav = Enums.eu銀行換匯比較表Navigations.None;

    private byte _SubIndex = 1;

    private byte _PageIndex = 1;

    private byte _PageSize = 25;

    private Enums.euRateType _RateType = Enums.euRateType.機動;

    private Enums.euOrderDirection? _OrderDirection = null;

    private byte? _OrderIndex = null;

    private string _Keyword = null;

    private string _BCode = String.Empty;

    private string _BName = String.Empty;

    private Enums.euBankServiceDetail台灣地區 _IsBankServiceDetail台灣地區 = Enums.euBankServiceDetail台灣地區.是;

    private string _FootJs = String.Empty;

    private Enums.euBankMoneyExchangeType _BankMoneyExchangeType = Enums.euBankMoneyExchangeType.Bank;
    #endregion 變數

    #region 屬性

    #region Enums.euBankServiceNavigations BankServiceNav
    public Enums.euBankServiceNavigations BankServiceNav
    {
        set
        {
            _BankServiceNav = value;
        }
        get
        {
            return _BankServiceNav;
        }
    }
    #endregion Enums.euBankServiceNavigations BankServiceNav

    #region Enums.eu外幣存款利率Navigations 外幣存款利率Nav
    public Enums.eu外幣存款利率Navigations 外幣存款利率Nav
    {
        set
        {
            _外幣存款利率Nav = value;
        }
        get
        {
            return _外幣存款利率Nav;
        }
    }
    #endregion Enums.eu外幣存款利率Navigations 外幣存款利率Nav

    #region Enums.eu外幣存款利率SubNavigations 外幣存款利率SubNav
    public Enums.eu外幣存款利率SubNavigations 外幣存款利率SubNav
    {
        set
        {
            _外幣存款利率SubNav = value;
        }
        get
        {
            return _外幣存款利率SubNav;
        }
    }
    #endregion Enums.eu外幣存款利率SubNavigations 外幣存款利率SubNav

    #region string Currency
    public string Currency
    {
        set
        {
            _Currency = value;
        }
    }
    #endregion string Currency

    #region byte? ItemPeriod
    public byte? ItemPeriod
    {
        set
        {
            _ItemPeriod = value;
        }
        get
        {
            return _ItemPeriod;
        }
    }
    #endregion byte? ItemPeriod

    #region Enums.eu存款利率Navigations 存款利率Nav
    public Enums.eu存款利率Navigations 存款利率Nav
    {
        set
        {
            _存款利率Nav = value;
        }
        get
        {
            return _存款利率Nav;
        }
    }
    #endregion Enums.eu存款利率Navigations 存款利率Nav

    #region Enums.eu銀行換匯比較表Navigations 銀行換匯比較表Nav
    public Enums.eu銀行換匯比較表Navigations 銀行換匯比較表Nav
    {
        set
        {
            _銀行換匯比較表Nav = value;
        }
        get
        {
            return _銀行換匯比較表Nav;
        }
    }
    #endregion Enums.eu銀行換匯比較表Navigations 銀行換匯比較表Nav

    #region byte SubIndex
    public byte SubIndex
    {
        set
        {
            _SubIndex = value;
        }
        get
        {
            return _SubIndex;
        }
    }
    #endregion byte SubIndex

    #region byte PageIndex
    public byte PageIndex
    {
        set
        {
            _PageIndex = value;
        }
    }
    #endregion byte PageIndex

    #region byte PageSize
    public byte PageSize
    {
        set
        {
            _PageSize = value;
        }
    }
    #endregion byte PageSize

    #region Enums.euRateType RateType
    public Enums.euRateType RateType
    {
        set
        {
            _RateType = value;
        }
        get
        {
            return _RateType;
        }
    }
    #endregion Enums.euRateType RateType

    #region Enums.euOrderDirection? OrderDirection
    public Enums.euOrderDirection? OrderDirection
    {
        set
        {
            _OrderDirection = value;
        }
        get
        {
            return _OrderDirection;
        }
    }
    #endregion Enums.euOrderDirection? OrderDirection

    #region byte? OrderIndex
    public byte? OrderIndex
    {
        set
        {
            _OrderIndex = value;
        }
        get
        {
            return _OrderIndex;
        }
    }
    #endregion byte? OrderIndex

    #region string Keyword
    public string Keyword
    {
        set
        {
            _Keyword = value;
        }
        get
        {
            return _Keyword;
        }
    }
    #endregion string Keyword

    #region string BCode
    public string BCode
    {
        set
        {
            _BCode = value;
        }
        get
        {
            return _BCode;
        }
    }
    #endregion string BCode

    #region string BName
    public string BName
    {
        get
        {
            return _BName;
        }
    }
    #endregion string BName

    #region Enums.euBankServiceDetail台灣地區 IsBankServiceDetail台灣地區
    public Enums.euBankServiceDetail台灣地區 IsBankServiceDetail台灣地區
    {
        get
        {
            return _IsBankServiceDetail台灣地區;
        }
    }
    #endregion Enums.euBankServiceDetail台灣地區 IsBankServiceDetail台灣地區

    #region string FootJs
    public string FootJs
    {
        get
        {
            return _FootJs;
        }
    }
    #endregion string FootJs

    #region Enums.euBankMoneyExchangeType BankMoneyExchangeType
    public Enums.euBankMoneyExchangeType BankMoneyExchangeType
    {
        set
        {
            _BankMoneyExchangeType = value;
        }
        get
        {
            return _BankMoneyExchangeType;
        }
    }
    #endregion Enums.euBankMoneyExchangeType BankMoneyExchangeType

    #endregion 屬性

    public Class_BankServiceContent()
	{
		//
		// TODO: 在此加入建構函式的程式碼
		//
    }

    #region string Create ()
    public string Create ()
    {
        //WriteLog.Log<string>("Class_BankServiceContent _SubIndex", _SubIndex.ToString());
        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        switch (_BankServiceNav)
        {
            case Enums.euBankServiceNavigations.牌告利率:
                if (1 == _SubIndex)
                    sb.Append((new Class_Content牌告利率1()).Create());
                else
                    sb.Append((new Class_Content牌告利率2_and_Up()).Create(_SubIndex));
                break;
            case Enums.euBankServiceNavigations.外幣存款利率:
                //if (3 == _SubIndex && _外幣存款利率Nav == Enums.eu外幣存款利率Navigations.台灣利率排行)
                //    sb.Append((new Class_Content外幣存款利率3()).Create());
                Class_Content外幣存款利率1 content外幣存款利率1 = new Class_Content外幣存款利率1();
                content外幣存款利率1.外幣存款利率Nav = _外幣存款利率Nav;
                content外幣存款利率1.外幣存款利率SubNav = _外幣存款利率SubNav;
                content外幣存款利率1.Currency = _Currency;
                content外幣存款利率1.PageIndex = _PageIndex;
                content外幣存款利率1.PageSize = _PageSize;
                content外幣存款利率1.OrderDirection = _OrderDirection;
                content外幣存款利率1.OrderIndex = _OrderIndex;
                content外幣存款利率1.ItemPeriod = _ItemPeriod;

                sb.Append(content外幣存款利率1.Create());
                _FootJs = content外幣存款利率1.FootJs;
                break;
            case Enums.euBankServiceNavigations.存款利率:
                #region //Mark
                //switch (String.Format("Content{0}{1}", _BankServiceNav, _SubIndex))
                //{
                //    case "Content存款利率1":
                //        sb.Append((new Class_Content存款利率1()).Create(_存款利率Nav, _PageIndex, _PageSize, _RateType, _OrderDirection, _OrderIndex, _Keyword));
                //        break;
                //    case "Content存款利率2":
                //    case "Content存款利率3":
                //        sb.Append((new Class_Content存款利率3_4_5()).Create(_PageIndex, _PageSize, _OrderDirection, (Enums.euDepositRate13_14_15_Item)(_SubIndex - 3)));
                //        break;
                //}
                #endregion //Mark
                if (_存款利率Nav == Enums.eu存款利率Navigations.台灣利率排行)
                    sb.Append((new Class_Content存款利率1()).Create(_存款利率Nav, _PageIndex, _PageSize, _RateType, _OrderDirection, _OrderIndex, _Keyword));
                else
                    sb.Append((new Class_Content存款利率2_3()).Create(_存款利率Nav, _PageIndex, _PageSize, _OrderDirection, _OrderIndex));
                break;
            case Enums.euBankServiceNavigations.放款利率:
                sb.Append((new Class_Content放款利率1()).Create(_PageIndex, _PageSize, _OrderDirection, _OrderIndex));
                break;
            case Enums.euBankServiceNavigations.銀行換匯:
                Class_Content銀行換匯 content銀行換匯 = new Class_Content銀行換匯();
                if ((_BCode != null && _BCode.Length > 0) || (_Currency != null && _Currency.Length > 0))
                {
                    content銀行換匯.Type = _BankMoneyExchangeType;
                    content銀行換匯.BCode = _BCode;
                    content銀行換匯.Currency = _Currency;
                    content銀行換匯.OrderDirection = _OrderDirection;
                    content銀行換匯.OrderIndex = _OrderIndex;
                    sb.Append(content銀行換匯.Create());
                }

                _FootJs = content銀行換匯.FootJs;
                break;
            case Enums.euBankServiceNavigations.銀行換匯比較表:
                Class_Content銀行換匯比較表 content銀行換匯比較表 = new Class_Content銀行換匯比較表();
                content銀行換匯比較表.OrderDirection = _OrderDirection;
                content銀行換匯比較表.OrderIndex = _OrderIndex;
                content銀行換匯比較表.SubIndex = _SubIndex;
                sb.Append(content銀行換匯比較表.Create());
                break;
            case Enums.euBankServiceNavigations.Detail:
                Class_Content銀行Detail detail = new Class_Content銀行Detail(_BCode);
                //detail.SubIndex = _SubIndex;
                _BName = detail.BName;
                _IsBankServiceDetail台灣地區 = detail.IsBankServiceDetail台灣地區;
                sb.Append(detail.Create());
                break;
        }

        return sb.ToString();
    }
    #endregion string Create ()
}
