﻿using System;
using System.Data;
using System.Configuration;

/// <summary>
/// Class_Content牌告利率2_and_Up 的摘要描述
/// </summary>
public class Class_Content牌告利率2_and_Up
{
    #region 變數
    private byte _SubIndex = 1;
    #endregion 變數

    #region 屬性

    #region byte SubIndex
    public byte SubIndex
    {
        set
        {
            _SubIndex = value;
        }
    }
    #endregion byte SubIndex

    #endregion 屬性

    public Class_Content牌告利率2_and_Up ()
    {
        //
        // TODO: 在此加入建構函式的程式碼
        //
    }

    #region string Create (byte subIndex)
    public string Create (byte subIndex)
    {
        _SubIndex = subIndex;
        return Create();
    }
    #endregion string Create (byte subIndex)

    #region string Create ()
    public string Create ()
    {
        System.Text.StringBuilder sb = new System.Text.StringBuilder();

        if (_SubIndex <= (byte)Enums.eu牌告利率Navigations.各項業務利率)
            _SubIndex = (byte)Enums.eu牌告利率Navigations.基準利率;

        #region 最上面導覽區塊
        sb.Append("<div class='blank'></div>");
        sb.Append("<div class='fmTb2 mgbm'>");
        sb.Append("<table cellspacing='0' cellpadding='0'>");

        string name = System.IO.Path.GetFileNameWithoutExtension(Enums.GetEnumDesc(Enums.euBankServiceNavigations.牌告利率));
        for (byte i = 3; i <= (byte)Enums.eu牌告利率Navigations.現金卡最低利率; i++)
        {
            if ((i - 2) % 4 == 1)
                sb.Append("<tr>");

            if (i != 17 && ((Enums.eu牌告利率Navigations)i) != Enums.eu牌告利率Navigations.台灣郵政儲金利率)
            {
                sb.Append(String.Format("<td{0}><a href='{1}{2}{3}.aspx'{4} title='{5}'>{5}</a></td>", (((i - 2) % 4 == 0) ? " class='nTdno'" : String.Empty), System.Web.HttpContext.Current.Request.Path.Substring(0, System.Web.HttpContext.Current.Request.Path.LastIndexOf('/') + 1), name, i, ((i == _SubIndex) ? " class='onr'" : String.Empty), (Enums.eu牌告利率Navigations)i));
            }
            else if (((Enums.eu牌告利率Navigations)i) == Enums.eu牌告利率Navigations.台灣郵政儲金利率)
            {
                sb.Append(String.Format("<td><a href='{0}BankDetail/7010000.aspx' title='{1}'>{1}</a></td>", System.Web.HttpContext.Current.Request.Path.Substring(0, System.Web.HttpContext.Current.Request.Path.LastIndexOf('/') + 1), (Enums.eu牌告利率Navigations)i));
            }
            else
                sb.Append("<td>&nbsp;</td>");

            if ((i - 2) % 4 == 0)
                sb.Append("</tr>");
        }

        sb.Append("</table>");
        sb.Append("</div><!-- fmTb:end -->");
        #endregion 最上面導覽區塊

        DataProvider._24.Bank _24BankProvider = new DataProvider._24.Bank();

        Globals g = new Globals();

        if (((Enums.eu牌告利率Navigations)_SubIndex) != Enums.eu牌告利率Navigations.台灣郵政儲金利率)
        {
            sb.Append(String.Format("<div class='path2'>各項業務利率 &gt; {0}</div><!-- path:end -->", (Enums.eu牌告利率Navigations)_SubIndex));

            string description = Enums.GetEnumDesc((Enums.eu牌告利率Navigations)_SubIndex);
            _24BankProvider.Get各項業務利率(description.Split(',')[0], (((",q_crate4,q_crate5,q_crate6,").IndexOf("," + description.Split(',')[0] + ",") >= 0) ? true : false), description.Split(',')[1], description.Split(',')[2]);

            //WriteLog.LogDataSet(_24BankProvider.ResultDataSet);

            if (_24BankProvider.ResultDataSet.Tables.Count > 1 && _24BankProvider.ResultDataSet.Tables[0].Rows.Count > 0 && _24BankProvider.ResultDataSet.Tables[1].Rows.Count > 0)
            {
                #region 第二個 Table
                sb.Append("<div class='fmTb2 mgbm'>");
                sb.Append("<table cellspacing='0' cellpadding='0'>");
                sb.Append("<tr>");
                sb.Append("<td colspan='3' class='nTdno'>額度別：一般</td>");
                sb.Append("</tr>");
                sb.Append("<tr>");
                sb.Append(String.Format("<td>最高利率：{0:F4} %</td>", g.MyFloatConvert(_24BankProvider.ResultDataSet.Tables[1].Rows[0][DataProvider._24.Bank.Get各項業務利率_ReturnTable2Fields.MaxRate.ToString()].ToString(), String.Empty, String.Empty, String.Empty)));
                sb.Append(String.Format("<td>眾數利率一：{0} %</td>", _24BankProvider.ResultDataSet.Tables[0].Rows[0][0]));
                sb.Append(String.Format("<td class='nTdno'>牌告家數：{0}</td>", _24BankProvider.ResultDataSet.Tables[2].Rows.Count));
                sb.Append("</tr>");
                sb.Append("<tr>");
                sb.Append(String.Format("<td>最低利率：{0} %</td>", g.MyFloatConvert(_24BankProvider.ResultDataSet.Tables[1].Rows[0][DataProvider._24.Bank.Get各項業務利率_ReturnTable2Fields.MinRate.ToString()].ToString(), String.Empty, String.Empty, String.Empty)));
                sb.Append(String.Format("<td>眾數利率二：{0} %</td>", _24BankProvider.ResultDataSet.Tables[0].Rows[1][0]));
                sb.Append(String.Format("<td class='nTdno'>平均利率：{0:F4} %</td>", float.Parse(_24BankProvider.ResultDataSet.Tables[1].Rows[0][DataProvider._24.Bank.Get各項業務利率_ReturnTable2Fields.ARate.ToString()].ToString())));
                sb.Append("</tr>");
                sb.Append("</table>");
                sb.Append("</div><!-- fmTb:end -->");
                #endregion 第二個 Table
            }
        }

        if (_24BankProvider.ResultDataSet != null && _24BankProvider.ResultDataSet.Tables.Count > 2)
        {
            #region 第三個 Table
            sb.Append("<div class='fmTb'>");
            sb.Append("<table cellspacing='0' cellpadding='0'>");
            sb.Append("<thead>");
            sb.Append("<tr>");
            sb.Append("<th class='nthBg'>利率(%)</th>");
            sb.Append("<th>金融機構</th>");
            sb.Append("</tr>");
            sb.Append("</thead>");
            sb.Append("<tbody>");

            foreach (DataRow dr in _24BankProvider.ResultDataSet.Tables[2].Rows)
            {
                sb.Append("<tr>");
                sb.Append(String.Format("<td class='lt'>{0}</td>", g.MyFloatConvert(dr[DataProvider._24.Bank.Get各項業務利率_ReturnTable3Fields.Rate.ToString()].ToString(), String.Empty, String.Empty, String.Empty)));
                sb.Append(String.Format("<td class='cnlt'><a href='BankDetail/{0}.aspx' title='{1}'>{1}</a></td>", dr[DataProvider._24.Bank.Get各項業務利率_ReturnTable3Fields.BCode.ToString()], dr[DataProvider._24.Bank.Get各項業務利率_ReturnTable3Fields.BName.ToString()]));
                sb.Append("</tr>");
            }

            #region 強制加入
            if (8 == _SubIndex)
            {
                sb.Append("<tr><td class='lt'>0.45~0.8</td><td class='cnlt'><a href='http://www.cnyes.com/money/BankDetail/8050000.aspx' title='遠東商銀 FE Direct'>遠東商銀 FE Direct</a></td></tr>");
            }
            #endregion 強制加入

            sb.Append("</tbody>");
            sb.Append("</table>");
            sb.Append("</div><!-- fmTb:end -->");

            #endregion 第三個 Table
        }
        return sb.ToString();
    }
    #endregion string Create ()
}
