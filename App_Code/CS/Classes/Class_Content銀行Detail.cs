﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Class_Content銀行Detail 的摘要描述
/// </summary>
public class Class_Content銀行Detail
{
    #region 變數
    private string _BCode = String.Empty;

    private string _BName = String.Empty;

    private DataProvider._24.Bank _24BankProvider = null;

    private Enums.euBankServiceDetail台灣地區 _IsBankServiceDetail台灣地區 = Enums.euBankServiceDetail台灣地區.是;

    //private byte _SubIndex = 1;

    #endregion 變數

    #region 屬性

    #region string BCode
    public string BCode
    {
        set
        {
            _BCode = value;
        }
        get
        {
            return _BCode;
        }
    }
    #endregion string BCode

    #region string BName
    public string BName
    {
        get
        {
            return _BName;
        }
    }
    #endregion string BName

    #region Enums.euBankServiceDetail台灣地區 IsBankServiceDetail台灣地區
    public Enums.euBankServiceDetail台灣地區 IsBankServiceDetail台灣地區
    {
        set
        {
            _IsBankServiceDetail台灣地區 = value;
        }
        get
        {
            return _IsBankServiceDetail台灣地區;
        }
    }
    #endregion Enums.euBankServiceDetail台灣地區 IsBankServiceDetail台灣地區

    #region //byte SubIndex
    //public byte SubIndex
    //{
    //    set
    //    {
    //        _SubIndex = value;
    //    }
    //}
    #endregion //byte SubIndex

    #endregion 屬性

    #region 建構子
    public Class_Content銀行Detail ()
    {
    }

    public Class_Content銀行Detail (string bCode)
    {
        _BCode = bCode;

        _24BankProvider = new DataProvider._24.Bank();
        _24BankProvider.GetBankDetail(_BCode);

        if (_24BankProvider.ResultDataSet.Tables[0].Rows.Count > 0)
            _BName = _24BankProvider.ResultDataSet.Tables[0].Rows[0][1].ToString();

        if (("2").Equals(_24BankProvider.ResultDataSet.Tables[0].Rows[0][0].ToString()))
            _IsBankServiceDetail台灣地區 = Enums.euBankServiceDetail台灣地區.否;

        #region //Mark
        //if (_SubIndex < 3)
        //{
        //    _24BankProvider.GetBankDetail(_BCode);

        //    if (_24BankProvider.ResultDataSet.Tables[0].Rows.Count > 0)
        //        _BName = _24BankProvider.ResultDataSet.Tables[0].Rows[0][1].ToString();

        //    if (("2").Equals(_24BankProvider.ResultDataSet.Tables[0].Rows[0][0].ToString()))
        //        _IsBankServiceDetail台灣地區 = Enums.euBankServiceDetail台灣地區.否;
        //}
        #endregion //Mark
    }
    #endregion 建構子

    #region string Create ()
    public string Create ()
    {
        //WriteLog.Log<string>("Class_Content銀行Detail Create 1");

        System.Text.StringBuilder sb = new System.Text.StringBuilder();

        if (_IsBankServiceDetail台灣地區 == Enums.euBankServiceDetail台灣地區.否)
            sb.Append("<div class='fmTb'>");

        sb.Append(String.Format("<div class='hd'><span class='dataInfo'>{0:yyyy-MM-dd}</span></div><!-- hd:end -->", DateTime.Today));

        Globals g = new Globals();

        #region 台灣地區
        if (_IsBankServiceDetail台灣地區 == Enums.euBankServiceDetail台灣地區.是)
        {
            #region 台幣
            sb.Append("<div class='Bx mgbm'>");
            sb.Append("<div class='fmTb'>");

            sb.Append("<table cellspacing='0' cellpadding='0'>");
            sb.Append("<thead>");
            sb.Append("<tr>");
            sb.Append("<th width='28%'>項目</th>");
            sb.Append("<th width='12%'>存期 </th>");
            sb.Append("<th width='13%'>額度別 </th>");
            sb.Append("<th width='16%'>生效日期 </th>");
            sb.Append("<th width='17%'>固定利率(%)</th>");
            sb.Append("<th width='17%'>機動利率(%)</th>");
            sb.Append("</tr>");
            sb.Append("</thead>");
            sb.Append("<tbody>");

            foreach (DataRow dr in _24BankProvider.ResultDataSet.Tables[1].Rows)
            {
                sb.Append("<tr>");
                sb.Append(String.Format("<td class='cnlt'>{0}</td>", dr[DataProvider._24.Bank.GetBankDetail_ReturnTable1Fields.Item.ToString()]));
                sb.Append(String.Format("<td class='cnlt'>{0}</td>", GetString(dr[DataProvider._24.Bank.GetBankDetail_ReturnTable1Fields.Period.ToString()])));
                sb.Append(String.Format("<td class='cnlt'>{0}</td>", GetString(dr[DataProvider._24.Bank.GetBankDetail_ReturnTable1Fields.Amount.ToString()])));
                sb.Append(String.Format("<td>{0}</td>", dr[DataProvider._24.Bank.GetBankDetail_ReturnTable1Fields.EDate.ToString()]));
                sb.Append(String.Format("<td>{0}</td>", g.MyFloatConvert(dr[DataProvider._24.Bank.GetBankDetail_ReturnTable1Fields.FRate.ToString()].ToString(), String.Empty, String.Empty, String.Empty)));
                sb.Append(String.Format("<td>{0}</td>", g.MyFloatConvert(dr[DataProvider._24.Bank.GetBankDetail_ReturnTable1Fields.MRate.ToString()].ToString(), String.Empty, String.Empty, String.Empty)));
                sb.Append("</tr>");
            }

            if (("8050000").Equals(_BCode))
            {
                sb.Append("<tr><td class='cnlt'>FE Direct</td><td class='cnlt'>&nbsp;</td><td class='cnlt'>0 ~ 8萬(不含)</td><td>2012-08-31</td><td>&nbsp;</td><td>0.45</td></tr>");
                sb.Append("<tr><td class='cnlt'>FE Direct</td><td class='cnlt'>&nbsp;</td><td class='cnlt'>8萬 ~ 58萬(不含)</td><td>2012-08-31</td><td>&nbsp;</td><td>0.50</td></tr>");
                sb.Append("<tr><td class='cnlt'>FE Direct</td><td class='cnlt'>&nbsp;</td><td class='cnlt'>58萬 ~ 88萬(不含)</td><td>2012-08-31</td><td>&nbsp;</td><td>0.66</td></tr>");
                sb.Append("<tr><td class='cnlt'>FE Direct</td><td class='cnlt'>&nbsp;</td><td class='cnlt'>88萬以上</td><td>2012-08-31</td><td>&nbsp;</td><td>0.80</td></tr>");
            }

            sb.Append("</tbody>");
            sb.Append("</table>");

            sb.Append("</div><!-- fmTb:end -->");
            sb.Append("</div><!-- Bx:end -->");
            #endregion 台幣

            #region 外幣
            if (_24BankProvider.ResultDataSet.Tables.Count > 1)
            {
                sb.Append("<div class='Bx mgbm'>");
                sb.Append("<div class='fmTb'>");
                sb.Append("<table width='100%' cellpadding='0' cellspacing='0' border='0'>");
                sb.Append("<tr>");
                sb.Append("<th width='20%'>幣別</th>");
                sb.Append("<th width='20%'>項目</th>");
                sb.Append("<th width='20%'>存期</th>");
                sb.Append("<th width='20%'>額度別</th>");
                sb.Append("<th width='20%'>利率</th>");
                sb.Append("</tr>");
                foreach (DataRow dr in _24BankProvider.ResultDataSet.Tables[2].Rows)
                {
                    sb.Append("<tr>");
                    sb.Append(String.Format("<td class='cnlt'>{0}</td>", restring(dr[DataProvider._24.Bank.GetBankDetail_ReturnTable2Fields.currency.ToString()].ToString())));
                    sb.Append(String.Format("<td class='cnlt'>{0}</td>", dr[DataProvider._24.Bank.GetBankDetail_ReturnTable2Fields.bankitem.ToString()]));
                    sb.Append(String.Format("<td class='cnlt'>{0}</td>", dr[DataProvider._24.Bank.GetBankDetail_ReturnTable2Fields.bankperiod.ToString()]));
                    sb.Append(String.Format("<td>{0}</td>", ((dr[DataProvider._24.Bank.GetBankDetail_ReturnTable2Fields.banklimit.ToString()].ToString().Trim().Length != 0) ? dr[DataProvider._24.Bank.GetBankDetail_ReturnTable2Fields.banklimit.ToString()].ToString().Trim() : "--")));
                    sb.Append(String.Format("<td>{0}</td>", g.MyFloatConvert(dr[DataProvider._24.Bank.GetBankDetail_ReturnTable2Fields.bankrate.ToString()].ToString(), String.Empty, String.Empty, String.Empty, 100)));
                    sb.Append("</tr>");
                }
                sb.Append("</table>");
                sb.Append("</div><!-- fmTb:end -->");
                sb.Append("</div><!-- Bx:end -->");
            }
            #endregion 外幣
        }
        #endregion 台灣地區
        #region 香港、中國
        else
        {
            sb.Append("<table width='100%' cellpadding='0' cellspacing='0' border='0'>");
            sb.Append("<thead>");
            sb.Append("<tr>");
            sb.Append("<th width='15%'>幣別</th>");
            sb.Append("<th width='15%'>項目</th>");
            sb.Append("<th width='15%'>存期</th>");
            sb.Append("<th width='40%'>額度別</th>");
            sb.Append("<th width='15%'>利率</th>");
            sb.Append("</tr>");
            sb.Append("</thead>");
            sb.Append("<tbody>");

            foreach (DataRow dr in _24BankProvider.ResultDataSet.Tables[1].Rows)
            {
                sb.Append("<tr>");
                sb.Append(String.Format("<td class='cnlt'>{0}</td>", dr[DataProvider._24.Bank.GetBankDetail_ReturnTable2Fields.currency.ToString()]));
                sb.Append(String.Format("<td class='cnlt'>{0}</td>", dr[DataProvider._24.Bank.GetBankDetail_ReturnTable2Fields.bankitem.ToString()]));
                sb.Append(String.Format("<td class='cnlt'>{0}</td>", dr[DataProvider._24.Bank.GetBankDetail_ReturnTable2Fields.bankperiod.ToString()]));
                sb.Append(String.Format("<td class='cnlt'>{0}</td>", ((dr[DataProvider._24.Bank.GetBankDetail_ReturnTable2Fields.banklimit.ToString()].ToString().Trim().Length != 0) ? dr[DataProvider._24.Bank.GetBankDetail_ReturnTable2Fields.banklimit.ToString()].ToString().Trim() : "--")));
                sb.Append(String.Format("<td>{0}</td>", g.MyFloatConvert(dr[DataProvider._24.Bank.GetBankDetail_ReturnTable2Fields.bankrate.ToString()].ToString(), String.Empty, String.Empty, String.Empty, 100)));
                sb.Append("</tr>");
            }

            sb.Append("</tbody>");
            sb.Append("</table>");

            sb.Append("</div><!-- fmTb:end -->");
        }
        #endregion 香港、中國
        //WriteLog.Log<string>("Class_Content銀行Detail Create 2");

        return sb.ToString();
    }
    #endregion string Create ()

    #region string GetString (object o)
    private string GetString (object o)
    {
        if (o.ToString().Trim().Length == 0)
            return "&nbsp;";

        return o.ToString().Trim();
    }
    #endregion string GetString (object o)


    private string restring(string p)
    {
        switch (p)
        {
            case "AUD":
                return "澳幣/AUD";
                break;
            case "CAD":
                return "加拿大幣/CAD";
                break;
            case "CHF":
                return "瑞士法郎/CHF";
                break;
            case "CNY":
                return "人民幣/CNY";
                break;
            case "EUR":
                return "歐元/EUR";
                break;
            case "GBP":
                return "英鎊/GBP";
                break;
            case "HKD":
                return "港幣/HKD";
                break;
            case "IDR":
                return "印尼幣/IDR";
                break;
            case "JPY":
                return "日元/JPY";
                break;
            case "KRW":
                return "韓圜/KRW";
                break;
            case "MYR":
                return "馬來西亞幣/MYR";
                break;
            case "NZD":
                return "紐元/NZD";
                break;
            case "PHP":
                return "菲律賓披索/PHP";
                break;
            case "SEK":
                return "瑞典幣/SEK";
                break;
            case "SGD":
                return "新加坡幣/SGD";
                break;
            case "THB":
                return "泰幣/THB";
                break;
            case "USD":
                return "美元/USD";
                break;
            case "VND":
                return "越南盾/VND";
                break;
            case "ZAR":
                return "南非幣/ZAR";
                break;
            case "DKK":
                return "丹麥克郎/DKK";
                break;
            default:
                return p;
                break;
      }
    }
}
