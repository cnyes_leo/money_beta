﻿using System;
using System.Data;
using System.Configuration;

/// <summary>
/// Class_Content牌告利率1 的摘要描述
/// </summary>
public class Class_Content牌告利率1
{
	public Class_Content牌告利率1()
	{
		//
		// TODO: 在此加入建構函式的程式碼
		//
    }

    #region string Create ()
    public string Create ()
    {
        System.Text.StringBuilder sb = new System.Text.StringBuilder();

        DataProvider._24.Bank_NewBankRate _24Bank_NewBankRateProvider = new DataProvider._24.Bank_NewBankRate();
        _24Bank_NewBankRateProvider.GetAllBanks();

        sb.Append("<div class='fmTb2 mgbm'>");
        sb.Append("<p class='mrelt4'>▍ 台灣地區</p>");
        sb.Append("<table cellspacing='0' cellpadding='0' width='100%'>");
        sb.Append("<tr>");
        sb.Append("<td colspan='4' class='nTdBg'><strong>銀行&amp;郵局</strong></td>");
        sb.Append("</tr>");

        int index = 0;

        DataView dv = _24Bank_NewBankRateProvider.ResultDataSet.Tables[0].DefaultView;
        dv.RowFilter = String.Format("{0} <> '外商銀行'", DataProvider._24.Bank_NewBankRate.GetAllBanks_ReturnTableFields.BankType);

        foreach (DataRowView drv in dv)
        {
            if (index % 4 == 0)
                sb.Append("<tr>");

            sb.Append(String.Format("<td width='25%'{0}><a href='BankDetail/{1}.aspx' title='{2}'>{2}</a></td>", ((index % 4 == 0 && index != 0) ? " class='nTdno'" : String.Empty), drv[DataProvider._24.Bank_NewBankRate.GetAllBanks_ReturnTableFields.BCode.ToString()], drv[DataProvider._24.Bank_NewBankRate.GetAllBanks_ReturnTableFields.BName.ToString()]));

            if (index % 4 == 3 || index == dv.Count - 1)
                sb.Append("</tr>");

            index++;
        }

        sb.Append("<tr>");
        sb.Append("<td colspan='4' class='nTdBg'><strong>外商銀行</strong></a></td>");
        sb.Append("</tr>");

        dv = _24Bank_NewBankRateProvider.ResultDataSet.Tables[0].DefaultView;
        dv.RowFilter = String.Format("{0} = '外商銀行'", DataProvider._24.Bank_NewBankRate.GetAllBanks_ReturnTableFields.BankType);

        index = 0;
        foreach (DataRowView drv in dv)
        {
            if (index % 4 == 0)
                sb.Append("<tr>");

            sb.Append(String.Format("<td width='25%'{0}><a href='BankDetail/{1}.aspx' title='{2}'>{2}</a></td>", ((index % 4 == 0 && index != 0) ? " class='nTdno'" : String.Empty), drv[DataProvider._24.Bank_NewBankRate.GetAllBanks_ReturnTableFields.BCode.ToString()], drv[DataProvider._24.Bank_NewBankRate.GetAllBanks_ReturnTableFields.BName.ToString()]));

            if (index % 4 == 3 || index == dv.Count - 1)
                sb.Append("</tr>");

            index++;
        }

        sb.Append("</table>");

        sb.Append("</div><!-- fmTb:end -->");

        sb.Append("<div class='fmTb2 mgbm'>");
        sb.Append("<p class='mrelt4'>▍ 香港地區</p>");
        sb.Append("<table border='0' cellspacing='0' cellpadding='0' width='100%' class='tbBxline'>");

        foreach (DataRow dr in _24Bank_NewBankRateProvider.ResultDataSet.Tables[1].Rows)
        {
            index = _24Bank_NewBankRateProvider.ResultDataSet.Tables[1].Rows.IndexOf(dr);

            if (index % 4 == 0)
                sb.Append("<tr>");

            sb.Append(String.Format("<td width='25%'{0}><a href='BankDetail/{1}.aspx' title='{2}'>{2}</a></td>", ((index % 4 == 0 && index != 0) ? " class='nTdno'" : String.Empty), dr[DataProvider._24.Bank_NewBankRate.GetAllBanks_ReturnTableFields.BCode.ToString()], dr[DataProvider._24.Bank_NewBankRate.GetAllBanks_ReturnTableFields.BName.ToString()]));

            if (index % 4 == 3 || index == _24Bank_NewBankRateProvider.ResultDataSet.Tables[1].Rows.Count - 1)
                sb.Append("</tr>");
        }

        sb.Append("</table>");
        sb.Append("</div>");

        sb.Append("<div class='fmTb2 mgbm'>");
        sb.Append("<p class='mrelt4'>▍ 中國地區</p>");
        sb.Append("<table border='0' cellspacing='0' cellpadding='0' width='100%' class='tbBxline'>");

        foreach (DataRow dr in _24Bank_NewBankRateProvider.ResultDataSet.Tables[2].Rows)
        {
            index = _24Bank_NewBankRateProvider.ResultDataSet.Tables[2].Rows.IndexOf(dr);

            if (index % 4 == 0)
                sb.Append("<tr>");

            sb.Append(String.Format("<td width='25%'{0}><a href='BankDetail/{1}.aspx' title='{2}'>{2}</a></td>", ((index % 4 == 0 && index != 0) ? " class='nTdno'" : String.Empty), dr[DataProvider._24.Bank_NewBankRate.GetAllBanks_ReturnTableFields.BCode.ToString()], dr[DataProvider._24.Bank_NewBankRate.GetAllBanks_ReturnTableFields.BName.ToString()]));

            if (index % 4 == 3 || index == _24Bank_NewBankRateProvider.ResultDataSet.Tables[2].Rows.Count - 1)
                sb.Append("</tr>");
        }

        sb.Append("</table>");
        sb.Append("</div>");

        return sb.ToString();
    }
    #endregion string Create ()
}
