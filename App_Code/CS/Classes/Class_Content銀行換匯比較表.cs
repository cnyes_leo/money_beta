﻿using System;
using System.Data;
using System.Configuration;

/// <summary>
/// Class_Content銀行換匯比較表 的摘要描述
/// </summary>
public class Class_Content銀行換匯比較表
{
    #region 變數
    private byte _SubIndex = 1;

    private Enums.euOrderDirection? _OrderDirection = null;

    private byte? _OrderIndex = null;
    #endregion 變數

    #region 屬性

    #region byte SubIndex
    public byte SubIndex
    {
        set
        {
            _SubIndex = value;
        }
    }
    #endregion byte SubIndex

    #region Enums.euOrderDirection? OrderDirection
    public Enums.euOrderDirection? OrderDirection
    {
        set
        {
            _OrderDirection = value;
        }
        get
        {
            return _OrderDirection;
        }
    }
    #endregion Enums.euOrderDirection? OrderDirection

    #region byte? OrderIndex
    public byte? OrderIndex
    {
        set
        {
            _OrderIndex = value;
        }
        get
        {
            return _OrderIndex;
        }
    }
    #endregion byte? OrderIndex

    #endregion 屬性

	public Class_Content銀行換匯比較表()
	{
		//
		// TODO: 在此加入建構函式的程式碼
		//
    }

    #region string Create ()
    public string Create ()
    {
        System.Text.StringBuilder sb = new System.Text.StringBuilder();

        Enums.euCurrency[] currencys = 
            {
                Enums.euCurrency.USD,
                Enums.euCurrency.EUR,
                Enums.euCurrency.JPY,
                Enums.euCurrency.HKD,
                Enums.euCurrency.CNY,
                Enums.euCurrency.AUD
            };

        sb.AppendLine("<div class='fmTb'>");
        sb.AppendLine("<table border='0' cellspacing='0' cellpadding='0' width='100%'>");
        sb.AppendLine("<caption class='tbtitle'>銀行牌告匯率表</caption>");
        sb.AppendLine("<tr>");
        sb.AppendLine("<th>項目</td>");
        #region //Mark
        //sb.AppendLine("<th colspan='2'>美元USD</th>");
        //sb.AppendLine("<th colspan='2'>歐元EUR</th>");
        //sb.AppendLine("<th colspan='2'>日元JPY</th>");
        //sb.AppendLine("<th colspan='2'>港幣HKD</th>");
        //sb.AppendLine("<th colspan='2'>人民幣CNY</th>");
        //sb.AppendLine("<th colspan='2'>澳幣AUD</th>");
        #endregion //Mark
        foreach (Enums.euCurrency c in currencys)
        {
            sb.AppendLine(String.Format("<th colspan='2'>{0}{1}</th>", Enums.GetEnumDesc(c), c));
        }

        sb.AppendLine("</tr>");
        sb.AppendLine("<tr class='tdSy01'>");

        string orderMark = (_OrderDirection == null || ((Enums.euOrderDirection)_OrderDirection) == Enums.euOrderDirection.ASC) ? "▲" : "▼";
        string a_d = ((orderMark == "▼") ? "a" : "d");

        string[] classes = new string[13];

        // bob 說 預設為 美元（_OrderIndex == 2）
        if (_OrderIndex == null)
            _OrderIndex = 2;

        if (_OrderIndex != null)
            classes[(byte)_OrderIndex - 1] = " class='bgc01'";

        byte classesIndex = 0;

        sb.AppendLine(String.Format("<td class='cnlt'><a href='{0}?{1}={2}' title='鉅亨理財'>銀行名稱{3}</a></td>", System.IO.Path.GetFileNameWithoutExtension(System.Web.HttpContext.Current.Request.RawUrl), a_d, ++classesIndex, orderMark));

        foreach (Enums.euCurrency c in currencys)
        {
            sb.AppendLine(String.Format("<td{0}><a href='{1}.aspx?{2}={3}' title='鉅亨理財'>{4}買進{5}</td>", /*0*/classes[classesIndex++], /*1*/System.IO.Path.GetFileNameWithoutExtension(System.Web.HttpContext.Current.Request.RawUrl), /*2*/a_d, /*3*/classesIndex, /*4*/(DataProvider._24.Bank_all.euGet銀行換匯對照表_GetType)(_SubIndex - 1), /*5*/orderMark));
            sb.AppendLine(String.Format("<td{0}><a href='{1}.aspx?{2}={3}' title='鉅亨理財'>{4}賣出{5}</td>", /*0*/classes[classesIndex++], /*1*/System.IO.Path.GetFileNameWithoutExtension(System.Web.HttpContext.Current.Request.RawUrl), /*2*/a_d, /*3*/classesIndex, /*4*/(DataProvider._24.Bank_all.euGet銀行換匯對照表_GetType)(_SubIndex - 1), /*5*/orderMark));
        }
        sb.AppendLine("</tr>");

        DataProvider._24.Bank_all _24_Bank_all_Dp = new DataProvider._24.Bank_all();

        DataProvider._24.Bank_all.Get銀行換匯對照表_ReturnTableFields? orderField = null;
        if (_OrderIndex != null)
        {
            orderField = (DataProvider._24.Bank_all.Get銀行換匯對照表_ReturnTableFields)_OrderIndex;
        }
        _24_Bank_all_Dp.Get銀行換匯對照表((DataProvider._24.Bank_all.euGet銀行換匯對照表_GetType)(_SubIndex - 1), orderField, _OrderDirection);

        Globals g = new Globals();
        foreach (DataRow dr in _24_Bank_all_Dp.ResultDataTable.Rows)
        {
            classesIndex = 1;
            sb.AppendLine("<tr>");
            sb.AppendLine(String.Format("<td class='cnlt'><a href='{0}/{1}/{2}.aspx' title='{3}'>{3}</a></td>", System.IO.Path.GetFileNameWithoutExtension(Enums.GetEnumDesc(Enums.euBankServiceNavigations.銀行換匯)), Enums.euBankMoneyExchangeType.Bank, dr[DataProvider._24.Bank_all.Get銀行換匯對照表_ReturnTableFields.BCode.ToString()], dr[DataProvider._24.Bank_all.Get銀行換匯對照表_ReturnTableFields.BankName.ToString()]));
            sb.AppendLine(String.Format("<td{0}>{1}</td>", classes[classesIndex++], g.MyFloatConvert(dr[DataProvider._24.Bank_all.Get銀行換匯對照表_ReturnTableFields.Rate_USD_B.ToString()].ToString(), "F4", "--", String.Empty)));
            sb.AppendLine(String.Format("<td{0}>{1}</td>", classes[classesIndex++], g.MyFloatConvert(dr[DataProvider._24.Bank_all.Get銀行換匯對照表_ReturnTableFields.Rate_USD_S.ToString()].ToString(), "F4", "--", String.Empty)));
            sb.AppendLine(String.Format("<td{0}>{1}</td>", classes[classesIndex++], g.MyFloatConvert(dr[DataProvider._24.Bank_all.Get銀行換匯對照表_ReturnTableFields.Rate_EUR_B.ToString()].ToString(), "F4", "--", String.Empty)));
            sb.AppendLine(String.Format("<td{0}>{1}</td>", classes[classesIndex++], g.MyFloatConvert(dr[DataProvider._24.Bank_all.Get銀行換匯對照表_ReturnTableFields.Rate_EUR_S.ToString()].ToString(), "F4", "--", String.Empty)));
            sb.AppendLine(String.Format("<td{0}>{1}</td>", classes[classesIndex++], g.MyFloatConvert(dr[DataProvider._24.Bank_all.Get銀行換匯對照表_ReturnTableFields.Rate_JPY_B.ToString()].ToString(), "F4", "--", String.Empty)));
            sb.AppendLine(String.Format("<td{0}>{1}</td>", classes[classesIndex++], g.MyFloatConvert(dr[DataProvider._24.Bank_all.Get銀行換匯對照表_ReturnTableFields.Rate_JPY_S.ToString()].ToString(), "F4", "--", String.Empty)));
            sb.AppendLine(String.Format("<td{0}>{1}</td>", classes[classesIndex++], g.MyFloatConvert(dr[DataProvider._24.Bank_all.Get銀行換匯對照表_ReturnTableFields.Rate_HKD_B.ToString()].ToString(), "F4", "--", String.Empty)));
            sb.AppendLine(String.Format("<td{0}>{1}</td>", classes[classesIndex++], g.MyFloatConvert(dr[DataProvider._24.Bank_all.Get銀行換匯對照表_ReturnTableFields.Rate_HKD_S.ToString()].ToString(), "F4", "--", String.Empty)));
            sb.AppendLine(String.Format("<td{0}>{1}</td>", classes[classesIndex++], g.MyFloatConvert(dr[DataProvider._24.Bank_all.Get銀行換匯對照表_ReturnTableFields.Rate_CNY_B.ToString()].ToString(), "F4", "--", String.Empty)));
            sb.AppendLine(String.Format("<td{0}>{1}</td>", classes[classesIndex++], g.MyFloatConvert(dr[DataProvider._24.Bank_all.Get銀行換匯對照表_ReturnTableFields.Rate_CNY_S.ToString()].ToString(), "F4", "--", String.Empty)));
            sb.AppendLine(String.Format("<td{0}>{1}</td>", classes[classesIndex++], g.MyFloatConvert(dr[DataProvider._24.Bank_all.Get銀行換匯對照表_ReturnTableFields.Rate_AUD_B.ToString()].ToString(), "F4", "--", String.Empty)));
            sb.AppendLine(String.Format("<td{0}>{1}</td>", classes[classesIndex++], g.MyFloatConvert(dr[DataProvider._24.Bank_all.Get銀行換匯對照表_ReturnTableFields.Rate_AUD_S.ToString()].ToString(), "F4", "--", String.Empty)));
            sb.AppendLine("</tr>");
        }

        sb.AppendLine("</table>");
        sb.AppendLine("</div><!-- fmTb:end -->");

        return sb.ToString();
    }
    #endregion string Create ()
}
