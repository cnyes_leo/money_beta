﻿using System;
using System.Data;
using System.Configuration;

/// <summary>
/// Class_Content存款利率1 的摘要描述
/// </summary>
public class Class_Content存款利率1
{
    #region 變數
    private Enums.eu存款利率Navigations _存款利率Nav = Enums.eu存款利率Navigations.None;

    private byte _PageIndex = 1;

    private byte _PageSize = 25;

    private Enums.euRateType _RateType = Enums.euRateType.機動;

    private Enums.euOrderDirection? _OrderDirection = null;

    private byte? _OrderIndex = null;

    private string _Keyword = null;
    #endregion 變數

    #region 屬性

    #region Enums.eu存款利率Navigations 存款利率Nav
    public Enums.eu存款利率Navigations 存款利率Nav
    {
        set
        {
            _存款利率Nav = value;
        }
        get
        {
            return _存款利率Nav;
        }
    }
    #endregion Enums.eu存款利率Navigations 存款利率Nav

    #region byte PageIndex
    public byte PageIndex
    {
        set
        {
            _PageIndex = value;
        }
    }
    #endregion byte PageIndex

    #region byte PageSize
    public byte PageSize
    {
        set
        {
            _PageSize = value;
        }
    }
    #endregion byte PageSize

    #region Enums.euRateType RateType
    public Enums.euRateType RateType
    {
        set
        {
            _RateType = value;
        }
        get
        {
            return _RateType;
        }
    }
    #endregion Enums.euRateType RateType

    #region Enums.euOrderDirection? OrderDirection
    public Enums.euOrderDirection? OrderDirection
    {
        set
        {
            _OrderDirection = value;
        }
        get
        {
            return _OrderDirection;
        }
    }
    #endregion Enums.euOrderDirection? OrderDirection

    #region byte? OrderIndex
    public byte? OrderIndex
    {
        set
        {
            _OrderIndex = value;
        }
        get
        {
            return _OrderIndex;
        }
    }
    #endregion byte? OrderIndex

    #region string Keyword
    public string Keyword
    {
        set
        {
            _Keyword = value;
        }
        get
        {
            return _Keyword;
        }
    }
    #endregion string Keyword

    #endregion 屬性

	public Class_Content存款利率1()
	{
		//
		// TODO: 在此加入建構函式的程式碼
		//
    }

    #region string Create (Enums.eu存款利率Navigations nav, byte pageIndex, byte pageSize, Enums.euRateType rateType, Enums.euOrderDirection? orderDirection, byte? orderIndex, string keyword)
    public string Create (Enums.eu存款利率Navigations nav, byte pageIndex, byte pageSize, Enums.euRateType rateType, Enums.euOrderDirection? orderDirection, byte? orderIndex, string keyword)
    {
        _存款利率Nav = nav;
        _PageIndex = pageIndex;
        _PageSize = pageSize;
        _RateType = rateType;
        _OrderDirection = orderDirection;
        _OrderIndex = orderIndex;
        _Keyword = keyword;

        return Create();
    }
    #endregion string Create (Enums.eu存款利率Navigations nav, byte pageIndex, byte pageSize, Enums.euRateType rateType, Enums.euOrderDirection? orderDirection, byte? orderIndex, string keyword)

    #region string Create ()
    public string Create ()
    {
        System.Text.StringBuilder sb = new System.Text.StringBuilder();

        sb.Append("<div class='fmTb'>");
        sb.Append("<div class='hd'>");
        if (_存款利率Nav == Enums.eu存款利率Navigations.台灣利率排行)
            sb.Append(String.Format("<span class='hdInp'><input name='choice' type='radio' value=''{0} onclick='location.href=\"{1}.aspx?rt=m{2}{3}{4}\"' />機動利率<input name='choice' type='radio' value=''{5} onclick='location.href=\"{1}.aspx?rt=f{2}{3}{4}\"' />固定利率</span>", ((_RateType == Enums.euRateType.機動) ? "checked='checked'" : String.Empty), System.IO.Path.GetFileNameWithoutExtension(System.Web.HttpContext.Current.Request.RawUrl), ((_OrderDirection != null && _OrderIndex != null) ? ((((Enums.euOrderDirection)_OrderDirection) == Enums.euOrderDirection.ASC) ? String.Format("&a={0}", _OrderIndex) : String.Format("&d={0}", _OrderIndex)) : String.Empty), ((_Keyword != null && _Keyword.Length > 0) ? "&k=" + _Keyword : String.Empty), ((1 != _PageIndex) ? "&p=" + _PageIndex.ToString() : String.Empty), ((_RateType == Enums.euRateType.固定) ? "checked='checked'" : String.Empty)));
        sb.Append(String.Format("<span class='dataInfo'> 單位：年息 ％<cite>{0:yyyy-MM-dd}</cite></span>", DateTime.Today));
        sb.Append("</div><!-- hd:end -->");
        sb.Append("<table border='0' cellspacing='0' cellpadding='0' width='100%' class='tabType'>");
        sb.Append("<tr>");
        sb.Append("<th width='14%' rowspan='2'>銀行名稱</th>");
        sb.Append("<th colspan='3'>活期儲蓄存款</th>");
        sb.Append("<th colspan='5'>定期存款</th>");
        sb.Append("<th colspan='3'>定期儲蓄存款</th>");
        sb.Append("</tr>");
        sb.Append("<tr>");

        string orderMark = (_OrderDirection == null || ((Enums.euOrderDirection)_OrderDirection) == Enums.euOrderDirection.ASC) ? "▲" : "▼";
        string a_d = ((orderMark == "▼") ? "a" : "d");

        string type = ((_RateType == Enums.euRateType.機動) ? "&rt=m" : "&rt=f");
        string k = String.Empty;
        if (_Keyword != null)
            k = "&k=" + _Keyword;

        string p = String.Empty;
        if (_PageIndex > 1)
            p = "&p=" + _PageIndex.ToString();

        string[] classes = new string[11];

        if (_OrderIndex != null)
            classes[(byte)_OrderIndex - 1] = " class='bgc01'";

        byte classesIndex = 0;

        sb.Append(String.Format("<td width='8%'{0}><a href='{1}.aspx?{2}=1{3}{4}{5}' title='鉅亨理財'>一般{6}</a></td>", classes[classesIndex++], System.IO.Path.GetFileNameWithoutExtension(System.Web.HttpContext.Current.Request.RawUrl), a_d, type, k, p, orderMark));
        sb.Append(String.Format("<td width='9%'{0}><a href='{1}.aspx?{2}=2{3}{4}{5}' title='鉅亨理財'>薪資轉帳{6}</a></td>", classes[classesIndex++], System.IO.Path.GetFileNameWithoutExtension(System.Web.HttpContext.Current.Request.RawUrl), a_d, type, k, p, orderMark));
        sb.Append(String.Format("<td width='8%'{0}><a href='{1}.aspx?{2}=3{3}{4}{5}' title='鉅亨理財'>證券戶{6}</a></td>", classes[classesIndex++], System.IO.Path.GetFileNameWithoutExtension(System.Web.HttpContext.Current.Request.RawUrl), a_d, type, k, p, orderMark));
        sb.Append(String.Format("<td width='8%'{0}><a href='{1}.aspx?{2}=4{3}{4}{5}' title='鉅亨理財'>1個月{6}</a></td>", classes[classesIndex++], System.IO.Path.GetFileNameWithoutExtension(System.Web.HttpContext.Current.Request.RawUrl), a_d, type, k, p, orderMark));
        sb.Append(String.Format("<td width='9%'{0}><a href='{1}.aspx?{2}=5{3}{4}{5}' title='鉅亨理財'>3個月{6}</a></td>", classes[classesIndex++], System.IO.Path.GetFileNameWithoutExtension(System.Web.HttpContext.Current.Request.RawUrl), a_d, type, k, p, orderMark));
        sb.Append(String.Format("<td width='8%'{0}><a href='{1}.aspx?{2}=6{3}{4}{5}' title='鉅亨理財'>6個月{6}</a></td>", classes[classesIndex++], System.IO.Path.GetFileNameWithoutExtension(System.Web.HttpContext.Current.Request.RawUrl), a_d, type, k, p, orderMark));
        sb.Append(String.Format("<td width='8%'{0}><a href='{1}.aspx?{2}=7{3}{4}{5}' title='鉅亨理財'>9個月{6}</a></td>", classes[classesIndex++], System.IO.Path.GetFileNameWithoutExtension(System.Web.HttpContext.Current.Request.RawUrl), a_d, type, k, p, orderMark));
        sb.Append(String.Format("<td width='7%'{0}><a href='{1}.aspx?{2}=8{3}{4}{5}' title='鉅亨理財'>1年{6}</a></td>", classes[classesIndex++], System.IO.Path.GetFileNameWithoutExtension(System.Web.HttpContext.Current.Request.RawUrl), a_d, type, k, p, orderMark));
        sb.Append(String.Format("<td width='7%'{0}><a href='{1}.aspx?{2}=9{3}{4}{5}' title='鉅亨理財'>1年{6}</a></td>", classes[classesIndex++], System.IO.Path.GetFileNameWithoutExtension(System.Web.HttpContext.Current.Request.RawUrl), a_d, type, k, p, orderMark));
        sb.Append(String.Format("<td width='7%'{0}><a href='{1}.aspx?{2}=10{3}{4}{5}' title='鉅亨理財'>2年{6}</a></td>", classes[classesIndex++], System.IO.Path.GetFileNameWithoutExtension(System.Web.HttpContext.Current.Request.RawUrl), a_d, type, k, p, orderMark));
        sb.Append(String.Format("<td width='7%'{0}><a href='{1}.aspx?{2}=11{3}{4}{5}' title='鉅亨理財'>3年{6}</a></td>", classes[classesIndex++], System.IO.Path.GetFileNameWithoutExtension(System.Web.HttpContext.Current.Request.RawUrl), a_d, type, k, p, orderMark));
        sb.Append("</tr>");

        // 內容
        DataProvider._24.Bank _24BankProvider = new DataProvider._24.Bank();

        DataProvider._24.Bank.GetTwDepositRate_ReturnTable2Fields? orderField = null;
        if (_OrderIndex != null)
            orderField = (DataProvider._24.Bank.GetTwDepositRate_ReturnTable2Fields)((byte)_OrderIndex + 1);

        _24BankProvider.GetTwDepositRate(_PageIndex, _PageSize, _RateType, _OrderDirection, orderField);

        Globals g = new Globals();

        if (_24BankProvider.ResultDataSet.Tables.Count > 1)
        {
            foreach (DataRow dr in _24BankProvider.ResultDataSet.Tables[1].Rows)
            {
                classesIndex = 0;
                sb.Append("<tr>");
                sb.Append(String.Format("<td class='cnlt'><a href='{0}BankDetail/{1}.aspx' title='{2}'>{2}</a></td>", System.Web.HttpContext.Current.Request.Path.Substring(0, System.Web.HttpContext.Current.Request.Path.LastIndexOf('/') + 1), dr[DataProvider._24.Bank.GetTwDepositRate_ReturnTable2Fields.BCode.ToString()], dr[DataProvider._24.Bank.GetTwDepositRate_ReturnTable2Fields.BName.ToString()]));
                sb.Append(String.Format("<td{0}>{1}</td>", classes[classesIndex++], g.MyFloatConvert(dr[DataProvider._24.Bank.GetTwDepositRate_ReturnTable2Fields.Rate1.ToString()].ToString(), "F2", "--", String.Empty)));
                sb.Append(String.Format("<td{0}>{1}</td>", classes[classesIndex++], g.MyFloatConvert(dr[DataProvider._24.Bank.GetTwDepositRate_ReturnTable2Fields.Rate2.ToString()].ToString(), "F2", "--", String.Empty)));
                sb.Append(String.Format("<td{0}>{1}</td>", classes[classesIndex++], g.MyFloatConvert(dr[DataProvider._24.Bank.GetTwDepositRate_ReturnTable2Fields.Rate3.ToString()].ToString(), "F2", "--", String.Empty)));
                sb.Append(String.Format("<td{0}>{1}</td>", classes[classesIndex++], g.MyFloatConvert(dr[DataProvider._24.Bank.GetTwDepositRate_ReturnTable2Fields.Rate4.ToString()].ToString(), "F4", "--", String.Empty)));
                sb.Append(String.Format("<td{0}>{1}</td>", classes[classesIndex++], g.MyFloatConvert(dr[DataProvider._24.Bank.GetTwDepositRate_ReturnTable2Fields.Rate5.ToString()].ToString(), "F4", "--", String.Empty)));
                sb.Append(String.Format("<td{0}>{1}</td>", classes[classesIndex++], g.MyFloatConvert(dr[DataProvider._24.Bank.GetTwDepositRate_ReturnTable2Fields.Rate6.ToString()].ToString(), "F4", "--", String.Empty)));
                sb.Append(String.Format("<td{0}>{1}</td>", classes[classesIndex++], g.MyFloatConvert(dr[DataProvider._24.Bank.GetTwDepositRate_ReturnTable2Fields.Rate7.ToString()].ToString(), "F4", "--", String.Empty)));
                sb.Append(String.Format("<td{0}>{1}</td>", classes[classesIndex++], g.MyFloatConvert(dr[DataProvider._24.Bank.GetTwDepositRate_ReturnTable2Fields.Rate8.ToString()].ToString(), "F4", "--", String.Empty)));
                sb.Append(String.Format("<td{0}>{1}</td>", classes[classesIndex++], g.MyFloatConvert(dr[DataProvider._24.Bank.GetTwDepositRate_ReturnTable2Fields.Rate9.ToString()].ToString(), "F4", "--", String.Empty)));
                sb.Append(String.Format("<td{0}>{1}</td>", classes[classesIndex++], g.MyFloatConvert(dr[DataProvider._24.Bank.GetTwDepositRate_ReturnTable2Fields.Rate10.ToString()].ToString(), "F4", "--", String.Empty)));
                sb.Append(String.Format("<td{0}>{1}</td>", classes[classesIndex++], g.MyFloatConvert(dr[DataProvider._24.Bank.GetTwDepositRate_ReturnTable2Fields.Rate11.ToString()].ToString(), "F4", "--", String.Empty)));
                sb.Append("</tr>");
            }
        }

        sb.Append("</table>");

        if (_24BankProvider.ResultDataSet.Tables.Count > 0)
        {
            // 分頁
            int totalPage = 1;
            int.TryParse(_24BankProvider.ResultDataSet.Tables[0].Rows[0][DataProvider._24.Bank.ReturnTable1_CommonFields.TotalPage.ToString()].ToString(), out totalPage);

            string baseURL = String.Format("?{0}{1}{2}", ((_RateType == Enums.euRateType.固定) ? "rt=f" : "rt=m"), ((_OrderDirection != null && _OrderIndex != null) ? ((((Enums.euOrderDirection)_OrderDirection) == Enums.euOrderDirection.ASC) ? String.Format("&a={0}", _OrderIndex) : String.Format("&d={0}", _OrderIndex)) : String.Empty), ((_Keyword != null && _Keyword.Length > 0) ? "&k=" + _Keyword : String.Empty)) + "&p={0}";
            sb.Append(g.BankServicePager(totalPage, _PageIndex, 10, baseURL));
        }

        sb.Append("</div><!-- fmTb:end -->");

        sb.Append("<p class='comt'><span>備註:</span>1.僅供參考，以銀行實際利率為準。 2.有些銀行會有最低存款下限。</p>");

        return sb.ToString();
    }
    #endregion string Create ()
}
