﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;
using Dundas.Charting.WebControl;
using System.IO;

public partial class t013 : System.Web.UI.Page
{
    #region
    private ArrayList x_ser = new ArrayList();
    private ArrayList x_ser1 = new ArrayList();
    private ArrayList x_ser2 = new ArrayList();
    private ArrayList x_ser3 = new ArrayList();
    private ArrayList x_ser4 = new ArrayList();
    private ArrayList x_ser5 = new ArrayList();
    private ArrayList y_ser1 = new ArrayList();
    private ArrayList y_ser2 = new ArrayList();
    private ArrayList y_ser3 = new ArrayList();
    private ArrayList y_ser4 = new ArrayList();
    private ArrayList y_ser5 = new ArrayList();
    private ArrayList y_ser6 = new ArrayList();
    private ArrayList y_ser7 = new ArrayList();
    private ArrayList y_ser8 = new ArrayList();
    private ArrayList y_ser9 = new ArrayList();
    private ArrayList y_ser10 = new ArrayList();
    private ArrayList y_ser11 = new ArrayList();
    private string ChartPath;
    private string ChartName;
    #endregion



    protected void Page_Load(object sender, EventArgs e)
    {
        clear_arraylist();
        DefaultOption();
        Chart1.Series[0].Enabled = true;
        Chart1.Series[1].Enabled = true;


        for (int i = 0; i < 10; i++)
        {
            x_ser.Add(i);
            y_ser1.Add(i);
            y_ser2.Add(i);
        }

        x_ser.Reverse();
        y_ser1.Reverse();
        y_ser2.Reverse();

        Chart1.Series["Series1"].Points.DataBindXY(x_ser, y_ser1);
        Chart1.Series["Series2"].Points.DataBindXY(x_ser, y_ser2);

        //設定圖表屬性
        Option_Chart();
        Style();

        //屬性修正
        Chart1.Width = 652;
        Chart1.Height = 180;


        //設定title
        Chart1.Titles["Title1"].Text = "Title1";
        Chart1.Series["Series1"].LegendText = "外資";
        Chart1.Series["Series2"].LegendText = "投信";

        //save
        //string path = Server.MapPath("~/test.png");
        //Chart1.Save(path, ChartImageFormat.Png);        
    }



    public string convertNull(string v)
    {
        if (v == null)
            return "";
        else
            return v;
    }


    //清空陣列值
    private void clear_arraylist()
    {
        x_ser.Clear();
        x_ser1.Clear();
        x_ser2.Clear();
        x_ser3.Clear();
        x_ser4.Clear();
        x_ser5.Clear();
        y_ser1.Clear();
        y_ser2.Clear();
        y_ser3.Clear();
        y_ser4.Clear();
        y_ser5.Clear();
        y_ser6.Clear();
        y_ser7.Clear();
        y_ser8.Clear();
        y_ser9.Clear();
        y_ser10.Clear();
        y_ser11.Clear();
    }

    #region chart 設定

    private void Option_Chart()
    {
        //設定數列
        Chart1.Series["Series1"].XValueIndexed = true;

        Chart1.Series["Series1"].Type = SeriesChartType.StackedColumn;
        //Chart1.Series["Series1"].BorderWidth = 2;//圓角
        Chart1.Series["Series1"].BorderColor = System.Drawing.Color.FromArgb(216, 163, 162);
        Chart1.Series["Series1"].Color = System.Drawing.Color.FromArgb(230, 185, 184);
        Chart1.Series["Series1"].EmptyPointStyle.Color = System.Drawing.Color.Transparent;
        //Chart1.Series["Series1"].XValueIndexed = true;
        Chart1.Series["Series1"].CustomAttributes = "PointWidth=0.5";
        //Chart1.Series["Series1"].YAxisType = AxisType.Primary;
        Chart1.Series["Series1"].LegendText = "Series1";
        //---------
        Chart1.Series["Series2"].Type = SeriesChartType.StackedColumn;
        //Chart1.Series["Series1"].BorderWidth = 2;//圓角
        Chart1.Series["Series2"].BorderColor = System.Drawing.Color.FromArgb(188, 209, 146);
        Chart1.Series["Series2"].Color = System.Drawing.Color.FromArgb(215, 228, 189);
        Chart1.Series["Series2"].EmptyPointStyle.Color = System.Drawing.Color.Transparent;
        //Chart1.Series["Series2"].XValueIndexed = true;
        Chart1.Series["Series2"].CustomAttributes = "PointWidth=0.5";
        //Chart1.Series["Series2"].YAxisType = AxisType.Primary;
        Chart1.Series["Series2"].LegendText = "Series1";
        //---------
        Chart1.Series["Series3"].Type = SeriesChartType.StackedColumn;
        //Chart1.Series["Series1"].BorderWidth = 2;//圓角
        Chart1.Series["Series3"].BorderColor = System.Drawing.Color.FromArgb(154, 128, 184);
        Chart1.Series["Series3"].Color = System.Drawing.Color.FromArgb(179, 162, 199);
        Chart1.Series["Series3"].EmptyPointStyle.Color = System.Drawing.Color.Transparent;
        //Chart1.Series["Series3"].XValueIndexed = true;
        Chart1.Series["Series3"].CustomAttributes = "PointWidth=0.5";
        //Chart1.Series["Series3"].YAxisType = AxisType.Primary;
        Chart1.Series["Series3"].LegendText = "Series1";
        //---------
        Chart1.Series["Series4"].Type = SeriesChartType.Line;
        //Chart1.Series["Series1"].BorderWidth = 2;//圓角
        //Chart1.Series["Series4"].BorderColor = System.Drawing.Color.FromArgb(51, 127, 229);
        Chart1.Series["Series4"].Color = System.Drawing.Color.FromArgb(51, 127, 229);
        Chart1.Series["Series4"].EmptyPointStyle.Color = System.Drawing.Color.Transparent;
        //Chart1.Series["Series4"].XValueIndexed = true;
        Chart1.Series["Series4"].CustomAttributes = "PointWidth=0.5";
        //Chart1.Series["Series4"].YAxisType = AxisType.Primary;
        Chart1.Series["Series4"].LegendText = "Series1";
        //---------
        Chart1.Series["Series5"].Type = SeriesChartType.Line;
        //Chart1.Series["Series1"].BorderWidth = 2;//圓角
        //Chart1.Series["Series5"].BorderColor = System.Drawing.Color.FromArgb(154, 128, 184);
        Chart1.Series["Series5"].Color = System.Drawing.Color.FromArgb(224, 52, 51);
        Chart1.Series["Series5"].EmptyPointStyle.Color = System.Drawing.Color.Transparent;
        //Chart1.Series["Series5"].XValueIndexed = true;
        Chart1.Series["Series5"].CustomAttributes = "PointWidth=0.5";
        Chart1.Series["Series5"].YAxisType = AxisType.Secondary;
        Chart1.Series["Series5"].LegendText = "Series1";
    }

    private void Style()
    {
        //-----------------
        //三大法人風格
        //-----------------
        //左側說明
        Chart1.Titles.Add("Title3");
        Chart1.Titles["Title3"].Text = "張數";
        Chart1.Titles["Title3"].DockToChartArea = "Default";
        Chart1.Titles["Title3"].Font = new Font("Microsoft Sans Serif", float.Parse("10.5"), FontStyle.Bold);
        Chart1.Titles["Title3"].Color = Color.Gray;
        Chart1.Titles["Title3"].BackColor = Color.Transparent;
        Chart1.Titles["Title3"].Position.Height = 50;
        Chart1.Titles["Title3"].Position.Width = 5;
        Chart1.Titles["Title3"].Position.X = 0;
        Chart1.Titles["Title3"].Position.Y = 45;
        Chart1.Titles["Title3"].Docking = Docking.Top;
        Chart1.Titles["Title3"].Alignment = ContentAlignment.TopRight;

        //右左側說明
        Chart1.Titles.Add("Title4");
        Chart1.Titles["Title4"].Text = "股價";
        Chart1.Titles["Title4"].DockToChartArea = "Default";
        Chart1.Titles["Title4"].Font = new Font("Microsoft Sans Serif", float.Parse("10.5"), FontStyle.Bold);
        Chart1.Titles["Title4"].Color = Color.Gray;
        Chart1.Titles["Title4"].BackColor = Color.Transparent;
        Chart1.Titles["Title4"].Position.Height = 20;
        Chart1.Titles["Title4"].Position.Width = 12;
        Chart1.Titles["Title4"].Position.X = 96;
        Chart1.Titles["Title4"].Position.Y = 45;
        Chart1.Titles["Title4"].Docking = Docking.Top;
        //---
        Chart1.Titles["Title1"].Visible = false;
        Chart1.Titles["Title1"].Color = Color.DarkGray;
        Chart1.Titles["Title1"].Font = new Font("Microsoft Sans Serif", float.Parse("8.8"), FontStyle.Bold);
        Chart1.Titles["Title1"].Position.Height = 6;
        Chart1.Titles["Title1"].Position.Width = 10;
        Chart1.Titles["Title1"].Position.X = 64;
        Chart1.Titles["Title1"].Position.Y = 18;
        //---
        Chart1.Titles["Title2"].Color = Color.DarkGray;
        Chart1.Titles["Title2"].Font = new Font("Microsoft Sans Serif", float.Parse("7.5"));
        Chart1.Titles["Title2"].Position.Height = 7;
        Chart1.Titles["Title2"].Position.Width = 25;
        Chart1.Titles["Title2"].Position.X = 70;//浮水印
        Chart1.Titles["Title2"].Position.Y = 20;//浮水印


        //設定Legends(數據文字區塊)
        Chart1.Legends["Default"].Font = new Font("Microsoft Sans Serif", float.Parse("8.1"), FontStyle.Bold);
        Chart1.Legends["Default"].FontColor = System.Drawing.Color.FromArgb(64, 64, 64);
        //Chart1.Legends["Default"].BackColor = System.Drawing.Color.FromArgb(242, 242, 242);
        Chart1.Legends["Default"].AutoFitText = false;    //數據文字說明
        Chart1.Legends["Default"].Alignment = StringAlignment.Center;//預設置中
        Chart1.Legends["Default"].LegendStyle = LegendStyle.Row;
        Chart1.Legends["Default"].Docking = LegendDocking.Top;//齊底
        //Chart1.Legends["Default"].Position.X = 0;
        //Chart1.Legends["Default"].Position.Y = 100;
        //Chart1.Legends["Default"].Position.Height = 14;//高
        //Chart1.Legends["Default"].Position.Width = 100;//寬
        //Chart1.Legends["Default"].Enabled = true;//顯示區塊


        //圖表設定(外距)
        Chart1.ChartAreas["Default"].BorderStyle = ChartDashStyle.Solid;
        Chart1.ChartAreas["Default"].BorderColor = System.Drawing.Color.Black;
        Chart1.ChartAreas["Default"].BorderWidth = 1;
        Chart1.ChartAreas["Default"].Position.X = float.Parse("5.5");
        Chart1.ChartAreas["Default"].Position.Y = 13;
        Chart1.ChartAreas["Default"].Position.Width = 92;
        Chart1.ChartAreas["Default"].Position.Height = 85;
        Chart1.ChartAreas["Default"].BorderColor = System.Drawing.Color.LightGray;

        //圖表設定(內距)
        Chart1.ChartAreas["Default"].InnerPlotPosition.Height = float.Parse("82.61029");
        Chart1.ChartAreas["Default"].InnerPlotPosition.Width = float.Parse("82.61029");      //內框
        Chart1.ChartAreas["Default"].InnerPlotPosition.X = float.Parse("7.83502");
        Chart1.ChartAreas["Default"].InnerPlotPosition.Y = float.Parse("4.32353");

        //設定X軸
        Chart1.ChartAreas["Default"].AxisX.MajorGrid.LineColor = System.Drawing.Color.Gainsboro;
        Chart1.ChartAreas["Default"].AxisX.MajorGrid.Enabled = false;
        Chart1.ChartAreas["Default"].AxisX.MajorTickMark.Size = 2;
        Chart1.ChartAreas["Default"].AxisX.Margin = false;      //與邊界保持距儀離
        Chart1.ChartAreas["Default"].AxisX.LineColor = System.Drawing.Color.Silver;
        Chart1.ChartAreas["Default"].AxisX.LabelStyle.Font = new Font("Microsoft Sans Serif", float.Parse("7.5"));
        Chart1.ChartAreas["Default"].AxisX.LabelStyle.FontColor = System.Drawing.Color.FromArgb(64, 64, 64);
        Chart1.ChartAreas["Default"].AxisX.LabelStyle.OffsetLabels = false;  //數據分行顯示
        Chart1.ChartAreas["Default"].AxisX.LabelStyle.ShowEndLabels = true;    //顯示最後一個值        
        //Chart1.ChartAreas["Default"].AxisX.StartFromZero = false;//從0開始
        Chart1.ChartAreas["Default"].AxisX.MajorTickMark.Enabled = true;
        Chart1.ChartAreas["Default"].AxisX.MajorTickMark.LineColor = System.Drawing.Color.Silver;
        Chart1.ChartAreas["Default"].AxisX.MajorTickMark.LineStyle = ChartDashStyle.Dot;
        Chart1.ChartAreas["Default"].AxisX.MajorTickMark.Style = TickMarkStyle.Outside;
        Chart1.ChartAreas["Default"].AxisX.MajorTickMark.Size = 3;
        Chart1.ChartAreas["Default"].AxisX.LabelsAutoFit = false;//自動縮放字級
        //code中已經設定
        //Chart1.ChartAreas["Default"].AxisX.Interval = 10;//間距設定，定義每隔幾筆資料顯示一次x軸

        //設定Y軸
        Chart1.ChartAreas["Default"].AxisY.LabelsAutoFit = false;
        Chart1.ChartAreas["Default"].AxisY.MajorGrid.Enabled = true;
        Chart1.ChartAreas["Default"].AxisY.MajorGrid.LineStyle = ChartDashStyle.Dot;
        Chart1.ChartAreas["Default"].AxisY.MajorGrid.LineColor = System.Drawing.Color.LightGray;
        Chart1.ChartAreas["Default"].AxisY.LineColor = System.Drawing.Color.Silver;
        Chart1.ChartAreas["Default"].AxisY.StartFromZero = false;//從0開始        
        //Chart1.ChartAreas["Default"].AxisY.LabelStyle.Format = "0";//單位規格
        Chart1.ChartAreas["Default"].AxisY.LabelStyle.FontColor = System.Drawing.Color.FromArgb(64, 64, 64);
        Chart1.ChartAreas["Default"].AxisY.MajorTickMark.Enabled = false;
        Chart1.ChartAreas["Default"].AxisY.MajorTickMark.LineColor = System.Drawing.Color.Silver;
        Chart1.ChartAreas["Default"].AxisY.MajorTickMark.LineStyle = ChartDashStyle.Dot;
        //Chart1.ChartAreas["Default"].AxisY.MajorTickMark.Style = TickMarkStyle.Cross;
        Chart1.ChartAreas["Default"].AxisY.MajorTickMark.Size = 2;
        Chart1.ChartAreas["Default"].AxisY.LabelStyle.Font = new Font("Microsoft Sans Serif", float.Parse("7.5"));
        //code中已經設定
        //Chart1.ChartAreas["Default"].AxisY.Interval = 0.5;//間距設定，定義每隔幾筆資料顯示一次x軸

        Chart1.ChartAreas["Default"].AxisY2.MajorGrid.Enabled = false;
        Chart1.ChartAreas["Default"].AxisY2.MajorGrid.LineStyle = ChartDashStyle.Dot;
        Chart1.ChartAreas["Default"].AxisY2.LabelStyle.Font = new Font("Microsoft Sans Serif", float.Parse("7.5"));
        Chart1.ChartAreas["Default"].AxisY2.MajorGrid.LineColor = System.Drawing.Color.Transparent;
        Chart1.ChartAreas["Default"].AxisY2.LineColor = System.Drawing.Color.Silver;
        Chart1.ChartAreas["Default"].AxisY2.LabelsAutoFit = false;
        Chart1.ChartAreas["Default"].AxisY2.StartFromZero = false;//從0開始
        Chart1.ChartAreas["Default"].AxisY2.LabelStyle.FontColor = System.Drawing.Color.FromArgb(64, 64, 64);
        //Chart1.ChartAreas["Default"].AxisY2.LabelStyle.Format = "0%";//單位規格
        //Chart1.ChartAreas["Default"].AxisY2.MajorTickMark.Enabled = false;
        Chart1.ChartAreas["Default"].AxisY2.MajorTickMark.Enabled = false;
        Chart1.ChartAreas["Default"].AxisY2.MajorTickMark.LineColor = System.Drawing.Color.Silver;
        Chart1.ChartAreas["Default"].AxisY2.MajorTickMark.LineStyle = ChartDashStyle.Dot;
        //Chart1.ChartAreas["Default"].AxisY2.MajorTickMark.Style = TickMarkStyle.Cross;
        Chart1.ChartAreas["Default"].AxisY2.MajorTickMark.Size = 2;
        Chart1.ChartAreas["Default"].AxisY2.LabelStyle.Font = new Font("Microsoft Sans Serif", float.Parse("7.5"));
        //code中已經設定
        //Chart1.ChartAreas["Default"].AxisY2.Interval = 10;//間距設定，定義每隔幾筆資料顯示一次x軸
    }

    private void DefaultOption()
    {
        //預設提供兩組Title設定
        Chart1.Titles.Clear();
        Chart1.Titles.Add("Title1");
        Chart1.Titles.Add("Title2");

        //預設提供兩組圖形字設定
        Chart1.Legends.Clear();
        Chart1.Legends.Add("Default");
        Chart1.Legends.Add("Default1");
        Chart1.Legends.Add("Default2");

        //繪圖區域相關設定重建
        Chart1.Series.Clear();
        Chart1.Series.Add("Series1");
        Chart1.Series.Add("Series2");
        Chart1.Series.Add("Series3");
        Chart1.Series.Add("Series4");
        Chart1.Series.Add("Series5");

        //數據圖預設只開放一個
        Chart1.Series[0].Enabled = true;
        Chart1.Series[1].Enabled = false;
        Chart1.Series[2].Enabled = false;
        Chart1.Series[3].Enabled = false;
        Chart1.Series[4].Enabled = false;


        //設定元件
        Chart1.Width = 644;
        Chart1.Height = 209;

        //背景
        //Chart1.BackColor = System.Drawing.Color.FromArgb(225, 225, 225);
        Chart1.BackColor = System.Drawing.Color.White;


        //標題
        Chart1.Titles["Title1"].DockToChartArea = "Default";
        Chart1.Titles["Title1"].Font = new Font("Microsoft Sans Serif", float.Parse("8"));
        Chart1.Titles["Title1"].Color = Color.Black;
        Chart1.Titles["Title1"].BackColor = Color.Transparent;
        Chart1.Titles["Title1"].Position.Height = 15;
        Chart1.Titles["Title1"].Position.Width = 100;
        Chart1.Titles["Title1"].Position.X = 0;
        Chart1.Titles["Title1"].Position.Y = 0;
        Chart1.Titles["Title1"].Alignment = ContentAlignment.MiddleCenter;//預設置中
        //浮水印
        Chart1.Titles["Title2"].Text = "© cnYES鉅亨網";
        Chart1.Titles["Title2"].DockToChartArea = "Default";
        Chart1.Titles["Title2"].Font = new Font("Microsoft Sans Serif", float.Parse("7.5"));
        Chart1.Titles["Title2"].Color = Color.Gray;
        Chart1.Titles["Title2"].BackColor = Color.Transparent;
        Chart1.Titles["Title2"].Position.Height = 10;
        Chart1.Titles["Title2"].Position.Width = 50;
        Chart1.Titles["Title2"].Position.X = 45;
        Chart1.Titles["Title2"].Position.Y = 12;
    }
    #endregion
}