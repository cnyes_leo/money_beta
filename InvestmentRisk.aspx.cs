﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Net;



public partial class InvestmentRisk : System.Web.UI.Page
{
    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        #region !this.IsPostBack
        if (!this.IsPostBack)
        {
        }
        #endregion !this.IsPostBack


        #region 載入右側選單
        try
        {
            //WebClient wc = new WebClient();
            //Uri uri = Request.Url;
            //string strUrl = uri.AbsoluteUri.Replace(uri.Segments[uri.Segments.Length - 1], "");
            //byte[] b = wc.DownloadData(strUrl + "batRightList.htm");
            //string strHCode = Encoding.UTF8.GetString(b);

            //Literal1.Text = strHCode;
            //wc.Dispose();

            //FileStream myFile = File.Open(Server.MapPath("batRightList.htm"), FileMode.Open, FileAccess.Read);
            //StreamReader myReader = new StreamReader(myFile);
            //int dl = System.Convert.ToInt32(myFile.Length);
            //Literal1.Text = myReader.ReadToEnd();
            //myReader.Close();
            //myFile.Close();

            Literal1.Text = System.IO.File.ReadAllText(Server.MapPath("batRightList.htm"));
        }
        catch (InvalidCastException ex)
        {
            Literal1.Text = "";
        }
        #endregion


        #region seo
        MPs_MP1 master = (MPs_MP1)this.Master as MPs_MP1;

        Seo seo = new Seo("InvestmentRisk_01");
        master.Title = seo.sTitle;
        master.Description = seo.sDesc;
        master.Keywords = seo.keyword;
        master.H1 = seo.sH1;
        seo = null;
        #endregion
    }
    #endregion Page_Load

    #region override void OnPreInit (EventArgs e)
    protected override void OnPreInit(EventArgs e)
    {
        base.OnPreInit(e);

        MPs_MP1 master = (MPs_MP1)this.Master as MPs_MP1;

        master.NavigationCategory = Enums.euNavigationCategory.投資分析;
        
    }
    #endregion override void OnPreInit (EventArgs e)
    protected void Button1_Click(object sender, EventArgs e)
    {
        int count = 0;
        switch (RadioButtonList1.SelectedValue)
        {
            case "1": count += 1;
                break;
            case "2": count += 2;
                break;
            case "3": count += 3;
                break;
            case "4": count += 4;
                break;
            case "5": count += 5;
                break;
        }
        switch (RadioButtonList2.SelectedValue)
        {
            case "1": count += 1;
                break;
            case "2": count += 2;
                break;
            case "3": count += 3;
                break;
            case "4": count += 4;
                break;
            case "5": count += 5;
                break;
        } 
        switch (RadioButtonList3.SelectedValue)
        {
            case "1": count += 1;
                break;
            case "2": count += 2;
                break;
            case "3": count += 3;
                break;
            case "4": count += 4;
                break;
            case "5": count += 5;
                break;
        } 
        switch (RadioButtonList4.SelectedValue)
        {
            case "1": count += 1;
                break;
            case "2": count += 2;
                break;
            case "3": count += 3;
                break;
            case "4": count += 4;
                break;
            case "5": count += 5;
                break;
        } 
        switch (RadioButtonList5.SelectedValue)
        {
            case "1": count += 1;
                break;
            case "2": count += 2;
                break;
            case "3": count += 3;
                break;
            case "4": count += 4;
                break;
            case "5": count += 5;
                break;
        } 
        switch (RadioButtonList6.SelectedValue)
        {
            case "1": count += 1;
                break;
            case "2": count += 2;
                break;
            case "3": count += 3;
                break;
            case "4": count += 4;
                break;
            case "5": count += 5;
                break;
        } 
        switch (RadioButtonList7.SelectedValue)
        {
            case "1": count += 1;
                break;
            case "2": count += 2;
                break;
            case "3": count += 3;
                break;
            case "4": count += 4;
                break;
            case "5": count += 5;
                break;
        }
        Response.Redirect("InvestmentRiskAs.aspx?count=" + count);
    }
}
