﻿<%@ Page Language="C#" MasterPageFile="~/MPs/MP1.master" AutoEventWireup="true" CodeFile="BankService.aspx.cs" Inherits="BankService" %>

<%@ OutputCache Duration="60" VaryByParam="c;g;a;d;cc;i;bic;ip;rt;bmeet;p" VaryByCustom="browser" %>

<%--@ Register Src="~/UCs/BankServiceNavigation.ascx" TagName="BankServiceNavigation" TagPrefix="ucMoney2" --%>

<%--@ Register Src="~/UCs/BankServiceLevel2Navigation.ascx" TagName="BankServiceLevel2Navigation" TagPrefix="ucMoney2" --%>

<%--@ Register Src="~/UCs/BankServiceContent.ascx" TagName="BankServiceContent" TagPrefix="ucMoney2" --%>

<%@ Register Src="~/UCs/BankServiceRight.ascx" TagName="BankServiceRight" TagPrefix="ucMoney2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div id="container">
<asp:Literal ID="Literal_DivStart" runat="server" EnableViewState="false"></asp:Literal>
<%--<div class='hdPath'><h3><asp:Literal ID="Literal_Level2Title" runat="server" EnableViewState="false"></asp:Literal></h3></div>--%>
<div class="bdBk">
<asp:Literal ID="Literal_DetailPath" runat="server" EnableViewState="false"></asp:Literal>
<%--<ucMoney2:BankServiceNavigation ID="BankServiceNavigation1" runat="server" EnableViewState="false" />--%>
<asp:Literal ID="Literal1_BankServiceNavigation" runat="server" EnableViewState="false"></asp:Literal>
<%--<ucMoney2:BankServiceLevel2Navigation ID="BankServiceLevel2Navigation1" runat="server" EnableViewState="false" />
<ucMoney2:BankServiceContent ID="BankServiceContent1" runat="server" EnableViewState="false" />--%>
<asp:Literal ID="Literal_BankServiceContent" runat="server" EnableViewState="false"></asp:Literal>

</div><!-- bdBk:end -->
<asp:Literal ID="Literal_DivEnd" runat="server" EnableViewState="false"></asp:Literal>

<ucMoney2:BankServiceRight ID="BankServiceRight1" runat="server" EnableViewState="false" Visible="false" />
</div><!-- container:end -->
</asp:Content>