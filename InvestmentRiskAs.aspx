<%@ Page Language="C#" AutoEventWireup="true" CodeFile="InvestmentRiskAs.aspx.cs" Inherits="InvestmentRisk" EnableViewStateMac ="true" MasterPageFile="~/MPs/MP1.master" %>

<%@ Register Src="UCs/GLidx.ascx" TagName="GLidx" TagPrefix="uc4" %>

<%@ Register Src="UCs/FundGood.ascx" TagName="FundGood" TagPrefix="uc3" %>
<%@ Register Src="UCs/fundptcalcu_02.ascx" TagName="fundptcalcu_02" TagPrefix="uc2" %>
<%@ Register Src="UCs/fundptcalcu_01.ascx" TagName="fundptcalcu_01" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form method="post" runat="server" ID=Form1>
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
<div id="container">
<div id="main">
<div class="hdPath"><h3>投資屬性分析</h3></div>
<div class="bdBk">
<div class="lfap">
<div class="fmTbG">

<p>您的投資風險屬性 : <span class="tbTitText01">
    <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label></span><span class="tbBtn"><asp:Button
        ID="Button2" runat="server" Text="返回重測" OnClick="Button2_Click" /></span></p>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<th width="11%">您的分數 </th>
<th width="13%">風險水平 </th>
<th width="76%">投資取向 </th>
</tr>
<tr>
<td class="pgrs cr" rowspan="5">
    <asp:Label ID="Label2" runat="server" Text="Label"></asp:Label></td>
<td id="td7_1" runat="server">保守型<br />7 - 10</td>
<td id="td7_2" runat="server">代表您不願意接受任何投資風險, 您適合的金融產品偏向報酬來自利息收入的產品，縱使利率水準未必跟得上通貨膨脹的幅度。<a href="http://fund.cnyes.com/Page/searchPerformance.aspx?nation1=all&std1=<&stdt=5&SortValue=two&OrderDire=desc&PageIndex=1" target="_blank" title="保守型">基金選擇</a> </td>
</tr>
<tr>
<td id="td11_1" runat="server">安穩型<br />11 –15</td>
<td id="td11_2" runat="server">代表您可以接受低投資風險,希望預期報酬率可以優於中期存款利率,以期投資本金不因通貨膨脹而貶值。<a href="http://fund.cnyes.com/Page/searchPerformance.aspx?nation1=all&SortValue=two&OrderDire=desc&PageIndex=1&std1=>&stdt=6&std02=<&stdt02=10" target="_blank" title="安穩型">基金選擇</a></td>
</tr>
<tr>
<td id="td16_1" runat="server">穩健型<br />16 –22</td>
<td id="td16_2" runat="server">代表您可以接受中等的投資風險,希望預期報酬率可以優於長期存款利率;以期投資本金不因通貨膨脹而貶值,您可以接受高一點程度的波動。<a href="http://fund.cnyes.com/Page/searchPerformance.aspx?nation1=all&SortValue=two&OrderDire=desc&PageIndex=1&std1=>&stdt=11&std02=<&stdt02=15" target="_blank">基金選擇</a></td>
</tr>
<tr>
<td id="td23_1" runat="server">成長型<br />23 –30</td>
<td id="td23_2" runat="server">代表您可以接受高度的投資風險,短、中及長期的波動性均高,希望預期報酬率可以大幅超過通貨膨脹率且適合具波動性的投資品;了解風險及報酬是相對應的，您適合投資的產品是具高報酬且高風險的。<a href="http://fund.cnyes.com/Page/searchPerformance.aspx?nation1=all&SortValue=two&OrderDire=desc&PageIndex=1&std1=>&stdt=16&std02=<&stdt02=20" target="_blank" title="成長型">基金選擇</a></td>
</tr>
<tr>
<td id="td31_1" runat="server">積極型<br />31 –35</td>
<td id="td31_2" runat="server">代表您可以接受非常高度的投資風險,短、中及長期的波動性均高,也希望預期報酬極度豐厚,您適合波動性極大的投資產品;預期波動性極高，你可以接受極大的資本損失。<a href="http://fund.cnyes.com/Page/searchPerformance.aspx?nation1=all&std1=>&stdt=21&SortValue=two&OrderDire=desc&PageIndex=1" target="_blank" title="積極型">基金選擇</a></td>
</tr>
</table>
</div><!-- fmTbG:end -->
</div><!-- lfap:end -->
</div><!-- bdBk:end -->
</div><!-- main:end -->


<div id="side">
<div class="gads300x250 mgbm"><iframe src="http://ad.cnyes.com/cnyesAD/showad.asp?TAG=STOCKV2_RIGHT" frameborder="no" vspace="0" hspace="0" width="300" height="250" marginheight="0" marginwidth="0" scrolling="no"></iframe></div><!-- Ads gads300x250:end -->

<div id="moneytools" class="mgbm">
<div class="hd2"><h3>理財小工具</h3></div>
<ul class="listbtns">
<li><a href="#">定存試算</a></li>	 
<li><a href="#">房貸試算</a></li>
<li><a href="#">基金試算</a></li> 
<li><a href="#">複利&amp;年金試算</a></li>
<li><a href="#">房租報酬率試算</a></li>	 
<li><a href="#">股票報酬率試算</a></li>
<li><a href="#">手續費利率試算</a></li>	 
<li><a href="#">貸款利率試算</a></li>
<li><a href="#">購屋和租屋哪個划算</a></li>	 
<li><a href="#">購屋需準備多少錢</a></li>
<li><a href="#">退休要準備多少錢</a></li>	 
<li><a href="#">教育金試算</a></li>
<li><a href="#">遺產稅試算</a></li>	 
<li><a href="#">汽車貸款利率試算</a></li>
</ul>
</div><!-- 理財小工具:end -->

<div id="bankrate" class="mgbm">
<div class="hd2"><h3>各銀行利率</h3></div>
<ul class="listlinks">
<li><a href="#">銀行牌告利率</a></li>	 
<li><a href="#">台灣郵政儲金利率</a></li>
<li><a href="#">活期存款	</a></li> 
<li><a href="#">活期儲蓄存款</a></li>
<li><a href="#">定期(儲)存款</a></li>	 
<li><a href="#">指數房貸利率</a></li>
<li><a href="#">薪資轉帳活期儲蓄</a></li>	 
<li><a href="#">証券戶活期儲蓄存款</a></li>
<li><a href="#">信用卡最低利率</a></li>	 
<li><a href="#">信用卡最高利率</a></li>
<li><a href="#">現金卡最低利率</a></li>	 
<li><a href="#">現金卡最高利率</a></li>
<li><a href="#">基準利率</a></li>	 
<li><a href="#">基準利率之指標利率</a></li>
</ul>
</div><!-- 各銀行利率:end -->

    <uc3:FundGood ID="FundGood1" runat="server" />
    <uc4:GLidx ID="GLidx1" runat="server" />
 
</div><!-- side:end -->
</div><!-- container:end -->
</form>
 </asp:Content>