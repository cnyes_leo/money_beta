﻿//-------------------------------------
//右側選單-排程用  brian
//-------------------------------------
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;



public partial class batRightList : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        System.Text.StringBuilder sb = new System.Text.StringBuilder();

        sb.Append(@"<div id=""side"">");

        sb.Append(@"<div class=""gads300x250 mgbm""> ");
        sb.Append(@"<iframe src=""http://mo.cnyes.com/showad.ad?TAG=STOCKV2_RIGHT"" frameborder=""no"" ");
        sb.Append(@"vspace=""0"" hspace=""0"" width=""300"" height=""250"" marginheight=""0"" marginwidth=""0"" ");
        sb.Append(@"scrolling=""no""></iframe> ");
        sb.Append(@"</div> ");

        sb.Append(getHTML(this.MoneyTools1));
        sb.Append(getHTML(this.HomeBankRate1));
        sb.Append(getHTML(this.FundGood1));
        sb.Append(getHTML(this.GLidx1));

        sb.Append("</div>");

        try
        {
            //建立html
            string filePath = Server.MapPath("batRightList.htm");
            StreamWriter fw = new StreamWriter(filePath, false, System.Text.Encoding.UTF8);
            fw.Write(sb.ToString());
            fw.Close();
        }
        catch
        {
            throw;
        }


        sb.Length = 0;
    }



    private string getHTML(UserControl objHtmlControl)
    {
        string reSult = "";
        try
        {
            System.IO.StringWriter w = new System.IO.StringWriter();
            HtmlTextWriter a = new HtmlTextWriter(w);

            objHtmlControl.RenderControl(a);

            reSult = w.ToString();
        }
        catch (Exception ex)
        {
            reSult = "";
        }

        return reSult;
    }
}