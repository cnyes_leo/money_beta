﻿var SearchType='N';
 
var city_area = eval ("(" + eval("CityArea") + ")");
$(document).ready(function(){
    for (var i=0; i<city_area.length; i++)
    {
        var op1 = $("<option>").attr("value", city_area[i][0].cid).text(city_area[i][0].cname);
        var op2 = op1.clone();
        var op3 = op1.clone();
        if (city_area[i][0].cid > 0 && city_area[i][0].cid < 26)
        {
            op1.appendTo("#SearchCity1");
            AddArea(1, 1);
        }
        op2.appendTo("#SearchCity2");
        if (city_area[i][0].cid < 26)
            op3.appendTo($("select[id*='SearchCity3']"));
        if (i==0)
            AddArea(i, 7);
    }
    $("#SearchCity1").bind("change", function(){
        AddArea($(this)[0].selectedIndex + 1, 1);
    });
    $("#SearchCity2").bind("change", function(){
        AddArea($(this)[0].selectedIndex, 2);
    });
    $("select[id*='SearchCity3']").bind("change", function(){
        AddArea($(this)[0].selectedIndex, 4);
    });
    for (var i=0; i<LevelGround.length; i++)
    {
        var op1 = $("<option>").attr("value", LevelGround[i].val).text(LevelGround[i].txt);
        var op2 = op1.clone();
        op1.appendTo("#SearchLevelground1");
        op2.appendTo("#SearchLevelground2");
    }
    for (var i=-1;i<=40;i++)
    {
        var op1, op2, op3, op4;
        if (i==-1)
            op1 = $("<option>").attr("value", "-1").text("請選擇");
        else
            op1 = $("<option>").attr("value", i*10).text(i*10);
        op2 = op1.clone();
        op3 = op1.clone();
        op4 = op1.clone();
        op1.appendTo("#SearchCostMin3");
        op2.appendTo("#SearchCostMax3");
        op3.appendTo("#SearchLevelgroundMin3");
        op4.appendTo("#SearchLevelgroundMax3");
    }
});   