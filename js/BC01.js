
function selType(typeID) {
    hdDD.style.display = '';
    hdDT.style.display = '';
    if (typeID == '1') sel1Type();
	if (typeID == '2') sel2Type();
	if (typeID == '3') sel3Type();
}

function CalculateBC() {
    var form_name = document.getElementById('aspnetForm');
    var isChecked = false;

    for (var i = 0; i < form_name.sType.length; i++) {
        if (form_name.sType[i].checked) { isChecked = true; }
    }
    if (!isChecked) {
        alert("請選取項目");
        return;
    }
    //---
    for (var i = 0; i < form_name.sType.length; i++) {
        if (form_name.sType[i].checked) {
            if (form_name.sType[i].value == '1') cal1Type();
            if (form_name.sType[i].value == '2') cal2Type();
            if (form_name.sType[i].value == '3') cal3Type();
            break;
        }
    }
}


function sel3Type() {
    initValuesBC();
    document.getElementById('strM').style.display = 'none';
    for (var i = 0; i < document.getElementById('aspnetForm').rbDate.length; i++) {
        document.getElementById('aspnetForm').rbDate[i].style.display = 'none';
    }

    // 20141001 上海商銀需求
    $($($(".toolFm")[0]).find("dt")[0]).text("每月存入金額");
    $($(".results .toolFm dt")[0]).text("滿期總共領取");
    $($(".results .toolFm dt")[1]).show();
    $($(".results .toolFm dd")[1]).show();

    $ByID('money').focus();
}

function cal3Type() {

    if ($ByID('money').value == ""
        || $ByID('yRate').value == ""
        || $ByID('mDate').value == "") {
        alert("請輸入所有資料");
        return false;
    }

    var money = chkInt($ByID('money').value, 0)
    var yrate = chkInt($ByID('yRate').value, 1)
    var year = chkInt($ByID('mDate').value, 0)
    var month  = year * 12
    var rate_m = yrate / 12 / 100
    var rate_t = 1
	var totalMoney = money * month;
	
    for (i = 0; i < month; i++)
	{
        rate_t *= (1 + rate_m)
	}
	
    var sum_mon = money * (rate_t - 1) / rate_m * (1 + rate_m)

    $ByID('money').value = IntToString(money);
    $ByID('yRate').value = yrate;
    $ByID('mDate').value = year;

	if (Math.round(sum_mon) < 0) {
	    $ByID('sumMoney').value = "數值錯誤!!"
	    $ByID('sumRate').value = "數值錯誤!!"
    }
    else 
    {
        $ByID('sumMoney').value = IntToString(Math.round(sum_mon));
        $ByID('sumRate').value = IntToString(Math.round(sum_mon - totalMoney));
    }
}


function sel1Type() {
    initValuesBC();
    document.getElementById('strM').style.display = 'none';
    for (var i = 0; i < document.getElementById('aspnetForm').rbDate.length; i++) {
        document.getElementById('aspnetForm').rbDate[i].style.display = 'none';
    }

    // 20141001 上海商銀需求
    $($($(".toolFm")[0]).find("dt")[0]).text("本金");
    $($(".results .toolFm dt")[0]).text("滿期總共領取");
    $($(".results .toolFm dt")[1]).show();
    $($(".results .toolFm dd")[1]).show();

    $ByID('money').focus();
}

function cal1Type() {

    if ($ByID('money').value == ""
        || $ByID('yRate').value == ""
        || $ByID('mDate').value == "") {
        alert("請輸入所有資料");
        return false;
    }

	var rDate = "m";
	for (var i = 0; i < document.getElementById('aspnetForm').rbDate.length; i++) {
		if (document.getElementById('aspnetForm').rbDate[i].checked) {
			if(document.getElementById('aspnetForm').rbDate[i].value == 'm') rDate = "m";
			if(document.getElementById('aspnetForm').rbDate[i].value == 'y') rDate = "y";
			break;
		}
    }

	var month = 0.00;
	var rate = 0.00;
	var yrate = chkInt($ByID('yRate').value, 1)
	var money = chkInt($ByID('money').value, 0)
    var sum_mon = money
	var totalMoney = money * month;

	if (rDate == "m") month = chkInt($ByID('mDate').value, 0);
	if (rDate == "y") month = chkInt($ByID('mDate').value, 0) * 12;
	rate  = yrate / 12;

	for(var i = 0; i < month; i ++)
	{
		sum_mon *= (1 + (rate / 100))
	}

    $ByID('money').value = IntToString(money)
    $ByID('yRate').value = yrate

    if (rDate == "m") $ByID('mDate').value = month;
    if (rDate == "y") $ByID('mDate').value = month / 12;
    if (Math.round(sum_mon) < 0) {
        $ByID('sumMoney').value = "數值錯誤!!"
        $ByID('sumRate').value = "數值錯誤!!"
    }
    else {
        $ByID('sumMoney').value = IntToString(Math.round(sum_mon));
        $ByID('sumRate').value = IntToString(Math.round(sum_mon - money));
    }
}


function sel2Type() {
    initValuesBC();
    hdDD.style.display = 'none';
    hdDT.style.display = 'none';

    // 20141001 上海商銀需求
    $($($(".toolFm")[0]).find("dt")[0]).text("本金");
    $($(".results .toolFm dt")[0]).text("每月利息收入");    
    $($(".results .toolFm dt")[1]).hide();
    $($(".results .toolFm dd")[1]).hide();
    
    $ByID('money').focus();
}

function cal2Type() {

    if ($ByID('money').value == ""
        || $ByID('yRate').value == "") 
    {
        alert("請輸入所有資料");
        return false;
    }

    var money = chkInt($ByID('money').value, 0)
    var yrate = chkInt($ByID('yRate').value, 1)
    var rate  = yrate / 12
    var sum_mon = money * rate / 100
	var totalMoney = money * 12;

	$ByID('money').value = IntToString(money)
	$ByID('yRate').value = yrate

    if (Math.round(sum_mon) < 0) {
        $ByID('sumMoney').value = "數值錯誤!!"
        $ByID('sumRate').value = "數值錯誤!!"
    }
    else {
        $ByID('sumMoney').value = IntToString(Math.round(sum_mon));
        $ByID('sumRate').value = IntToString(Math.round(totalMoney + sum_mon));
    }
}

function initValuesBC() {
    $ByID('money').value = '';
    $ByID('mDate').value = '';
    $ByID('yRate').value = '';
}

function chkInt(InPut, IntType) {
    var HavePoint = 0
    var TextValue = ""
    var CharVar = ""
    for (i = 0 ; i < InPut.length ; i++)
        {
         CharVar = InPut.charAt(i)
         if (CharVar == ".") HavePoint = HavePoint + 1
         if (HavePoint > IntType) break
         if ((CharVar == ".") || ((CharVar >= "0") && (CharVar <= "9")))
            {
              TextValue = TextValue + CharVar
            }
      }

    if (TextValue == ".") TextValue = "0"
    if (TextValue == "") TextValue = "0"
    if (TextValue.charAt(TextValue.length-1) == ".")
    {
        TextValue = TextValue.substring(0,TextValue.length-1)
    }
    var IntValue = TextValue
    return Math.abs(IntValue)
}

function IntToString(IntValue){
    var Tempstr = '_' + IntValue
    var TextString = "" ;
    var IntCommon = 0
    for (i = 0; i < Tempstr.length-1 ; i++)
    {
        if (IntCommon == 3)
        {
            IntCommon = 0
            TextString = "," + TextString
        }
        IntCommon = IntCommon + 1
        TextString = Tempstr.charAt(Tempstr.length-1-i) + TextString
    }
    return TextString
}

function $ByID(id) {
    return document.getElementById(id);
}
