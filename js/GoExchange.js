// 匯率計算
function IsNumeric(sBuf)
{
   var iLen = sBuf.length;
   var i;
   
   if(iLen == 0)
      return false;
   
   for(i=0;i<iLen;i++)
   {
      if(parseInt(sBuf.substring(i,i+1)) >=0 || sBuf.substring(i,i+1)=='.')
      {
      }
      else
        return false;
   }
   return true;
}
function GoExchange(myDollar,Dollar1,Dollar2)
{
   if(! IsNumeric(myDollar))
   {
      alert("換算數值輸入錯誤");
      return;
   }
   
   var Data1 = Dollar1.options[Dollar1.selectedIndex].value.split("-");
   var Data2 = Dollar2.options[Dollar2.selectedIndex].value.split("-");
   
   var myName1 = Data1[0];
   var myName2 = Data2[0];
   var myValue1 = Data1[1];
   var myValue2 = Data2[1];
   
   var myExchange = myValue2 / myValue1 * myDollar;
   var myExchange1 =  myValue2 / myValue1;
   var myExchange2 =  myValue1 / myValue2;
   var sTEMP = "";
   myExchange += "";
   if(myExchange.indexOf(".",0) > 0)
      myExchange = myExchange.substring(0,myExchange.indexOf(".",0) + 4);
   myExchange1 += "";
   if(myExchange1.indexOf(".",0) > 0)
      myExchange1 = myExchange1.substring(0,myExchange1.indexOf(".",0) + 4);
   myExchange2 += "";
   if(myExchange2.indexOf(".",0) > 0)
      myExchange2 = myExchange2.substring(0,myExchange2.indexOf(".",0) + 4);
   
      sTEMP = "<p><em>" +  myDollar + "</em>" + myName1 + "可換<em>" + myExchange + "</em>" + myName2 + "</p>";
      sTEMP = sTEMP + "<p>(<em>1</em>" + myName1 + " = " + myExchange1 + myName2 + " ,<em>1</em>" + myName2 + " = " + myExchange2 + myName1  + ")</p>"
 	document.getElementById("cur_result").innerHTML = sTEMP;
	document.getElementById("cur_result").className = 'disyshow';
    //document.getElementById("cur_result").style.display = '';
 }  
