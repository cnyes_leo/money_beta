﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class t002 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string str = (Request.QueryString["t"] != null) ? Request.QueryString["t"] : "";
        Literal1.Text = str + " ~ " + DateTime.Now.ToString("HH:mm:ss-ff");
    }
}