﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="batLivelihoodPrice.aspx.cs" Inherits="Charts_batLivelihoodPrice" %>
<%@ Register Assembly="DundasWebChart" Namespace="Dundas.Charting.WebControl" TagPrefix="DCWC" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <DCWC:Chart ID="Chart1" runat="server">
            <ChartAreas>
                <DCWC:ChartArea Name="Default">
                </DCWC:ChartArea>
            </ChartAreas>
            <DCWC:Legend Name="Default">
            </DCWC:Legend>
        </DCWC:Chart>
        <br />
    </div>
    </form>
</body>
</html>
