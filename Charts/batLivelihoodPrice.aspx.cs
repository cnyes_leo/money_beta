﻿//----------------------------------------
//民生物資圖表
//36月走勢圖
//by Brian
//----------------------------------------
using System;
using System.IO;
using System.Drawing;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using Dundas.Charting.WebControl;
using System.Configuration;



//using System.Web;
//using System.Web.Security;
//using System.Web.UI;
//using System.Web.UI.WebControls;
//using System.Web.UI.WebControls.WebParts;
//using System.Web.UI.HtmlControls;
//using System.Globalization;
//using System.Collections.Generic;





public partial class Charts_batLivelihoodPrice : System.Web.UI.Page
{
    #region pram
    private ArrayList x_ser = new ArrayList();
    private ArrayList x_ser1 = new ArrayList();
    private ArrayList x_ser2 = new ArrayList();
    private ArrayList x_ser3 = new ArrayList();
    private ArrayList x_ser4 = new ArrayList();
    private ArrayList x_ser5 = new ArrayList();
    private ArrayList y_ser1 = new ArrayList();
    private ArrayList y_ser2 = new ArrayList();
    private ArrayList y_ser3 = new ArrayList();
    private ArrayList y_ser4 = new ArrayList();
    private ArrayList y_ser5 = new ArrayList();
    private ArrayList y_ser6 = new ArrayList();
    private ArrayList y_ser7 = new ArrayList();
    private ArrayList y_ser8 = new ArrayList();
    private ArrayList y_ser9 = new ArrayList();
    private ArrayList y_ser10 = new ArrayList();
    private ArrayList y_ser11 = new ArrayList();
    private string ChartPath;
    private string ChartName;
    //----
    private ArrayList arr01 = new ArrayList();
    private ArrayList arr02 = new ArrayList();
    private ArrayList arr03 = new ArrayList();
    #endregion 

    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            setInit();
            bind();
        }
    }


    //預設動作
    private void setInit()
    {
        ChartPath = Server.MapPath("~/ChartsIMG/livelihoodPrice/");

        if (System.IO.Directory.Exists(ChartPath) == false)
        {
            System.IO.Directory.CreateDirectory(ChartPath);
        }

        Chart1.Series.Add("Series1");
        Chart1.Series.Add("Series2");
        Chart1.Series.Add("Series3");
        Chart1.Series.Add("Series4");
        Chart1.Series.Add("Series5");
        Chart1.Series.Add("Series6");
        Chart1.Series.Add("Series7");
        Chart1.Series.Add("Series8");
        Chart1.Series.Add("Series9");
        Chart1.Titles.Add("Title1");
        //Chart1.Titles.Add("Title2");
        Chart1.Legends.Add("Default1");
        Chart1.Legends.Add("Default2");
        Page.Title = "民生物資圖片繪製";
    }


    //繫結
    private void bind()
    {
        //食物類_食用油
        food_EdibleOil();
        //食物類_肉類
        food_Meat();
        //食物類_蛋類
        food_Eggs();
        //食物類_水果
        food_Fruit();
        //食物類_蔬菜
        food_Vegetables();
        //食物類_水產
        food_Fisheries();

        
        //交通類_油料費
        traffic_FuelFee();
        //交通類_交通服務及維修零件
        traffic_ServiceParts();


        //居住類_水電瓦斯
        live_HydropowerGas();
        //居住類_房租
        live_Rent();
        //居住類_家庭用品
        live_HouseholdGoods();
        
        
        //衣服類_成衣
        clothing_Clothing();
        //衣服類_鞋襪
        clothing_ShoesSocks();
        //衣服類_衣著服務及配件
        clothing_ServicesAccessories();
            
        
        //其他類_醫療費用
        other_MedicalExpenses();
        //其他類_教養費用
        other_RearingCosts();
        //其他類_香菸及檳榔
        other_CigarettesBetelNuts();
        //其他類_美容及衛生用品
        other_BeautyHygieneProducts();        
    }



    //------------------------------------------------------------------
    //FN
    //------------------------------------------------------------------
    
    //食物類_食用油
    private void food_EdibleOil()
    {
        string typeID = "010113"; //項目代號
        ChartName = "food_EdibleOil.png";
        clear_arraylist();
        DefaultOption();
        Chart1.Series[0].Enabled = true;

        try
        {
            DataBaseClass db = new DataBaseClass();
            DataTable Dt = new DataTable();
            string sql = string.Empty;
            string temp_Month = string.Empty;
            string temp_Val = string.Empty;

            sql += " SELECT TOP 36 Date, CursorValue ";
            sql += " FROM  Gov_StatData ";
            sql += " WHERE MyIndex = @MyIndex ";
            sql += " ORDER BY Date DESC ";

            SqlParameter[] prams = { new SqlParameter("@MyIndex", SqlDbType.NVarChar) };
            prams[0].Value = typeID;

            Dt = new DataBaseClass().Select_DataReader_Parameters(ConfigurationManager.ConnectionStrings["_24_Money_ConnStr"].ConnectionString, sql, prams);

            if (Dt.Rows.Count == 0 || Dt.Rows == null) return;
            foreach (DataRow rows in Dt.Rows)
            {
                //月份
                temp_Month = (rows["Date"].ToString() != "")
                    ? rows["Date"].ToString().Substring(0, 4) + "/" + rows["Date"].ToString().Substring(4, 2)
                    : "";

                //數據
                temp_Val = (rows["CursorValue"].ToString() != "")
                    ? rows["CursorValue"].ToString()
                    : "0";

                if (temp_Month != "")
                {
                    x_ser1.Add(temp_Month);
                    y_ser1.Add(temp_Val);
                }
            }

            db = null;
            Dt.Dispose();
        }
        catch (Exception ee)
        { Response.Write(ee.ToString()); }

        x_ser1.Reverse();
        y_ser1.Reverse();

        Chart1.Series[0].Points.DataBindXY(x_ser1, y_ser1);

        //設定圖表屬性
        Option_Chart1();
        Style01();

        //屬性修正
        Chart1.Series["Series1"].YAxisType = AxisType.Primary;
        Chart1.Series["Series1"].Type = SeriesChartType.Line;
        Chart1.Series["Series1"].Color = System.Drawing.Color.FromArgb(255, 64, 119, 183);

        //設定title
        Chart1.Series["Series1"].LegendText = "食用油";

        //save
        string path = ChartPath + ChartName;
        Chart1.Save(path, ChartImageFormat.Png);
    }
    //食物類_肉類
    private void food_Meat()
    {
        string typeID = "010102"; //項目代號
        ChartName = "food_Meat.png";
        clear_arraylist();
        DefaultOption();
        Chart1.Series[0].Enabled = true;

        try
        {
            DataBaseClass db = new DataBaseClass();
            DataTable Dt = new DataTable();
            string sql = string.Empty;
            string temp_Month = string.Empty;
            string temp_Val = string.Empty;

            sql += " SELECT TOP 36 Date, CursorValue ";
            sql += " FROM  Gov_StatData ";
            sql += " WHERE MyIndex = @MyIndex ";
            sql += " ORDER BY Date DESC ";

            SqlParameter[] prams = { new SqlParameter("@MyIndex", SqlDbType.NVarChar) };
            prams[0].Value = typeID;

            Dt = new DataBaseClass().Select_DataReader_Parameters(ConfigurationManager.ConnectionStrings["_24_Money_ConnStr"].ConnectionString, sql, prams);

            if (Dt.Rows.Count == 0 || Dt.Rows == null) return;
            foreach (DataRow rows in Dt.Rows)
            {
                //月份
                temp_Month = (rows["Date"].ToString() != "")
                    ? rows["Date"].ToString().Substring(0, 4) + "/" + rows["Date"].ToString().Substring(4, 2)
                    : "";

                //數據
                temp_Val = (rows["CursorValue"].ToString() != "")
                    ? rows["CursorValue"].ToString()
                    : "0";

                if (temp_Month != "")
                {
                    x_ser1.Add(temp_Month);
                    y_ser1.Add(temp_Val);
                }
            }

            db = null;
            Dt.Dispose();
        }
        catch (Exception ee)
        { Response.Write(ee.ToString()); }

        x_ser1.Reverse();
        y_ser1.Reverse();

        Chart1.Series[0].Points.DataBindXY(x_ser1, y_ser1);

        //設定圖表屬性
        Option_Chart1();
        Style01();

        //屬性修正
        Chart1.Series["Series1"].YAxisType = AxisType.Primary;
        Chart1.Series["Series1"].Type = SeriesChartType.Line;
        Chart1.Series["Series1"].Color = System.Drawing.Color.FromArgb(255, 64, 119, 183);

        //設定title
        Chart1.Series["Series1"].LegendText = "肉類";

        //save
        string path = ChartPath + ChartName;
        Chart1.Save(path, ChartImageFormat.Png);
    }
    //食物類_蛋類
    private void food_Eggs()
    {
        string typeID = "010104"; //項目代號
        ChartName = "food_Eggs.png";
        clear_arraylist();
        DefaultOption();
        Chart1.Series[0].Enabled = true;

        try
        {
            DataBaseClass db = new DataBaseClass();
            DataTable Dt = new DataTable();
            string sql = string.Empty;
            string temp_Month = string.Empty;
            string temp_Val = string.Empty;

            sql += " SELECT TOP 36 Date, CursorValue ";
            sql += " FROM  Gov_StatData ";
            sql += " WHERE MyIndex = @MyIndex ";
            sql += " ORDER BY Date DESC ";

            SqlParameter[] prams = { new SqlParameter("@MyIndex", SqlDbType.NVarChar) };
            prams[0].Value = typeID;

            Dt = new DataBaseClass().Select_DataReader_Parameters(ConfigurationManager.ConnectionStrings["_24_Money_ConnStr"].ConnectionString, sql, prams);

            if (Dt.Rows.Count == 0 || Dt.Rows == null) return;
            foreach (DataRow rows in Dt.Rows)
            {
                //月份
                temp_Month = (rows["Date"].ToString() != "")
                    ? rows["Date"].ToString().Substring(0, 4) + "/" + rows["Date"].ToString().Substring(4, 2)
                    : "";

                //數據
                temp_Val = (rows["CursorValue"].ToString() != "")
                    ? rows["CursorValue"].ToString()
                    : "0";

                if (temp_Month != "")
                {
                    x_ser1.Add(temp_Month);
                    y_ser1.Add(temp_Val);
                }
            }

            db = null;
            Dt.Dispose();
        }
        catch (Exception ee)
        { Response.Write(ee.ToString()); }

        x_ser1.Reverse();
        y_ser1.Reverse();

        Chart1.Series[0].Points.DataBindXY(x_ser1, y_ser1);

        //設定圖表屬性
        Option_Chart1();
        Style01();

        //屬性修正
        Chart1.Series["Series1"].YAxisType = AxisType.Primary;
        Chart1.Series["Series1"].Type = SeriesChartType.Line;
        Chart1.Series["Series1"].Color = System.Drawing.Color.FromArgb(255, 64, 119, 183);

        //設定title
        Chart1.Series["Series1"].LegendText = "蛋類";

        //save
        string path = ChartPath + ChartName;
        Chart1.Save(path, ChartImageFormat.Png);
    }
    //食物類_水果
    private void food_Fruit()
    {
        string typeID = "010109"; //項目代號
        ChartName = "food_Fruit.png";
        clear_arraylist();
        DefaultOption();
        Chart1.Series[0].Enabled = true;

        try
        {
            DataBaseClass db = new DataBaseClass();
            DataTable Dt = new DataTable();
            string sql = string.Empty;
            string temp_Month = string.Empty;
            string temp_Val = string.Empty;

            sql += " SELECT TOP 36 Date, CursorValue ";
            sql += " FROM  Gov_StatData ";
            sql += " WHERE MyIndex = @MyIndex ";
            sql += " ORDER BY Date DESC ";

            SqlParameter[] prams = { new SqlParameter("@MyIndex", SqlDbType.NVarChar) };
            prams[0].Value = typeID;

            Dt = new DataBaseClass().Select_DataReader_Parameters(ConfigurationManager.ConnectionStrings["_24_Money_ConnStr"].ConnectionString, sql, prams);

            if (Dt.Rows.Count == 0 || Dt.Rows == null) return;
            foreach (DataRow rows in Dt.Rows)
            {
                //月份
                temp_Month = (rows["Date"].ToString() != "")
                    ? rows["Date"].ToString().Substring(0, 4) + "/" + rows["Date"].ToString().Substring(4, 2)
                    : "";

                //數據
                temp_Val = (rows["CursorValue"].ToString() != "")
                    ? rows["CursorValue"].ToString()
                    : "0";

                if (temp_Month != "")
                {
                    x_ser1.Add(temp_Month);
                    y_ser1.Add(temp_Val);
                }
            }

            db = null;
            Dt.Dispose();
        }
        catch (Exception ee)
        { Response.Write(ee.ToString()); }

        x_ser1.Reverse();
        y_ser1.Reverse();

        Chart1.Series[0].Points.DataBindXY(x_ser1, y_ser1);

        //設定圖表屬性
        Option_Chart1();
        Style01();

        //屬性修正
        Chart1.Series["Series1"].YAxisType = AxisType.Primary;
        Chart1.Series["Series1"].Type = SeriesChartType.Line;
        Chart1.Series["Series1"].Color = System.Drawing.Color.FromArgb(255, 64, 119, 183);

        //設定title
        Chart1.Series["Series1"].LegendText = "水果";

        //save
        string path = ChartPath + ChartName;
        Chart1.Save(path, ChartImageFormat.Png);
    }
    //食物類_蔬菜
    private void food_Vegetables()
    {
        string typeID = "010107"; //項目代號
        ChartName = "food_Vegetables.png";
        clear_arraylist();
        DefaultOption();
        Chart1.Series[0].Enabled = true;

        try
        {
            DataBaseClass db = new DataBaseClass();
            DataTable Dt = new DataTable();
            string sql = string.Empty;
            string temp_Month = string.Empty;
            string temp_Val = string.Empty;

            sql += " SELECT TOP 36 Date, CursorValue ";
            sql += " FROM  Gov_StatData ";
            sql += " WHERE MyIndex = @MyIndex ";
            sql += " ORDER BY Date DESC ";

            SqlParameter[] prams = { new SqlParameter("@MyIndex", SqlDbType.NVarChar) };
            prams[0].Value = typeID;

            Dt = new DataBaseClass().Select_DataReader_Parameters(ConfigurationManager.ConnectionStrings["_24_Money_ConnStr"].ConnectionString, sql, prams);

            if (Dt.Rows.Count == 0 || Dt.Rows == null) return;
            foreach (DataRow rows in Dt.Rows)
            {
                //月份
                temp_Month = (rows["Date"].ToString() != "")
                    ? rows["Date"].ToString().Substring(0, 4) + "/" + rows["Date"].ToString().Substring(4, 2)
                    : "";

                //數據
                temp_Val = (rows["CursorValue"].ToString() != "")
                    ? rows["CursorValue"].ToString()
                    : "0";

                if (temp_Month != "")
                {
                    x_ser1.Add(temp_Month);
                    y_ser1.Add(temp_Val);
                }
            }

            db = null;
            Dt.Dispose();
        }
        catch (Exception ee)
        { Response.Write(ee.ToString()); }

        x_ser1.Reverse();
        y_ser1.Reverse();

        Chart1.Series[0].Points.DataBindXY(x_ser1, y_ser1);

        //設定圖表屬性
        Option_Chart1();
        Style01();

        //屬性修正
        Chart1.Series["Series1"].YAxisType = AxisType.Primary;
        Chart1.Series["Series1"].Type = SeriesChartType.Line;
        Chart1.Series["Series1"].Color = System.Drawing.Color.FromArgb(255, 64, 119, 183);

        //設定title
        Chart1.Series["Series1"].LegendText = "蔬菜";

        //save
        string path = ChartPath + ChartName;
        Chart1.Save(path, ChartImageFormat.Png);
    }
    //食物類_水產
    private void food_Fisheries()
    {
        string typeID = "010105"; //項目代號
        ChartName = "food_Fisheries.png";
        clear_arraylist();
        DefaultOption();
        Chart1.Series[0].Enabled = true;

        try
        {
            DataBaseClass db = new DataBaseClass();
            DataTable Dt = new DataTable();
            string sql = string.Empty;
            string temp_Month = string.Empty;
            string temp_Val = string.Empty;

            sql += " SELECT TOP 36 Date, CursorValue ";
            sql += " FROM  Gov_StatData ";
            sql += " WHERE MyIndex = @MyIndex ";
            sql += " ORDER BY Date DESC ";

            SqlParameter[] prams = { new SqlParameter("@MyIndex", SqlDbType.NVarChar) };
            prams[0].Value = typeID;

            Dt = new DataBaseClass().Select_DataReader_Parameters(ConfigurationManager.ConnectionStrings["_24_Money_ConnStr"].ConnectionString, sql, prams);

            if (Dt.Rows.Count == 0 || Dt.Rows == null) return;
            foreach (DataRow rows in Dt.Rows)
            {
                //月份
                temp_Month = (rows["Date"].ToString() != "")
                    ? rows["Date"].ToString().Substring(0, 4) + "/" + rows["Date"].ToString().Substring(4, 2)
                    : "";

                //數據
                temp_Val = (rows["CursorValue"].ToString() != "")
                    ? rows["CursorValue"].ToString()
                    : "0";

                if (temp_Month != "")
                {
                    x_ser1.Add(temp_Month);
                    y_ser1.Add(temp_Val);
                }
            }

            db = null;
            Dt.Dispose();
        }
        catch (Exception ee)
        { Response.Write(ee.ToString()); }

        x_ser1.Reverse();
        y_ser1.Reverse();

        Chart1.Series[0].Points.DataBindXY(x_ser1, y_ser1);

        //設定圖表屬性
        Option_Chart1();
        Style01();

        //屬性修正
        Chart1.Series["Series1"].YAxisType = AxisType.Primary;
        Chart1.Series["Series1"].Type = SeriesChartType.Line;
        Chart1.Series["Series1"].Color = System.Drawing.Color.FromArgb(255, 64, 119, 183);

        //設定title
        Chart1.Series["Series1"].LegendText = "水產";

        //save
        string path = ChartPath + ChartName;
        Chart1.Save(path, ChartImageFormat.Png);
    }



    //交通類_油料費
    private void traffic_FuelFee()
    {
        string typeID = "010402"; //項目代號
        ChartName = "traffic_FuelFee.png";
        clear_arraylist();
        DefaultOption();
        Chart1.Series[0].Enabled = true;

        try
        {
            DataBaseClass db = new DataBaseClass();
            DataTable Dt = new DataTable();
            string sql = string.Empty;
            string temp_Month = string.Empty;
            string temp_Val = string.Empty;

            sql += " SELECT TOP 36 Date, CursorValue ";
            sql += " FROM  Gov_StatData ";
            sql += " WHERE MyIndex = @MyIndex ";
            sql += " ORDER BY Date DESC ";

            SqlParameter[] prams = { new SqlParameter("@MyIndex", SqlDbType.NVarChar) };
            prams[0].Value = typeID;

            Dt = new DataBaseClass().Select_DataReader_Parameters(ConfigurationManager.ConnectionStrings["_24_Money_ConnStr"].ConnectionString, sql, prams);

            if (Dt.Rows.Count == 0 || Dt.Rows == null) return;
            foreach (DataRow rows in Dt.Rows)
            {
                //月份
                temp_Month = (rows["Date"].ToString() != "")
                    ? rows["Date"].ToString().Substring(0, 4) + "/" + rows["Date"].ToString().Substring(4, 2)
                    : "";

                //數據
                temp_Val = (rows["CursorValue"].ToString() != "")
                    ? rows["CursorValue"].ToString()
                    : "0";

                if (temp_Month != "")
                {
                    x_ser1.Add(temp_Month);
                    y_ser1.Add(temp_Val);
                }
            }

            db = null;
            Dt.Dispose();
        }
        catch (Exception ee)
        { Response.Write(ee.ToString()); }

        x_ser1.Reverse();
        y_ser1.Reverse();

        Chart1.Series[0].Points.DataBindXY(x_ser1, y_ser1);

        //設定圖表屬性
        Option_Chart1();
        Style01();

        //屬性修正
        Chart1.Series["Series1"].YAxisType = AxisType.Primary;
        Chart1.Series["Series1"].Type = SeriesChartType.Line;
        Chart1.Series["Series1"].Color = System.Drawing.Color.FromArgb(255, 64, 119, 183);

        //設定title
        Chart1.Series["Series1"].LegendText = "油料費";

        //save
        string path = ChartPath + ChartName;
        Chart1.Save(path, ChartImageFormat.Png);
    }
    //交通類_交通服務及維修零件
    private void traffic_ServiceParts()
    {
        string typeID = "010403"; //項目代號
        ChartName = "traffic_ServiceParts.png";
        clear_arraylist();
        DefaultOption();
        Chart1.Series[0].Enabled = true;

        try
        {
            DataBaseClass db = new DataBaseClass();
            DataTable Dt = new DataTable();
            string sql = string.Empty;
            string temp_Month = string.Empty;
            string temp_Val = string.Empty;

            sql += " SELECT TOP 36 Date, CursorValue ";
            sql += " FROM  Gov_StatData ";
            sql += " WHERE MyIndex = @MyIndex ";
            sql += " ORDER BY Date DESC ";

            SqlParameter[] prams = { new SqlParameter("@MyIndex", SqlDbType.NVarChar) };
            prams[0].Value = typeID;

            Dt = new DataBaseClass().Select_DataReader_Parameters(ConfigurationManager.ConnectionStrings["_24_Money_ConnStr"].ConnectionString, sql, prams);

            if (Dt.Rows.Count == 0 || Dt.Rows == null) return;
            foreach (DataRow rows in Dt.Rows)
            {
                //月份
                temp_Month = (rows["Date"].ToString() != "")
                    ? rows["Date"].ToString().Substring(0, 4) + "/" + rows["Date"].ToString().Substring(4, 2)
                    : "";

                //數據
                temp_Val = (rows["CursorValue"].ToString() != "")
                    ? rows["CursorValue"].ToString()
                    : "0";

                if (temp_Month != "")
                {
                    x_ser1.Add(temp_Month);
                    y_ser1.Add(temp_Val);
                }
            }

            db = null;
            Dt.Dispose();
        }
        catch (Exception ee)
        { Response.Write(ee.ToString()); }

        x_ser1.Reverse();
        y_ser1.Reverse();

        Chart1.Series[0].Points.DataBindXY(x_ser1, y_ser1);

        //設定圖表屬性
        Option_Chart1();
        Style01();

        //屬性修正
        Chart1.Series["Series1"].YAxisType = AxisType.Primary;
        Chart1.Series["Series1"].Type = SeriesChartType.Line;
        Chart1.Series["Series1"].Color = System.Drawing.Color.FromArgb(255, 64, 119, 183);

        //設定title
        Chart1.Series["Series1"].LegendText = "交通服務及維修零件";

        //save
        string path = ChartPath + ChartName;
        Chart1.Save(path, ChartImageFormat.Png);
    }



    //居住類_水電瓦斯
    private void live_HydropowerGas()
    {
        string typeID = "010305"; //項目代號
        ChartName = "live_HydropowerGas.png";
        clear_arraylist();
        DefaultOption();
        Chart1.Series[0].Enabled = true;

        try
        {
            DataBaseClass db = new DataBaseClass();
            DataTable Dt = new DataTable();
            string sql = string.Empty;
            string temp_Month = string.Empty;
            string temp_Val = string.Empty;

            sql += " SELECT TOP 36 Date, CursorValue ";
            sql += " FROM  Gov_StatData ";
            sql += " WHERE MyIndex = @MyIndex ";
            sql += " ORDER BY Date DESC ";

            SqlParameter[] prams = { new SqlParameter("@MyIndex", SqlDbType.NVarChar) };
            prams[0].Value = typeID;

            Dt = new DataBaseClass().Select_DataReader_Parameters(ConfigurationManager.ConnectionStrings["_24_Money_ConnStr"].ConnectionString, sql, prams);

            if (Dt.Rows.Count == 0 || Dt.Rows == null) return;
            foreach (DataRow rows in Dt.Rows)
            {
                //月份
                temp_Month = (rows["Date"].ToString() != "")
                    ? rows["Date"].ToString().Substring(0, 4) + "/" + rows["Date"].ToString().Substring(4, 2)
                    : "";

                //數據
                temp_Val = (rows["CursorValue"].ToString() != "")
                    ? rows["CursorValue"].ToString()
                    : "0";

                if (temp_Month != "")
                {
                    x_ser1.Add(temp_Month);
                    y_ser1.Add(temp_Val);
                }
            }

            db = null;
            Dt.Dispose();
        }
        catch (Exception ee)
        { Response.Write(ee.ToString()); }

        x_ser1.Reverse();
        y_ser1.Reverse();

        Chart1.Series[0].Points.DataBindXY(x_ser1, y_ser1);

        //設定圖表屬性
        Option_Chart1();
        Style01();

        //屬性修正
        Chart1.Series["Series1"].YAxisType = AxisType.Primary;
        Chart1.Series["Series1"].Type = SeriesChartType.Line;
        Chart1.Series["Series1"].Color = System.Drawing.Color.FromArgb(255, 64, 119, 183);

        //設定title
        Chart1.Series["Series1"].LegendText = "水電瓦斯";

        //save
        string path = ChartPath + ChartName;
        Chart1.Save(path, ChartImageFormat.Png);
    }
    //居住類_房租
    private void live_Rent()
    {
        string typeID = "010301"; //項目代號
        ChartName = "live_Rent.png";
        clear_arraylist();
        DefaultOption();
        Chart1.Series[0].Enabled = true;

        try
        {
            DataBaseClass db = new DataBaseClass();
            DataTable Dt = new DataTable();
            string sql = string.Empty;
            string temp_Month = string.Empty;
            string temp_Val = string.Empty;

            sql += " SELECT TOP 36 Date, CursorValue ";
            sql += " FROM  Gov_StatData ";
            sql += " WHERE MyIndex = @MyIndex ";
            sql += " ORDER BY Date DESC ";

            SqlParameter[] prams = { new SqlParameter("@MyIndex", SqlDbType.NVarChar) };
            prams[0].Value = typeID;

            Dt = new DataBaseClass().Select_DataReader_Parameters(ConfigurationManager.ConnectionStrings["_24_Money_ConnStr"].ConnectionString, sql, prams);

            if (Dt.Rows.Count == 0 || Dt.Rows == null) return;
            foreach (DataRow rows in Dt.Rows)
            {
                //月份
                temp_Month = (rows["Date"].ToString() != "")
                    ? rows["Date"].ToString().Substring(0, 4) + "/" + rows["Date"].ToString().Substring(4, 2)
                    : "";

                //數據
                temp_Val = (rows["CursorValue"].ToString() != "")
                    ? rows["CursorValue"].ToString()
                    : "0";

                if (temp_Month != "")
                {
                    x_ser1.Add(temp_Month);
                    y_ser1.Add(temp_Val);
                }
            }

            db = null;
            Dt.Dispose();
        }
        catch (Exception ee)
        { Response.Write(ee.ToString()); }

        x_ser1.Reverse();
        y_ser1.Reverse();

        Chart1.Series[0].Points.DataBindXY(x_ser1, y_ser1);

        //設定圖表屬性
        Option_Chart1();
        Style01();

        //屬性修正
        Chart1.Series["Series1"].YAxisType = AxisType.Primary;
        Chart1.Series["Series1"].Type = SeriesChartType.Line;
        Chart1.Series["Series1"].Color = System.Drawing.Color.FromArgb(255, 64, 119, 183);

        //設定title
        Chart1.Series["Series1"].LegendText = "房租";

        //save
        string path = ChartPath + ChartName;
        Chart1.Save(path, ChartImageFormat.Png);
    }
    //居住類_家庭用品
    private void live_HouseholdGoods()
    {
        string typeID = "010303"; //項目代號
        ChartName = "live_HouseholdGoods.png";
        clear_arraylist();
        DefaultOption();
        Chart1.Series[0].Enabled = true;

        try
        {
            DataBaseClass db = new DataBaseClass();
            DataTable Dt = new DataTable();
            string sql = string.Empty;
            string temp_Month = string.Empty;
            string temp_Val = string.Empty;

            sql += " SELECT TOP 36 Date, CursorValue ";
            sql += " FROM  Gov_StatData ";
            sql += " WHERE MyIndex = @MyIndex ";
            sql += " ORDER BY Date DESC ";

            SqlParameter[] prams = { new SqlParameter("@MyIndex", SqlDbType.NVarChar) };
            prams[0].Value = typeID;

            Dt = new DataBaseClass().Select_DataReader_Parameters(ConfigurationManager.ConnectionStrings["_24_Money_ConnStr"].ConnectionString, sql, prams);

            if (Dt.Rows.Count == 0 || Dt.Rows == null) return;
            foreach (DataRow rows in Dt.Rows)
            {
                //月份
                temp_Month = (rows["Date"].ToString() != "")
                    ? rows["Date"].ToString().Substring(0, 4) + "/" + rows["Date"].ToString().Substring(4, 2)
                    : "";

                //數據
                temp_Val = (rows["CursorValue"].ToString() != "")
                    ? rows["CursorValue"].ToString()
                    : "0";

                if (temp_Month != "")
                {
                    x_ser1.Add(temp_Month);
                    y_ser1.Add(temp_Val);
                }
            }

            db = null;
            Dt.Dispose();
        }
        catch (Exception ee)
        { Response.Write(ee.ToString()); }

        x_ser1.Reverse();
        y_ser1.Reverse();

        Chart1.Series[0].Points.DataBindXY(x_ser1, y_ser1);

        //設定圖表屬性
        Option_Chart1();
        Style01();

        //屬性修正
        Chart1.Series["Series1"].YAxisType = AxisType.Primary;
        Chart1.Series["Series1"].Type = SeriesChartType.Line;
        Chart1.Series["Series1"].Color = System.Drawing.Color.FromArgb(255, 64, 119, 183);

        //設定title
        Chart1.Series["Series1"].LegendText = "家庭用品";

        //save
        string path = ChartPath + ChartName;
        Chart1.Save(path, ChartImageFormat.Png);
    }



    //衣服類_成衣
    private void clothing_Clothing()
    {
        string typeID = "010201"; //項目代號
        ChartName = "clothing_Clothing.png";
        clear_arraylist();
        DefaultOption();
        Chart1.Series[0].Enabled = true;

        try
        {
            DataBaseClass db = new DataBaseClass();
            DataTable Dt = new DataTable();
            string sql = string.Empty;
            string temp_Month = string.Empty;
            string temp_Val = string.Empty;

            sql += " SELECT TOP 36 Date, CursorValue ";
            sql += " FROM  Gov_StatData ";
            sql += " WHERE MyIndex = @MyIndex ";
            sql += " ORDER BY Date DESC ";

            SqlParameter[] prams = { new SqlParameter("@MyIndex", SqlDbType.NVarChar) };
            prams[0].Value = typeID;

            Dt = new DataBaseClass().Select_DataReader_Parameters(ConfigurationManager.ConnectionStrings["_24_Money_ConnStr"].ConnectionString, sql, prams);

            if (Dt.Rows.Count == 0 || Dt.Rows == null) return;
            foreach (DataRow rows in Dt.Rows)
            {
                //月份
                temp_Month = (rows["Date"].ToString() != "")
                    ? rows["Date"].ToString().Substring(0, 4) + "/" + rows["Date"].ToString().Substring(4, 2)
                    : "";

                //數據
                temp_Val = (rows["CursorValue"].ToString() != "")
                    ? rows["CursorValue"].ToString()
                    : "0";

                if (temp_Month != "")
                {
                    x_ser1.Add(temp_Month);
                    y_ser1.Add(temp_Val);
                }
            }

            db = null;
            Dt.Dispose();
        }
        catch (Exception ee)
        { Response.Write(ee.ToString()); }

        x_ser1.Reverse();
        y_ser1.Reverse();

        Chart1.Series[0].Points.DataBindXY(x_ser1, y_ser1);

        //設定圖表屬性
        Option_Chart1();
        Style01();

        //屬性修正
        Chart1.Series["Series1"].YAxisType = AxisType.Primary;
        Chart1.Series["Series1"].Type = SeriesChartType.Line;
        Chart1.Series["Series1"].Color = System.Drawing.Color.FromArgb(255, 64, 119, 183);

        //設定title
        Chart1.Series["Series1"].LegendText = "成衣";

        //save
        string path = ChartPath + ChartName;
        Chart1.Save(path, ChartImageFormat.Png);
    }
    //衣服類_鞋襪
    private void clothing_ShoesSocks()
    {
        string typeID = "010202"; //項目代號
        ChartName = "clothing_ShoesSocks.png";
        clear_arraylist();
        DefaultOption();
        Chart1.Series[0].Enabled = true;

        try
        {
            DataBaseClass db = new DataBaseClass();
            DataTable Dt = new DataTable();
            string sql = string.Empty;
            string temp_Month = string.Empty;
            string temp_Val = string.Empty;

            sql += " SELECT TOP 36 Date, CursorValue ";
            sql += " FROM  Gov_StatData ";
            sql += " WHERE MyIndex = @MyIndex ";
            sql += " ORDER BY Date DESC ";

            SqlParameter[] prams = { new SqlParameter("@MyIndex", SqlDbType.NVarChar) };
            prams[0].Value = typeID;

            Dt = new DataBaseClass().Select_DataReader_Parameters(ConfigurationManager.ConnectionStrings["_24_Money_ConnStr"].ConnectionString, sql, prams);

            if (Dt.Rows.Count == 0 || Dt.Rows == null) return;
            foreach (DataRow rows in Dt.Rows)
            {
                //月份
                temp_Month = (rows["Date"].ToString() != "")
                    ? rows["Date"].ToString().Substring(0, 4) + "/" + rows["Date"].ToString().Substring(4, 2)
                    : "";

                //數據
                temp_Val = (rows["CursorValue"].ToString() != "")
                    ? rows["CursorValue"].ToString()
                    : "0";

                if (temp_Month != "")
                {
                    x_ser1.Add(temp_Month);
                    y_ser1.Add(temp_Val);
                }
            }

            db = null;
            Dt.Dispose();
        }
        catch (Exception ee)
        { Response.Write(ee.ToString()); }

        x_ser1.Reverse();
        y_ser1.Reverse();

        Chart1.Series[0].Points.DataBindXY(x_ser1, y_ser1);

        //設定圖表屬性
        Option_Chart1();
        Style01();

        //屬性修正
        Chart1.Series["Series1"].YAxisType = AxisType.Primary;
        Chart1.Series["Series1"].Type = SeriesChartType.Line;
        Chart1.Series["Series1"].Color = System.Drawing.Color.FromArgb(255, 64, 119, 183);

        //設定title
        Chart1.Series["Series1"].LegendText = "鞋襪";

        //save
        string path = ChartPath + ChartName;
        Chart1.Save(path, ChartImageFormat.Png);
    }
    //衣服類_衣著服務及配件
    private void clothing_ServicesAccessories()
    {
        string typeID = "010203"; //項目代號
        ChartName = "clothing_ServicesAccessories.png";
        clear_arraylist();
        DefaultOption();
        Chart1.Series[0].Enabled = true;

        try
        {
            DataBaseClass db = new DataBaseClass();
            DataTable Dt = new DataTable();
            string sql = string.Empty;
            string temp_Month = string.Empty;
            string temp_Val = string.Empty;

            sql += " SELECT TOP 36 Date, CursorValue ";
            sql += " FROM  Gov_StatData ";
            sql += " WHERE MyIndex = @MyIndex ";
            sql += " ORDER BY Date DESC ";

            SqlParameter[] prams = { new SqlParameter("@MyIndex", SqlDbType.NVarChar) };
            prams[0].Value = typeID;

            Dt = new DataBaseClass().Select_DataReader_Parameters(ConfigurationManager.ConnectionStrings["_24_Money_ConnStr"].ConnectionString, sql, prams);

            if (Dt.Rows.Count == 0 || Dt.Rows == null) return;
            foreach (DataRow rows in Dt.Rows)
            {
                //月份
                temp_Month = (rows["Date"].ToString() != "")
                    ? rows["Date"].ToString().Substring(0, 4) + "/" + rows["Date"].ToString().Substring(4, 2)
                    : "";

                //數據
                temp_Val = (rows["CursorValue"].ToString() != "")
                    ? rows["CursorValue"].ToString()
                    : "0";

                if (temp_Month != "")
                {
                    x_ser1.Add(temp_Month);
                    y_ser1.Add(temp_Val);
                }
            }

            db = null;
            Dt.Dispose();
        }
        catch (Exception ee)
        { Response.Write(ee.ToString()); }

        x_ser1.Reverse();
        y_ser1.Reverse();

        Chart1.Series[0].Points.DataBindXY(x_ser1, y_ser1);

        //設定圖表屬性
        Option_Chart1();
        Style01();

        //屬性修正
        Chart1.Series["Series1"].YAxisType = AxisType.Primary;
        Chart1.Series["Series1"].Type = SeriesChartType.Line;
        Chart1.Series["Series1"].Color = System.Drawing.Color.FromArgb(255, 64, 119, 183);

        //設定title
        Chart1.Series["Series1"].LegendText = "衣著服務及配件";

        //save
        string path = ChartPath + ChartName;
        Chart1.Save(path, ChartImageFormat.Png);
    }



    //其他類_醫療費用
    private void other_MedicalExpenses()
    {
        string typeID = "010501"; //項目代號
        ChartName = "other_MedicalExpenses.png";
        clear_arraylist();
        DefaultOption();
        Chart1.Series[0].Enabled = true;

        try
        {
            DataBaseClass db = new DataBaseClass();
            DataTable Dt = new DataTable();
            string sql = string.Empty;
            string temp_Month = string.Empty;
            string temp_Val = string.Empty;

            sql += " SELECT TOP 36 Date, CursorValue ";
            sql += " FROM  Gov_StatData ";
            sql += " WHERE MyIndex = @MyIndex ";
            sql += " ORDER BY Date DESC ";

            SqlParameter[] prams = { new SqlParameter("@MyIndex", SqlDbType.NVarChar) };
            prams[0].Value = typeID;

            Dt = new DataBaseClass().Select_DataReader_Parameters(ConfigurationManager.ConnectionStrings["_24_Money_ConnStr"].ConnectionString, sql, prams);

            if (Dt.Rows.Count == 0 || Dt.Rows == null) return;
            foreach (DataRow rows in Dt.Rows)
            {
                //月份
                temp_Month = (rows["Date"].ToString() != "")
                    ? rows["Date"].ToString().Substring(0, 4) + "/" + rows["Date"].ToString().Substring(4, 2)
                    : "";

                //數據
                temp_Val = (rows["CursorValue"].ToString() != "")
                    ? rows["CursorValue"].ToString()
                    : "0";

                if (temp_Month != "")
                {
                    x_ser1.Add(temp_Month);
                    y_ser1.Add(temp_Val);
                }
            }

            db = null;
            Dt.Dispose();
        }
        catch (Exception ee)
        { Response.Write(ee.ToString()); }

        x_ser1.Reverse();
        y_ser1.Reverse();

        Chart1.Series[0].Points.DataBindXY(x_ser1, y_ser1);

        //設定圖表屬性
        Option_Chart1();
        Style01();

        //屬性修正
        Chart1.Series["Series1"].YAxisType = AxisType.Primary;
        Chart1.Series["Series1"].Type = SeriesChartType.Line;
        Chart1.Series["Series1"].Color = System.Drawing.Color.FromArgb(255, 64, 119, 183);

        //設定title
        Chart1.Series["Series1"].LegendText = "醫療費用";

        //save
        string path = ChartPath + ChartName;
        Chart1.Save(path, ChartImageFormat.Png);
    }
    //其他類_教養費用
    private void other_RearingCosts()
    {
        string typeID = "010601"; //項目代號
        ChartName = "other_RearingCosts.png";
        clear_arraylist();
        DefaultOption();
        Chart1.Series[0].Enabled = true;

        try
        {
            DataBaseClass db = new DataBaseClass();
            DataTable Dt = new DataTable();
            string sql = string.Empty;
            string temp_Month = string.Empty;
            string temp_Val = string.Empty;

            sql += " SELECT TOP 36 Date, CursorValue ";
            sql += " FROM  Gov_StatData ";
            sql += " WHERE MyIndex = @MyIndex ";
            sql += " ORDER BY Date DESC ";

            SqlParameter[] prams = { new SqlParameter("@MyIndex", SqlDbType.NVarChar) };
            prams[0].Value = typeID;

            Dt = new DataBaseClass().Select_DataReader_Parameters(ConfigurationManager.ConnectionStrings["_24_Money_ConnStr"].ConnectionString, sql, prams);

            if (Dt.Rows.Count == 0 || Dt.Rows == null) return;
            foreach (DataRow rows in Dt.Rows)
            {
                //月份
                temp_Month = (rows["Date"].ToString() != "")
                    ? rows["Date"].ToString().Substring(0, 4) + "/" + rows["Date"].ToString().Substring(4, 2)
                    : "";

                //數據
                temp_Val = (rows["CursorValue"].ToString() != "")
                    ? rows["CursorValue"].ToString()
                    : "0";

                if (temp_Month != "")
                {
                    x_ser1.Add(temp_Month);
                    y_ser1.Add(temp_Val);
                }
            }

            db = null;
            Dt.Dispose();
        }
        catch (Exception ee)
        { Response.Write(ee.ToString()); }

        x_ser1.Reverse();
        y_ser1.Reverse();

        Chart1.Series[0].Points.DataBindXY(x_ser1, y_ser1);

        //設定圖表屬性
        Option_Chart1();
        Style01();

        //屬性修正
        Chart1.Series["Series1"].YAxisType = AxisType.Primary;
        Chart1.Series["Series1"].Type = SeriesChartType.Line;
        Chart1.Series["Series1"].Color = System.Drawing.Color.FromArgb(255, 64, 119, 183);

        //設定title
        Chart1.Series["Series1"].LegendText = "教養費用";

        //save
        string path = ChartPath + ChartName;
        Chart1.Save(path, ChartImageFormat.Png);
    }
    //其他類_香菸及檳榔
    private void other_CigarettesBetelNuts()
    {
        string typeID = "010701"; //項目代號
        ChartName = "other_CigarettesBetelNuts.png";
        clear_arraylist();
        DefaultOption();
        Chart1.Series[0].Enabled = true;

        try
        {
            DataBaseClass db = new DataBaseClass();
            DataTable Dt = new DataTable();
            string sql = string.Empty;
            string temp_Month = string.Empty;
            string temp_Val = string.Empty;

            sql += " SELECT TOP 36 Date, CursorValue ";
            sql += " FROM  Gov_StatData ";
            sql += " WHERE MyIndex = @MyIndex ";
            sql += " ORDER BY Date DESC ";

            SqlParameter[] prams = { new SqlParameter("@MyIndex", SqlDbType.NVarChar) };
            prams[0].Value = typeID;

            Dt = new DataBaseClass().Select_DataReader_Parameters(ConfigurationManager.ConnectionStrings["_24_Money_ConnStr"].ConnectionString, sql, prams);

            if (Dt.Rows.Count == 0 || Dt.Rows == null) return;
            foreach (DataRow rows in Dt.Rows)
            {
                //月份
                temp_Month = (rows["Date"].ToString() != "")
                    ? rows["Date"].ToString().Substring(0, 4) + "/" + rows["Date"].ToString().Substring(4, 2)
                    : "";

                //數據
                temp_Val = (rows["CursorValue"].ToString() != "")
                    ? rows["CursorValue"].ToString()
                    : "0";

                if (temp_Month != "")
                {
                    x_ser1.Add(temp_Month);
                    y_ser1.Add(temp_Val);
                }
            }

            db = null;
            Dt.Dispose();
        }
        catch (Exception ee)
        { Response.Write(ee.ToString()); }

        x_ser1.Reverse();
        y_ser1.Reverse();

        Chart1.Series[0].Points.DataBindXY(x_ser1, y_ser1);

        //設定圖表屬性
        Option_Chart1();
        Style01();

        //屬性修正
        Chart1.Series["Series1"].YAxisType = AxisType.Primary;
        Chart1.Series["Series1"].Type = SeriesChartType.Line;
        Chart1.Series["Series1"].Color = System.Drawing.Color.FromArgb(255, 64, 119, 183);

        //設定title
        Chart1.Series["Series1"].LegendText = "香菸及檳榔";

        //save
        string path = ChartPath + ChartName;
        Chart1.Save(path, ChartImageFormat.Png);
    }
    //其他類_美容及衛生用品
    private void other_BeautyHygieneProducts()
    {
        string typeID = "010702"; //項目代號
        ChartName = "other_BeautyHygieneProducts.png";
        clear_arraylist();
        DefaultOption();
        Chart1.Series[0].Enabled = true;

        try
        {
            DataBaseClass db = new DataBaseClass();
            DataTable Dt = new DataTable();
            string sql = string.Empty;
            string temp_Month = string.Empty;
            string temp_Val = string.Empty;

            sql += " SELECT TOP 36 Date, CursorValue ";
            sql += " FROM  Gov_StatData ";
            sql += " WHERE MyIndex = @MyIndex ";
            sql += " ORDER BY Date DESC ";

            SqlParameter[] prams = { new SqlParameter("@MyIndex", SqlDbType.NVarChar) };
            prams[0].Value = typeID;

            Dt = new DataBaseClass().Select_DataReader_Parameters(ConfigurationManager.ConnectionStrings["_24_Money_ConnStr"].ConnectionString, sql, prams);

            if (Dt.Rows.Count == 0 || Dt.Rows == null) return;
            foreach (DataRow rows in Dt.Rows)
            {
                //月份
                temp_Month = (rows["Date"].ToString() != "")
                    ? rows["Date"].ToString().Substring(0, 4) + "/" + rows["Date"].ToString().Substring(4, 2)
                    : "";

                //數據
                temp_Val = (rows["CursorValue"].ToString() != "")
                    ? rows["CursorValue"].ToString()
                    : "0";

                if (temp_Month != "")
                {
                    x_ser1.Add(temp_Month);
                    y_ser1.Add(temp_Val);
                }
            }

            db = null;
            Dt.Dispose();
        }
        catch (Exception ee)
        { Response.Write(ee.ToString()); }

        x_ser1.Reverse();
        y_ser1.Reverse();

        Chart1.Series[0].Points.DataBindXY(x_ser1, y_ser1);

        //設定圖表屬性
        Option_Chart1();
        Style01();

        //屬性修正
        Chart1.Series["Series1"].YAxisType = AxisType.Primary;
        Chart1.Series["Series1"].Type = SeriesChartType.Line;
        Chart1.Series["Series1"].Color = System.Drawing.Color.FromArgb(255, 64, 119, 183);

        //設定title
        Chart1.Series["Series1"].LegendText = "美容及衛生用品";

        //save
        string path = ChartPath + ChartName;
        Chart1.Save(path, ChartImageFormat.Png);
    }




    //------------------------------------------------------------------
    //設定
    //------------------------------------------------------------------

    //清空陣列值
    private void clear_arraylist()
    {
        x_ser.Clear();
        x_ser1.Clear();
        x_ser2.Clear();
        x_ser3.Clear();
        x_ser4.Clear();
        x_ser5.Clear();
        y_ser1.Clear();
        y_ser2.Clear();
        y_ser3.Clear();
        y_ser4.Clear();
        y_ser5.Clear();
        y_ser6.Clear();
        y_ser7.Clear();
        y_ser8.Clear();
        y_ser9.Clear();
        y_ser10.Clear();
        y_ser11.Clear();
    }


    #region chart 設定
    private void Option_Chart1()
    {
        //設定數列
        Chart1.Series["Series1"].XValueIndexed = true;

        Chart1.Series["Series1"].Type = SeriesChartType.Line;
        Chart1.Series["Series1"].BorderWidth = 2;
        Chart1.Series["Series1"].Color = System.Drawing.Color.FromArgb(190, 90, 89);
        Chart1.Series["Series1"].EmptyPointStyle.Color = System.Drawing.Color.Transparent;
        //Chart1.Series["Series1"].XValueIndexed = true;
        Chart1.Series["Series1"].CustomAttributes = "PointWidth=0.5";
        Chart1.Series["Series1"].YAxisType = AxisType.Primary;
        Chart1.Series["Series1"].LegendText = "Series1";
    }

    private void Style01()
    {
        //設定Legends(數據文字區塊)
        Chart1.Legends["Default"].Font = new Font("Microsoft Sans Serif", float.Parse("7.5"));
        Chart1.Legends["Default"].BackColor = Color.Transparent;
        Chart1.Legends["Default"].AutoFitText = false;    //數據文字說明
        Chart1.Legends["Default"].Position.X = 0;
        Chart1.Legends["Default"].Position.Y = 100;
        Chart1.Legends["Default"].Position.Height = 14;//高
        Chart1.Legends["Default"].Position.Width = 100;//寬
        Chart1.Legends["Default"].Alignment = StringAlignment.Center;//預設置中
        Chart1.Legends["Default"].Enabled = true;//顯示區塊
        Chart1.Legends["Default"].LegendStyle = LegendStyle.Row;
        Chart1.Legends["Default"].Docking = LegendDocking.Bottom;//齊底

        //圖表設定(外距)
        Chart1.ChartAreas["Default"].BorderStyle = ChartDashStyle.Solid;
        Chart1.ChartAreas["Default"].BorderColor = System.Drawing.Color.Black;
        Chart1.ChartAreas["Default"].BorderWidth = 1;
        Chart1.ChartAreas["Default"].Position.X = 5;
        Chart1.ChartAreas["Default"].Position.Y = 0;
        Chart1.ChartAreas["Default"].Position.Width = 95;
        Chart1.ChartAreas["Default"].Position.Height = 85;
        Chart1.ChartAreas["Default"].BorderColor = System.Drawing.Color.FromArgb(255, 249, 249, 249);
        //圖表設定(內距)
        Chart1.ChartAreas["Default"].InnerPlotPosition.Height = 85;
        Chart1.ChartAreas["Default"].InnerPlotPosition.Width = 85;      //內框
        Chart1.ChartAreas["Default"].InnerPlotPosition.X = 10;
        Chart1.ChartAreas["Default"].InnerPlotPosition.Y = 6;

        //設定X軸
        Chart1.ChartAreas["Default"].AxisX.MajorGrid.Enabled = false;
        Chart1.ChartAreas["Default"].AxisX.LineColor = System.Drawing.Color.Gray;
        Chart1.ChartAreas["Default"].AxisX.Interval = 8;    //間距設定，定義每隔幾筆資料顯示一次x軸
        Chart1.ChartAreas["Default"].AxisX.LabelsAutoFit = false;//自動縮放字級
        Chart1.ChartAreas["Default"].AxisX.MajorTickMark.LineColor = System.Drawing.Color.Gray;
        Chart1.ChartAreas["Default"].AxisX.LabelStyle.Font = new Font("Microsoft Sans Serif", float.Parse("7.0"));
        Chart1.ChartAreas["Default"].AxisX.LabelStyle.OffsetLabels = false;  //數據分行顯示
        Chart1.ChartAreas["Default"].AxisX.StartFromZero = false;//從0開始
        Chart1.ChartAreas["Default"].AxisX.LabelStyle.ShowEndLabels = true;    //顯示最後一個值
        Chart1.ChartAreas["Default"].AxisX.Margin = true;      //與邊界保持距儀離

        //設定Y軸
        Chart1.ChartAreas["Default"].AxisY.LabelsAutoFit = false;
        Chart1.ChartAreas["Default"].AxisY.MajorGrid.Enabled = true;
        Chart1.ChartAreas["Default"].AxisY.MajorGrid.LineStyle = ChartDashStyle.Dot;
        Chart1.ChartAreas["Default"].AxisY.MajorGrid.LineColor = System.Drawing.Color.LightGray;
        Chart1.ChartAreas["Default"].AxisY.LineColor = System.Drawing.Color.Gray;
        Chart1.ChartAreas["Default"].AxisY2.LineColor = System.Drawing.Color.Gray;
        Chart1.ChartAreas["Default"].AxisY2.LabelsAutoFit = false;
        Chart1.ChartAreas["Default"].AxisY2.MajorGrid.Enabled = false;
        Chart1.ChartAreas["Default"].AxisY2.MajorGrid.LineStyle = ChartDashStyle.Dot;
        Chart1.ChartAreas["Default"].AxisY2.MajorGrid.LineColor = System.Drawing.Color.LightGray;
        Chart1.ChartAreas["Default"].AxisY.StartFromZero = false;//從0開始
        Chart1.ChartAreas["Default"].AxisY2.StartFromZero = false;//從0開始
        Chart1.ChartAreas["Default"].AxisY.LabelStyle.Format = "0.00";//單位規格
        Chart1.ChartAreas["Default"].AxisY.LabelStyle.FontColor = Color.FromArgb(0, 0, 0);
        Chart1.ChartAreas["Default"].AxisY2.LabelStyle.FontColor = Color.FromArgb(0, 0, 0);
        Chart1.ChartAreas["Default"].AxisY2.LabelStyle.Format = "N0";//單位規格
        //Chart1.ChartAreas["Default"].AxisY.Interval = 2;//間距設定，定義每隔幾筆資料顯示一次x軸
        //Chart1.ChartAreas["Default"].AxisY2.Interval = 6;//間距設定，定義每隔幾筆資料顯示一次x軸
        Chart1.ChartAreas["Default"].AxisY.MajorTickMark.LineColor = System.Drawing.Color.Gray;
        Chart1.ChartAreas["Default"].AxisY2.MajorTickMark.LineColor = System.Drawing.Color.Gray;
        Chart1.ChartAreas["Default"].AxisY.LabelStyle.Font = new Font("Microsoft Sans Serif", float.Parse("7.0"));
        Chart1.ChartAreas["Default"].AxisY2.LabelStyle.Font = new Font("Microsoft Sans Serif", float.Parse("7.0"));
    }

    private void DefaultOption()
    {
        //預設提供兩組Title設定
        Chart1.Titles.Clear();
        Chart1.Titles.Add("Title1");
        //Chart1.Titles.Add("Title2");

        //預設提供兩組圖形字設定
        Chart1.Legends.Clear();
        Chart1.Legends.Add("Default");
        Chart1.Legends.Add("Default1");
        Chart1.Legends.Add("Default2");

        //繪圖區域相關設定重建
        Chart1.Series.Clear();
        Chart1.Series.Add("Series1");
        Chart1.Series.Add("Series2");
        Chart1.Series.Add("Series3");
        Chart1.Series.Add("Series4");
        Chart1.Series.Add("Series5");

        //數據圖預設只開放一個
        Chart1.Series[0].Enabled = true;
        Chart1.Series[1].Enabled = false;
        Chart1.Series[2].Enabled = false;
        Chart1.Series[3].Enabled = false;
        Chart1.Series[4].Enabled = false;


        //設定元件
        Chart1.Width = 355;
        Chart1.Height = 229;

        //背景
        //Chart1.BackColor = System.Drawing.Color.FromArgb(225, 225, 225);
        Chart1.BackColor = System.Drawing.Color.White;


        //標題
        //Chart1.Titles["Title1"].DockToChartArea = "Default";
        //Chart1.Titles["Title1"].Font = new Font("Microsoft Sans Serif", float.Parse("9"));
        //Chart1.Titles["Title1"].Color = Color.Black;
        //Chart1.Titles["Title1"].BackColor = Color.Transparent;
        //Chart1.Titles["Title1"].Position.Height = 15;
        //Chart1.Titles["Title1"].Position.Width = 100;
        //Chart1.Titles["Title1"].Position.X = 0;
        //Chart1.Titles["Title1"].Position.Y = 0;
        //Chart1.Titles["Title1"].Alignment = ContentAlignment.MiddleCenter;//預設置中
        //浮水印
        Chart1.Titles["Title1"].Text = "c cnYES鉅亨網";
        Chart1.Titles["Title1"].DockToChartArea = "Default";
        Chart1.Titles["Title1"].Font = new Font("Microsoft Sans Serif", float.Parse("6.5"));
        Chart1.Titles["Title1"].Color = Color.Gray;
        Chart1.Titles["Title1"].BackColor = Color.Transparent;
        Chart1.Titles["Title1"].Position.Height = 10;
        Chart1.Titles["Title1"].Position.Width = 50;
        Chart1.Titles["Title1"].Position.X = 68;
        Chart1.Titles["Title1"].Position.Y = 4;
    }
    #endregion
}