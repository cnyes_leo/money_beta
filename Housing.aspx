﻿<%@ Page Language="C#" MasterPageFile="~/MPs/MP1.master" EnableViewState="false"
    AutoEventWireup="true" CodeFile="Housing.aspx.cs" Inherits="Housing" %>

<%@ OutputCache Duration="60" VaryByParam="*" %>
<%@ Register Src="UCs/SpreadsheetMenu.ascx" TagName="SpreadsheetMenu" TagPrefix="uc6" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <form method="post" runat="server" id="Form1">
    <script src="js/jquery.blockUI.js" type="text/javascript"></script>
    <div id="container">
        <uc6:SpreadsheetMenu ID="SpreadsheetMenu1" runat="server" />
        <!-- side2:end -->
        <asp:Panel ID="pnContent" runat="server">
        </asp:Panel>
        <!-- mymain:end -->
        <asp:Literal ID="Literal1" runat="server"></asp:Literal>
        <!-- side:end -->
    </div>
    <script type="text/javascript">
        //MenuCurrent
        document.getElementById(<%= strID %>).className = 'current';
        document.getElementById('housingmenu').className = 'current';
    </script>
    </form>
</asp:Content>
