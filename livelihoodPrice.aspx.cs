﻿//----------------------------------------
//民生物資
//14月走勢資訊&36月走勢圖
//by Brian
//----------------------------------------
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Text;
using System.Net;


public partial class livelihoodPrice : System.Web.UI.Page
{
    #region override void OnPreInit (EventArgs e)
    protected override void OnPreInit(EventArgs e)
    {
        base.OnPreInit(e);

        MPs_MP1 master = (MPs_MP1)this.Master as MPs_MP1;

        master.NavigationCategory = Enums.euNavigationCategory.民生物價;
    }
    #endregion override void OnPreInit (EventArgs e)


    #region 
    StringBuilder sbHTML = new StringBuilder();
    private string typeID = "";
    private string columVal01 = "";
    private string columVal02 = "";
    private string columVal03 = "";
    private string columVal04 = "";
    private string columVal05 = "";
    private string columVal06 = "";
    private string columVal07 = "";
    private string columVal08 = "";
    private string columVal09 = "";
    private string columVal10 = "";
    private string columVal11 = "";
    private string columVal12 = "";
    private string columVal13 = "";
    private string columVal14 = "";
    //---
    private string columDate01 = "";
    private string columDate02 = "";
    private string columDate03 = "";
    private string columDate04 = "";
    private string columDate05 = "";
    private string columDate06 = "";
    private string columDate07 = "";
    private string columDate08 = "";
    private string columDate09 = "";
    private string columDate10 = "";
    private string columDate11 = "";
    private string columDate12 = "";
    private string columDate13 = "";
    private string columDate14 = "";
    #endregion
        

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //table時間
            bindTableTime();


            //食物類_食用油
            food_EdibleOil();
            //食物類_肉類
            food_Meat();
            //食物類_蛋類
            food_Eggs();
            //食物類_水果
            food_Fruit();
            //食物類_蔬菜
            food_Vegetables();
            //食物類_水產
            food_Fisheries();


            //交通類_油料費
            traffic_FuelFee();
            //交通類_交通服務及維修零件
            traffic_ServiceParts();


            //居住類_水電瓦斯
            live_HydropowerGas();
            //居住類_房租
            live_Rent();
            //居住類_家庭用品
            live_HouseholdGoods();


            //衣服類_成衣
            clothing_Clothing();
            //衣服類_鞋襪
            clothing_ShoesSocks();
            //衣服類_衣著服務及配件
            clothing_ServicesAccessories();


            //其他類_醫療費用
            other_MedicalExpenses();
            //其他類_教養費用
            other_RearingCosts();
            //其他類_香菸及檳榔
            other_CigarettesBetelNuts();
            //其他類_美容及衛生用品
            other_BeautyHygieneProducts();
        }


        #region 載入右側選單
        try
        {
            //WebClient wc = new WebClient();
            //Uri uri = Request.Url;
            //string strUrl = uri.AbsoluteUri.Replace(uri.Segments[uri.Segments.Length - 1], "");
            //byte[] b = wc.DownloadData(strUrl + "batRightList.htm");
            //string strHCode = Encoding.UTF8.GetString(b);

            //Literal24.Text = strHCode;
            //wc.Dispose();

            //FileStream myFile = File.Open(Server.MapPath("batRightList.htm"), FileMode.Open, FileAccess.Read);
            //StreamReader myReader = new StreamReader(myFile);
            //int dl = System.Convert.ToInt32(myFile.Length);
            //Literal1.Text = myReader.ReadToEnd();
            //myReader.Close();
            //myFile.Close();

            Literal24.Text = System.IO.File.ReadAllText(Server.MapPath("batRightList.htm"));
        }
        catch (InvalidCastException ex)
        {
            Literal24.Text = "";
        }
        #endregion


        #region seo
        MPs_MP1 master = (MPs_MP1)this.Master as MPs_MP1;

        Seo seo = new Seo("livelihoodPrice_01");
        master.Title = seo.sTitle;
        master.Description = seo.sDesc;
        master.Keywords = seo.keyword;
        master.H1 = seo.sH1;
        seo = null;
        #endregion
    }




    //table時間
    private void bindTableTime()
    {
        string dateNow = DateTime.Now.ToString("yyyy-MM-dd");

        Literal19.Text = dateNow;
        Literal20.Text = dateNow;
        Literal21.Text = dateNow;
        Literal22.Text = dateNow;
        Literal23.Text = dateNow;
    }
    
    //食物類_食用油1
    private void food_EdibleOil()
    {
        string typeID = "010113"; //項目代號

        try
        {
            #region

            DataBaseClass db = new DataBaseClass();
            DataTable Dt = new DataTable();
            string sql = string.Empty;
            string temp_Month = string.Empty;
            string temp_Val = string.Empty;

            sql += " SELECT TOP 14 Date, CursorValue ";
            sql += " FROM  Gov_StatData ";
            sql += " WHERE MyIndex = @MyIndex ";
            sql += " ORDER BY Date Desc ";

            SqlParameter[] prams = { new SqlParameter("@MyIndex", SqlDbType.NVarChar) };
            prams[0].Value = typeID;

            Dt = new DataBaseClass().Select_DataReader_Parameters(ConfigurationManager.ConnectionStrings["_24_Money_ConnStr"].ConnectionString, sql, prams);

            if (Dt.Rows.Count == 0 || Dt.Rows == null) return;
            #endregion


            bindLitera(Dt, Literal1);

            db = null;
            Dt.Dispose();
        }
        catch (Exception ee)
        { Response.Write(ee.ToString()); }
    }
    //食物類_肉類2
    private void food_Meat()
    {
        string typeID = "010102"; //項目代號
       
        try
        {
            #region

            DataBaseClass db = new DataBaseClass();
            DataTable Dt = new DataTable();
            string sql = string.Empty;
            string temp_Month = string.Empty;
            string temp_Val = string.Empty;

            sql += " SELECT TOP 14 Date, CursorValue ";
            sql += " FROM  Gov_StatData ";
            sql += " WHERE MyIndex = @MyIndex ";
            sql += " ORDER BY Date Desc ";

            SqlParameter[] prams = { new SqlParameter("@MyIndex", SqlDbType.NVarChar) };
            prams[0].Value = typeID;

            Dt = new DataBaseClass().Select_DataReader_Parameters(ConfigurationManager.ConnectionStrings["_24_Money_ConnStr"].ConnectionString, sql, prams);

            if (Dt.Rows.Count == 0 || Dt.Rows == null) return;
            #endregion


            bindLitera(Dt, Literal2);

            db = null;
            Dt.Dispose();
        }
        catch (Exception ee)
        { Response.Write(ee.ToString()); }
    }
    //食物類_蛋類3
    private void food_Eggs()
    {
        string typeID = "010104"; //項目代號

        try
        {
            #region

            DataBaseClass db = new DataBaseClass();
            DataTable Dt = new DataTable();
            string sql = string.Empty;
            string temp_Month = string.Empty;
            string temp_Val = string.Empty;

            sql += " SELECT TOP 14 Date, CursorValue ";
            sql += " FROM  Gov_StatData ";
            sql += " WHERE MyIndex = @MyIndex ";
            sql += " ORDER BY Date Desc ";

            SqlParameter[] prams = { new SqlParameter("@MyIndex", SqlDbType.NVarChar) };
            prams[0].Value = typeID;

            Dt = new DataBaseClass().Select_DataReader_Parameters(ConfigurationManager.ConnectionStrings["_24_Money_ConnStr"].ConnectionString, sql, prams);

            if (Dt.Rows.Count == 0 || Dt.Rows == null) return;
            #endregion


            bindLitera(Dt, Literal3);

            db = null;
            Dt.Dispose();
        }
        catch (Exception ee)
        { Response.Write(ee.ToString()); }
    }
    //食物類_水果4
    private void food_Fruit()
    {
        string typeID = "010109"; //項目代號
        
        try
        {
            #region

            DataBaseClass db = new DataBaseClass();
            DataTable Dt = new DataTable();
            string sql = string.Empty;
            string temp_Month = string.Empty;
            string temp_Val = string.Empty;

            sql += " SELECT TOP 14 Date, CursorValue ";
            sql += " FROM  Gov_StatData ";
            sql += " WHERE MyIndex = @MyIndex ";
            sql += " ORDER BY Date Desc ";

            SqlParameter[] prams = { new SqlParameter("@MyIndex", SqlDbType.NVarChar) };
            prams[0].Value = typeID;

            Dt = new DataBaseClass().Select_DataReader_Parameters(ConfigurationManager.ConnectionStrings["_24_Money_ConnStr"].ConnectionString, sql, prams);

            if (Dt.Rows.Count == 0 || Dt.Rows == null) return;
            #endregion

            bindLitera(Dt, Literal4);

            db = null;
            Dt.Dispose();
        }
        catch (Exception ee)
        { Response.Write(ee.ToString()); }
    }
    //食物類_蔬菜5
    private void food_Vegetables()
    {
        string typeID = "010107"; //項目代號

        try
        {
            #region

            DataBaseClass db = new DataBaseClass();
            DataTable Dt = new DataTable();
            string sql = string.Empty;
            string temp_Month = string.Empty;
            string temp_Val = string.Empty;

            sql += " SELECT TOP 14 Date, CursorValue ";
            sql += " FROM  Gov_StatData ";
            sql += " WHERE MyIndex = @MyIndex ";
            sql += " ORDER BY Date Desc ";

            SqlParameter[] prams = { new SqlParameter("@MyIndex", SqlDbType.NVarChar) };
            prams[0].Value = typeID;

            Dt = new DataBaseClass().Select_DataReader_Parameters(ConfigurationManager.ConnectionStrings["_24_Money_ConnStr"].ConnectionString, sql, prams);

            if (Dt.Rows.Count == 0 || Dt.Rows == null) return;
            #endregion


            bindLitera(Dt, Literal5);
            
            db = null;
            Dt.Dispose();
        }
        catch (Exception ee)
        { Response.Write(ee.ToString()); }
    }
    //食物類_水產6
    private void food_Fisheries()
    {
        string typeID = "010105"; //項目代號

        try
        {
            #region

            DataBaseClass db = new DataBaseClass();
            DataTable Dt = new DataTable();
            string sql = string.Empty;
            string temp_Month = string.Empty;
            string temp_Val = string.Empty;

            sql += " SELECT TOP 14 Date, CursorValue ";
            sql += " FROM  Gov_StatData ";
            sql += " WHERE MyIndex = @MyIndex ";
            sql += " ORDER BY Date Desc ";

            SqlParameter[] prams = { new SqlParameter("@MyIndex", SqlDbType.NVarChar) };
            prams[0].Value = typeID;

            Dt = new DataBaseClass().Select_DataReader_Parameters(ConfigurationManager.ConnectionStrings["_24_Money_ConnStr"].ConnectionString, sql, prams);

            if (Dt.Rows.Count == 0 || Dt.Rows == null) return;
            #endregion


            bindLitera(Dt, Literal6);

            db = null;
            Dt.Dispose();
        }
        catch (Exception ee)
        { Response.Write(ee.ToString()); }
    }
    

    //交通類_油料費7
    private void traffic_FuelFee()
    {
        string typeID = "010402"; //項目代號

        try
        {
            #region

            DataBaseClass db = new DataBaseClass();
            DataTable Dt = new DataTable();
            string sql = string.Empty;
            string temp_Month = string.Empty;
            string temp_Val = string.Empty;

            sql += " SELECT TOP 14 Date, CursorValue ";
            sql += " FROM  Gov_StatData ";
            sql += " WHERE MyIndex = @MyIndex ";
            sql += " ORDER BY Date Desc ";

            SqlParameter[] prams = { new SqlParameter("@MyIndex", SqlDbType.NVarChar) };
            prams[0].Value = typeID;

            Dt = new DataBaseClass().Select_DataReader_Parameters(ConfigurationManager.ConnectionStrings["_24_Money_ConnStr"].ConnectionString, sql, prams);

            if (Dt.Rows.Count == 0 || Dt.Rows == null) return;
            #endregion


            bindLitera(Dt, Literal7);

            db = null;
            Dt.Dispose();
        }
        catch (Exception ee)
        { Response.Write(ee.ToString()); }
    }
    //交通類_交通服務及維修零件8
    private void traffic_ServiceParts()
    {
        string typeID = "010403"; //項目代號

        try
        {
            #region

            DataBaseClass db = new DataBaseClass();
            DataTable Dt = new DataTable();
            string sql = string.Empty;
            string temp_Month = string.Empty;
            string temp_Val = string.Empty;

            sql += " SELECT TOP 14 Date, CursorValue ";
            sql += " FROM  Gov_StatData ";
            sql += " WHERE MyIndex = @MyIndex ";
            sql += " ORDER BY Date Desc ";

            SqlParameter[] prams = { new SqlParameter("@MyIndex", SqlDbType.NVarChar) };
            prams[0].Value = typeID;

            Dt = new DataBaseClass().Select_DataReader_Parameters(ConfigurationManager.ConnectionStrings["_24_Money_ConnStr"].ConnectionString, sql, prams);

            if (Dt.Rows.Count == 0 || Dt.Rows == null) return;
            #endregion


            bindLitera(Dt, Literal8);

            db = null;
            Dt.Dispose();
        }
        catch (Exception ee)
        { Response.Write(ee.ToString()); }
    }
    

    //居住類_水電瓦斯9
    private void live_HydropowerGas()
    {
        string typeID = "010305"; //項目代號

        try
        {
            #region

            DataBaseClass db = new DataBaseClass();
            DataTable Dt = new DataTable();
            string sql = string.Empty;
            string temp_Month = string.Empty;
            string temp_Val = string.Empty;

            sql += " SELECT TOP 14 Date, CursorValue ";
            sql += " FROM  Gov_StatData ";
            sql += " WHERE MyIndex = @MyIndex ";
            sql += " ORDER BY Date Desc ";

            SqlParameter[] prams = { new SqlParameter("@MyIndex", SqlDbType.NVarChar) };
            prams[0].Value = typeID;

            Dt = new DataBaseClass().Select_DataReader_Parameters(ConfigurationManager.ConnectionStrings["_24_Money_ConnStr"].ConnectionString, sql, prams);

            if (Dt.Rows.Count == 0 || Dt.Rows == null) return;
            #endregion


            bindLitera(Dt, Literal9);

            db = null;
            Dt.Dispose();
        }
        catch (Exception ee)
        { Response.Write(ee.ToString()); }
    }
    //居住類_房租10
    private void live_Rent()
    {
        string typeID = "010301"; //項目代號

        try
        {
            #region

            DataBaseClass db = new DataBaseClass();
            DataTable Dt = new DataTable();
            string sql = string.Empty;
            string temp_Month = string.Empty;
            string temp_Val = string.Empty;

            sql += " SELECT TOP 14 Date, CursorValue ";
            sql += " FROM  Gov_StatData ";
            sql += " WHERE MyIndex = @MyIndex ";
            sql += " ORDER BY Date Desc ";

            SqlParameter[] prams = { new SqlParameter("@MyIndex", SqlDbType.NVarChar) };
            prams[0].Value = typeID;

            Dt = new DataBaseClass().Select_DataReader_Parameters(ConfigurationManager.ConnectionStrings["_24_Money_ConnStr"].ConnectionString, sql, prams);

            if (Dt.Rows.Count == 0 || Dt.Rows == null) return;
            #endregion


            bindLitera(Dt, Literal10);

            db = null;
            Dt.Dispose();
        }
        catch (Exception ee)
        { Response.Write(ee.ToString()); }
    }
    //居住類_家庭用品11
    private void live_HouseholdGoods()
    {
        string typeID = "010303"; //項目代號

        try
        {
            #region

            DataBaseClass db = new DataBaseClass();
            DataTable Dt = new DataTable();
            string sql = string.Empty;
            string temp_Month = string.Empty;
            string temp_Val = string.Empty;

            sql += " SELECT TOP 14 Date, CursorValue ";
            sql += " FROM  Gov_StatData ";
            sql += " WHERE MyIndex = @MyIndex ";
            sql += " ORDER BY Date Desc ";

            SqlParameter[] prams = { new SqlParameter("@MyIndex", SqlDbType.NVarChar) };
            prams[0].Value = typeID;

            Dt = new DataBaseClass().Select_DataReader_Parameters(ConfigurationManager.ConnectionStrings["_24_Money_ConnStr"].ConnectionString, sql, prams);

            if (Dt.Rows.Count == 0 || Dt.Rows == null) return;
            #endregion


            bindLitera(Dt, Literal11);

            db = null;
            Dt.Dispose();
        }
        catch (Exception ee)
        { Response.Write(ee.ToString()); }
    }
    

    //衣服類_成衣12
    private void clothing_Clothing()
    {
        string typeID = "010201"; //項目代號

        try
        {
            #region

            DataBaseClass db = new DataBaseClass();
            DataTable Dt = new DataTable();
            string sql = string.Empty;
            string temp_Month = string.Empty;
            string temp_Val = string.Empty;

            sql += " SELECT TOP 14 Date, CursorValue ";
            sql += " FROM  Gov_StatData ";
            sql += " WHERE MyIndex = @MyIndex ";
            sql += " ORDER BY Date Desc ";

            SqlParameter[] prams = { new SqlParameter("@MyIndex", SqlDbType.NVarChar) };
            prams[0].Value = typeID;

            Dt = new DataBaseClass().Select_DataReader_Parameters(ConfigurationManager.ConnectionStrings["_24_Money_ConnStr"].ConnectionString, sql, prams);

            if (Dt.Rows.Count == 0 || Dt.Rows == null) return;
            #endregion


            bindLitera(Dt, Literal12);

            db = null;
            Dt.Dispose();
        }
        catch (Exception ee)
        { Response.Write(ee.ToString()); }
    }
    //衣服類_鞋襪13
    private void clothing_ShoesSocks()
    {
        string typeID = "010202"; //項目代號

        try
        {
            #region

            DataBaseClass db = new DataBaseClass();
            DataTable Dt = new DataTable();
            string sql = string.Empty;
            string temp_Month = string.Empty;
            string temp_Val = string.Empty;

            sql += " SELECT TOP 14 Date, CursorValue ";
            sql += " FROM  Gov_StatData ";
            sql += " WHERE MyIndex = @MyIndex ";
            sql += " ORDER BY Date Desc ";

            SqlParameter[] prams = { new SqlParameter("@MyIndex", SqlDbType.NVarChar) };
            prams[0].Value = typeID;

            Dt = new DataBaseClass().Select_DataReader_Parameters(ConfigurationManager.ConnectionStrings["_24_Money_ConnStr"].ConnectionString, sql, prams);

            if (Dt.Rows.Count == 0 || Dt.Rows == null) return;
            #endregion


            bindLitera(Dt, Literal13);

            db = null;
            Dt.Dispose();
        }
        catch (Exception ee)
        { Response.Write(ee.ToString()); }
    }
    //衣服類_衣著服務及配件14
    private void clothing_ServicesAccessories()
    {
        string typeID = "010203"; //項目代號

        try
        {
            #region

            DataBaseClass db = new DataBaseClass();
            DataTable Dt = new DataTable();
            string sql = string.Empty;
            string temp_Month = string.Empty;
            string temp_Val = string.Empty;

            sql += " SELECT TOP 14 Date, CursorValue ";
            sql += " FROM  Gov_StatData ";
            sql += " WHERE MyIndex = @MyIndex ";
            sql += " ORDER BY Date Desc ";

            SqlParameter[] prams = { new SqlParameter("@MyIndex", SqlDbType.NVarChar) };
            prams[0].Value = typeID;

            Dt = new DataBaseClass().Select_DataReader_Parameters(ConfigurationManager.ConnectionStrings["_24_Money_ConnStr"].ConnectionString, sql, prams);

            if (Dt.Rows.Count == 0 || Dt.Rows == null) return;
            #endregion


            bindLitera(Dt, Literal14);

            db = null;
            Dt.Dispose();
        }
        catch (Exception ee)
        { Response.Write(ee.ToString()); }
    }
    

    //其他類_醫療費用15
    private void other_MedicalExpenses()
    {
        string typeID = "010501"; //項目代號

        try
        {
            #region

            DataBaseClass db = new DataBaseClass();
            DataTable Dt = new DataTable();
            string sql = string.Empty;
            string temp_Month = string.Empty;
            string temp_Val = string.Empty;

            sql += " SELECT TOP 14 Date, CursorValue ";
            sql += " FROM  Gov_StatData ";
            sql += " WHERE MyIndex = @MyIndex ";
            sql += " ORDER BY Date Desc ";

            SqlParameter[] prams = { new SqlParameter("@MyIndex", SqlDbType.NVarChar) };
            prams[0].Value = typeID;

            Dt = new DataBaseClass().Select_DataReader_Parameters(ConfigurationManager.ConnectionStrings["_24_Money_ConnStr"].ConnectionString, sql, prams);

            if (Dt.Rows.Count == 0 || Dt.Rows == null) return;
            #endregion


            bindLitera(Dt, Literal15);

            db = null;
            Dt.Dispose();
        }
        catch (Exception ee)
        { Response.Write(ee.ToString()); }
    }
    //其他類_教養費用16
    private void other_RearingCosts()
    {
        string typeID = "010601"; //項目代號

        try
        {
            #region

            DataBaseClass db = new DataBaseClass();
            DataTable Dt = new DataTable();
            string sql = string.Empty;
            string temp_Month = string.Empty;
            string temp_Val = string.Empty;

            sql += " SELECT TOP 14 Date, CursorValue ";
            sql += " FROM  Gov_StatData ";
            sql += " WHERE MyIndex = @MyIndex ";
            sql += " ORDER BY Date Desc ";

            SqlParameter[] prams = { new SqlParameter("@MyIndex", SqlDbType.NVarChar) };
            prams[0].Value = typeID;

            Dt = new DataBaseClass().Select_DataReader_Parameters(ConfigurationManager.ConnectionStrings["_24_Money_ConnStr"].ConnectionString, sql, prams);

            if (Dt.Rows.Count == 0 || Dt.Rows == null) return;
            #endregion


            bindLitera(Dt, Literal16);

            db = null;
            Dt.Dispose();
        }
        catch (Exception ee)
        { Response.Write(ee.ToString()); }
    }
    //其他類_香菸及檳榔17
    private void other_CigarettesBetelNuts()
    {
        string typeID = "010701"; //項目代號

        try
        {
            #region

            DataBaseClass db = new DataBaseClass();
            DataTable Dt = new DataTable();
            string sql = string.Empty;
            string temp_Month = string.Empty;
            string temp_Val = string.Empty;

            sql += " SELECT TOP 14 Date, CursorValue ";
            sql += " FROM  Gov_StatData ";
            sql += " WHERE MyIndex = @MyIndex ";
            sql += " ORDER BY Date Desc ";

            SqlParameter[] prams = { new SqlParameter("@MyIndex", SqlDbType.NVarChar) };
            prams[0].Value = typeID;

            Dt = new DataBaseClass().Select_DataReader_Parameters(ConfigurationManager.ConnectionStrings["_24_Money_ConnStr"].ConnectionString, sql, prams);

            if (Dt.Rows.Count == 0 || Dt.Rows == null) return;
            #endregion


            bindLitera(Dt, Literal17);

            db = null;
            Dt.Dispose();
        }
        catch (Exception ee)
        { Response.Write(ee.ToString()); }
    }
    //其他類_美容及衛生用品18
    private void other_BeautyHygieneProducts()
    {
        string typeID = "010702"; //項目代號

        try
        {
            #region

            DataBaseClass db = new DataBaseClass();
            DataTable Dt = new DataTable();
            string sql = string.Empty;
            string temp_Month = string.Empty;
            string temp_Val = string.Empty;

            sql += " SELECT TOP 14 Date, CursorValue ";
            sql += " FROM  Gov_StatData ";
            sql += " WHERE MyIndex = @MyIndex ";
            sql += " ORDER BY Date Desc ";

            SqlParameter[] prams = { new SqlParameter("@MyIndex", SqlDbType.NVarChar) };
            prams[0].Value = typeID;

            Dt = new DataBaseClass().Select_DataReader_Parameters(ConfigurationManager.ConnectionStrings["_24_Money_ConnStr"].ConnectionString, sql, prams);

            if (Dt.Rows.Count == 0 || Dt.Rows == null) return;
            #endregion


            bindLitera(Dt, Literal18);

            db = null;
            Dt.Dispose();
        }
        catch (Exception ee)
        { Response.Write(ee.ToString()); }
    }



    //---------------------------------

    private void bindLitera(DataTable Dt, Literal lt)
    {
        #region
        columVal01 = (Dt.Rows[0]["CursorValue"] != null) ? Dt.Rows[0]["CursorValue"].ToString() : "";
        columVal02 = (Dt.Rows[1]["CursorValue"] != null) ? Dt.Rows[1]["CursorValue"].ToString() : "";
        columVal03 = (Dt.Rows[2]["CursorValue"] != null) ? Dt.Rows[2]["CursorValue"].ToString() : "";
        columVal04 = (Dt.Rows[3]["CursorValue"] != null) ? Dt.Rows[3]["CursorValue"].ToString() : "";
        columVal05 = (Dt.Rows[4]["CursorValue"] != null) ? Dt.Rows[4]["CursorValue"].ToString() : "";
        columVal06 = (Dt.Rows[5]["CursorValue"] != null) ? Dt.Rows[5]["CursorValue"].ToString() : "";
        columVal07 = (Dt.Rows[6]["CursorValue"] != null) ? Dt.Rows[6]["CursorValue"].ToString() : "";
        columVal08 = (Dt.Rows[7]["CursorValue"] != null) ? Dt.Rows[7]["CursorValue"].ToString() : "";
        columVal09 = (Dt.Rows[8]["CursorValue"] != null) ? Dt.Rows[8]["CursorValue"].ToString() : "";
        columVal10 = (Dt.Rows[9]["CursorValue"] != null) ? Dt.Rows[9]["CursorValue"].ToString() : "";
        columVal11 = (Dt.Rows[10]["CursorValue"] != null) ? Dt.Rows[10]["CursorValue"].ToString() : "";
        columVal12 = (Dt.Rows[11]["CursorValue"] != null) ? Dt.Rows[11]["CursorValue"].ToString() : "";
        columVal13 = (Dt.Rows[12]["CursorValue"] != null) ? Dt.Rows[12]["CursorValue"].ToString() : "";
        columVal14 = (Dt.Rows[13]["CursorValue"] != null) ? Dt.Rows[13]["CursorValue"].ToString() : "";
        //---
        columDate01 = (Dt.Rows[0]["Date"] != null) ? Dt.Rows[0]["Date"].ToString() : "";
        columDate02 = (Dt.Rows[1]["Date"] != null) ? Dt.Rows[1]["Date"].ToString() : "";
        columDate03 = (Dt.Rows[2]["Date"] != null) ? Dt.Rows[2]["Date"].ToString() : "";
        columDate04 = (Dt.Rows[3]["Date"] != null) ? Dt.Rows[3]["Date"].ToString() : "";
        columDate05 = (Dt.Rows[4]["Date"] != null) ? Dt.Rows[4]["Date"].ToString() : "";
        columDate06 = (Dt.Rows[5]["Date"] != null) ? Dt.Rows[5]["Date"].ToString() : "";
        columDate07 = (Dt.Rows[6]["Date"] != null) ? Dt.Rows[6]["Date"].ToString() : "";
        columDate08 = (Dt.Rows[7]["Date"] != null) ? Dt.Rows[7]["Date"].ToString() : "";
        columDate09 = (Dt.Rows[8]["Date"] != null) ? Dt.Rows[8]["Date"].ToString() : "";
        columDate10 = (Dt.Rows[9]["Date"] != null) ? Dt.Rows[9]["Date"].ToString() : "";
        columDate11 = (Dt.Rows[10]["Date"] != null) ? Dt.Rows[10]["Date"].ToString() : "";
        columDate12 = (Dt.Rows[11]["Date"] != null) ? Dt.Rows[11]["Date"].ToString() : "";
        columDate13 = (Dt.Rows[12]["Date"] != null) ? Dt.Rows[12]["Date"].ToString() : "";
        columDate14 = (Dt.Rows[13]["Date"] != null) ? Dt.Rows[13]["Date"].ToString() : "";

        #endregion

        #region
        sbHTML.Length = 0;
        sbHTML.Append(@"<tr>");
        sbHTML.Append(@"<td>" + columDate01.Substring(0, 4) + "/" + columDate01.Substring(4, 2) + @"</td>");
        sbHTML.Append(@"<td>" + columVal01 + @"</td>");
        sbHTML.Append(@"<td>" + columDate08.Substring(0, 4) + "/" + columDate08.Substring(4, 2) + @"</td>");
        sbHTML.Append(@"<td>" + columVal08 + @"</td>");
        sbHTML.Append(@"</tr>");
        sbHTML.Append(@"<tr>");
        sbHTML.Append(@"<td>" + columDate02.Substring(0, 4) + "/" + columDate02.Substring(4, 2) + @"</td>");
        sbHTML.Append(@"<td>" + columVal02 + @"</td>");
        sbHTML.Append(@"<td>" + columDate09.Substring(0, 4) + "/" + columDate09.Substring(4, 2) + @"</td>");
        sbHTML.Append(@"<td>" + columVal09 + @"</td>");
        sbHTML.Append(@"</tr>");
        sbHTML.Append(@"<tr>");
        sbHTML.Append(@"<td>" + columDate03.Substring(0, 4) + "/" + columDate03.Substring(4, 2) + @"</td>");
        sbHTML.Append(@"<td>" + columVal03 + @"</td>");
        sbHTML.Append(@"<td>" + columDate10.Substring(0, 4) + "/" + columDate10.Substring(4, 2) + @"</td>");
        sbHTML.Append(@"<td>" + columVal10 + @"</td>");
        sbHTML.Append(@"</tr>");
        sbHTML.Append(@"<tr>");
        sbHTML.Append(@"<td>" + columDate04.Substring(0, 4) + "/" + columDate04.Substring(4, 2) + @"</td>");
        sbHTML.Append(@"<td>" + columVal04 + @"</td>");
        sbHTML.Append(@"<td>" + columDate11.Substring(0, 4) + "/" + columDate11.Substring(4, 2) + @"</td>");
        sbHTML.Append(@"<td>" + columVal11 + @"</td>");
        sbHTML.Append(@"</tr>");
        sbHTML.Append(@"<tr>");
        sbHTML.Append(@"<td>" + columDate05.Substring(0, 4) + "/" + columDate05.Substring(4, 2) + @"</td>");
        sbHTML.Append(@"<td>" + columVal05 + @"</td>");
        sbHTML.Append(@"<td>" + columDate12.Substring(0, 4) + "/" + columDate12.Substring(4, 2) + @"</td>");
        sbHTML.Append(@"<td>" + columVal12 + @"</td>");
        sbHTML.Append(@"</tr>");
        sbHTML.Append(@"<tr>");
        sbHTML.Append(@"<td>" + columDate06.Substring(0, 4) + "/" + columDate06.Substring(4, 2) + @"</td>");
        sbHTML.Append(@"<td>" + columVal06 + @"</td>");
        sbHTML.Append(@"<td>" + columDate13.Substring(0, 4) + "/" + columDate13.Substring(4, 2) + @"</td>");
        sbHTML.Append(@"<td>" + columVal13 + @"</td>");
        sbHTML.Append(@"</tr>");
        sbHTML.Append(@"<tr>");
        sbHTML.Append(@"<td>" + columDate07.Substring(0, 4) + "/" + columDate07.Substring(4, 2) + @"</td>");
        sbHTML.Append(@"<td>" + columVal07 + @"</td>");
        sbHTML.Append(@"<td>" + columDate14.Substring(0, 4) + "/" + columDate14.Substring(4, 2) + @"</td>");
        sbHTML.Append(@"<td>" + columVal14 + @"</td>");
        sbHTML.Append(@"</tr>");
        #endregion

        lt.Text = sbHTML.ToString();
    }
}