<%@ Page Language="C#" MasterPageFile="~/MPs/MP1.master" EnableEventValidation="false"
    EnableViewStateMac="true" AutoEventWireup="true" CodeFile="CreditCalculation.aspx.cs"
    Inherits="CreditCalculation" %>

<%@ OutputCache Duration="60" VaryByParam="*" %>
<%@ Register Src="UCs/SpreadsheetMenu.ascx" TagName="SpreadsheetMenu" TagPrefix="uc4" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <form method="post" runat="server" id="Form1">
    <script src="js/jquery.blockUI.js" type="text/javascript"></script>
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div id="container">
        <uc4:SpreadsheetMenu ID="SpreadsheetMenu1" runat="server" />
        <!-- side2:end -->
        <asp:Panel ID="pnContent" runat="server">
        </asp:Panel>
        <!-- mymain:end -->
        <asp:Literal ID="Literal1" runat="server"></asp:Literal>
        <!-- side:end -->
 <div class="adunit img-responsive display-block" data-adunit="cnyes_bank_485*90" data-dimensions="485x90"></div>
       
    </div>
    <script type="text/javascript">
        //MenuCurrent
        document.getElementById(<%= strID %>).className = 'current';
        document.getElementById('ccmenu').className = 'current';
    </script>
    </form>
</asp:Content>
