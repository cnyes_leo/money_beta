﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;

public partial class BankService : System.Web.UI.Page
{
    #region 變數
    private Class_BankServiceNavigation _BankServiceNavigation = null;

    private Enums.euBankServiceNavigations _BankServiceNav = Enums.euBankServiceNavigations.牌告利率;

    private byte _SubIndex = 1;
    #endregion 變數

    #region Page_Load
    protected void Page_Load (object sender, EventArgs e)
    {
        //WriteLog.Log<string>("BankService Page_Load 1");
        MPs_MP1 master = (MPs_MP1)this.Master as MPs_MP1;

        master.NavigationCategory = Enums.euNavigationCategory.銀行服務;

        //System.Text.StringBuilder sb = new System.Text.StringBuilder();
        //foreach (String aa in Request.QueryString.AllKeys)
        //{
        //    sb.AppendLine(String.Format("Request.QueryString[{0}] = {1}", aa, Request.QueryString[aa]));
        //}
        //sb.AppendLine("Request.Url.AbsoluteUri = " + Request.Url.AbsoluteUri);

        #region QueryString Key 說明
        // c：Enums.euBankServiceNavigations
        // a、d：排序方向
        // g：Enums.eu外幣存款利率SubNavigations
        // cc：Currency（幣別代碼 3碼）
        // i：BCode（Detail 頁面用）
        // bic：BCode（銀行換匯 頁面用）
        // ip：ItemPeriod（外幣存款利率 頁面用）      1~6
        // rt：Enums.euRateType（存款利率 頁面用）    f、m
        // bmet：BankMoneyExchange（銀行換匯 頁面用） Bank、Currency
        // k：搜尋關鍵字
        // p：PageIndex
        #endregion QueryString Key 說明

        string c = Request.QueryString.Get("c");
        _BankServiceNav = Enums.euBankServiceNavigations.牌告利率;
        Enums.eu牌告利率Navigations _牌告利率Nav = Enums.eu牌告利率Navigations.None;
        Enums.eu外幣存款利率Navigations _外幣存款利率Nav = Enums.eu外幣存款利率Navigations.None;
        Enums.eu外幣存款利率SubNavigations _外幣存款利率SubNav = Enums.eu外幣存款利率SubNavigations.All;
        Enums.eu存款利率Navigations _存款利率Nav = Enums.eu存款利率Navigations.None;
        Enums.eu銀行換匯比較表Navigations _銀行換匯比較表Nav = Enums.eu銀行換匯比較表Navigations.None;
        //WriteLog.Log<string>("c", c);

        byte pageIndex = 1;
        byte.TryParse(Request.QueryString.Get("p"), out pageIndex);
        if (pageIndex == 0 || pageIndex > 100)
            pageIndex = 1;

        Enums.euOrderDirection? orderDirection = null;
        byte? orderIndex = null;

        byte tmp = 0;
        if (Request.QueryString.Get("a") != null && byte.TryParse(Request.QueryString.Get("a"), out tmp))
        {
            orderDirection = Enums.euOrderDirection.ASC;
            orderIndex = tmp;
        }
        else if (Request.QueryString.Get("d") != null && byte.TryParse(Request.QueryString.Get("d"), out tmp))
        {
            orderDirection = Enums.euOrderDirection.DESC;
            orderIndex = tmp;
        }

        if (Request.QueryString.Get("g") != null && Enum.IsDefined(typeof(Enums.eu外幣存款利率SubNavigations), Request.QueryString.Get("g")))
            _外幣存款利率SubNav = (Enums.eu外幣存款利率SubNavigations)Enum.Parse(typeof(Enums.eu外幣存款利率SubNavigations), Request.QueryString.Get("g"));

        string currency = String.Empty;
        if (_外幣存款利率SubNav == Enums.eu外幣存款利率SubNavigations.Currency && Request.QueryString.Get("cc") != null)
        {
            currency = Request.QueryString.Get("cc");
            if (currency.Length != 3)
                currency = String.Empty;
        }

        byte? itemPeriod = null;

        #region //Mark
        //bool? type = null;
        //if (Request.QueryString.Get("t") != null)
        //{
        //    if (("f").Equals(Request.QueryString.Get("t").ToLower()))
        //        type = true;
        //    else if (("m").Equals(Request.QueryString.Get("t").ToLower()))
        //        type = false;
        //}
        #endregion //Mark

        string k = Request.QueryString.Get("k");

        // Detail 頁面
        string bcode = Request.QueryString.Get("i");
        if (bcode != null)
            _BankServiceNav = Enums.euBankServiceNavigations.Detail;

        // BankMoneyExchange 銀行換匯
        Enums.euBankMoneyExchangeType bankMoneyExchangeType = Enums.euBankMoneyExchangeType.Bank;
        if (Request.QueryString.Get("bmet") != null && Enum.IsDefined(typeof(Enums.euBankMoneyExchangeType), Request.QueryString.Get("bmet")))
        {
            bankMoneyExchangeType = (Enums.euBankMoneyExchangeType)Enum.Parse(typeof(Enums.euBankMoneyExchangeType), Request.QueryString.Get("bmet"));

            if (Request.QueryString.Get("bic") != null)
            {
                if (Request.QueryString.Get("bic").Length == 3)
                    currency = Request.QueryString.Get("bic");
                else
                    bcode = Request.QueryString.Get("bic");
            }
        }

        //byte subIndex = 1;
        if (c != null)
        {
            if (!c.Equals(Path.GetFileNameWithoutExtension(Enums.GetEnumDesc(Enums.euNavigationCategory.銀行服務)), StringComparison.InvariantCultureIgnoreCase))
            {
                foreach (Enums.euBankServiceNavigations nav in Enum.GetValues(typeof(Enums.euBankServiceNavigations)))
                {
                    if (c.IndexOf(Path.GetFileNameWithoutExtension(Enums.GetEnumDesc(nav)), StringComparison.InvariantCultureIgnoreCase) == 0 && c.Length - Enums.GetEnumDesc(nav).Length <= 1)
                    {
                        _BankServiceNav = nav;

                        tmp = 0;
                        byte.TryParse(c.Substring(Path.GetFileNameWithoutExtension(Enums.GetEnumDesc(nav)).Length), out tmp);

                        if (tmp == 0 && nav != Enums.euBankServiceNavigations.放款利率)
                            tmp = 1;
                        //if (("dongdong05|cnyes_Alexc").IndexOf(System.Web.HttpContext.Current.Server.UrlDecode(Globals.AicLogin(System.Web.HttpContext.Current.Request))) >= 0)
                        //{
                        //    WriteLog.Log1<string>("__d.txt", "_BankServiceNav", _BankServiceNav.ToString());
                        //    WriteLog.Log1<string>("__d.txt", "tmp", tmp.ToString());
                        //}
                        _SubIndex = tmp;

                        switch (nav)
                        {
                            case Enums.euBankServiceNavigations.牌告利率:
                                if (Enum.IsDefined(typeof(Enums.eu牌告利率Navigations), tmp))
                                    _牌告利率Nav = (Enums.eu牌告利率Navigations)tmp;
                                break;
                            case Enums.euBankServiceNavigations.外幣存款利率:
                                #region //Mark
                                //if (tmp > 10)
                                //{
                                //    _SubIndex = (byte)(tmp % 10);
                                //    tmp /= 10;
                                //}
                                ////else
                                ////    _SubIndex = 1;
                                #endregion //Mark

                                if (Enum.IsDefined(typeof(Enums.eu外幣存款利率Navigations), tmp))
                                    _外幣存款利率Nav = (Enums.eu外幣存款利率Navigations)tmp;
                                break;
                            case Enums.euBankServiceNavigations.存款利率:
                                #region //Mark
                                //if (tmp > 10)
                                //{
                                //    _SubIndex = (byte)(tmp % 10);
                                //    tmp /= 10;
                                //}
                                //else
                                //    _SubIndex = 1;
                                #endregion //Mark

                                if (Enum.IsDefined(typeof(Enums.eu存款利率Navigations), tmp))
                                    _存款利率Nav = (Enums.eu存款利率Navigations)tmp;
                                break;
                            case Enums.euBankServiceNavigations.銀行換匯比較表:
                                if (Enum.IsDefined(typeof(Enums.eu銀行換匯比較表Navigations), tmp))
                                    _銀行換匯比較表Nav = (Enums.eu銀行換匯比較表Navigations)tmp;
                                break;
                        }
                        break;
                    }
                }
            }
        }

        //sb.AppendLine(String.Format("_SubIndex = {0}", _SubIndex.ToString()));
        //sb.AppendLine(String.Format("_BankServiceNav = {0}", _BankServiceNav.ToString()));
        if (_BankServiceNav == Enums.euBankServiceNavigations.外幣存款利率 && Request.QueryString.Get("ip") != null)
        {
            if (Request.QueryString.Get("ip").Length == 1 && byte.TryParse(Request.QueryString.Get("ip"), out tmp))
                itemPeriod = tmp;
        }

        Class_BankServiceContent bankServiceContent = new Class_BankServiceContent();
        bankServiceContent.BankServiceNav = _BankServiceNav;
        bankServiceContent.外幣存款利率Nav = _外幣存款利率Nav;
        bankServiceContent.外幣存款利率SubNav = _外幣存款利率SubNav;
        bankServiceContent.Currency = currency;
        bankServiceContent.ItemPeriod = itemPeriod;
        bankServiceContent.存款利率Nav = _存款利率Nav;
        bankServiceContent.銀行換匯比較表Nav = _銀行換匯比較表Nav;
        bankServiceContent.SubIndex = _SubIndex;
        bankServiceContent.PageIndex = pageIndex;
        bankServiceContent.OrderDirection = orderDirection;
        bankServiceContent.OrderIndex = orderIndex;
        bankServiceContent.Keyword = k;
        bankServiceContent.BCode = bcode;
        bankServiceContent.BankMoneyExchangeType = bankMoneyExchangeType;

        if (Request.QueryString.Get("rt") != null && Request.QueryString.Get("rt").Length == 1)
        {
            Enums.euRateType rateType = Enums.euRateType.機動;
            if (("f").Equals(Request.QueryString.Get("rt").ToLower()))
                rateType = Enums.euRateType.固定;

            bankServiceContent.RateType = rateType;
        }

        Literal_BankServiceContent.Text = bankServiceContent.Create();

        master.FootJs = bankServiceContent.FootJs;

        #region //Mark
        //BankServiceNavigation1.BankServiceNav = _BankServiceNav;
        //BankServiceNavigation1.牌告利率Nav = _牌告利率Nav;
        //BankServiceNavigation1.外幣存款利率Nav = _外幣存款利率Nav;
        //BankServiceNavigation1.存款利率Nav = _存款利率Nav;
        //BankServiceNavigation1.銀行換匯比較表Nav = _銀行換匯比較表Nav;
        //BankServiceNavigation1.SubIndex = _SubIndex;
        #endregion //Mark
        _BankServiceNavigation = new Class_BankServiceNavigation();
        _BankServiceNavigation.BankServiceNav = _BankServiceNav;
        _BankServiceNavigation.牌告利率Nav = _牌告利率Nav;
        _BankServiceNavigation.外幣存款利率Nav = _外幣存款利率Nav;
        _BankServiceNavigation.外幣存款利率SubNav = _外幣存款利率SubNav;
        _BankServiceNavigation.存款利率Nav = _存款利率Nav;
        _BankServiceNavigation.銀行換匯比較表Nav = _銀行換匯比較表Nav;
        _BankServiceNavigation.SubIndex = _SubIndex;

        if (_BankServiceNav == Enums.euBankServiceNavigations.銀行換匯)
        {
            _BankServiceNavigation.BankMoneyExchangeType = bankMoneyExchangeType;
            _BankServiceNavigation.BCode = bcode;
            _BankServiceNavigation.Currency = currency;
        }

        if (_BankServiceNav == Enums.euBankServiceNavigations.Detail)
        {
            _BankServiceNavigation.IsBankServiceDetail台灣地區 = bankServiceContent.IsBankServiceDetail台灣地區;

            Literal_DetailPath.Text = String.Format("<div class='path'><a href='../{0}'>銀行牌告匯率</a>&gt;{1}</div>", Enums.GetEnumDesc(Enums.euBankServiceNavigations.牌告利率), bankServiceContent.BName);
        }

        Literal1_BankServiceNavigation.Text = _BankServiceNavigation.Create();

        #region //Mark
        //BankServiceLevel2Navigation1.BankServiceNav = _BankServiceNav;
        //BankServiceLevel2Navigation1.外幣存款利率Nav = _外幣存款利率Nav;
        //BankServiceLevel2Navigation1.存款利率Nav = _存款利率Nav;
        //BankServiceLevel2Navigation1.SubIndex = _SubIndex;

        //BankServiceContent1.BankServiceNav = _BankServiceNav;
        //BankServiceContent1.外幣存款利率Nav = _外幣存款利率Nav;
        //BankServiceContent1.存款利率Nav = _存款利率Nav;
        //BankServiceContent1.銀行換匯比較表Nav = _銀行換匯比較表Nav;
        //BankServiceContent1.SubIndex = _SubIndex;
        //BankServiceContent1.PageIndex = p;
        //BankServiceContent1.OrderDirection = orderDirection;
        //BankServiceContent1.OrderIndex = orderIndex;
        //BankServiceContent1.Keyword = k;
        //BankServiceContent1.BCode = detail;
        #endregion //Mark

        Literal_DivStart.Text = String.Format("<div class='subBk'><div class='hdPath'><h3>{0}</h3></div>", _BankServiceNav);
        Literal_DivEnd.Text = "</div><!-- subBk:end -->";

        switch (_BankServiceNav)
        {
            case Enums.euBankServiceNavigations.None:
                Literal_DivStart.Text = "<div id='main'><div class='hdPathL2'><h3>銀行牌告利率/台幣</h3></div>";
                Literal_DivEnd.Text = "</div><!-- main:end -->";

                BankServiceRight1.Visible = true;
                break;
            case Enums.euBankServiceNavigations.牌告利率:
            case Enums.euBankServiceNavigations.存款利率:
            case Enums.euBankServiceNavigations.放款利率:
            case Enums.euBankServiceNavigations.銀行換匯:
            case Enums.euBankServiceNavigations.Detail:
                if (_BankServiceNav == Enums.euBankServiceNavigations.牌告利率 || _BankServiceNav == Enums.euBankServiceNavigations.銀行換匯 || _BankServiceNav == Enums.euBankServiceNavigations.Detail)
                {
                    Literal_DivStart.Text = String.Format("<div id='main'><div class='hdPathL'><h3>{0}</h3></div>", ((_BankServiceNav == Enums.euBankServiceNavigations.Detail) ? Enums.euBankServiceNavigations.牌告利率 : _BankServiceNav));
                    Literal_DivEnd.Text = "<div style='color:red'>本公司所揭露之銀行換匯價格,僅供用戶參考,不保證與銀行實際交易價格同步更新.<br/>正確價格請參考各銀行公告之價格為準. </div></div><!-- main:end -->";

                    BankServiceRight1.Visible = true;
                }
                break;
        }

        #region SEO
        Seo seo = new Seo();
        seo.NavigationCategory = Enums.euNavigationCategory.銀行服務;
        seo.BankServiceNavigations = _BankServiceNav;
        seo.牌告利率Navigations = _牌告利率Nav;
        seo.外幣存款利率Navigations = _外幣存款利率Nav;
        seo.存款利率Navigations = _存款利率Nav;

        if (_BankServiceNav == Enums.euBankServiceNavigations.銀行換匯)
        {
            seo.BName = _BankServiceNavigation.BName;
            seo.CurrencyName = _BankServiceNavigation.CurrencyName;
        }

        seo.銀行換匯比較表Navigations = _銀行換匯比較表Nav;

        seo.Create();

        master.Title = seo.sTitle;
        master.Description = seo.sDesc;
        master.H1 = seo.sH1;
        master.Keywords = seo.keyword;
        
        #endregion SEO

        //master.Title = String.Format("{0}_{1}_理財", _BankServiceNav, Enums.euNavigationCategory.銀行服務);
        //WriteLog.Log<string>(sb.ToString());

        //WriteLog.Log<string>("BankService Page_Load 2");
    }
    #endregion Page_Load

    #region //OnInit
    //protected override void OnInit (EventArgs e)
    //{
    //    //BankServiceContent1.BankServiceDataEvent += new BankServiceDataEventHandler(BankServiceContent1_BankServiceDataEvent);

    //    base.OnInit(e);
    //}
    #endregion //OnInit

    #region //BankServiceContent1_BankServiceDataEvent (object sender, Class_BankServiceData data)
    //protected void BankServiceContent1_BankServiceDataEvent (object sender, Class_BankServiceData data)
    //{
    //    if (data != null)
    //    {
    //        //BankServiceNavigation1.IsBankServiceDetail台灣地區 = data.IsBankServiceDetail台灣地區;
    //        _BankServiceNavigation.IsBankServiceDetail台灣地區 = data.IsBankServiceDetail台灣地區;
    //        Literal1_BankServiceNavigation.Text = _BankServiceNavigation.Create();

    //        Literal_DetailPath.Text = String.Format("<div class='path'><a href='../{0}'>銀行牌告匯率</a>&gt;{1}</div>", Enums.GetEnumDesc(Enums.euBankServiceNavigations.牌告利率), data.BName);
    //    }
    //}
    #endregion //BankServiceContent1_BankServiceDataEvent (object sender, Class_BankServiceData data)
}