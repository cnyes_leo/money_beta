﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.IO;

public partial class StaticPage : System.Web.UI.Page
{
    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        #region !this.IsPostBack
        if (!this.IsPostBack)
        {
        }
        #endregion !this.IsPostBack
    }
    #endregion Page_Load

    #region OnInit
    protected override void OnInit (EventArgs e)
    {
        string page = "default";

        if (Request.QueryString.Get("Pn") != null)
            page = Request.QueryString.Get("Pn");

        string filename = Server.MapPath(page + ".htm");

        //嘗試讀取已有文件 
        Stream s = GetFileStream(filename);
        //如果文件存在並且讀取成功 
        if (s != null)
        {
            using (s)
            {
                Stream2Stream(s, Response.OutputStream);
                Response.End();
                return;
            }
        }

        //調用Main_Execute,並且獲取其輸出 
        StringWriter sw = new StringWriter();

        Server.Execute("BankService.aspx" + ((Request.QueryString.Get("q") != null) ? String.Format("?{0}", Request.QueryString.Get("q").Replace("|", "&")) : String.Empty), sw, false);

        string content = sw.ToString();

        if (content.IndexOf("<!--tear webpage ok!-->") != -1)
        {
            //寫進文件 
            try
            {
                using (FileStream fs = new FileStream(filename, FileMode.Create, FileAccess.Write, FileShare.Write))
                {
                    using (StreamWriter streamwriter = new StreamWriter(fs, Response.ContentEncoding))
                    {
                        content = System.Text.RegularExpressions.Regex.Replace(content, "staticpage\\.aspx", page + ".aspx", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                        streamwriter.Write(content);
                    }
                }
            }
            catch
            {
            }
        }

        //輸出到客戶端
        Response.Write(content);
        Response.Flush();

        //base.OnInit(e);
    }
    #endregion OnInit

    #region Page_Error
    public void Page_Error (Object sender, EventArgs e)
    {
        WriteLog.Log1<string>("StaticPage.txt", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss dddd"));
        WriteLog.Log1<string>("StaticPage.txt", "Server.GetLastError()", Server.GetLastError().ToString(), true);
        if (Server.GetLastError().GetType().ToString() == "System.Web.HttpException" &&
            (Server.GetLastError().Message == "The remote host closed the connection. The error code is 0x80072746." ||
            Server.GetLastError().Message.IndexOf("工作階段狀態已經建立工作階段 ID，但由於應用程式已經清除回應而無法儲存。") != -1))
            Server.ClearError();
    }
    #endregion Page_Error

    #region Stream GetFileStream (string filename)
    private Stream GetFileStream (string filename)
    {
        try
        {
            DateTime dt = File.GetLastWriteTime(filename);
            TimeSpan ts = DateTime.Now - dt;

            if (ts.Minutes > 5)
                return null;　　　 // 5分鐘後過期 

            return new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.Read);
        }
        catch
        {
            return null;
        }
    }
    #endregion Stream GetFileStream (string filename)

    #region Stream2Stream (Stream src, Stream dst)
    private void Stream2Stream (Stream src, Stream dst)
    {
        byte[] buf = new byte[4096];
        while (true)
        {
            int c = src.Read(buf, 0, buf.Length);
            if (c == 0)
                break;

            dst.Write(buf, 0, c);
        }
    }
    #endregion Stream2Stream (Stream src, Stream dst)
}
