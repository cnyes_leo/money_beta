﻿<%@ Page Language="C#" %>
<%@ Import Namespace="System" %>
<%@ Import Namespace="System.Data" %>

<script language="c#" runat="server">
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            //if (Request.UrlReferrer == null || ("").IndexOf(Request.UrlReferrer.Host, StringComparison.InvariantCultureIgnoreCase) == -1)
            //{
            //    Response.Write("錯誤要求！");
            //    return;
            //}

            System.Text.StringBuilder sb = new System.Text.StringBuilder();

            if ((Request.Form.Count == 1 && Request.Form["ads"] != null && Request.Form["ads"] != String.Empty) ||
                (Request.QueryString.Count == 1 && Request.QueryString["ads"] != null && Request.QueryString["ads"] != String.Empty))
            {
                string _ads = ((Request.Form["ads"] != null) ? Request.Form["ads"] : Request.QueryString["ads"]);
                if (System.Text.RegularExpressions.Regex.IsMatch(_ads, "[0-9a-zA-Z_,]+", System.Text.RegularExpressions.RegexOptions.IgnorePatternWhitespace))
                {
                    string[] ads = _ads.Split(',');

                    DataTable dt = null;

                    bool isGetNew = true;

                    System.Xml.XmlTextWriter xtw = null;

                    string xmlFile = Server.MapPath("~/Log/") + "ADs.xml";
                    
                    if (System.IO.File.Exists(xmlFile))
                    {
                        System.Xml.XmlDocument xd = new System.Xml.XmlDocument();
                        xd.Load(xmlFile);

                        DateTime getDatetime = DateTime.Parse(xd.SelectSingleNode(String.Format("/{0}/{1}", Enums.euXml_ADs_Path.HouseV2, Enums.euXml_ADs_Path.DateTime)).InnerText);

                        if (((TimeSpan)(DateTime.Now - getDatetime)).TotalHours < 6)
                        {
                            isGetNew = false;
                            dt = new DataTable();

                            DataColumn dc1 = new DataColumn(Enums.euXml_ADs_Path.creative_id.ToString());
                            DataColumn dc2 = new DataColumn(Enums.euXml_ADs_Path.url_value.ToString());
                            DataColumn dc3 = new DataColumn(Enums.euXml_ADs_Path.text_value.ToString());

                            dt.Columns.Add(dc1);
                            dt.Columns.Add(dc2);
                            dt.Columns.Add(dc3);

                            System.Xml.XmlNodeList nodeList = xd.SelectNodes(String.Format("/{0}/{1}", Enums.euXml_ADs_Path.HouseV2, Enums.euXml_ADs_Path.Tag));

                            for (int i = 0; i < nodeList.Count; i++)
                            {
                                DataRow newDr = dt.NewRow();

                                newDr[Enums.euXml_ADs_Path.creative_id.ToString()] = nodeList[i].ChildNodes[0].InnerText;
                                newDr[Enums.euXml_ADs_Path.url_value.ToString()] = nodeList[i].ChildNodes[1].InnerText;
                                newDr[Enums.euXml_ADs_Path.text_value.ToString()] = nodeList[i].ChildNodes[2].InnerText;

                                dt.Rows.Add(newDr);
                            }
                        }
                    }

                    if (isGetNew)
                    {
                        DataProvider._5_224.ADSRVDB _5_224_ADSRVDB_Dp = new DataProvider._5_224.ADSRVDB();
                        _5_224_ADSRVDB_Dp.GetADs(_ads);
                        dt = _5_224_ADSRVDB_Dp.ResultDataTable.Copy();

                        _5_224_ADSRVDB_Dp.Close();
                        _5_224_ADSRVDB_Dp = null;
                    }

                    if (dt.Rows.Count > 0)
                    {
                        sb.Append("[ ");
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            sb.Append("{ ");
                            sb.Append(String.Format("\"Tag\":\"{0}\",", ads[i]));
                            for (int j = 0; j < dt.Columns.Count; j++)
                            {
                                string columnName = dt.Columns[j].ColumnName;
                                sb.Append(String.Format("\"{0}\":\"{1}\"", columnName, dt.Rows[i][columnName]));
                                if (j != dt.Columns.Count - 1)
                                    sb.Append(",");
                            }
                            sb.Append(" }");

                            if (i != dt.Rows.Count - 1)
                                sb.Append(", ");
                        }

                        sb.Append(" ]");
                    }

                    if (isGetNew && dt.Rows.Count > 0)
                    {
                        xtw = new System.Xml.XmlTextWriter(xmlFile, System.Text.Encoding.UTF8);
                        xtw.Formatting = System.Xml.Formatting.Indented;
                        xtw.WriteStartDocument();
                        xtw.WriteStartElement(Enums.euXml_ADs_Path.HouseV2.ToString());

                        xtw.WriteStartElement(Enums.euXml_ADs_Path.DateTime.ToString());
                        xtw.WriteCData(DateTime.Now.ToString());
                        xtw.WriteEndElement();

                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            xtw.WriteStartElement(Enums.euXml_ADs_Path.Tag.ToString());
                            xtw.WriteAttributeString("Name", ads[i]);

                            xtw.WriteStartElement(Enums.euXml_ADs_Path.creative_id.ToString());
                            xtw.WriteCData(dt.Rows[i][Enums.euXml_ADs_Path.creative_id.ToString()].ToString());
                            xtw.WriteEndElement();

                            xtw.WriteStartElement(Enums.euXml_ADs_Path.url_value.ToString());
                            xtw.WriteCData(dt.Rows[i][Enums.euXml_ADs_Path.url_value.ToString()].ToString());
                            xtw.WriteEndElement();

                            xtw.WriteStartElement(Enums.euXml_ADs_Path.text_value.ToString());
                            xtw.WriteCData(dt.Rows[i][Enums.euXml_ADs_Path.text_value.ToString()].ToString());
                            xtw.WriteEndElement();
                            
                            xtw.WriteEndElement();
                        }
                        
                        xtw.WriteEndElement();
                        xtw.WriteEndDocument();
                        xtw.Close();
                        xtw = null;
                    }

                    dt.Clear();
                    dt = null;
                }
            }

            Response.Write(sb.ToString());
        }
    }
</script>