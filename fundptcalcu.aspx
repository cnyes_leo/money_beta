﻿<%@ Page Language="C#" AutoEventWireup="true" EnableEventValidation="false" CodeFile="fundptcalcu.aspx.cs"
    Inherits="fundptcalcu" EnableViewStateMac="true" MasterPageFile="~/MPs/MP1.master" %>

<%@ OutputCache Duration="60" VaryByParam="*" %>
<%@ Register Src="UCs/FundGood.ascx" TagName="FundGood" TagPrefix="uc5" %>
<%@ Register Src="UCs/MoneyTools.ascx" TagName="MoneyTools" TagPrefix="uc3" %>
<%@ Register Src="UCs/HomeBankRate.ascx" TagName="HomeBankRate" TagPrefix="uc4" %>
<%@ Register Src="UCs/fundptcalcu_02.ascx" TagName="fundptcalcu_02" TagPrefix="uc2" %>
<%@ Register Src="UCs/fundptcalcu_01.ascx" TagName="fundptcalcu_01" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script src="js/jquery.blockUI.js" type="text/javascript"></script>
    <script type="text/javascript">  

    //定義日期格式用
    $.datepicker.setDefaults($.datepicker.regional['xx']);
<!--
$(document).ready(function(){
    $("input[id*='ctl00_ContentPlaceHolder1_Fundptcalcu_01_1_TextBox2']").datepicker({dateFormat:"yy/mm/dd"});
    $("input[id*='ctl00_ContentPlaceHolder1_Fundptcalcu_01_1_TextBox3']").datepicker({dateFormat:"yy/mm/dd"});
    $("input[id*='ctl00_ContentPlaceHolder1_Fundptcalcu_02_1_TextBox2']").datepicker({dateFormat:"yy/mm/dd"});
    $("input[id*='ctl00_ContentPlaceHolder1_Fundptcalcu_02_1_TextBox3']").datepicker({dateFormat:"yy/mm/dd"});
});
//--> 
    </script>
    <form method="post" runat="server" id="Form1">
    <asp:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="999">
    </asp:ScriptManager>
    <div id="container">
        <div id="main">
            <div class="hdPathL">
                <h3>
                    基金試算</h3>
                <div class="hdInNavsR">
                    <a id="" href="http://fund.cnyes.com/Page/calcmyfund.aspx" target="_blank" title="基金透視鏡">
                        基金透視鏡</a>｜ <a id="A1" href="http://fund.cnyes.com/Page/value.aspx?fundtype=all.html"
                            target="_blank" title="基金淨值">基金淨值</a>｜ <a id="A2" href="http://fund.cnyes.com/Page/ranking.aspx?fundtype=all&RT=L3m&SortValue=five.html"
                                target="_blank">基金排行</a>｜ <a id="A3" href="http://fund.cnyes.com/myfund/myfundValue.aspx"
                                    target="_blank" title="我的基金">我的基金</a>｜ <a id="A4" href="http://fund.cnyes.com/Page/saleMechanism.aspx"
                                        target="_blank" title="銷售機構">銷售機構</a>
                </div>
                <!-- hdInNavs:end -->
            </div>
            <!-- hdPathL:end -->
            <div class="bdBk">
                <div class="fmTb">
                    <ul class="hdNavs wdpd">
                        <li><a href="javascript:;" title="單筆投資試算">單筆投資試算</a></li>
                        <li><a href="javascript:;" title="定時定額試算">定時定額試算</a></li>
                    </ul>
                    <!-- hdNavs wdpd:end -->
                    <div class="toolBx Bx">
                        <uc1:fundptcalcu_01 ID="Fundptcalcu_01_1" runat="server" />
                    </div>
                    <!-- 單筆投資試算_toolBx:end -->
                    <div class="toolBx Bx">
                        <uc2:fundptcalcu_02 ID="Fundptcalcu_02_1" runat="server" />
                    </div>
                    <!-- 單筆投資試算_toolBx:end -->
                    <div class="caption">
                        <cite>備註：</cite><br />
                        1.僅供參考，以實際績效為準 2.選擇的投資期間要在基金成立之後才會有淨值 3.不計算手續費 4.不計算配息
                    </div>
                </div>
                <!-- fmTb:end -->
            </div>
            <!-- bdBk:end -->
        </div>
        <!-- main:end -->
        <asp:Literal ID="Literal1" runat="server"></asp:Literal>
        <!-- side:end -->
    </div>
    <!-- container:end -->
    </form>
</asp:Content>
