﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Index : System.Web.UI.Page
{
    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        #region !this.IsPostBack
        if (!this.IsPostBack)
        {
            

        }
        #endregion !this.IsPostBack
    }
    #endregion Page_Load

    #region override void OnPreInit (EventArgs e)
    protected override void OnPreInit (EventArgs e)
    {
        base.OnPreInit(e);

        MPs_MP1 master = (MPs_MP1)this.Master as MPs_MP1;

        master.NavigationCategory = Enums.euNavigationCategory.首頁;
    }
    #endregion override void OnPreInit (EventArgs e)
}
