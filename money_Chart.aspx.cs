﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Globalization;
using Dundas.Charting.WebControl;
using System.Drawing;
using System.IO;
using System.Text;

public partial class _money_Chart : System.Web.UI.Page
{
    //理財頻道 圖片使用
    //http://fund.cnyes.com/chart/money/money_Chart.aspx?code=7010000,1010000,8060000&item=%E5%AE%9A%E6%9C%9F%E5%AD%98%E6%AC%BE&Period=1%E6%9C%88

    DataBaseClass EDB = new DataBaseClass();
    protected void Page_Load(object sender, EventArgs e)
    {
        string code = string.Empty;
        string item = string.Empty;
        string Period = string.Empty;
        string cash = string.Empty;
        if (Request.QueryString["code"] != null)
        {
            code = ForCheck.HtmlEncode(Request.QueryString["code"].ToString());
        }
        if (Request.QueryString["item"] != null)
        {
            item = ForCheck.HtmlEncode(Request.QueryString["item"].ToString());
        }
        if (Request.QueryString["Period"] != null)
        {
            Period = ForCheck.HtmlEncode(Request.QueryString["Period"].ToString());
        }
        if (Request.QueryString["cash"] != null)
        {
            cash = ForCheck.HtmlEncode(Request.QueryString["cash"].ToString());
        }
        draw(code, item, Period, cash);
    }

    private void draw(string code, string item, string Period, string cash)
    {
        try
        {
            ArrayList xser = new ArrayList();
            ArrayList yser = new ArrayList();

            string sql = string.Empty;
            DataTable dt = new DataTable();

            //銀行換匯
            if (item == "B" || item == "S"  
             || item == "B1" || item == "S1")
            {
                sql = "select Distinct Bank , " + item + " " +
                      "from Bank_all as b_all " +
                      "inner join Bank_Rate as b_rate ON b_all.Bank = b_rate.BName " +
                      "WHERE BCode in (" + code + ") and cash = '" + cash + "' and " + item + " <> '' ";

                dt = EDB.Select_DataReader(ConfigurationManager.ConnectionStrings["_24_bank_Read"].ConnectionString, sql);

                if (dt.Rows.Count == 0)
                { Chart1.Visible = false; Response.Write("無足夠資料，可供繪圖！"); }
                foreach (DataRow row in dt.Rows)
                {
                    xser.Add(row["Bank"]);
                    yser.Add(row[""+item+""]);
                }

            }
            else
            {
                sql = "select BName , MRate " +
                             "from bank_rate " +
                             "WHERE BCode in (" + code + ") " +
                             "AND item = '" + item + "' and Period = '" + Period + "' and Amount = '一般' " +
                             "order by MRate DESC ";

                dt = EDB.Select_DataReader(ConfigurationManager.ConnectionStrings["_24_bank_Read"].ConnectionString, sql);

                if (dt.Rows.Count == 0)
                { Chart1.Visible = false; Response.Write("無足夠資料，可供繪圖！"); }
                foreach (DataRow row in dt.Rows)
                {
                    xser.Add(row["BName"]);
                    yser.Add("0" + row["MRate"]);
                }
            }

            xser.Reverse();
            yser.Reverse();

            Chart1.Series["Series1"].Points.DataBindXY(xser, yser);
            Chart1.Series["Series1"].YAxisType = AxisType.Primary;
            Chart1.Series["Series1"].Color = Color.FromArgb(79, 129, 189);
            Chart1.Series["Series1"].BorderWidth = 1;
            Chart1.Series["Series1"].EmptyPointStyle.Color = Color.Transparent;
            Chart1.Series["Series1"].LabelFormat = "P";
            Chart1.Series["Series1"].ShowLabelAsValue = true;
            //隨機的數列顏色
            int R, G, B;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                Random Ran = new Random(Guid.NewGuid().GetHashCode());
                R = Ran.Next(180, 255);
                G = Ran.Next(180, 255);
                B = Ran.Next(180, 255);

                Chart1.Series["Series1"].Points[i].Color = System.Drawing.Color.FromArgb(R, G, B);
                Chart1.Series["Series1"].Points[i].BorderColor = System.Drawing.Color.FromArgb(R, G, B);
            }
            //計算數列寬度
            int pointwidth = xser.Count / 5 + 2;
            if (pointwidth < 10)
            {
                Chart1.Series["Series1"].CustomAttributes = "PointWidth=0." + pointwidth + "";
            }
            else
            {
                Chart1.Series["Series1"].CustomAttributes = "PointWidth=1." + pointwidth + "";
            }
            //元件設定長寬
            Chart1.Width = 540;
            Chart1.Height = 400;
            Chart1.BackColor = Color.FromArgb(255,255,255);
            Chart1.ChartAreas["Default"].Position.X = 2;
            Chart1.ChartAreas["Default"].Position.Y = 0;
            Chart1.ChartAreas["Default"].Position.Width = 100;
            Chart1.ChartAreas["Default"].Position.Height = 90;
            Chart1.ChartAreas["Default"].InnerPlotPosition.Height = 80;
            Chart1.ChartAreas["Default"].InnerPlotPosition.Width = 70;
            Chart1.ChartAreas["Default"].InnerPlotPosition.X = 25;
            Chart1.ChartAreas["Default"].InnerPlotPosition.Y = 10;
            Chart1.ChartAreas["Default"].AxisX.LabelStyle.Interval = 1;
            Chart1.ChartAreas["Default"].AxisX.LineColor = Color.Gray;
            Chart1.ChartAreas["Default"].AxisX.LabelStyle.Font = new Font("Microsoft Sans Serif", float.Parse("9"));
            Chart1.ChartAreas["Default"].AxisX.MajorGrid.LineColor = Color.LightGray;
            Chart1.ChartAreas["Default"].AxisX.MajorGrid.LineStyle = ChartDashStyle.Dot;
            Chart1.ChartAreas["Default"].AxisX.MajorTickMark.Style = TickMarkStyle.Outside;
            Chart1.ChartAreas["Default"].AxisX.MajorTickMark.LineColor = Color.LightGray;
            Chart1.ChartAreas["Default"].AxisX.LabelStyle.FontColor = Color.Gray;
            Chart1.ChartAreas["Default"].AxisY.Enabled = AxisEnabled.True;
            Chart1.ChartAreas["Default"].AxisY.MajorGrid.LineColor = Color.LightGray;
            Chart1.ChartAreas["Default"].AxisY.MajorGrid.LineStyle = ChartDashStyle.Dot;
            Chart1.ChartAreas["Default"].AxisY.LineColor = Color.Gray;
            Chart1.ChartAreas["Default"].AxisY.LineStyle = ChartDashStyle.Solid;
            Chart1.ChartAreas["Default"].AxisY.MajorTickMark.Style = TickMarkStyle.Outside;
            Chart1.ChartAreas["Default"].AxisY.LabelStyle.Format = "P3";
            //Chart1.ChartAreas["Default"].AxisY.Interval = xser.Count / 4;
            Chart1.ChartAreas["Default"].AxisY.LabelStyle.FontColor = Color.Gray;
            Chart1.ChartAreas["Default"].AxisY.LabelStyle.Font = new Font("Microsoft Sans Serif", float.Parse("9"));
            //LegendText
            Chart1.Legends[0].Enabled = false;
            //cnyes
            Chart1.Titles.Clear();
            Chart1.Titles.Add("© cnYES鉅亨網");
            Chart1.Titles[0].Text = "© cnYES鉅亨網";
            Chart1.Titles[0].Position.Auto = false;
            Chart1.Titles[0].Position.X = 75;
            Chart1.Titles[0].Position.Y = 8;
            Chart1.Titles[0].Position.Width = 20;
            Chart1.Titles[0].Position.Height = 7;
            Chart1.Titles[0].Color = Color.Gray;
            Chart1.Titles[0].Font = new Font("Microsoft Sans Serif", float.Parse("8"));
        }
        catch(Exception ee)
        { }
    }

}
