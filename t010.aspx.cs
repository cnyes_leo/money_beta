﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Configuration;


public partial class t010 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
       try
       {
           DataBaseClass dbc = new DataBaseClass();
           DataTable dt = new DataTable();
           string strConn = ConfigurationManager.ConnectionStrings["_24_bank_ConnStr"].ConnectionString;
           string strComd = "";


           #region 下拉選單-銀行

           strComd = " select * from URL where Hide <> true order by Index ";

           dt.Clear();
           dt = dbc.Select_DataReader(strConn, strComd);

           DropDownList2.DataSource = dt;
           DropDownList2.DataTextField = "nName";
           DropDownList2.DataValueField = "Index";
           DropDownList2.DataBind();
           #endregion


           #region 選擇銀行

           string bank = "jb2";
           strComd = " select * from " + bank + " order by nRow ";
           
           dt.Clear();
           dt = dbc.Select_DataReader(strConn, strComd);
           
           GridView2.DataSource = dt;
           GridView2.DataBind();
           #endregion


           #region 選擇幣別
           string dollar = "jpy";
           strComd = "select * from bank_all where Cash = '" + dollar + "'";

           dt.Clear();
           dt = dbc.Select_DataReader(strConn, strComd);

           GridView1.DataSource = dt;
           GridView1.DataBind();

           #endregion


           #region 下拉選單-幣別
           string[] aryDollar = ("美元 - USD,歐元 - EUR,港幣 - HKD,人民幣 - CNY,日圓 - JPY,韓國 - KRW,印尼幣 - IDR,菲律賓披索 - PHP,泰幣 - THB,新加坡幣 - SGD,紐元 - NZD,澳幣 - AUD,瑞士法郎 - CHF,英鎊 - GBP,加拿大幣 - CAD,南非幣 - ZAR,瑞典幣 - SEK").Split(',');

           foreach (string item in aryDollar)
           {
               DropDownList1.Items.Add(new ListItem(item, item));
           }
           #endregion


           dbc = null;
       }
       catch (Exception ex)
       {
           Literal1.Text += ex.Message;
       }









        ////定義OLE======================================================
        ////1.檔案位置
        //string FileName = Server.MapPath("tt/rate3.mdb");
        ////2.提供者名稱
        //string ProviderName = "Microsoft.Jet.OLEDB.4.0;";
        ////3.帳號
        //string UserId = ";";
        ////4.密碼
        //string Password = ";";
        ////=============================================================

        //string DataSource = FileName;
        //if (!File.Exists(DataSource))
        //{
        //    Literal1.Text = "檔案不存在";
        //    return;
        //}
        ////連線字串
        //string cs =
        //        "Data Source=" + DataSource + ";" +
        //        "Provider=" + ProviderName +
        //        "User Id=" + UserId +
        //        "Password=" + Password;

        //using (OleDbConnection cn = new OleDbConnection(cs))
        //{
        //    cn.Open();

        //    #region 選擇銀行

        //    string bank = "jb2";
        //    string qs02 = " select * from " + bank + " order by nRow ";
        //    //string qs02 = " select * from bank_all ";

        //    using (OleDbCommand cmd = new OleDbCommand(qs02, cn))
        //    {
        //        using (OleDbDataReader dr = cmd.ExecuteReader())
        //        {
        //            DataTable dt = new DataTable();
        //            dt.Load(dr);
        //            GridView2.DataSource = dt;
        //            GridView2.DataBind();
        //        }
        //    }
        //    #endregion


        //    #region 下拉選單-銀行

        //    string qs03 = " select * from URL where Hide <> true order by Index ";

        //    using (OleDbCommand cmd = new OleDbCommand(qs03, cn))
        //    {
        //        using (OleDbDataReader dr = cmd.ExecuteReader())
        //        {
        //            DataTable dt = new DataTable();
        //            dt.Load(dr);

        //            DropDownList2.DataSource = dt;
        //            DropDownList2.DataTextField = "nName";
        //            DropDownList2.DataValueField = "Index";
        //            DropDownList2.DataBind();
        //        }
        //    }
        //    #endregion
        //}
    }
}