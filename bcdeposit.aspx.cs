﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class bcdeposit : System.Web.UI.Page
{
    #region override void OnPreInit (EventArgs e)
    protected override void OnPreInit(EventArgs e)
    {
        base.OnPreInit(e);
        MPs_MP1 master = (MPs_MP1)this.Master as MPs_MP1;
        master.NavigationCategory = Enums.euNavigationCategory.理財規劃;
    }
    #endregion override void OnPreInit (EventArgs e)

    protected void Page_Load(object sender, EventArgs e)
    {
        //add_hearder("存款利率_理財比較_理財", "提供台灣、香港、中國地區各家銀行存款利率比較圖。", "存款,活存,定存");

        #region seo
        MPs_MP1 master = (MPs_MP1)this.Master as MPs_MP1;

        Seo seo = new Seo("bcdeposit_01");
        master.Title = seo.sTitle;
        master.Description = seo.sDesc;
        master.Keywords = seo.keyword;
        master.H1 = seo.sH1;
        seo = null;
        #endregion
    }

    private void add_hearder(string title, string _description, string _keywords)
    {
        Page.Title = title;

        HtmlMeta description = new HtmlMeta();
        description.Name = "description";
        description.Content = _description;
        Page.Header.Controls.Add(description);

        HtmlMeta keywords = new HtmlMeta();
        keywords.Name = "keywords";
        keywords.Content = _keywords;
        Page.Header.Controls.Add(keywords);

    }


}
