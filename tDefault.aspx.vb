﻿
Partial Class _tDefault
    Inherits System.Web.UI.Page


    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        'MPs_MP1 master = (MPs_MP1)this.Master as MPs_MP1;
        'master.NavigationCategory = Enums.euNavigationCategory.首頁;

        Dim master As MPs_MP1 = TryCast(Me.Master, MPs_MP1)
        master.NavigationCategory = Enums.euNavigationCategory.首頁


        '-------------
        'SEO
        '-------------
        Dim seo As Seo = New Seo("index")
        master.Title = seo.sTitle
        master.Description = seo.sDesc
        master.Keywords = seo.keyword
        master.H1 = seo.sH1

    End Sub

End Class
