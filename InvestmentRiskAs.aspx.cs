﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class InvestmentRisk : System.Web.UI.Page
{
    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        #region !this.IsPostBack
        if (!this.IsPostBack)
        {
            Label2.Text = Request["count"];

            int score = int.Parse(Request["count"]);

            if (score <11)
            {
                td7_1.Attributes.Add("class", "pgrs cr");
                td7_2.Attributes.Add("class", "pgrs");
                Label1.Text = "保守型";
            }
            else
            {
                td7_1.Attributes.Add("class", "cr");
            }

            if (score < 16 && score > 10)
            {
                td11_1.Attributes.Add("class", "pgrs cr");
                td11_2.Attributes.Add("class", "pgrs");
                Label1.Text = "安穩型";
            }
            else
            {
                td11_1.Attributes.Add("class", "cr");
            }

            if (score < 23 && score > 15)
            {
                td16_1.Attributes.Add("class", "pgrs cr");
                td16_2.Attributes.Add("class", "pgrs");
                Label1.Text = "穩健型";
            }
            else
            {
                td16_1.Attributes.Add("class", "cr");
            }

            if (score < 31 && score > 22)
            {
                td23_1.Attributes.Add("class", "pgrs cr");
                td23_2.Attributes.Add("class", "pgrs");
                Label1.Text = "成長型";
            }
            else
            {
                td23_1.Attributes.Add("class", "cr");
            }

            if (score > 30)
            {
                td31_1.Attributes.Add("class", "pgrs cr");
                td31_2.Attributes.Add("class", "pgrs");
                Label1.Text = "積極型";
            }
            else
            {
                td31_1.Attributes.Add("class", "cr");
            }

            //pgrs cr
            //pgrs
        }
        #endregion !this.IsPostBack
    }
    #endregion Page_Load

    #region override void OnPreInit (EventArgs e)
    protected override void OnPreInit(EventArgs e)
    {
        base.OnPreInit(e);

        MPs_MP1 master = (MPs_MP1)this.Master as MPs_MP1;

        master.NavigationCategory = Enums.euNavigationCategory.投資分析;
        
    }
    #endregion override void OnPreInit (EventArgs e)

    protected void Button2_Click(object sender, EventArgs e)
    {
        Response.Redirect("InvestmentRisk.aspx");
    }
}
