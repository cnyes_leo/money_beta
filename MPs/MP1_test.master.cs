﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class MPs_MP1_test : System.Web.UI.MasterPage
{
    #region 變數
    private Enums.euNavigationCategory _NavigationCategory = Enums.euNavigationCategory.首頁;
    private String _Description = String.Empty;
    private String _Keywords = String.Empty;
	private String _H1 = String.Empty;
    private System.Text.StringBuilder sbFootJs = new System.Text.StringBuilder();
	#endregion 變數

    #region 屬性

    #region Enums.euNavigationCategory NavigationCategory
    public Enums.euNavigationCategory NavigationCategory
    {
        set
        {
            _NavigationCategory = value;
        }
        get
        {
            return _NavigationCategory;
        }
    }
    #endregion Enums.euNavigationCategory NavigationCategory

    #region String Title
    public String Title
    {
        set
        {
            Title1.Text = value;
        }
    }
    #endregion String Title

    #region String Description
    public String Description
    {
        set
        {
            _Description = value;
        }
    }
    #endregion String Description

    #region String Keywords
    public String Keywords
    {
        set
        {
            _Keywords = value;
        }
    }
    #endregion String Keywords
	
    #region String H1
    public String H1
    {
        set
        {
            _H1 = value;
        }
    }
    #endregion String H1

    #region String FootJs
    public String FootJs
    {
        set
        {
            sbFootJs.Append(value);
        }
    }
    #endregion String FootJs
		
    #endregion 屬性

    #region Page_Load
    protected void Page_Load (object sender, EventArgs e)
    {
        System.Text.StringBuilder sb = new System.Text.StringBuilder();

        sb.AppendLine(String.Format("<link href=\"{0}css/public.css\" rel=\"stylesheet\" type=\"text/css\" />", Request.Path.Substring(0, Request.Path.LastIndexOf('/') + 1)));
        sb.AppendLine(String.Format("<link href='{0}css/money{1}.css' rel='stylesheet' type='text/css' />", Request.Path.Substring(0, Request.Path.LastIndexOf('/') + 1), ((_NavigationCategory == Enums.euNavigationCategory.首頁) ? "Home" : String.Empty)));
        Literal_TopCss.Text = sb.ToString();

        sb.Length = 0;
        sb.AppendLine(String.Format("<script type=\"text/javascript\" src=\"{0}js/tools.scrollable.min.js\"></script>", Request.Path.Substring(0, Request.Path.LastIndexOf('/') + 1)));
        sb.AppendLine(String.Format("<script type=\"text/javascript\" src=\"{0}js/tools.tabs-1.0.4.js\"></script>", Request.Path.Substring(0, Request.Path.LastIndexOf('/') + 1)));
        sb.AppendLine(String.Format("<script type=\"text/javascript\" src=\"{0}js/common.js\"></script>", Request.Path.Substring(0, Request.Path.LastIndexOf('/') + 1)));
        //---------------------------------------------------------------
        //留校察看區，看不出用在哪~確定無用可刪除  brian   0503
		//sb.AppendLine(String.Format("<script type=\"text/javascript\" src=\"http://img.cnyes.com/js/GoExchange.js\"></script>", Request.Path.Substring(0, Request.Path.LastIndexOf('/') + 1)));	//這條好像只有首頁再用,在某個uc中已經有獨立寫入
		//sb.AppendLine(String.Format("<script type=\"text/javascript\" src=\"{0}js/house2.js\"></script>", Request.Path.Substring(0, Request.Path.LastIndexOf('/') + 1)));
        //sb.AppendLine(String.Format("<script type=\"text/javascript\" src=\"{0}Charts/FusionCharts.js\"></script>", Request.Path.Substring(0, Request.Path.LastIndexOf('/') + 1)));
		//---------------------------------------------------------------
		if (_NavigationCategory == Enums.euNavigationCategory.投資分析)
        {
            //換成下面的 brian 0430
            //sb.AppendLine("<script type=\"text/javascript\" src=\"http://img.cnyes.com/js/ui.datepicker_M.js\"></script>");
            //sb.AppendLine("<link href=\"http://img.cnyes.com/js/ui.datepicker.css\" rel=\"stylesheet\" type=\"text/css\" />");
            sb.AppendLine(String.Format(@"<script src=""{0}js/ui.datepicker_M.js"" type=""text/javascript""></script>", Request.Path.Substring(0, Request.Path.LastIndexOf('/') + 1)));
            sb.AppendLine(String.Format(@"<link href=""{0}js/ui.datepicker_M.css"" rel=""stylesheet"" type=""text/css"" />", Request.Path.Substring(0, Request.Path.LastIndexOf('/') + 1)));
        }
        //---------------------------------------------------------------

        Literal_TopJs.Text = sb.ToString();


        #region !this.IsPostBack
        if (!this.IsPostBack)
        {
            #region Headers1
            Headers1.ChannelName = "理財";
            Headers1.ChannelURL = "http://www.cnyes.com/money/";
            Headers1.ChannelPicURL = String.Format("{0}images/money.gif", Request.Path.Substring(0, Request.Path.LastIndexOf('/') + 1));

            Headers1.AD_Tag = "HOMEBANNER";

        //    Headers1.TopTextAD_Tags = "MONEYHOME_A,MONEYHOME_B,MONEYHOME_C";
            Headers1.TopTextAD_Tags = "DFP:cnyes_money_word_new_A:315x20,DFP:cnyes_money_word_new_B:315x20,DFP:cnyes_money_word_new_C:315x20";

            Headers1.jQuery = CommonWebControls_Headers_Html_test.eujQuery.cookie;
            #endregion Headers1
        }
        #endregion !this.IsPostBack

        #region SEO
        sb.Length = 0;
        if (_Description.Trim() != "") sb.Append(@" <meta name=""description"" content=""" + _Description + @""" /> ");
        if (_Keywords.Trim() != "") sb.Append(@" <meta name=""keywords"" content=""" + _Keywords + @""" /> ");
        if (sb.ToString() != "") Literal_Description_Keywords.Text = sb.ToString();
        sb.Length = 0;


        if (_H1.Trim() != "")
        {
            sb.Append(@" <h1 class=""h"">" + _H1 + @" </h1> ");
            Literal_H1.Text = sb.ToString();
            sb.Length = 0;
        }
        #endregion SEO

        //sbFootJs.AppendLine("<script type=\"text/javascript\">");
        //sbFootJs.AppendLine("<!--");
        //sbFootJs.AppendLine(String.Format("$.post(\"{0}ajax/getad.aspx\", \"ads=FUND_A,FUND_B,FUND_C,FUND_D,FUND_E\",", Request.Path.Substring(0, Request.Path.LastIndexOf('/') + 1)));
        //sbFootJs.AppendLine("function(r){if (r != \"錯誤要求！\"){var ads = eval (\"(\" + r + \")\");var cyglodtxt = new StringBuilder();for (var i=0; i<ads.length; i++)");
        //sbFootJs.AppendLine("{switch(ads[i].Tag){case \"FUND_A\": case \"FUND_B\": case \"FUND_C\": case \"FUND_D\": case \"FUND_E\": cyglodtxt.Append(\"<td><a target='_blank' href='http://ad.cnyes.com/cnYESAD/adredir.asp?ciid=\" + ads[i].creative_id + \"&url=\" + ads[i].url_value + \"'>\" + ads[i].text_value + \"</a></td>\");");
        //sbFootJs.AppendLine("break;}} if (cyglodtxt.ToString().length > 0) $(\"div[class='cyglodtxt']\").html(\"<table border='0' cellspacing='0' cellpadding='0'><tr>\" + cyglodtxt.ToString() + \"</tr></table>\"); }else alert(r);});");
        //sbFootJs.AppendLine("//-->");
        //sbFootJs.AppendLine("</script>");

        Literal_FootJs.Text = sbFootJs.ToString();

        String filename = System.IO.Path.GetFileName(Request.PhysicalPath).ToLower();

        switch (filename)
        {
            case "fundptcalcu.aspx":
            case "investmentrisk.aspx":
                Headers1._secid = 1;
                break;
            case "livelihoodprice.aspx":
                Headers1._secid = 2;
                break;
            case "bankcalculation.aspx":
                Headers1._secid = 3;
                break;
            default:
                Headers1._secid = 0;
                break;
        }
    }
    #endregion Page_Load
}
