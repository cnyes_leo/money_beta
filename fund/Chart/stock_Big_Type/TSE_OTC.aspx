﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TSE_OTC.aspx.cs" Inherits="stock_Big_Type_TSE_OTC" %>

<%@ Register Assembly="DundasWebChart" Namespace="Dundas.Charting.WebControl" TagPrefix="DCWC" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>未命名頁面</title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <DCWC:Chart ID="c" runat="server" BackColor="#F2F2F2" Height="225px" Width="460px" Visible="False">
                <Legends>
                    <DCWC:Legend Alignment="Center" AutoFitText="False" BackColor="242, 242, 242" Docking="Bottom"
                        FontColor="64, 64, 64" Name="Default">
                        <Position Height="10" Width="100" Y="2" />
                    </DCWC:Legend>
                </Legends>
                <Series>
                    <DCWC:Series BorderWidth="2" ChartType="Line" Color="61, 115, 179" LegendText="資金流向率"
                        Name="s1">
                    </DCWC:Series>
                    <DCWC:Series BorderWidth="2" ChartType="Line" Color="187, 77, 76" LegendText="市值增減率"
                        Name="s2" YAxisType="Secondary">
                    </DCWC:Series>
                </Series>
                <ChartAreas>
                    <DCWC:ChartArea BorderColor="DarkGray" BorderStyle="Solid" Name="Default">
                        <AxisY LabelsAutoFit="False" LineColor="DarkGray" StartFromZero="False">
                            <MajorGrid LineColor="Silver" Interval="Auto" IntervalOffset="Auto" IntervalOffsetType="Auto"
                                IntervalType="Auto" LineStyle="Dot" />
                            <MajorTickMark LineColor="DarkGray" Interval="Auto" IntervalOffset="Auto" IntervalOffsetType="Auto"
                                IntervalType="Auto" />
                            <LabelStyle FontColor="61, 115, 179" Format="P0" Interval="Auto" IntervalOffset="Auto"
                                IntervalOffsetType="Auto" IntervalType="Auto" />
                        </AxisY>
                        <AxisX LabelsAutoFit="False" LineColor="DarkGray" Margin="False">
                            <MajorGrid Enabled="False" Interval="Auto" IntervalOffset="Auto" IntervalOffsetType="Auto"
                                IntervalType="Auto" />
                            <MajorTickMark LineColor="DarkGray" Size="2" Interval="Auto" IntervalOffset="Auto"
                                IntervalOffsetType="Auto" IntervalType="Auto" />
                            <LabelStyle FontColor="64, 64, 64" Interval="Auto" IntervalOffset="Auto" IntervalOffsetType="Auto"
                                IntervalType="Auto" />
                        </AxisX>
                        <Position Height="90" Width="93" X="3" Y="10" />
                        <AxisY2 LabelsAutoFit="False" LineColor="DarkGray" StartFromZero="False">
                            <MajorGrid Enabled="False" Interval="Auto" IntervalOffset="Auto" IntervalOffsetType="Auto"
                                IntervalType="Auto" />
                            <MajorTickMark LineColor="DarkGray" Interval="Auto" IntervalOffset="Auto" IntervalOffsetType="Auto"
                                IntervalType="Auto" />
                            <LabelStyle FontColor="187, 77, 76" Format="P0" Interval="Auto" IntervalOffset="Auto"
                                IntervalOffsetType="Auto" IntervalType="Auto" />
                        </AxisY2>
                        <InnerPlotPosition Height="81.52343" Width="85" X="7.79402" Y="4.59375" />
                        <AxisX2>
                            <MajorGrid Interval="Auto" IntervalOffset="Auto" IntervalOffsetType="Auto" IntervalType="Auto" />
                            <MajorTickMark Interval="Auto" IntervalOffset="Auto" IntervalOffsetType="Auto" IntervalType="Auto" />
                            <LabelStyle Interval="Auto" IntervalOffset="Auto" IntervalOffsetType="Auto" IntervalType="Auto" />
                        </AxisX2>
                    </DCWC:ChartArea>
                </ChartAreas>
                <Titles>
                    <DCWC:Title Color="DarkGray" Font="Microsoft Sans Serif, 8.8pt" Name="Title1" Text="&#169;">
                        <Position Height="6" Width="10" X="68" Y="15" />
                    </DCWC:Title>
                    <DCWC:Title Color="DarkGray" Name="Title2" Text="cnYES鉅亨網" Font="Microsoft Sans Serif, 7.5pt">
                        <Position Height="7" Width="25" X="69" Y="14" />
                    </DCWC:Title>
                </Titles>
            </DCWC:Chart>
            </div>
    </form>
</body>
</html>
