﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="EN_Index.aspx.cs" Inherits="stock_hk_EN_Index" %>

<%@ Register Assembly="DundasWebChart" Namespace="Dundas.Charting.WebControl" TagPrefix="DCWC" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>台灣經濟指標圖</title>
    
</head>
<body>
    <form id="form1" runat="server">
        <asp:Image ID="Image1" runat="server" ImageUrl="~/stock_hk/File/EN_Index1.png" />
        <asp:Image ID="Image2" runat="server" ImageUrl="~/stock_hk/File/EN_Index2.png" /><asp:Image ID="Image5" runat="server" ImageUrl="~/stock_hk/File/EN_Index3.png" />
        <asp:Image ID="Image3" runat="server" ImageUrl="~/stock_hk/File/EN_Index4.png" /><asp:Image ID="Image4" runat="server" ImageUrl="~/stock_hk/File/EN_Index5.png" />
        <asp:Image ID="Image6" runat="server" ImageUrl="~/stock_hk/File/EN_Index6.png" /><DCWC:Chart ID="c1" runat="server" BackGradientEndColor="White" BorderLineColor="Gray" BorderLineStyle="Solid"
            Height="200px" Width="350px" BorderLineWidth="0" BackColor="#F2F2F2" Visible="False" > 
            <Legends>
                <DCWC:Legend Alignment="Center" AutoFitText="False" BackColor="242, 242, 242" BorderColor="Gray"
                    BorderWidth="0" Docking="Bottom" DockToChartArea="Default" Font="Microsoft Sans Serif, 7.5pt"
                    FontColor="64, 64, 64" Name="Default">
                    <Position Height="10" Width="100" Y="87" />
                </DCWC:Legend>
            </Legends>
            <BorderSkin FrameBackColor="Honeydew" FrameBackGradientEndColor="Teal" PageColor="AliceBlue" />
            <Series>
                <DCWC:Series Color="89, 166, 232" Name="Series1" BorderColor="11, 109, 193" LegendText="本地生產總值/百萬港幣">
                </DCWC:Series>
                <DCWC:Series BorderWidth="2" ChartType="Line" Color="224, 52, 51"
                    MarkerColor="White" MarkerSize="4" MarkerStyle="Circle" Name="Series2" MarkerBorderColor="224, 52, 51" YAxisType="Secondary" LegendText="本地生產總值/年增">
                </DCWC:Series>
            </Series>
            <ChartAreas>
                <DCWC:ChartArea BackColor="White" BorderColor="Peru" Name="Default">
                    <AxisX LineColor="Gray" LabelsAutoFit="False">
                        <MajorGrid Enabled="False" />
                        <LabelStyle FontColor="64, 64, 64" Font="Microsoft Sans Serif, 7.5pt" />
                        <MajorTickMark LineColor="Gray" />
                    </AxisX>
                    <InnerPlotPosition Height="75" Width="81" X="9" Y="8" />
                    <Position Height="83" Width="91" X="6" Y="7" />
                    <AxisY LabelsAutoFit="False" LabelsAutoFitMaxFontSize="5" LabelsAutoFitMinFontSize="5"
                        LineColor="LightGray" LogarithmBase="5" StartFromZero="False">
                        <LabelStyle Format="N0" FontColor="64, 64, 64" Font="Microsoft Sans Serif, 7.5pt" />
                        <MajorTickMark Enabled="False"  LineColor="Gray"/>
                        <MajorGrid LineColor="DarkGray" LineStyle="Dot" />
                    </AxisY>
                    <AxisY2 LineColor="LightGray" LabelsAutoFit="False">
                        <MajorGrid Enabled="False" />
                        <MajorTickMark LineColor="DarkGray" Style="inside" />
                        <LabelStyle FontColor="64, 64, 64" Font="Microsoft Sans Serif, 7.5pt" />
                    </AxisY2>
                    <Area3DStyle RightAngleAxes="False" WallWidth="5" XAngle="9" YAngle="11" />
                </DCWC:ChartArea>
            </ChartAreas>
            <Titles>
                <DCWC:Title BackColor="242, 242, 242" Color="64, 64, 64" Name="Title1">
                    <Position Height="8.74686" Width="94" X="3" Y="2" />
                </DCWC:Title>
                <DCWC:Title Color="DarkGray" Font="Microsoft Sans Serif, 7.5pt" Name="Title3" Text="&#169; cnYES鉅亨網">
                    <Position Height="7" Width="25" X="64" Y="14" />
                </DCWC:Title>
            </Titles>
        </DCWC:Chart>
     
  
    </form>
 
</body>
</html>
