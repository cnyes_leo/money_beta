﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

public partial class stock_hk_EN_Index : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack) 
        {
            this.bind();
            this.SEO();
        }
    }

    #region//SEO
    protected void SEO()
    {

        this.Title = "鉅亨網-香港經濟指標圖";     
         
        HtmlMeta meta1 = new HtmlMeta();  
        HtmlMeta meta2 = new HtmlMeta();
        HtmlMeta meta3 = new HtmlMeta();
         
        meta1.Name = "description";
        meta1.Content = "鉅亨網-香港經濟指標圖,Dundas-香港經濟指標圖,鉅亨網-香港經濟指標圖Chart";
        this.Page.Header.Controls.Add(meta1);
        
        meta2.Name = "keywords";         
        meta2.Content = "鉅亨網-香港經濟指標圖,Dundas-香港經濟指標圖,鉅亨網-香港經濟指標圖Chart";
        this.Page.Header.Controls.Add(meta2);
         
        meta3.HttpEquiv = "Content-Type";
        meta3.Content = "text/html; charset=utf-8";
        this.Page.Header.Controls.Add(meta3);

    }
    #endregion

    #region//bind 

    protected void bind()
    {
        stock_hk.Draw_hk_EN_Index_main(c1, "1");  
        stock_hk.Draw_hk_EN_Index_main(c1, "2");  
        stock_hk.Draw_hk_EN_Index_main(c1, "3");  
        stock_hk.Draw_hk_EN_Index_main(c1, "4");  
        stock_hk.Draw_hk_EN_Index_main(c1, "5");  
        stock_hk.Draw_hk_EN_Index_main(c1, "6");   
    }

    #endregion

}
