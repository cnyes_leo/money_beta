﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="small_vote.aspx.cs" Inherits="Vote_small_vote" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>未命名頁面</title>

<link href="http://www.cnyes.com//twstock/css09/public.css" rel="stylesheet" type="text/css" />
<link href="http://www.cnyes.com//twstock/css09/twstock.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
.gdvote .bd {padding:12px 8px 0px 12px; border:1px solid #dddddd; border-top:0px;text-align:left;}
.bar_r { display:block; background:#e6b9b8; border:1px solid #953735; height:12px; overflow:hidden;}
.bar_g { display:block; background:#b9cde5; border:1px solid #376092; height:12px; overflow:hidden;}
.bar_y { display:block; background:#d7e4bd; border:1px solid #7b9743; height:12px; overflow:hidden;}
.inqnote {height:25px; line-height:25px; font:12px Arial, Helvetica, sans-serif; color:#333;}
.inqnote span {color:#FF6600; font-size:11px;}
.inqlt {float:left; width:330px;}
.inqlt .bd {border:0; padding:10px 0px 0px 0px;}
.inqlt em {width:60px; padding-left:5px;}
.inqlt .percent {margin-left:15px;}
.diden { float:right; width:330px;}
.diden .hd1,.inqlt .hd1 {height:20px; line-height:20px; background:#fff; font:bold 13px Arial, Helvetica, sans-serif;}
.diden table {margin-top:3px;}
.diden th {background:#eee; padding:3px 2px 2px 2px; border:1px solid #ccc;}
.diden td {border-bottom:1px dotted #ccc; height:1.8em; line-height:1.8em;}

</style>

</head>
<body>
    <form id="form1" runat="server">
<div style=" margin:0px auto; width:330px;overflow:hidden; height:1%; text-align:left;">
        <!-- 多空調查:start -->
        <asp:HiddenField ID="Vote_yes_no" runat="server" />
        <div class="mainboxs2 gdvote inqlt">
<%--            <div class="hd1">
                <a href="http://www.cnyes.com/twstock/vote.aspx" target="_blank" ><asp:Literal ID="TopOneName" runat="server"></asp:Literal>多空調查</a></div>--%>
            <div class="bd">
                <div class="linebar">
                    <em>看多</em>
                    <div class="bars">
                        <span class="bar_r" style="width :<%=p1%>%;"></span>
                    </div>
                    <cite class="percent"><%=p1%>%</cite>
                  <asp:Button ID="Button1" class="butn" runat="server" Text="投票" Width="42px" OnClick="Button1_Click" />
                </div>
                <div class="linebar">
                    <em>看空</em>
                    <div class="bars">
                        <span class="bar_y" style="width:<%=p3%>%;"></span>
                    </div>
                    <cite class="percent"><%=p3%>%</cite>
                    <asp:Button ID="Button3" class="butn" runat="server" Text="投票" Width="42px" OnClick="Button3_Click" />
                </div>
                <div class="linebar">
                    <em>盤整</em>
                    <div class="bars">
                        <span class="bar_g" style="width:<%=p2%>%;"></span>
                    </div>
                    <cite class="percent"><%=p2%>%</cite>
                   <asp:Button ID="Button2" class="butn" runat="server" Text="投票" Width="42px" OnClick="Button2_Click" />
                </div>
                <div class="inqnote">
                    <asp:Literal ID="L2" runat="server"></asp:Literal>當日多空調查指數：<span runat ="server" id="cResult">0</span></div>
            </div>
            <!-- bd:end -->
        </div>
        <!-- 多空調查:end -->   
    
 </div>    

    </form>
</body>
</html>
