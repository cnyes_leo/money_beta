﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using System.IO;


public partial class Vote_Default2 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string line = "";
         ArrayList aa=new ArrayList();
        using (StreamReader sr = new StreamReader(Server.MapPath("~/Vote/EMGRPT.TXT"), Encoding.Default))
        {
            int j = 1;
            while ((line = sr.ReadLine()) != null)
            {
                if (j == 24)
                {
                    ArrayList aaa = ConvertEMGRPT(line);
                }
                j++;
            }
        }
    }

    private System.Collections.ArrayList ConvertEMGRPT(string str)
    {
        System.Collections.ArrayList result = new System.Collections.ArrayList();

        int[] indexs = { 6, 17, 26, 35, 44, 53, 62, 73, 81, 89, 97, 116, 139, 146, 158, 167, 176 };
        byte[] strBig5Bytes = Encoding.GetEncoding("BIG5").GetBytes(str);
        byte[] resultBytes = new byte[30];
        for (int i = 0; i < indexs.Length - 1; i++)
        {
            int startIndex = 0;
            int len = 0;
            if (i != 0)
                startIndex = indexs[i - 1] + 1;

            len = indexs[i] - startIndex;

            Array.Copy(strBig5Bytes, startIndex, resultBytes, 0, len);

            result.Add(Encoding.GetEncoding("BIG5").GetString(resultBytes).Replace((char)0x0, ' ').Trim());

            Array.Clear(resultBytes, 0, resultBytes.Length);
        }

        return result;
    }

}
