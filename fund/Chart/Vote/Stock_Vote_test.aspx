﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Stock_Vote_test.aspx.cs" Inherits="Vote_Stock_Vote_test" %>

<%@ Register Assembly="DundasWebChart" Namespace="Dundas.Charting.WebControl" TagPrefix="DCWC" %>
<%@ Register Src="~/Vote/uc_Stock/uc_vote_Top_test.ascx" TagName="vote_Top" TagPrefix="ucvote_Top" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>多空調查_外匯_金融中心_鉅亨網</title>
    <link href="uc/inquire.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
        <div id="main2">
            <asp:HiddenField ID="everychennal_chart_hf1" runat="server" />
            <asp:HiddenField ID="everychennal_chart_hf2" runat="server" />
            <ucvote_Top:vote_Top ID="vote_Top_Name" runat="server" />
            <!-- 多空調查統計圖:start -->
            <div class="mainboxs inquchart">
                <div class="sphd">
                    多空調查統計圖</div>
                <div class="sphdselt">
                    <span class="yrs"><span class="yr">
                        <asp:Button ID="One_1M" runat="server" OnClick="One_1M_Click" Text="1M" /></span>
                        <span class="yr">
                            <asp:Button ID="One_3M" runat="server" OnClick="One_3M_Click" Text="3M" /></span>
                        <span class="yr">
                            <asp:Button ID="One_6M" runat="server" OnClick="One_6M_Click" Text="6M" /></span>
                        <span class="yr">
                            <asp:Button ID="One_1Y" runat="server" OnClick="One_1Y_Click" Text="1Y" /></span>
                    </span>
                    <!-- yrs:end -->
                </div>
                <!-- sphdselt:end -->
                <div class="bd cr">
                    <div class="crtex">
                        <DCWC:Chart ID="c1" runat="server" BorderLineColor="LightGray" BorderLineStyle="Solid"
                            Height="250px" Width="640px">
                            <Legends>
                                <DCWC:Legend Alignment="Center" Docking="Top" FontColor="64, 64, 64" Name="Default">
                                    <Position Height="9.638555" Width="32" X="33.9593124" Y="3" />
                                </DCWC:Legend>
                            </Legends>
                            <Series>
                                <DCWC:Series BorderColor="149, 55, 53" ChartType="100%StackedColumn" Color="217, 150, 148"
                                    LegendText="看多" Name="Series3">
                                </DCWC:Series>
                                <DCWC:Series BorderColor="119, 147, 60" ChartType="100%StackedColumn" Color="215, 228, 189"
                                    LegendText="看空" Name="Series1">
                                </DCWC:Series>
                                <DCWC:Series BorderColor="51, 96, 148" ChartType="100%StackedColumn" Color="185, 205, 229"
                                    LegendText="盤整" Name="Series2">
                                </DCWC:Series>
                            </Series>
                            <ChartAreas>
                                <DCWC:ChartArea BorderColor="Silver" BorderStyle="Solid" Name="Default">
                                    <AxisY LabelsAutoFit="False" LineColor="Silver">
                                        <MajorGrid LineColor="Silver" LineStyle="Dot" />
                                        <MinorGrid LineColor="Silver" />
                                        <MajorTickMark LineColor="Silver" />
                                        <LabelStyle Font="Microsoft Sans Serif, 8.25pt" FontColor="64, 64, 64" Format="p0" />
                                    </AxisY>
                                    <AxisX LabelsAutoFit="False" LineColor="Silver">
                                        <MajorTickMark LineColor="Silver" />
                                        <LabelStyle Font="Microsoft Sans Serif, 8.25pt" FontColor="64, 64, 64" />
                                    </AxisX>
                                    <Position Height="85" Width="95" X="2" Y="10" />
                                    <InnerPlotPosition Height="84.92647" Width="90.07553" X="8.81921" Y="3.70588" />
                                </DCWC:ChartArea>
                            </ChartAreas>
                            <Titles>
                                <DCWC:Title Color="Silver" Font="Microsoft Sans Serif, 9pt" Name="Title1" Text="&#169; cnYES鉅亨網">
                                    <Position Height="6.05704069" Width="20" X="78" Y="13" />
                                </DCWC:Title>
                            </Titles>
                        </DCWC:Chart>
                        <!-- 多空調查指數走勢圖:start -->
                    </div>   
                        </div>   </div>   
                        <div class="mainboxs inquchart">
                            <div class="sphd">
                                多空調查指數走勢圖</div>
                            <div class="sphdselt">
                                <span class="yrs"><span class="yr">
                                    <asp:Button ID="Two_1M" runat="server" OnClick="Two_1M_Click" Text="1M" /></span>
                                    <span class="yr">
                                        <asp:Button ID="Two_3M" runat="server" OnClick="Two_3M_Click" Text="3M" /></span>
                                    <span class="yr">
                                        <asp:Button ID="Two_6M" runat="server" OnClick="Two_6M_Click" Text="6M" /></span>
                                    <span class="yr">
                                        <asp:Button ID="Two_1Y" runat="server" OnClick="Two_1Y_Click" Text="1Y" /></span>
                                </span>
                                <!-- yrs:end -->
                            </div>
                            <!-- sphdselt:end -->
                            <div class="bd cr">
                                <div class="crtex crtexdotd">
                                    <DCWC:Chart ID="c2" runat="server" BorderLineColor="Transparent" BorderLineStyle="Solid"
                                        Height="250px" Width="640px">
                                        <Legends>
                                            <DCWC:Legend Alignment="Center" AutoFitText="False" Docking="Top" Font="Microsoft Sans Serif, 9pt"
                                                FontColor="64, 64, 64" Name="Default">
                                                <Position Height="9.638555" Width="100" Y="1.5" />
                                            </DCWC:Legend>
                                        </Legends>
                                        <Titles>
                                            <DCWC:Title Color="Silver" Font="Microsoft Sans Serif, 9pt" Name="Title1" Text="&#169; cnYES鉅亨網">
                                                <Position Height="6.05704069" Width="20" X="75" Y="13" />
                                            </DCWC:Title>
                                        </Titles>
                                        <Series>
                                            <DCWC:Series BorderColor="177, 89, 16" BorderWidth="2" ChartType="Line" Color="177, 89, 16"
                                                LegendText="台灣加權股價指數" Name="Series1">
                                            </DCWC:Series>
                                            <DCWC:Series BorderColor="147, 75, 199" BorderWidth="2" ChartType="Line" Color="147, 75, 199"
                                                LegendText="台股多空調查指數" Name="Series2" YAxisType="Secondary">
                                            </DCWC:Series>
                                        </Series>
                                        <ChartAreas>
                                            <DCWC:ChartArea BorderColor="Silver" BorderStyle="Solid" Name="Default">
                                                <AxisY LabelsAutoFit="False" LineColor="Silver" StartFromZero="False">
                                                    <MajorGrid LineColor="Silver" LineStyle="Dot" />
                                                    <MinorGrid LineColor="Silver" />
                                                    <MajorTickMark LineColor="Silver" Enabled="False" />
                                                    <LabelStyle Font="Microsoft Sans Serif, 8.25pt" FontColor="64, 64, 64" Format="0.00" />
                                                </AxisY>
                                                <AxisX LabelsAutoFit="False" LineColor="Silver">
                                                    <MajorGrid Enabled="False" />
                                                    <MajorTickMark LineColor="Silver" />
                                                    <LabelStyle Font="Microsoft Sans Serif, 8.25pt" FontColor="64, 64, 64" />
                                                </AxisX>
                                                <AxisY2 LabelsAutoFit="False" LineColor="Silver" StartFromZero="False">
                                                    <MajorGrid Enabled="False" />
                                                    <MajorTickMark LineColor="DarkGray" Style="inside" LineStyle="Dot" />
                                                    <LabelStyle Font="Microsoft Sans Serif, 8.25pt" FontColor="64, 64, 64" />
                                                </AxisY2>
                                                <Position Height="85" Width="95" X="3" Y="10" />
                                                <InnerPlotPosition Height="84.92647" Width="87" X="8" Y="3.70588" />
                                            </DCWC:ChartArea>
                                        </ChartAreas>
                                    </DCWC:Chart>
                                </div>
                                <div class="crtex">
                                    <DCWC:Chart ID="c3" runat="server" BorderLineColor="Transparent" BorderLineStyle="Solid"
                                        Height="250px" Width="640px">
                                        <Legends>
                                            <DCWC:Legend Alignment="Center" AutoFitText="False" Docking="Top" Font="Microsoft Sans Serif, 9pt"
                                                FontColor="64, 64, 64" Name="Default">
                                                <Position Height="9.638555" Width="100" Y="1.5" />
                                            </DCWC:Legend>
                                        </Legends>
                                        <Titles>
                                            <DCWC:Title Color="Silver" Font="Microsoft Sans Serif, 9pt" Name="Title1" Text="&#169; cnYES鉅亨網">
                                                <Position Height="6.05704069" Width="20" X="75" Y="13" />
                                            </DCWC:Title>
                                        </Titles>
                                        <Series>
                                            <DCWC:Series BorderColor="149, 55, 53" BorderWidth="2" Color="217, 150, 148" LegendText="台股指數漲跌幅"
                                                Name="Series1">
                                            </DCWC:Series>
                                            <DCWC:Series BorderColor="147, 75, 199" BorderWidth="2" ChartType="Line" Color="147, 75, 199"
                                                LegendText="台股多空調查指數" Name="Series2" YAxisType="Secondary">
                                            </DCWC:Series>
                                        </Series>
                                        <ChartAreas>
                                            <DCWC:ChartArea BorderColor="Silver" BorderStyle="Solid" Name="Default">
                                                <AxisY LabelsAutoFit="False" LineColor="Silver" StartFromZero="False">
                                                    <MajorGrid LineColor="Silver" LineStyle="Dot" />
                                                    <MinorGrid LineColor="Silver" />
                                                    <MajorTickMark LineColor="Silver" Enabled="False" />
                                                    <LabelStyle Font="Microsoft Sans Serif, 8.25pt" FontColor="64, 64, 64" Format="P" />
                                                </AxisY>
                                                <AxisX LabelsAutoFit="False" LineColor="Silver">
                                                    <MajorGrid Enabled="False" />
                                                    <MajorTickMark LineColor="Silver" />
                                                    <LabelStyle Font="Microsoft Sans Serif, 8.25pt" FontColor="64, 64, 64" />
                                                </AxisX>
                                                <AxisY2 LabelsAutoFit="False" LineColor="Silver" StartFromZero="False">
                                                    <MajorGrid Enabled="False" />
                                                    <MajorTickMark LineColor="DarkGray" Style="inside" LineStyle="Dot" />
                                                    <LabelStyle Font="Microsoft Sans Serif, 8.25pt" FontColor="64, 64, 64" />
                                                </AxisY2>
                                                <Position Height="85" Width="95" X="3" Y="10" />
                                                <InnerPlotPosition Height="84.92647" Width="87" X="8" Y="3.70588" />
                                            </DCWC:ChartArea>
                                        </ChartAreas>
                                    </DCWC:Chart>
                                </div>
                            </div>
                            <!-- bd:end -->
                        </div>
                        <!-- 多空調查指數走勢圖:end -->
                   
            <!-- 多空調查統計圖:end -->
            <!-- 多空調查指數說明:start -->
            <div class="mainboxs inqanote">
                <ul class="hd3">
                    <li class="current"><em>多空調查指數說明</em></li>
                </ul>
                <div class="bd">
                    <p>
                        多空調查指數=看多+盤整+看空之加權平均／總投票數<br />
                        多空調查指數介於+100 ~ -100 之間；數值愈往+100代表看多成份愈高，愈往-100則代表看空成份愈高；數值為0則代表看多與看空相抵，或均投盤整。
                        <asp:Literal ID="BodyWidth" runat="server"></asp:Literal></p>
                </div>
                <!-- bd:end -->
            </div>
            <!-- 多空調查指數說明:end -->
        </div>
    </form>
</body>
</html>
