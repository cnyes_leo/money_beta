﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default_Vote.aspx.cs" Inherits="Vote_Default_Vote" %>

<%@ Register Assembly="DundasWebChart" Namespace="Dundas.Charting.WebControl" TagPrefix="DCWC" %>
<%@ Register Src="~/Vote/uc/uc_vote_Top.ascx" TagName="vote_Top" TagPrefix="ucvote_Top" %>
<%@ Register Src="~/Vote/uc/uc_Button_Image.ascx" TagName="Button_Image" TagPrefix="ucButton_Image" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
   <title>多空調查_外匯_金融中心_鉅亨網</title>
  <link href="uc/inquire.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
   <div id="main2">
        <ucvote_Top:vote_Top ID="vote_Top_Name" runat="server" />    
        <ucButton_Image:Button_Image ID="vote_Button_Image" runat="server" />  
    </div>
    </form>
</body>
</html>
