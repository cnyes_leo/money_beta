﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using System.Runtime.InteropServices;

using System.Text.RegularExpressions;
using System.Collections;
using System.Data.SqlClient;

public partial class Vote_uc_uc_vote_Top : System.Web.UI.UserControl
{

    #region//宣告
    public double p1 = 0;
    public double p2 = 0;
    public double p3 = 0;
    public double p4 = 0;
    VoteClass VC = new VoteClass();
    string ReStr1 = "";
    string ReStr2 = "";
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        ReStr1 = Request.QueryString["channel"];
        ReStr2 = Request.QueryString["type"];

        this.bind_Show_vote_today();

        if (!IsPostBack)
        {
            this.bind();
            try
            {
                string language = Request.ServerVariables["HTTP_REFERER"];
                if (language.IndexOf("cn.cnyes") >= 0)
                {
                    Button1.Text = SmallFuction.Convert_TraditionalChinese_To_SimpleChinese("投票");
                    Button2.Text = SmallFuction.Convert_TraditionalChinese_To_SimpleChinese("投票");
                    Button3.Text = SmallFuction.Convert_TraditionalChinese_To_SimpleChinese("投票");
                }
            }
            catch { } 

        }
    }

    protected void bind_Show_vote_today()
    {
        if (ReStr1 != null && ReStr2 != null)
        {
            if (VC.vote_Check_QueryString(ReStr1) == true )
            {
                #region//當日多空調查累積投票數
                TopOneName.Text = VC.Chennal_Type_Name(ReStr1, ReStr2);

                string now = DateTime.Now.ToShortDateString();

                if (ReStr1 == "us")
                {
                    now = DateTime.Now.AddHours(-12).ToShortDateString();
                }

                SqlParameter[] prams ={  
                                 new SqlParameter("@ReStr1", SqlDbType.VarChar, 10), 
                                 new SqlParameter("@ReStr2", SqlDbType.VarChar, 10),
                                 new SqlParameter("@D1", SqlDbType.VarChar, 10)             
                                      };
                prams[0].Value = ReStr1;
                prams[1].Value = ReStr2;
                prams[2].Value = now;
                DataTable dt1 = General.Procedure_DataReader_Parameters(Conn.Conn_24_News, "Vote_Top1_SqlStr", prams);

                if (dt1.Rows.Count > 0)
                {
                    double t1 = 0;
                    double t2 = 0;
                    double t3 = 0;
                    double allticket = 0;

                    t1 = Convert.ToInt32(dt1.Rows[0]["bull"]);
                    t2 = Convert.ToInt32(dt1.Rows[0]["Consolidation"]);
                    t3 = Convert.ToInt32(dt1.Rows[0]["bear"]);

                    allticket = t1 + t2 + t3;

                    p1 = Math.Round((t1 / allticket) * 100, 1);
                    p2 = Math.Round((t2 / allticket) * 100, 1);
                    p3 = Math.Round((t3 / allticket) * 100, 1);
                    p4 = 100.0;

                    v1.InnerText = Convert.ToString(dt1.Rows[0]["bull"]) + " 票";
                    v2.InnerText = Convert.ToString(dt1.Rows[0]["Consolidation"]) + " 票";
                    v3.InnerText = Convert.ToString(dt1.Rows[0]["bear"]) + " 票";
                    v4.InnerText = Convert.ToString(t1 + t2 + t3) + " 票";

                    tydate.InnerText = now;

                    cResult.InnerText = Convert.ToDouble((Convert.ToInt32(dt1.Rows[0]["bull"]) - Convert.ToInt32(dt1.Rows[0]["bear"])) / allticket * 100).ToString("0");

                }
                #endregion
            }
            else
                Response.Write("抱歉!網址輸入錯誤.");
        }
        else
            Response.Write("抱歉!網址輸入錯誤.");
    }

    protected void bind()
    {
        if (ReStr1 != null && ReStr2 != null)
        {
            if (VC.vote_Check_QueryString(ReStr1) == true )
            {
                VC.vote_top_vote_main(L_menu, L_top_vote, L_month_total,TopOneName, ReStr1, ReStr2);
            }
            else
                Response.Write("抱歉!網址輸入錯誤.");
        }
        else
            Response.Write("抱歉!網址輸入錯誤.");
    }


    protected void Button1_Click(object sender, EventArgs e)
    {
        #region//bull Click
        string strSQL = "";

        //if (Request.UrlReferrer == null || Request.UrlReferrer.AbsoluteUri.IndexOf("cnyes.com", StringComparison.InvariantCultureIgnoreCase) == -1)
        //{
        //    Response.Write("<script>alert('錯誤要求！') </script>");
        //    return;
        //}

        if (ReStr1.ToLower() == "detailvote")
        {

            string CheckTodayHaveVote = VC.CheckTodayHaveVote_exe(ReStr1, ReStr2);
            if (CheckTodayHaveVote == "yes")
            {
                strSQL = VC.Click_Votes_Update_SqlStr(ReStr1, ReStr2, "1", "yes");
            }
            else
            {
                strSQL = VC.Click_Votes_Update_SqlStr(ReStr1, ReStr2, "1", "no");
            }
            General.ExecSQL(Conn.Conn_24_News_Write, strSQL);
            Response.Write("<script>alert('多空調查投票成功！') ;location='" + Request.Url.PathAndQuery + "'</script>");

         
        }
        else
        {

            if (Request.Cookies[VC.Check_Cookies(ReStr1, ReStr2)] == null)
            {
                string CheckTodayHaveVote = VC.CheckTodayHaveVote_exe(ReStr1, ReStr2);
                if (CheckTodayHaveVote == "yes")
                {
                    strSQL = VC.Click_Votes_Update_SqlStr(ReStr1, ReStr2, "1", "yes");
                }
                else
                {
                    strSQL = VC.Click_Votes_Update_SqlStr(ReStr1, ReStr2, "1", "no");
                }

                VC.Give_Cookies_Values(ReStr1, ReStr2);

                General.ExecSQL(Conn.Conn_24_News_Write, strSQL);

                Response.Write("<script>alert('多空調查投票成功！') ;location='" + Request.Url.PathAndQuery + "'</script>");
              

            }
            else
                Response.Write("<script>alert('不得重複投票，每人一天限投一票。');location='" + Request.Url.PathAndQuery + "'</script>");
        }

        #region//Old code
        // //判斷cookies
        //if (Request.Cookies[VC.Check_Cookies(ReStr1, ReStr2)] == null)
        //{
        //    //判斷目前ip今天是否有投過

        //    string Check_ip = VC.CheckToday_Vote_ip(ReStr1, ReStr2);

        //    if (Check_ip == "yes")
        //    {
        //        Response.Write("<script>alert('不得重複投票，每人一天限投一票。') ;location='" + Request.Url.PathAndQuery + "'</script>");
        //    }
        //    else
        //    {
        //        //判斷今天台股是否有投第一票 
        //        string CheckTodayHaveVote = VC.CheckTodayHaveVote_exe(ReStr1, ReStr2);
        //        if (CheckTodayHaveVote == "yes")
        //        {
        //            //存ip
        //            VC.Save_Vote_ip(ReStr1, ReStr2); 

        //            //存channel_vote
        //            strSQL = VC.Click_Votes_Update_SqlStr(ReStr1, ReStr2, "1", "yes");

        //            General.ExecSQL(Conn.Conn_24_News_Write, strSQL);

        //            Response.Write("<script>alert('多空調查投票成功！') ;location='" + Request.Url.PathAndQuery + "'</script>");
        //            this.bind();
        //        }
        //        else
        //        {
        //            //存ip
        //            VC.Save_Vote_ip(ReStr1, ReStr2);

        //            strSQL = VC.Click_Votes_Update_SqlStr(ReStr1, ReStr2, "1", "no");
        //            General.ExecSQL(Conn.Conn_24_News_Write, strSQL);
        //            Response.Write("<script>alert('多空調查投票成功！') ;location='" + Request.Url.PathAndQuery + "'</script>");
        //            this.bind();
        //        }

        //    }
        //}
        //else
        //    Response.Write("<script>alert('不得重複投票，每人一天限投一票。');location='" + Request.Url.PathAndQuery + "'</script>");
        #endregion

        #endregion
    }

    protected void Button2_Click(object sender, EventArgs e)
    {
        #region//Consolidation Click
        string strSQL = "";

        //if (Request.UrlReferrer == null || Request.UrlReferrer.AbsoluteUri.IndexOf("cnyes.com", StringComparison.InvariantCultureIgnoreCase) == -1)
        //{
        //    Response.Write("<script>alert('錯誤要求！') </script>");
        //    return;
        //}

        if (ReStr1.ToLower() == "detailvote")
        {

            string CheckTodayHaveVote = VC.CheckTodayHaveVote_exe(ReStr1, ReStr2);
            if (CheckTodayHaveVote == "yes")
            {
                strSQL = VC.Click_Votes_Update_SqlStr(ReStr1, ReStr2, "2", "yes");
            }
            else
            {
                strSQL = VC.Click_Votes_Update_SqlStr(ReStr1, ReStr2, "2", "no");
            }
            General.ExecSQL(Conn.Conn_24_News_Write, strSQL);
            Response.Write("<script>alert('多空調查投票成功！') ;location='" + Request.Url.PathAndQuery + "'</script>");

        }
        else
        {

            if (Request.Cookies[VC.Check_Cookies(ReStr1, ReStr2)] == null)
            {
                string CheckTodayHaveVote = VC.CheckTodayHaveVote_exe(ReStr1, ReStr2);
                if (CheckTodayHaveVote == "yes")
                {
                    strSQL = VC.Click_Votes_Update_SqlStr(ReStr1, ReStr2, "2", "yes");
                }
                else
                {
                    strSQL = VC.Click_Votes_Update_SqlStr(ReStr1, ReStr2, "2", "no");
                }

                VC.Give_Cookies_Values(ReStr1, ReStr2);

                General.ExecSQL(Conn.Conn_24_News_Write, strSQL);

                Response.Write("<script>alert('多空調查投票成功！') ;location='" + Request.Url.PathAndQuery + "'</script>");
            }
            else
                Response.Write("<script>alert('不得重複投票，每人一天限投一票。');location='" + Request.Url.PathAndQuery + "'</script>");
        }
        #endregion
    }

    protected void Button3_Click(object sender, EventArgs e)
    {
        #region//bear Click
        string strSQL = "";

        //if (Request.UrlReferrer == null || Request.UrlReferrer.AbsoluteUri.IndexOf("cnyes.com", StringComparison.InvariantCultureIgnoreCase) == -1)
        //{
        //    Response.Write("<script>alert('錯誤要求！') </script>");
        //    return;
        //}
        if (ReStr1.ToLower() == "detailvote")
        {

            string CheckTodayHaveVote = VC.CheckTodayHaveVote_exe(ReStr1, ReStr2);
            if (CheckTodayHaveVote == "yes")
            {
                strSQL = VC.Click_Votes_Update_SqlStr(ReStr1, ReStr2, "3", "yes");
            }
            else
            {
                strSQL = VC.Click_Votes_Update_SqlStr(ReStr1, ReStr2, "3", "no");
            }
            General.ExecSQL(Conn.Conn_24_News_Write, strSQL);
            Response.Write("<script>alert('多空調查投票成功！') ;location='" + Request.Url.PathAndQuery + "'</script>");
        }
        else
        {
            if (Request.Cookies[VC.Check_Cookies(ReStr1, ReStr2)] == null)
            {

                string CheckTodayHaveVote = VC.CheckTodayHaveVote_exe(ReStr1, ReStr2);
                if (CheckTodayHaveVote == "yes")
                {
                    strSQL = VC.Click_Votes_Update_SqlStr(ReStr1, ReStr2, "3", "yes");
                }
                else
                {
                    strSQL = VC.Click_Votes_Update_SqlStr(ReStr1, ReStr2, "3", "no");
                }

                VC.Give_Cookies_Values(ReStr1, ReStr2);

                General.ExecSQL(Conn.Conn_24_News_Write, strSQL);

                Response.Write("<script>alert('多空調查投票成功！') ;location='" + Request.Url.PathAndQuery + "'</script>");

                this.bind();
            }
            else
                Response.Write("<script>alert('不得重複投票，每人一天限投一票。');location='" + Request.Url.PathAndQuery + "'</script>");
        }
        #endregion
    }

}
