﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Vote_uc_uc_Button_Image : System.Web.UI.UserControl
{
    VoteClass VC = new VoteClass();
    string ReStr1 = "";
    string ReStr2 = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        button_show_Or();

        ReStr1 = Request.QueryString["channel"];
        ReStr2 = Request.QueryString["type"];

        if (!IsPostBack)
        {
            everychennal_chart_hf1.Value = "One_1M"; 
            everychennal_chart_hf2.Value = "Two_1M";
            try
            {
                this.bind();
            }
            catch { }

            #region//Button_Color
            One_Button_Color();
            One_1M.ForeColor = System.Drawing.Color.Red;
            Two_Button_Color();
            Two_1M.ForeColor = System.Drawing.Color.Red;
            #endregion    

            try
            {
                string language = Request.ServerVariables["HTTP_REFERER"];
                if (language.IndexOf("cn.cnyes") >= 0)
                {

                    c1.Series[0].LegendText = SmallFuction.Convert_TraditionalChinese_To_SimpleChinese(c1.Series[0].LegendText);
                    c1.Series[1].LegendText = SmallFuction.Convert_TraditionalChinese_To_SimpleChinese(c1.Series[1].LegendText);
                    c1.Series[2].LegendText = SmallFuction.Convert_TraditionalChinese_To_SimpleChinese(c1.Series[2].LegendText);

                    c2.Series[0].LegendText = SmallFuction.Convert_TraditionalChinese_To_SimpleChinese(c2.Series[0].LegendText);
                    c2.Series[1].LegendText = SmallFuction.Convert_TraditionalChinese_To_SimpleChinese(c2.Series[1].LegendText);
                    c3.Series[0].LegendText = SmallFuction.Convert_TraditionalChinese_To_SimpleChinese(c3.Series[0].LegendText);
                    c3.Series[1].LegendText = SmallFuction.Convert_TraditionalChinese_To_SimpleChinese(c3.Series[1].LegendText);

                }
            }
            catch { }
        }       
    }

    //protected void Page_LoadComplete(object sender, EventArgs e)
    //{

    //    c2.Series[0].LegendText = vote_Top_Name.Get_Stock_Name.Trim() + "股價";
    //    c3.Series[0].LegendText = vote_Top_Name.Get_Stock_Name.Trim() + "股價漲跌幅";
    //    c2.Series[1].LegendText = vote_Top_Name.Get_Stock_Name.Trim() + "多空調查指數";
    //    c3.Series[1].LegendText = vote_Top_Name.Get_Stock_Name.Trim() + "多空調查指數";

    //}
  
    protected void bind()
    {
        if (ReStr1 != null && ReStr2 != null)
        {            
            if (VC.vote_Check_QueryString(ReStr1) == true )
            {
               
                    string eDate = DateTime.Now.ToShortDateString(); 
                    string sDate = ""; 
                    string sDate2 = ""; 
                 
                    //用此法原因 ajax 也改善不了 另一張圖內容會消失的問題 
                    if (everychennal_chart_hf1.Value == "One_1M") 
                        sDate = DateTime.Now.AddMonths(-1).AddDays(-1).ToShortDateString();
                    else if (everychennal_chart_hf1.Value == "One_3M")
                        sDate = DateTime.Now.AddMonths(-3).AddDays(-1).ToShortDateString();
                    else if (everychennal_chart_hf1.Value == "One_6M")
                        sDate = DateTime.Now.AddMonths(-6).AddDays(-1).ToShortDateString(); 
                    else if (everychennal_chart_hf1.Value == "One_1Y")
                        sDate = DateTime.Now.AddYears(-1).AddDays(-1).ToShortDateString();
                    else
                        sDate = DateTime.Now.AddMonths(-1).AddDays(-1).ToShortDateString();
                    
                    if (everychennal_chart_hf2.Value == "Two_1M") 
                        sDate2 = DateTime.Now.AddMonths(-1).AddDays(-1).ToShortDateString();
                    else if (everychennal_chart_hf2.Value == "Two_3M")
                        sDate2 = DateTime.Now.AddMonths(-3).AddDays(-1).ToShortDateString();
                    else if (everychennal_chart_hf2.Value == "Two_6M")
                        sDate2 = DateTime.Now.AddMonths(-6).AddDays(-1).ToShortDateString();
                    else if (everychennal_chart_hf2.Value == "Two_1Y")
                        sDate2 = DateTime.Now.AddYears(-1).AddDays(-1).ToShortDateString();
                    else
                        sDate2 = DateTime.Now.AddMonths(-1).AddDays(-1).ToShortDateString();
                    
                    VC.Button_Image_Main(c1, c2, c3, ReStr1, ReStr2, sDate, sDate2, eDate,everychennal_chart_hf1 .Value ,everychennal_chart_hf2 .Value  );

                    if (everychennal_chart_hf1.Value == "One_1M")
                    {
                        Dundas_Chart_Class.PointWidth_Change(c3, "Series1", "0.5");
                    }

                    if (ReStr1 == "gl")
                    {
                        BodyWidth.Text = "<script language=\"javascript\">";
                        BodyWidth.Text += "document.getElementById(\"main2\").style.width = \"640\"";
                        BodyWidth.Text += "</script>";
                        c2.Width = 610;
                        c3.Width = 610;
                    }
                    else
                    {
                        BodyWidth.Text = "<script language=\"javascript\">";
                        BodyWidth.Text += "document.getElementById(\"main2\").style.width = \"670\"";
                        BodyWidth.Text += "</script>";   
                    }

                    //c1.Series[0].ToolTip = " 日期 #VALX \n 比率 #VALY{0}% ";
                    //c1.Series[1].ToolTip = " 日期 #VALX \n 比率 #VALY{0}% ";
                    //c1.Series[2].ToolTip = " 日期 #VALX \n 比率 #VALY{0}% ";

                    //c2.Series[0].ToolTip = " 日期 #VALX \n 值 #VALY{0.00} ";
                    //c2.Series[1].ToolTip = " 日期 #VALX \n 值 #VALY{0.00} ";

                    //c3.Series[0].ToolTip = " 日期 #VALX \n 值 #VALY{0.00} ";
                    //c3.Series[1].ToolTip = " 日期 #VALX \n 值 #VALY{0.00} ";

                   // Response.Write(DateTime.Now.ToString("hh:mm:ss")+"test end");
                }
            else
                Response.Write("抱歉!網址輸入錯誤."); 
        }
        else
            Response.Write("抱歉!網址輸入錯誤."); 
    } 

    #region//Button_Color

    protected void One_Button_Color() 
    {
        One_1M.ForeColor = System.Drawing.Color.Black;
        One_3M.ForeColor = System.Drawing.Color.Black;
        One_6M.ForeColor = System.Drawing.Color.Black;
        One_1Y.ForeColor = System.Drawing.Color.Black;
    }

    protected void Two_Button_Color()
    {
        Two_1M.ForeColor = System.Drawing.Color.Black;
        Two_3M.ForeColor = System.Drawing.Color.Black;
        Two_6M.ForeColor = System.Drawing.Color.Black;
        Two_1Y.ForeColor = System.Drawing.Color.Black;
    }

    protected void One_1M_Click(object sender, EventArgs e)
    {
        One_Button_Color();
        One_1M.ForeColor = System.Drawing.Color.Red;
        everychennal_chart_hf1.Value = "One_1M";
        this.bind();

    } 

    protected void One_3M_Click(object sender, EventArgs e)
    {
        One_Button_Color();
        One_3M.ForeColor = System.Drawing.Color.Red;

        everychennal_chart_hf1.Value = "One_3M";
        this.bind();

    }

    protected void One_6M_Click(object sender, EventArgs e)
    {
        One_Button_Color();
        One_6M.ForeColor = System.Drawing.Color.Red;

        everychennal_chart_hf1.Value = "One_6M";
        this.bind();
    }

    protected void One_1Y_Click(object sender, EventArgs e)
    {
        One_Button_Color();
        One_1Y.ForeColor = System.Drawing.Color.Red;

        everychennal_chart_hf1.Value = "One_1Y";
        this.bind();
    }

    protected void Two_1M_Click(object sender, EventArgs e)
    {
        Two_Button_Color();
        Two_1M.ForeColor = System.Drawing.Color.Red;

        everychennal_chart_hf2.Value = "Two_1M";
        this.bind();
    }

    protected void Two_3M_Click(object sender, EventArgs e)
    {
        Two_Button_Color();
        Two_3M.ForeColor = System.Drawing.Color.Red;

        everychennal_chart_hf2.Value = "Two_3M";
        this.bind();

    }

    protected void Two_6M_Click(object sender, EventArgs e)
    {
        Two_Button_Color();
        Two_6M.ForeColor = System.Drawing.Color.Red;

        everychennal_chart_hf2.Value = "Two_6M";
        this.bind();

    }

    protected void Two_1Y_Click(object sender, EventArgs e)
    {
        Two_Button_Color();
        Two_1Y.ForeColor = System.Drawing.Color.Red;

        everychennal_chart_hf2.Value = "Two_1Y";
        this.bind();
    }

    protected void button_show_Or()
    {
        DateTime now = DateTime.Now;
        DateTime general = Convert.ToDateTime("2009/12/06");

        TimeSpan ts1 = now.Subtract(general);

        //One_3M.Enabled = false;
        //One_6M.Enabled = false;
        //One_1Y.Enabled = false;
        //Two_3M.Enabled = false;
        //Two_6M.Enabled = false;
        //Two_1Y.Enabled = false;

        //if (ts1.Days > 90)
        //{
        //    One_1M.Enabled = true;
        //    Two_3M.Enabled = true;
        //}

        //if (ts1.Days > 180)
        //{
        //    One_6M.Enabled = true;
        //    Two_6M.Enabled = true;
        //}

        //if (ts1.Days > 360)
        //{
        //    One_1Y.Enabled = true;
        //    Two_1Y.Enabled = true;
        //}
    }

    #endregion

}
