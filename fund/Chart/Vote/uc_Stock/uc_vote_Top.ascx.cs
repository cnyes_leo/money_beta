﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using System.Runtime.InteropServices;

using System.Text.RegularExpressions;
using System.Collections;
using System.Data.SqlClient;
using cnYES.AicMember.aspx;
using System.Web.UI.WebControls;

using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;


public partial class Vote_uc_Stock_uc_vote_Top : System.Web.UI.UserControl
{
    //<%@ Import Namespace="cnYES.AicMember.aspx" %>
    #region//宣告
    public double p1 = 0;
    public double p2 = 0;
    public double p3 = 0;
    public double p4 = 0;
    Vote_Stock_Class VC = new Vote_Stock_Class();
    string ReStr1 = "";
    string ReStr2 = "";
    CPMgr aic = new CPMgr();
    string cn="123";
   
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        //ReStr1 = Request.QueryString["channel"];
        ReStr2 = Request.QueryString["code"];
        ReStr1 ="tw";
       //ReStr2 = "2330";
  

        ReStr1 = SqlSave.HtmlEncode(ReStr1);
        ReStr2 = SqlSave.HtmlEncode(ReStr2);

        string type = Request.QueryString["type"];
        if (type != null)
        {
            // Response.Write(type+"test");
            try
            {
                // Response.Write(type + "test");
                if (Convert.ToInt32(type) == 11)
                {
                    Button_Disable();
                }
            }
            catch { }
        }

       // aic.CheckLoginV2("http://www.cnyes.com/_mem_bin/FormsLogin.asp", Request.Url.AbsoluteUri.ToString());
       // CAuo aic2 = new CAuo();
       // aic2.Who_Am_I();
       // cn = aic2.I.cn; 
       // cn = "santun"; 
          
        //什麼事件後也要顯示結果
        this.bind_Show_vote_today();

        if (!IsPostBack) 
        {
            this.bind();
          //  HttpContext.Current.Items["Literal"] = TopOneName.Text;
        }
    }

    public string Get_Stock_Name
    {
        set
        {
            TopOneName.Text = value;
        }
        get
        {
            return TopOneName.Text;
        
        
        }

    }

    protected void bind_Show_vote_today()
    {
        if (ReStr1 != null && ReStr2 != null)
        {
                #region//當日多空調查累積投票數
                TopOneName.Text = VC.Chennal_Type_Name(ReStr1, ReStr2);
                  
                string now = DateTime.Now.ToShortDateString();
                SqlParameter[] prams ={  
                                 new SqlParameter("@ReStr1", SqlDbType.VarChar, 3), 
                                 new SqlParameter("@ReStr2", SqlDbType.VarChar, 10),
                                 new SqlParameter("@D1", SqlDbType.VarChar, 10)             
                                      };
                prams[0].Value = ReStr1;
                prams[1].Value = ReStr2;
                prams[2].Value = now; 
                DataTable dt1 = General.Procedure_DataReader_Parameters(Conn.Conn_24_News, "Vote_Top1_Stock_SqlStr", prams);
                
                if (dt1.Rows.Count > 0)
                {
                    double t1 = 0;
                    double t2 = 0;
                    double t3 = 0;
                    double allticket = 0;

                    t1 = Convert.ToInt32(dt1.Rows[0]["bull"]);
                    t2 = Convert.ToInt32(dt1.Rows[0]["Consolidation"]);
                    t3 = Convert.ToInt32(dt1.Rows[0]["bear"]);

                    allticket = t1 + t2 + t3;

                    p1 = Math.Round((t1 / allticket) * 100, 1);
                    p2 = Math.Round((t2 / allticket) * 100, 1);
                    p3 = Math.Round((t3 / allticket) * 100, 1);
                    p4 = 100.0;

                    v1.InnerText = Convert.ToString(dt1.Rows[0]["bull"]) + " 票";
                    v2.InnerText = Convert.ToString(dt1.Rows[0]["Consolidation"]) + " 票";
                    v3.InnerText = Convert.ToString(dt1.Rows[0]["bear"]) + " 票";
                    v4.InnerText = Convert.ToString(t1 + t2 + t3) + " 票";

                    tydate.InnerText = DateTime.Now.ToShortDateString();

                    cResult.InnerText = Convert.ToDouble((Convert.ToInt32(dt1.Rows[0]["bull"]) - Convert.ToInt32(dt1.Rows[0]["bear"])) / allticket * 100).ToString("0");
               
                }
                #endregion
           
        }
        else
            Response.Write("抱歉!網址輸入錯誤.");
    }

    protected void bind()
    {
        if (ReStr1 != null && ReStr2 != null)
        {
            VC.vote_top_vote_main(L_menu, L_top_vote, L_month_total, TopOneName, ReStr1, ReStr2); 
        }
        else
            Response.Write("抱歉!網址輸入錯誤.");
    }

    protected void Button_Disable()
    {
     //   Button1.Enabled = false;
      //  Button2.Enabled = false;
      //  Button3.Enabled = false;
    }


    protected void Button1_Click(object sender, EventArgs e)
    {
        //#region//bull Click                       
       
        //    string strSQL = "";
        //    string CheckTodayHaveVote = VC.CheckTodayHaveVote_exe(ReStr1 ,ReStr2,cn);
        // //   Response.Write(CheckTodayHaveVote);
        //    if (CheckTodayHaveVote == "yes")
        //    {
        //        Response.Write("<script>alert('不得重複投票！') ;location='" + Request.Url.PathAndQuery + "'</script>");
        //    }
        //    else
        //    {
        //        VC.Click_Votes_Update_Insert_SqlStr(ReStr1, ReStr2, "1", cn);
        //        Response.Write("<script>alert('多空調查投票成功！') ;location='" + Request.Url.PathAndQuery + "'</script>");
        //    }

        //#endregion
        string strSQL = "";
        VC.Click_Votes_Update_Insert_SqlStr(ReStr1, ReStr2, "1", cn);
       // Response.Write("<script>alert('多空調查投票成功！');location='" + Request.Url.PathAndQuery + "'</script>");
        Response.Write("<script>alert('多空調查投票成功！');location='" + Request.Url.PathAndQuery + "'</script>");

        //Response.Write("<script>alert('多空調查投票成功！')'</script>");
        //bind_Show_vote_today();
        //Button_Disable();
    }

    protected void Button2_Click(object sender, EventArgs e)
    {
     //#region// Click

     //   string strSQL = "";
     //   string CheckTodayHaveVote = VC.CheckTodayHaveVote_exe(ReStr1, ReStr2, cn);
     //   if (CheckTodayHaveVote == "yes")
     //   {
     //       Response.Write("<script>alert('不得重複投票！') ;location='" + Request.Url.PathAndQuery + "'</script>");
     //   }
     //   else
     //   {
     //       VC.Click_Votes_Update_Insert_SqlStr(ReStr1, ReStr2, "2", cn);
     //       Response.Write("<script>alert('多空調查投票成功！') ;location='" + Request.Url.PathAndQuery + "'</script>");
     //   }


     // #endregion
        string strSQL = "";
        VC.Click_Votes_Update_Insert_SqlStr(ReStr1, ReStr2, "2", cn);
        //Response.Write("<script>alert('多空調查投票成功！');location='" + Request.Url.PathAndQuery + "'</script>");
        Response.Write("<script>alert('多空調查投票成功！');location='" + Request.Url.PathAndQuery + "'</script>");

        //Response.Write("<script>alert('多空調查投票成功！')'</script>");
        //bind_Show_vote_today();
        //Button_Disable();
    }

    protected void Button3_Click(object sender, EventArgs e)
    {
        //#region// Click

        //string strSQL = "";
        //string CheckTodayHaveVote = VC.CheckTodayHaveVote_exe(ReStr1, ReStr2, cn);
        //if (CheckTodayHaveVote == "yes")
        //{
        //    Response.Write("<script>alert('不得重複投票！') ;location='" + Request.Url.PathAndQuery + "'</script>");
        //} 
        //else 
        //{ 
        //    VC.Click_Votes_Update_Insert_SqlStr(ReStr1, ReStr2, "3", cn);
        //    Response.Write("<script>alert('多空調查投票成功！') ;location='" + Request.Url.PathAndQuery + "'</script>");
        //}


        //#endregion
        string strSQL = "";
        VC.Click_Votes_Update_Insert_SqlStr(ReStr1, ReStr2, "3", cn);
        //Response.Write("<script>alert('多空調查投票成功！')'</script>");
       // bind_Show_vote_today();
       // Button_Disable();
       Response.Write("<script>alert('多空調查投票成功！');location='" + Request.Url.PathAndQuery + "'</script>");
       // bind_Show_vote_today();        // Response.Write("<script>location='" + Request.Url.PathAndQuery + "'</script>");
        //}
      //  Button_Disable();
    }

}
