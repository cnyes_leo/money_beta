﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Dundas.Charting .WebControl ;

public partial class Vote_uc_Stock_aa : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
           bind();
        }

    }

    protected void bind()
    {
        // Initialize an array of doubles
        double[] yval = { 2, 6, 4, 5, 3 };

        // Initialize an array of strings
        string[] xval = { "Peter", "Andrew", "Julie", "Mary", "Dave" };

        // Bind the double array to the Y axis points of the Default data series
        Chart1.Series["Series1"].Points.DataBindXY(xval, yval);

    }

    public Chart aaa
    {
        set
        {
            Chart1  = value;
        }
        get
        {
            return Chart1;


        }
    }

}
