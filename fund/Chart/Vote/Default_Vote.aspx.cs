﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Vote_Default_Vote : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
       
    }

    protected void Page_PreRenderComplete(object sender, EventArgs e)
    {
        try
        {
            string language = Request.ServerVariables["HTTP_REFERER"];
            if (language.IndexOf("cn.cnyes") >= 0)
            {

                string rawUrl = Request.RawUrl.ToLower();
                if (rawUrl.IndexOf(".axd") == -1)
                {

                    this.Response.Buffer = false;
                    this.Response.Filter = new transGB.ResponseFilter(Response.Filter);
                }
            }
        }
        catch { }
    }


}
