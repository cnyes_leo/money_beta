﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using System.Collections;
using System.Data.SqlClient;
using cnYES.AicMember.aspx;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;


public partial class Vote_small_vote : System.Web.UI.Page
{
    #region//宣告
    public double p1 = 0;
    public double p2 = 0;
    public double p3 = 0;
    public double p4 = 0;
    Vote_Stock_Class VC = new Vote_Stock_Class();
    string ReStr1 = "";
    string ReStr2 = "";
    CPMgr aic = new CPMgr();
    string cn = "";

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        ReStr2 = Request.QueryString["code"];
        ReStr1 = "tw";
        if (ReStr2 != null)
        {
            ReStr1 = SqlSave.HtmlEncode(ReStr1);
            ReStr2 = SqlSave.HtmlEncode(ReStr2);
        }

        if (!IsPostBack)
        {
            try
            {
                this.bind_Show_vote_today();
            }
            catch { }
        }
    }

    protected void bind_Show_vote_today()
    {
        if (ReStr1 != null && ReStr2 != null)
        {
            #region//today vote
          
            L2.Text = VC.Chennal_Type_Name(ReStr1, ReStr2);

            string now = DateTime.Now.ToShortDateString();
            SqlParameter[] prams ={  
                                 new SqlParameter("@ReStr1", SqlDbType.VarChar, 3), 
                                 new SqlParameter("@ReStr2", SqlDbType.VarChar, 10),
                                 new SqlParameter("@D1", SqlDbType.VarChar, 10)             
                                      };
            prams[0].Value = ReStr1;
            prams[1].Value = ReStr2;
            prams[2].Value = now;
            DataTable dt1 = General.Procedure_DataReader_Parameters(Conn.Conn_24_News, "Vote_Top1_Stock_SqlStr", prams);

            if (dt1.Rows.Count > 0)
            {
                double t1 = 0;
                double t2 = 0;
                double t3 = 0;
                double allticket = 0;

                t1 = Convert.ToInt32(dt1.Rows[0]["bull"]);
                t2 = Convert.ToInt32(dt1.Rows[0]["Consolidation"]);
                t3 = Convert.ToInt32(dt1.Rows[0]["bear"]);

                allticket = t1 + t2 + t3;

                p1 = Math.Round((t1 / allticket) * 100, 1);
                p2 = Math.Round((t2 / allticket) * 100, 1);
                p3 = Math.Round((t3 / allticket) * 100, 1);
                p4 = 100.0;


                cResult.InnerText = Convert.ToDouble((Convert.ToInt32(dt1.Rows[0]["bull"]) - Convert.ToInt32(dt1.Rows[0]["bear"])) / allticket * 100).ToString("0");

            }
            #endregion
        }
        else
            Response.Write("抱歉!網址輸入錯誤.");
    }

    #region//Button_Event
    protected void Button_Event(string type1)
    {
        //http://fund.cnyes.com/chart/Vote/small_vote.aspx?code=6601&CheckPresh=presh

        VC.Click_Votes_Update_Insert_SqlStr(ReStr1, ReStr2, type1, cn);

        string CheckPresh = Request.QueryString["CheckPresh"];

        if (CheckPresh != null)
        {
            CheckPresh = CheckPresh.ToLower();
            if (CheckPresh != "presh")
                Response.Write("<script>alert('多空調查投票成功！') ;window.open('" + "http://www.cnyes.com/twstock/vote_p.asp?code=" + ReStr2 + "')</script>");
            else
                Response.Write("<script>alert('多空調查投票成功！') ;window.open('" + "http://www.cnyes.com/presh/vote_p.aspx?code=" + ReStr2 + "')</script>");
        }
        else
            Response.Write("<script>alert('多空調查投票成功！') ;window.open('" + "http://www.cnyes.com/twstock/vote_p.asp?code=" + ReStr2 + "')</script>");

        Response.Write("<script>location='" + Request.Url.PathAndQuery + "'</script>");
    }
    #endregion

    protected void Button1_Click(object sender, EventArgs e)
    {
        #region//bull Click
        Button_Event("1");
        #endregion
    }

    protected void Button2_Click(object sender, EventArgs e)
    {
        #region// Click
        Button_Event("2");
        #endregion
    }

    protected void Button3_Click(object sender, EventArgs e)
    {
        #region// Click
        Button_Event("3"); 
        #endregion
    }
}
