﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text.RegularExpressions;
using System.IO;
using System.Net;
using System;
using System.Collections;


public partial class Test_GetHttpFile_GetHttpFile : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
      bool ISTure= GetHttpFile("http://afund.cnyes.com/cn/", Server.MapPath("~/Test/GetHttpFile.htm"));
      if (ISTure == true)
      {
          Response.Write("Write is Success!");       
      }
    }

    public  bool GetHttpFile(string sUrl, string sSavePath)
    {
        bool bRslt = false;
        WebResponse oWebRps = null;
        WebRequest oWebRqst = WebRequest.Create(sUrl);
        oWebRqst.Timeout = 100000;
        try
        {
            oWebRps = oWebRqst.GetResponse();
        }
        catch (WebException e)
        {
            //sException = e.Message.ToString();
        }
        finally
        {
            if (oWebRps != null)
            {
                BinaryReader oBnyRd = new BinaryReader(oWebRps.GetResponseStream(), System.Text.Encoding.Default);
                int iLen = Convert.ToInt32(oWebRps.ContentLength);
                FileStream oFileStream;
                try
                {
                    if (File.Exists(System.Web.HttpContext.Current.Request.MapPath("RecievedData.tmp")))
                    {
                        oFileStream = File.OpenWrite(sSavePath);
                    }
                    else
                    {
                        oFileStream = File.Create(sSavePath);
                    }
                    oFileStream.SetLength((Int64)iLen);
                    oFileStream.Write(oBnyRd.ReadBytes(iLen), 0, iLen);
                    oFileStream.Close();
                }
                catch (Exception ex)
                {
                    bRslt= false; 
                }
                finally
                {
                    oBnyRd.Close();
                    oWebRps.Close();

                }
                bRslt = true;

            }
        }
        return bRslt;

    }
      
 
}
