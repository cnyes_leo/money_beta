﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Dundas.Charting.WebControl;
using System.Drawing;

public partial class Test_Stecked_Chart : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        // Hookup to the chart click event. 
        this.Chart1.Click += new ImageClickEventHandler(Chart1_Click);


        // Initialize an array of doubles
        double[] yval1 = { 3, 6, 4, 5, 3 }; 
        string[] xval1 = { "Peter", "Andrew", "Julie", "Mary", "Dave" }; 
      
        double[] yval2 = { -3, 7, 8, 6, 5 };
        string[] xval2 = { "Peter", "Andrew", "Julie", "Mary", "Dave" };
       
        double[] yval3 = { -3, 8, 7, 7, 1 };
        string[] xval3 = { "Peter", "Andrew", "Julie", "Mary", "Dave" };

        // Bind the double array to the Y axis points of the Default data series
        Chart1.Series["Series1"].Points.DataBindXY(xval1, yval1);
        Chart1.Series["Series2"].Points.DataBindXY(xval2, yval2);
        Chart1.Series["Series3"].Points.DataBindXY(xval3, yval3);
         

        // Set chart type
       // Chart1.Series["Series1"].Type = SeriesChartType.StackedArea100;

        // Show point labels
       // Chart1.Series["Series1"].ShowLabelAsValue = true;

        // Disable X axis margin
        Chart1.ChartAreas["Default"].AxisX.Margin = false; 

        // Enable 3D
      //  Chart1.ChartAreas["Default"].Area3DStyle.Enable3D = true;

        // Set the first two series to be grouped into Group1
        Chart1.Series["Series1"]["StackedGroupName"] = "Group1";
    
        // Set the last two series to be grouped into Group2
        Chart1.Series["Series2"]["StackedGroupName"] = "Group1"; 
        Chart1.Series["Series3"]["StackedGroupName"] = "Group1"; 



    }
    protected void Chart1_Click(object sender, ImageClickEventArgs e)
    {
        // Using the coordinates of the Click event determine which
        // chart element was clicked on.
        HitTestResult hitTestResult = this.Chart1.HitTest(e.X, e.Y);
        if (hitTestResult != null)
        {
         int aaa=   hitTestResult.PointIndex;
         aaa = aaa;
            // Update chart title with the name of the last clicked chart element
         //   this.Chart1.Titles["ClickedElement"].Text = "Last Clicked Element: " + hitTestResult.ChartElementType.ToString();
        }

    }
}
