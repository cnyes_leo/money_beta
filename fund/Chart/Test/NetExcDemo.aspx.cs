﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Net;


public partial class Test_NetExcDemo : System.Web.UI.Page
{ 
    protected void Page_Load(object sender, EventArgs e)
    {
        string content = String.Empty;
        try
        {
            System.Net.HttpWebRequest request = (System.Net.HttpWebRequest)System.Net.WebRequest.Create("http://www.cnyes.com/twstock/");
            request.UserAgent = "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2; .NET CLR 1.1.4322; .NET CLR 2.0.50727; InfoPath.1) Web-Sniffer/1.0.24";
            WebResponse response = request.GetResponse();
            Stream resStream = response.GetResponseStream();
            StreamReader sr = new StreamReader(resStream, System.Text.Encoding.Default);
            content = sr.ReadToEnd();
            resStream.Close();
            resStream = null;
            sr.Close();
            sr = null;
        }
        catch
        {
          //  return "Request error....";
        }
       // return content;
        Response.Write(content);
 
    }
}
