﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Stecked_Chart.aspx.cs" Inherits="Test_Stecked_Chart" %>

<%@ Register Assembly="DundasWebChart" Namespace="Dundas.Charting.WebControl" TagPrefix="DCWC" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>未命名頁面</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <DCWC:Chart ID="Chart1" runat="server" OnClick="Chart1_Click">
            <Legends>
                <DCWC:Legend Name="Default">
                </DCWC:Legend>
            </Legends>
            <Series>
                <DCWC:Series ChartType="StackedColumn" Name="Series1">
                </DCWC:Series>
                <DCWC:Series ChartType="StackedColumn" Name="Series2">
                </DCWC:Series>
                <DCWC:Series ChartType="StackedColumn" Name="Series3">
                </DCWC:Series>
            </Series>
            <ChartAreas>
                <DCWC:ChartArea Name="Default">
                </DCWC:ChartArea>
            </ChartAreas>
        </DCWC:Chart>
        &nbsp;</div>
    </form>
</body>
</html>
