﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="FS_Compare.aspx.cs" Inherits="stock_FSID_FS_Compare" %>

<%@ Register Assembly="DundasWebChart" Namespace="Dundas.Charting.WebControl" TagPrefix="DCWC" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="../stock/StockCompareChart/StockC_SS.css" rel="stylesheet"  type="text/css" />  
</head>
<body >
    <form id="form1" runat="server"> 
<div id="container">
       <DCWC:Chart ID="c1" runat="server" Height="200px" Width="350px" BackColor="#F2F2F2" Visible="False">
            <Legends>
                <DCWC:Legend Alignment="Center" BackColor="242, 242, 242" Docking="Top" FontColor="64, 64, 64"
                    Name="Default" AutoFitText="False">
                    <Position Height="11" Width="100" Y="2" />
                </DCWC:Legend>
            </Legends>
            <Titles>             
                <DCWC:Title Color="DarkGray" Font="Microsoft Sans Serif, 8.8pt" Name="Title2" Text="&#169;">
                    <Position Height="6" Width="10" X="62" Y="18" />
                </DCWC:Title>
                <DCWC:Title Color="DarkGray" Name="Title3" Text="cnYES鉅亨網" Font="Microsoft Sans Serif, 7.5pt">
                    <Position Height="7" Width="25" X="65.5" Y="17" />
                </DCWC:Title>
            </Titles>
            <Series>
                <DCWC:Series ChartType="Line" Name="Series1" Color="224, 52, 51" YAxisType="Secondary">
                </DCWC:Series>
                <DCWC:Series ChartType="Line" Name="Series2" Color="143, 194, 106" YAxisType="Secondary">
                </DCWC:Series>
                <DCWC:Series ChartType="Line" Color="51, 127, 229" Name="Series3" YAxisType="Secondary">
                </DCWC:Series>
            </Series>
            <ChartAreas>
                <DCWC:ChartArea Name="Default" BorderColor="Silver" BorderStyle="Solid">
                    <AxisX LabelsAutoFit="False" LineColor="Silver">
                        <MajorGrid Enabled="False" />
                        <MajorTickMark LineColor="Silver" Size="2" />
                        <LabelStyle Font="Microsoft Sans Serif, 7.5pt" FontColor="64, 64, 64" Format="d" />
                    </AxisX>
                    <AxisY LabelsAutoFit="False" LineColor="Silver">
                        <MajorGrid LineColor="Gray" Enabled="False" />
                        <MajorTickMark LineColor="Silver" Enabled="False" />
                        <LabelStyle Font="Microsoft Sans Serif, 7.5pt" FontColor="Transparent" Format="P0" />
                    </AxisY>
                    <Position Height="82" Width="94" X="3" Y="12" />
                    <InnerPlotPosition Height="85" Width="86.54972" X="6" Y="5.55882" />
                    <AxisY2 LabelsAutoFit="False" LineColor="Silver" StartFromZero="False">
                        <MajorGrid LineColor="Silver" Enabled="False" />
                        <MajorTickMark Enabled="False" />
                        <LabelStyle Font="Microsoft Sans Serif, 8.25pt" FontColor="64, 64, 64" Format="P0"
                            ShowEndLabels="False" />
                    </AxisY2>
                </DCWC:ChartArea>
            </ChartAreas>
        </DCWC:Chart>
        <asp:Literal ID="L1" runat="server"></asp:Literal>
</div>
    </form>
</body> 
</html>
