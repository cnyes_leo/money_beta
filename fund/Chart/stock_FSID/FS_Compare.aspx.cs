﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class stock_FSID_FS_Compare : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            this.bind();
        }
    }


    protected void bind()
    {
        // Response.Write(Convert.ToDateTime(DateTime.Now).ToString("yyyy/MM/dd hh:mm:ss"));

       string code = Request.QueryString["code"];
        //string code = "03000";//CITP沒名   IPIXQ 沒資料
        if (code != null)
        {
           Stock_Other.FS_StockCompare_Main(Page, c1, L1, code); 
        }
              
        // Response.Write("<br/>");    
        // Response.Write(Convert.ToDateTime(DateTime.Now).ToString("yyyy/MM/dd hh:mm:ss"));   
             
    }  
} 
