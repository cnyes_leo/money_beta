﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class stock_usETF_Read : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Show.Text = SmallFuction.StreamReader_File(HttpContext.Current.Server.MapPath("~/stock_usETF/File/" + "usETF" + ".htm"), System.Text.Encoding.GetEncoding("utf-8"));
           
        }
    }
}
