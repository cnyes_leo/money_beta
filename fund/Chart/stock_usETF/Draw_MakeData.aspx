﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Draw_MakeData.aspx.cs" Inherits="stock_usETF_Draw_MakeData" %>
<%@ Register Assembly="DundasWebChart" Namespace="Dundas.Charting.WebControl" TagPrefix="DCWC" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>未命名頁面</title>
</head>
<body>
    <form id="form1" runat="server">
    <DCWC:Chart ID="c1" runat="server" BackColor="#F2F2F2" Height="200px" Visible="False"
            Width="350px">
            <Legends>
                <DCWC:Legend Alignment="Center" AutoFitText="False" BackColor="242, 242, 242" Docking="Top" FontColor="64, 64, 64" Name="Default" Font="Microsoft Sans Serif, 7.5pt">
                    <Position Height="11" Width="100" Y="2" />
                </DCWC:Legend>
            </Legends>
            <Titles>
                <DCWC:Title Color="DarkGray" Font="Microsoft Sans Serif, 8.8pt" Name="Title2" Text="&#169;">
                    <Position Height="6" Width="10" X="64" Y="18" />
                </DCWC:Title>
                <DCWC:Title Color="DarkGray" Font="Microsoft Sans Serif, 7.5pt" Name="Title3" Text="cnYES鉅亨網">
                    <Position Height="7" Width="25" X="67.5" Y="17" />
                </DCWC:Title>
            </Titles>
            <Series>
                <DCWC:Series ChartType="Line" Color="224, 52, 51" Name="Series1" YAxisType="Secondary">
                </DCWC:Series>
                <DCWC:Series ChartType="Line" Color="143, 194, 106" Name="Series2" YAxisType="Secondary">
                </DCWC:Series>
                <DCWC:Series ChartType="Line" Color="51, 127, 229" Name="Series3" YAxisType="Secondary">
                </DCWC:Series>
            </Series>
            <ChartAreas>
                <DCWC:ChartArea BorderColor="Silver" BorderStyle="Solid" Name="Default">
                    <AxisY LabelsAutoFit="False" LineColor="Silver">
                        <MajorGrid Enabled="False" LineColor="Gray" />
                        <MajorTickMark Enabled="False" LineColor="Silver" />
                        <LabelStyle Font="Microsoft Sans Serif, 7.5pt" FontColor="Transparent" Format="P0" />
                    </AxisY>
                    <AxisX LabelsAutoFit="False" LineColor="Silver">
                        <MajorGrid Enabled="False" />
                        <MajorTickMark LineColor="Silver" Size="2" />
                        <LabelStyle Font="Microsoft Sans Serif, 7.5pt" FontColor="64, 64, 64" Format="d" />
                    </AxisX>
                    <AxisY2 LabelsAutoFit="False" LineColor="Silver" StartFromZero="False">
                        <MajorGrid LineColor="Silver" Enabled="False" />
                        <MajorTickMark Enabled="False" />
                        <LabelStyle Font="Microsoft Sans Serif, 8.25pt" FontColor="64, 64, 64" Format="P0"
                            ShowEndLabels="False" />
                    </AxisY2>
                    <Position Height="82" Width="94" X="2" Y="12" />
                    <InnerPlotPosition Height="85" Width="86.54972" X="7" Y="5.55882" />
                </DCWC:ChartArea>
            </ChartAreas>
        </DCWC:Chart>
    </form>
</body>
</html>
