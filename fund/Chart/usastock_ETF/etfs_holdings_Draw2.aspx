<%@ Page Language="C#" AutoEventWireup="true" CodeFile="etfs_holdings_Draw2.aspx.cs" Inherits="etfs_holdings_Draw2" %>

<%@ Register Assembly="DundasWebChart" Namespace="Dundas.Charting.WebControl" TagPrefix="DCWC" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>未命名頁面</title>
    <link href="http://www.cnyes.com/twstock/css/public2010.css" rel="stylesheet" type="text/css" />
    <link href="http://www.cnyes.com/twstock/css/warrant.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
        <div class="mbx performance">
            <ul class="hd">
                <li class="current"><a href="#">個股與指數績效表現</a></li>
            </ul>
            <cite class="tydate"><%Response.Write(get_date); %></cite>
            <div class="bd">
                <div class="perfchart">
                    <DCWC:Chart ID="Chart1" runat="server" Palette="Pastel">
                        <Legends>
                            <DCWC:Legend Name="Default">
                            </DCWC:Legend>
                        </Legends>
                        <Series>
                            <DCWC:Series Name="Series1" ShadowOffset="1" BorderColor="64, 64, 64">
                            </DCWC:Series>
                            <DCWC:Series Name="Series2" ShadowOffset="1" BorderColor="64, 64, 64">
                            </DCWC:Series>
                        </Series>
                        <ChartAreas>
                            <DCWC:ChartArea Name="Default">
                            </DCWC:ChartArea>
                        </ChartAreas>
                    </DCWC:Chart>
                </div>
                <div class="tab">
                    <table>
                        <tr class="thbgbr">
                            <th class="lt">
                                名稱</th>
                            <th class="rt">
                                <asp:Literal ID="Literal1" runat="server"></asp:Literal></th>
                            <th class="rt">
                                <asp:Literal ID="Literal2" runat="server"></asp:Literal></th>
                        </tr>
                        <asp:Literal ID="Literal3" runat="server"></asp:Literal>
                    </table>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
