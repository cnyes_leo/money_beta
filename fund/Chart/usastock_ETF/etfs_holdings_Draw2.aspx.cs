﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Dundas.Charting.WebControl;
using System.Drawing;
using System.Globalization;

public partial class etfs_holdings_Draw2 : System.Web.UI.Page
{
    #region 美股ETF圖片
    public string get_date = DateTime.Now.ToString("yyyy-M-d");
    public string code;
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = "美股ETF圖片";
        string area = "",code = "",holdType="";
        if (Request.QueryString["code"] != null)
        {
            code = ForCheck.HtmlEncode(Request.QueryString["code"].ToString());
            if (code.IndexOf("-") != -1 )
            {
            		string[] words = code.Split('-');
            		area=words[0];
            		code=words[1];
            }		
            else
            {
            		area="US";
            		code=code;
          	}	
        }
        else
        { Response.Write("無參數值"); form1.Visible = false; return; }
        get_data(area, code);
        
    }

    #region 權証首頁圖片 資料來源
    private void get_data(string area, string code)
    {
        try
        {
            ArrayList x_ser = new ArrayList();
            ArrayList y_ser = new ArrayList();
            ArrayList y_ser1 = new ArrayList();
            ArrayList c1 = new ArrayList();
            ArrayList c2 = new ArrayList();
            string code_name="";
            ////取得ETF收盤價
            string sql = "EXEC Get_TwoYear_Value '" + code + "','" + DateTime.Now.AddYears(-1).ToString("yyyyMMdd") + "','" + DateTime.Now.ToString("yyyyMMdd") + "'";
            DataTable Dtable = ObjectDBuse.SqlPro_DataReader(ConfigurationManager.ConnectionStrings["Conn_122_Global"].ToString(), sql);
            if (Dtable.Rows.Count == 0) { Response.Write("無此資料或產生錯誤"); form1.Visible = false; return; }
            double diff;
            foreach (DataRow row in Dtable.Rows)
            {
                diff = double.Parse(row["PClose"].ToString()) - double.Parse(Dtable.Rows[Dtable.Rows.Count - 1]["PClose"].ToString());
                x_ser.Add(row["PDate"].ToString().Substring(0, 4) + "/" + int.Parse(row["PDate"].ToString().Substring(4,2)) + "/" + int.Parse(row["PDate"].ToString().Substring(6,2)));
                y_ser.Add(diff / double.Parse(Dtable.Rows[Dtable.Rows.Count - 1]["PClose"].ToString()) * 100); 
            
            }
            //取得ETF績效
            sql = "SELECT CASE glcname WHEN '' THEN glname ELSE glcname END AS 'glcname' , perfor_ty as ny , perfor_1m as '1m' , perfor_3m as '3m' " + 
                  ", perfor_6m as '6m', perfor_1y as '1y' , perfor_3y as '3y' , perfor_5y as '5y' " +
                  "FROM ETF_PERFORMANCE where glcode = '"+code+"'";
            Dtable = ObjectDBuse.SqlPro_DataReader(ConfigurationManager.ConnectionStrings["Conn_24_Global"].ToString(), sql);
            if (Dtable.Rows.Count == 0) { Response.Write("無此資料或產生錯誤"); form1.Visible = false; return; }
            foreach (DataRow row in Dtable.Rows)
            {
                code_name = row["glcname"].ToString();
                c1.Add(row["ny"].ToString());
                c1.Add(row["1m"].ToString());
                c1.Add(row["3m"].ToString());
                c1.Add(row["6m"].ToString());
                c1.Add(row["1y"].ToString());
                c1.Add(row["3y"].ToString());
                c1.Add(row["5y"].ToString());
            }
            //取得 美國s&p 500 收盤
            sql = "EXEC Get_TwoYear_Value 'spf','" + DateTime.Now.AddYears(-1).ToString("yyyyMMdd") + "','" + DateTime.Now.ToString("yyyyMMdd") + "'";
            Dtable = ObjectDBuse.SqlPro_DataReader(ConfigurationManager.ConnectionStrings["Conn_122_Global"].ToString(), sql);
            if (Dtable.Rows.Count == 0)
            { return; }
            foreach (DataRow row in Dtable.Rows)
            {
                diff = double.Parse(row["PClose"].ToString()) - double.Parse(Dtable.Rows[Dtable.Rows.Count - 1]["PClose"].ToString());
                y_ser1.Add( diff/ double.Parse(Dtable.Rows[Dtable.Rows.Count - 1]["PClose"].ToString()) * 100);
            }

            //取得s&p 500今年初來有資料的那天
            string N_close;
            sql = "select min(PDate) FROM FGPrice where substring(PDate,0,5 ) + '01' = '"+DateTime.Now.Year.ToString() +"01"+"' and   PCode='spf' and PArea='US' ";
            Dtable = ObjectDBuse.SqlPro_DataReader(ConfigurationManager.ConnectionStrings["Conn_122_Global"].ToString(), sql);
            if (Dtable.Rows.Count == 0)
            { return; }
            N_close = Dtable.Rows[0][0].ToString();
            //取得 計算美國s&p 500 績效
            sql = "EXEC Get_TwoYear_Value 'spf','" + DateTime.Now.AddYears(-2).ToString("yyyyMMdd") + "','" + DateTime.Now.ToString("yyyyMMdd") + "'";
            Dtable = ObjectDBuse.SqlPro_DataReader(ConfigurationManager.ConnectionStrings["Conn_122_Global"].ToString(), sql);
            if (Dtable.Rows.Count == 0)
            { return; }
            string V1="", V2="", V3="", V4="", V5="", V6="", V7="";
            int count = 0;
            foreach (DataRow row in Dtable.Rows)
            {
                //今年以來
                if (row["PDate"].ToString() == N_close)
                {
                    diff = double.Parse(Dtable.Rows[0]["PClose"].ToString()) - double.Parse(row["PClose"].ToString());
                    V1 = Convert.ToDouble(diff / double.Parse(row["PClose"].ToString()) * 100).ToString("F2", System.Globalization.NumberFormatInfo.CurrentInfo );
                    c2.Add(V1);
                }
                //最近一周
                if (count == 0)
                {
                    if (Convert.ToDateTime(row["PDate"].ToString().Substring(0, 4) + "/" + row["PDate"].ToString().Substring(4, 2) + "/" + row["PDate"].ToString().Substring(6, 2)) <= DateTime.Now.AddDays(-7))
                    {
                        diff = double.Parse(Dtable.Rows[0]["PClose"].ToString()) - double.Parse(row["PClose"].ToString());
                        V2= Convert.ToDouble(diff / double.Parse(row["PClose"].ToString()) * 100).ToString("F2", System.Globalization.NumberFormatInfo.CurrentInfo);
                        c2.Add(V2);
                        count++;
                    }                  
                }
                //最近一個月
                if (count == 1)
                {
                    if (Convert.ToDateTime(row["PDate"].ToString().Substring(0, 4) + "/" + row["PDate"].ToString().Substring(4, 2) + "/" + row["PDate"].ToString().Substring(6, 2)) <= DateTime.Now.AddMonths(-1))
                    {
                        diff = double.Parse(Dtable.Rows[0]["PClose"].ToString()) - double.Parse(row["PClose"].ToString());
                        V3 = Convert.ToDouble(diff / double.Parse(row["PClose"].ToString()) * 100).ToString("F2", System.Globalization.NumberFormatInfo.CurrentInfo);
                        c2.Add(V3);
                        count++;
                    }
                }
                //最近三個月
                if (count == 2)
                {
                    if (Convert.ToDateTime(row["PDate"].ToString().Substring(0, 4) + "/" + row["PDate"].ToString().Substring(4, 2) + "/" + row["PDate"].ToString().Substring(6, 2)) <= DateTime.Now.AddMonths(-3))
                    {
                        diff = double.Parse(Dtable.Rows[0]["PClose"].ToString()) - double.Parse(row["PClose"].ToString());
                        V4 = Convert.ToDouble(diff / double.Parse(row["PClose"].ToString()) * 100).ToString("F2", System.Globalization.NumberFormatInfo.CurrentInfo);
                        c2.Add(V4);
                        count++;
                    }
                }
                //最近六個月
                if (count == 3)
                {
                    if (Convert.ToDateTime(row["PDate"].ToString().Substring(0, 4) + "/" + row["PDate"].ToString().Substring(4, 2) + "/" + row["PDate"].ToString().Substring(6, 2)) <= DateTime.Now.AddMonths(-6))
                    {
                        diff = double.Parse(Dtable.Rows[0]["PClose"].ToString()) - double.Parse(row["PClose"].ToString());
                        V5 = Convert.ToDouble(diff / double.Parse(row["PClose"].ToString()) * 100).ToString("F2", System.Globalization.NumberFormatInfo.CurrentInfo);
                        c2.Add(V5);
                        count++;
                    }
                }
                //最近一年
                if (count == 4)
                {
                    if (Convert.ToDateTime(row["PDate"].ToString().Substring(0, 4) + "/" + row["PDate"].ToString().Substring(4, 2) + "/" + row["PDate"].ToString().Substring(6, 2)) <= DateTime.Now.AddYears(-1))
                    {
                        diff = double.Parse(Dtable.Rows[0]["PClose"].ToString()) - double.Parse(row["PClose"].ToString());
                        V6 = Convert.ToDouble(diff / double.Parse(row["PClose"].ToString()) * 100).ToString("F2", System.Globalization.NumberFormatInfo.CurrentInfo);
                        c2.Add(V6);
                        count++;
                    }
                }
                //最近二年
                if (count == 5)
                {
                    if (Convert.ToDateTime(row["PDate"].ToString().Substring(0, 4) + "/" + row["PDate"].ToString().Substring(4, 2) + "/" + row["PDate"].ToString().Substring(6, 2)) <= Convert.ToDateTime(Dtable.Rows[Dtable.Rows.Count-1]["PDate"].ToString().Substring(0, 4) + "/" + Dtable.Rows[Dtable.Rows.Count-1]["PDate"].ToString().Substring(4, 2) + "/" + Dtable.Rows[Dtable.Rows.Count-1]["PDate"].ToString().Substring(6, 2)))
                    {
                        diff = double.Parse(Dtable.Rows[0]["PClose"].ToString()) - double.Parse(row["PClose"].ToString());
                        V7 = Convert.ToDouble(diff / double.Parse(row["PClose"].ToString()) * 100).ToString("F2", System.Globalization.NumberFormatInfo.CurrentInfo);
                        c2.Add(V7);
                        count++;
                    }
                }
            }
            
            string strtxt = "";
            string[] topic = { "今年以來", "最近一週", "最近一個月", "最近三個月", "最近六個月", "最近一年", "最近二年", };
            for (int i = 0; i < topic.Length; i++)
            {
                strtxt += "<tr>";
                strtxt += "<td>";
                strtxt += topic[i] + "</td>";
                if (c1[i].ToString() != string.Empty)
                {
                    if (double.Parse(c1[i].ToString()) > 0)
                    {
                        strtxt += "<td class='rt r'>";
                    }
                    else if (double.Parse(c1[i].ToString()) < 0)
                    {
                        strtxt += "<td class='rt g'>";
                    }
                    else
                    {
                        strtxt += "<td class='rt'>";
                    }
                    strtxt += c1[i] + "%</td>";
                }
                else
                {
                    strtxt += "<td class='rt'>";
                    strtxt += c1[i] + "</td>";
                }
            

                if (c2[i].ToString() != string.Empty)
                {
                    if (double.Parse(c2[i].ToString()) > 0)
                    {
                        strtxt += "<td class='rt r'>";
                    }
                    else if (double.Parse(c2[i].ToString()) < 0)
                    {
                        strtxt += "<td class='rt g'>";
                    }
                    else
                    {
                        strtxt += "<td class='rt'>";
                    }
                    strtxt += c2[i] + "%</td>";
                }
                else
                {
                    strtxt += "<td class='rt'>";
                    strtxt += c2[i] + "</td>";
                }
              

            }
            //判斷中英文
            char [] cName = code_name.ToCharArray();
            if (cName[0] > 128 || code_name.Length > 8)
            { code_name = code_name.Substring(0, 8) + "..."; }
            else if (code_name.Length > 15)
           {code_name = code_name.Substring(0,15) + "..."; }
           
            Literal1.Text = code_name;
            Literal2.Text = "S&P500指數";
            Literal3.Text = strtxt;
            x_ser.Reverse();
            y_ser.Reverse();
            y_ser1.Reverse();
            int cou;
            //計算節點數量
            if (y_ser1.Count > x_ser.Count)
            {
                cou = y_ser1.Count - x_ser.Count;
                for (int i = 0; i < cou; i++)
                {
                    y_ser1.RemoveAt(0);
                }
            }
            else
            {
                for (int i = 0; i < x_ser.Count - y_ser1.Count; i++)
                {
                    x_ser.RemoveAt(0);
                    y_ser.RemoveAt(0);
                }
            }


            Chart1.Series["Series1"].Points.DataBindXY(x_ser, y_ser);
            Chart1.Series["Series2"].Points.DataBindXY(x_ser, y_ser1);
            Option_Chart(code_name , x_ser.Count);
        }
        catch
        { Response.Write("圖片無法產生！"); }

    }
    #endregion
    #region 權証首頁圖片 元件屬性設定
    private void Option_Chart(string code_name, int AxisCount)
    {
        try
        {
            //設定title
            Chart1.Titles.Add("Title1");
            Chart1.Titles["Title1"].Text = "© cnYES鉅亨網";
            Chart1.Titles["Title1"].DockToChartArea = "Default";
            Chart1.Titles["Title1"].Font = new Font("Microsoft Sans Serif", float.Parse("7.5"));
            Chart1.Titles["Title1"].Color = Color.Gray;
            Chart1.Titles["Title1"].BackColor = Color.Transparent;
            Chart1.Titles["Title1"].Position.Height = 10;
            Chart1.Titles["Title1"].Position.Width = 50;
            Chart1.Titles["Title1"].Position.X = 48;
            Chart1.Titles["Title1"].Position.Y = 13;
            //設定數列
            Chart1.Series["Series1"].Type = SeriesChartType.Line;
            Chart1.Series["Series2"].Type = SeriesChartType.Line;
            Chart1.Series["Series1"].XValueIndexed = true;
            Chart1.Series["Series1"].YAxisType = AxisType.Secondary;
            Chart1.Series["Series2"].YAxisType = AxisType.Secondary;
            Chart1.Series["Series1"].Color = System.Drawing.Color.FromArgb(223, 66, 65);
            Chart1.Series["Series2"].Color = System.Drawing.Color.FromArgb(149, 198, 115);
            Chart1.Series["Series1"].LegendText = code_name;
            Chart1.Series["Series2"].LegendText = "S&P500指數";
            Chart1.Series["Series1"].EmptyPointStyle.Color = System.Drawing.Color.Transparent;
            Chart1.Series["Series2"].EmptyPointStyle.Color = System.Drawing.Color.Transparent;
            //設定Legends
            Chart1.Legends["Default"].Docking = LegendDocking.Top;
            Chart1.Legends["Default"].LegendStyle = LegendStyle.Table;
            Chart1.Legends["Default"].Alignment = StringAlignment.Center;
            Chart1.Legends["Default"].Position.X = 0;
            Chart1.Legends["Default"].Position.Y = 1;
            Chart1.Legends["Default"].Position.Height = 10;
            Chart1.Legends["Default"].Position.Width = 100;
            Chart1.Legends["Default"].Font = new Font("Microsoft Sans Serif", float.Parse("8"));
            Chart1.Legends["Default"].BackColor = Color.Transparent;
            Chart1.Legends["Default"].AutoFitText = false;
            //Chart1.Legends["Default"].Enabled = false;
            //設定元件
            Chart1.Width = 350;
            Chart1.Height = 200;
            Chart1.BackColor = System.Drawing.Color.FromArgb(240, 240, 240);
            //設定區域
            Chart1.ChartAreas["Default"].BorderStyle = ChartDashStyle.Solid;
            Chart1.ChartAreas["Default"].BorderColor = System.Drawing.Color.Black;
            Chart1.ChartAreas["Default"].BorderWidth = 1;
            Chart1.ChartAreas["Default"].Position.X = 6;
            Chart1.ChartAreas["Default"].Position.Y = 6;
            Chart1.ChartAreas["Default"].Position.Width = 90;
            Chart1.ChartAreas["Default"].Position.Height = 88;
            Chart1.ChartAreas["Default"].BorderColor = System.Drawing.Color.LightGray;
            Chart1.ChartAreas["Default"].InnerPlotPosition.Height = 80;
            Chart1.ChartAreas["Default"].InnerPlotPosition.Width = 85;
            Chart1.ChartAreas["Default"].InnerPlotPosition.X = 3;
            Chart1.ChartAreas["Default"].InnerPlotPosition.Y = 9;
            //設定X軸
            Chart1.ChartAreas["Default"].AxisX.MajorGrid.Enabled = false;
            Chart1.ChartAreas["Default"].AxisX.LineColor = System.Drawing.Color.LightGray;
            Chart1.ChartAreas["Default"].AxisX.Interval = AxisCount / 4;
            Chart1.ChartAreas["Default"].AxisX.LabelsAutoFit = false;
            Chart1.ChartAreas["Default"].AxisX.MajorTickMark.LineColor = System.Drawing.Color.LightGray;
            Chart1.ChartAreas["Default"].AxisX.LabelStyle.Font = new Font("Microsoft Sans Serif", float.Parse("7.5"));
            //設定Y軸
            Chart1.ChartAreas["Default"].AxisY.LabelsAutoFit = false;
            Chart1.ChartAreas["Default"].AxisY.MajorGrid.Enabled = true;
            Chart1.ChartAreas["Default"].AxisY.MajorGrid.LineStyle = ChartDashStyle.Dot;
            Chart1.ChartAreas["Default"].AxisY.MajorGrid.LineColor = System.Drawing.Color.LightGray;
            Chart1.ChartAreas["Default"].AxisY.LineColor = System.Drawing.Color.LightGray;
            Chart1.ChartAreas["Default"].AxisY2.LineColor = System.Drawing.Color.LightGray;
            Chart1.ChartAreas["Default"].AxisY2.LabelsAutoFit = false;
            Chart1.ChartAreas["Default"].AxisY2.MajorGrid.Enabled = true;
            Chart1.ChartAreas["Default"].AxisY2.MajorGrid.LineStyle = ChartDashStyle.Dot;
            Chart1.ChartAreas["Default"].AxisY2.MajorGrid.LineColor = System.Drawing.Color.LightGray;
            Chart1.ChartAreas["Default"].AxisY.StartFromZero = false;
            Chart1.ChartAreas["Default"].AxisY2.StartFromZero = false;
            Chart1.ChartAreas["Default"].AxisY.LabelStyle.Format = "P1";
            Chart1.ChartAreas["Default"].AxisY.LabelStyle.FontColor = Color.FromArgb(0, 0, 0);
            Chart1.ChartAreas["Default"].AxisY2.LabelStyle.FontColor = Color.FromArgb(0, 0, 0);
            Chart1.ChartAreas["Default"].AxisY2.LabelStyle.Format = "P1";
            Chart1.ChartAreas["Default"].AxisY.Interval = 0;
            Chart1.ChartAreas["Default"].AxisY.MajorTickMark.LineColor = System.Drawing.Color.Gray;
            Chart1.ChartAreas["Default"].AxisY2.MajorTickMark.LineColor = System.Drawing.Color.Gray;
            Chart1.ChartAreas["Default"].AxisY.LabelStyle.Font = new Font("Microsoft Sans Serif", float.Parse("7.5"));
            Chart1.ChartAreas["Default"].AxisY2.LabelStyle.Font = new Font("Microsoft Sans Serif", float.Parse("7.5"));
        }
        catch
        { Response.Write("圖片無法產生！"); }
    }
      #endregion

    #endregion

}
