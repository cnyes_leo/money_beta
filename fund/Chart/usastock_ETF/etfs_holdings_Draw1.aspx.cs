﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Drawing;
using Dundas.Charting.WebControl;

public partial class etfs_holdings_Draw1 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        Page.Title = "美股ETF圖片";
        string area = "",code = "",holdType="";
        if (Request.QueryString["code"] != null)
        {
            code = ForCheck.HtmlEncode(Request.QueryString["code"].ToString());
            if (code.IndexOf("-") != -1 )
            {
            		string[] words = code.Split('-');
            		area=words[0];
            		code=words[1];
            }		
            else
            {
            		area="US";
            		code=code;
          	}	
          }
        if (Request.QueryString["holdType"] != null)
        {
            holdType = ForCheck.HtmlEncode(Request.QueryString["holdType"].ToString());
        }

        draw(area, code, holdType);
    }
    private void draw(string area, string code, string holdType)
        {
        try
        {
            string Title_Name;
            ArrayList xser = new ArrayList();
            ArrayList yser = new ArrayList();
            string get_Newdate = "SELECT p2.haspname,p.hasp_sname,(p.haspp*100) AS 'val' " +
                                 "FROM g_etfhasp p " +
                                 "LEFT JOIN g_etfhasp_basic p2 ON p.hasp=p2.hasp " +
                                 "WHERE  p.glarea='" + area + "'  AND p.gt='etf' AND (p.hasid<11) AND p.glcode='" + code + "' and p.hasp='" + holdType + "' " +
                                 "ORDER BY p.hasp ASC,p.hasid ASC;";
            DataTable Dt_Newdate = ObjectDBuse.SqlPro_DataReader(ConfigurationManager.ConnectionStrings["Conn_24_Global"].ToString(), get_Newdate);
            if (Dt_Newdate.Rows.Count == 0)
            { Response.Write("此代碼無任何資料!!"); Chart1.Visible = false; return; }
            Title_Name = Dt_Newdate.Rows[0]["haspname"].ToString();
            foreach (DataRow row in Dt_Newdate.Rows)
            {
                xser.Add(row["hasp_sname"].ToString());
                yser.Add(row["val"].ToString());
            }
            xser.Reverse();
            yser.Reverse();

            Chart1.Series["Series1"].Points.DataBindXY(xser, yser);
            Chart1.Series["Series1"].LabelFormat = "P";
            //數列的顏色
            //Chart1.Series["Series1"].Color = System.Drawing.Color.FromArgb(198, 217, 241);
            int R, G, B;
            for (int i = 0; i < Dt_Newdate.Rows.Count; i++)
            {
                Random Ran = new Random(Guid.NewGuid().GetHashCode());
                R = Ran.Next(180, 255);
                G = Ran.Next(180, 255);
                B = Ran.Next(180, 255);
                
                Chart1.Series["Series1"].Points[i].Color = System.Drawing.Color.FromArgb(R,G,B);
                Chart1.Series["Series1"].Points[i].BorderColor = System.Drawing.Color.FromArgb(R,G,B);
            }
            //Chart1.Series["Series1"].BackGradientType = GradientType.LeftRight;
           
            //計算數列寬度
            int pointwidth = xser.Count / 5 + 2;
            if (pointwidth < 10)
            {
                Chart1.Series["Series1"].CustomAttributes = "PointWidth=0." + pointwidth + "";
            }
            else
            {
                Chart1.Series["Series1"].CustomAttributes = "PointWidth=1." + pointwidth + "";
            }
            //開啟Y軸Line
            Chart1.ChartAreas["Default"].AxisX.MajorGrid.Enabled = true;
            Chart1.ChartAreas["Default"].AxisX.MajorGrid.Interval = 100;

            //設定背景顏色
            //Chart1.Legends["Default"].BackColor = System.Drawing.Color.FromArgb(240, 240, 240);
            Chart1.Legends["Default"].BackColor = System.Drawing.Color.FromArgb(0, 0, 0);
            //元件設定長寬
            Chart1.Width = 600;
            Chart1.Height = 230;
            Chart1.Legends[0].Enabled = false;
            //Chart1.ChartAreas["Default"].AxisX.LabelStyle.Format = "Call";
            Chart1.Series["Series1"].ShowLabelAsValue = true;
            Chart1.ChartAreas["Default"].AxisX.MajorTickMark.Style = TickMarkStyle.None;
            Chart1.ChartAreas["Default"].Position.X = 5;
            Chart1.ChartAreas["Default"].Position.Y = 0;
            Chart1.ChartAreas["Default"].Position.Width = 95;
            Chart1.ChartAreas["Default"].Position.Height = 100;
            Chart1.ChartAreas["Default"].InnerPlotPosition.Height = 90;
            Chart1.ChartAreas["Default"].InnerPlotPosition.Width = 80;
            Chart1.ChartAreas["Default"].InnerPlotPosition.X = 25;
            Chart1.ChartAreas["Default"].InnerPlotPosition.Y = 0;
            Chart1.ChartAreas["Default"].AxisX.Interval = 1;
            Chart1.ChartAreas["Default"].AxisX.LineColor = Color.Gray;
            Chart1.ChartAreas["Default"].AxisX.LabelStyle.Font = new Font("Microsoft Sans Serif", float.Parse("9"));
            Chart1.ChartAreas["Default"].AxisX.MajorGrid.LineColor = Color.LightGray;
            Chart1.ChartAreas["Default"].AxisX.MajorGrid.LineStyle = ChartDashStyle.Dot;
            Chart1.ChartAreas["Default"].AxisX.MajorTickMark.Style = TickMarkStyle.Outside;
            Chart1.ChartAreas["Default"].AxisX.MajorTickMark.LineColor = Color.LightGray;
            Chart1.ChartAreas["Default"].AxisY.Enabled = AxisEnabled.True;
            Chart1.ChartAreas["Default"].AxisY.MajorGrid.LineColor = Color.LightGray;
            Chart1.ChartAreas["Default"].AxisY.MajorGrid.LineStyle = ChartDashStyle.Dot;
            Chart1.ChartAreas["Default"].AxisY.LineColor = Color.LightGray;
            Chart1.ChartAreas["Default"].AxisY.LineStyle = ChartDashStyle.Dot;
            Chart1.ChartAreas["Default"].AxisY.MajorTickMark.Style = TickMarkStyle.None;
            Chart1.ChartAreas["Default"].AxisY.LabelStyle.Format = "P0";
        }
        catch
        { }
    }
}
