//理財規劃
function isInt(elm){
    if(elm.length ==0) return false ;
    var fResult=true ; 
  for(var i=0;i<elm.length;i++){
    if ((elm.charAt(i)<"0" || elm.charAt(i)>"9" )&&  elm.charAt(i)!=".")
      fResult = false;
  }
  return fResult ;
}
function chkratestr(thisCHK){
	if (parseFloat(thisCHK.value)<0 || parseFloat(thisCHK.value)>20) {
		alert('年利率輸入範圍為0~20。');
			thisCHK.focus();
			return false;
	}
	return true;
}

function Calc(kind)
{
    var myform = document.countform;
    //單筆投資 期末金額 = 總投資金額 × (1+投資年報酬率)投資年數
      switch(kind)
        {
        case '1': //總投資金額
							myform.val1.value = Math.ceil(myform.val4.value/(Math.pow(1+myform.val3.value/100,myform.val2.value)));
	            break;
        case '2':   //  投資年數
							myform.val2.value = Math.ceil(Math.log(myform.val4.value/myform.val1.value) / Math.log(1+myform.val3.value/100));	
		          break;
        case '3':  //年報酬率
		        	var insrate;
		        	var sum_temp;
					    var rate_month = 10;	
					    var rate_half = 0.5;
					    for(count=1; count <101 ; count++) {
				      	sum_temp = Math.ceil(myform.val1.value * (Math.pow(1+rate_month/100,myform.val2.value)));
								if ( sum_temp == myform.val4.value) break;
								if ( sum_temp > myform.val4.value) rate_month = rate_month - rate_half;  
								else  rate_month = rate_month + rate_half;
					    }
						  myform.val3.value=Math.round(rate_month*100)/100;
							if ( Math.abs(sum_temp-myform.val4.value)/sum_temp > 0.1) myform.val3.value="不在-40~60%間";
	            break;
        case '4':  //期末金額
						  myform.val4.value = Math.ceil(myform.val1.value * (Math.pow(1+myform.val3.value/100,myform.val2.value)));
	            break;
		       }
		       return;
}
function Change1()
{
   var myform = document.countform;
var checkField = new Array('val2','val3','val4');
	myform.val1.readOnly=true;
	myform.val2.readOnly=false;
	myform.val3.readOnly=false;
	myform.val4.readOnly=false;
	myform.val1.className="tabBlueBlack";
	myform.val2.className="texts";
	myform.val3.className="texts";
	myform.val4.className="texts";
	fieldNum = checkField.length;
	for (i=0;i<fieldNum;i++){
		objID = checkField[i];
		objValue = document.getElementById(objID).value;
			if (!isInt(objValue)) {return;}
	}
 	Calc('1');
	return;
}
function Change2()
{
   var myform = document.countform;
var checkField = new Array('val1','val3','val4');
	myform.val1.readOnly=false;
	myform.val2.readOnly=true;
	myform.val3.readOnly=false;
	myform.val4.readOnly=false;
	myform.val1.className="texts";
	myform.val2.className="tabBlueBlack";
	myform.val3.className="texts";
	myform.val4.className="texts";
	fieldNum = checkField.length;
	for (i=0;i<fieldNum;i++){
		objID = checkField[i];
		objValue = document.getElementById(objID).value;
			if (!isInt(objValue)) {return;}
	}

 	Calc('2');
	return;
}
function Change3()
{
   var myform = document.countform;
var checkField = new Array('val1','val2','val4');
	myform.val1.readOnly=false;
	myform.val2.readOnly=false;
	myform.val3.readOnly=true;
	myform.val4.readOnly=false;
	myform.val1.className="texts";
	myform.val2.className="texts";
	myform.val3.className="tabBlueBlack";
	myform.val4.className="texts";
	fieldNum = checkField.length;
	for (i=0;i<fieldNum;i++){
		objID = checkField[i];
		objValue = document.getElementById(objID).value;
			if (!isInt(objValue)) {return;}
	}
 	Calc('3');
	return;
}
function Change4()
{

   var myform = document.countform;
var checkField = new Array('val1','val2','val3');
	myform.val1.readOnly=false;
	myform.val2.readOnly=false;
	myform.val3.readOnly=false;
	myform.val4.readOnly=true;
	myform.val1.className="texts";
	myform.val2.className="texts";
	myform.val3.className="texts";
	myform.val4.className="tabBlueBlack";
	fieldNum = checkField.length;
	for (i=0;i<fieldNum;i++){
		objID = checkField[i];
		objValue = document.getElementById(objID).value;
			if (!isInt(objValue)) {return;}
	}
 	Calc('4');
	return;
}
function CheckData()
{
   var myform = document.countform;
	var kind=0;
		for(i = 0; i < 4; i++) {
		if(myform.kind[i].checked) {
			kind= myform.kind[i].value;
			break;
		}
		}
	if( kind==0)	{alert('未選擇試算方法！！'); return false ;}
	      switch(kind)
        {
        case '1': //每年投資金額
		    var checkField = new Array('val2','val3','val4');
	            var checkFieldDesc = new Array('請正確的投資期間!','請輸入正確的年報酬率!','請輸入正確的期末金額!');
	            break;
        case '2':   //  投資年數
		    var checkField = new Array('val1','val3','val4');
	            var checkFieldDesc = new Array('請輸入正確的每年投資金額!','請輸入正確的年報酬率!','請輸入正確的期末金額!');
		            break;
        case '3':   //  年報酬率 
		    var checkField = new Array('val1','val2','val4');
	            var checkFieldDesc = new Array('請輸入正確的每年投資金額!','請輸入正確的投資期間!','請輸入正確的期末金額!');
		            break;
        case '4':  //期末金額
		    var checkField = new Array('val1','val2','val3');
	            var checkFieldDesc = new Array('請輸入正確的每年投資金額!','請輸入正確的投資期間!','請輸入正確的年報酬率!');
	            break;
		       }
	fieldNum = checkField.length;

	for (i=0;i<fieldNum;i++){
		objID = checkField[i];
		objDesc = checkFieldDesc[i];
		objValue = document.getElementById(objID).value;
			if (!isInt(objValue)) {alert(objDesc); document.getElementById(objID).focus(); return false ;}
	}

 	Calc(kind);
	return;
}
