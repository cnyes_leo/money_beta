// 頁籤切換效果
function switchTab(tab_title, pst, tab_cnt)
{
	for(var i=0 ; i<tab_cnt ; i++)
	{
		if (i == pst)
			document.getElementById(tab_title+'_tab_'+i).className = 'current';
		else
			document.getElementById(tab_title+'_tab_'+i).className = '';
		if (i == pst)
			document.getElementById(tab_title+'_'+i).className = 'disyshow';
		else
			document.getElementById(tab_title+'_'+i).className = 'disyhidn';
	}
}

$(document).ready(function() {
							   
	//表格間隔顏色
	$('.fmTb tr:nth-child(2n)').addClass('odd');	
	
	//IE6 hover效果
	$("#navs-h li").hover(
    function() { $(this).addClass("iehover"); },
    function() { $(this).removeClass("iehover"); }
  	);
	
	//左右輪動特效
	$("#browsable").scrollable({items:".items",item:"ul",size:1,next:".next1",prev:".prev1"});
	$("#browsable2").scrollable({items:".items",item:"div",size:4}).navigator();	
	
	//動態頁籤
	$("ul.hdNavs").tabs("div.Bx", {event:'mouseover',tabs:'li'});
	$("ul.hdNavs2").tabs("div.Bx2", {event:'mouseover',tabs:'li'});
	
	});
