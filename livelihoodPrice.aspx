﻿<%@ Page Language="C#" MasterPageFile="~/MPs/MP1.master" EnableViewStateMac="true"
    AutoEventWireup="true" CodeFile="livelihoodPrice.aspx.cs" Inherits="livelihoodPrice" %>

<%@ OutputCache Duration="60" VaryByParam="*" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <form id="form1" runat="server">
    <script src="js/jquery.blockUI.js" type="text/javascript"></script>
    <div id="container">
        <div id="main">
            <div class="hdPathL">
                <h3>
                    民生物資</h3>
            </div>
            <div class="bdBk">
                <div class="fmTb">
                    <div class="liveBox">
                        <cite class="mrelt4">∥ 食物類</cite>
                        <ul class="hdNavs wdpd">
                            <li><a href="javascript:;" title="食用油">食用油</a></li>
                            <li><a href="javascript:;" title="肉類">肉類</a></li>
                            <li><a href="javascript:;" title="蛋類">蛋類</a></li>
                            <li><a href="javascript:;" title="水果">水果</a></li>
                            <li><a href="javascript:;" title="蔬菜">蔬菜</a></li>
                            <li><a href="javascript:;" title="水產">水產</a></li>
                        </ul>
                        <!-- hdNavs wdpd:end -->
                        <span class="dataInfo mRt">
                            <asp:Literal ID="Literal19" runat="server"></asp:Literal>
                        </span>
                        <div class="toolBx Bx">
                            <div class="liveChart">
                                <img src="Chartsimg/livelihoodPrice/food_EdibleOil.png" width="355" height="229"
                                    alt="食用油" />
                            </div>
                            <!-- liveChart:end -->
                            <div class="liveTb">
                                <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>
                                                日期
                                            </th>
                                            <th>
                                                指數
                                            </th>
                                            <th>
                                                日期
                                            </th>
                                            <th>
                                                指數
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <asp:Literal ID="Literal1" runat="server"></asp:Literal>
                                    </tbody>
                                </table>
                            </div>
                            <!-- liveTb:end -->
                        </div>
                        <!-- 食用油_toolBx:end -->
                        <div class="toolBx Bx">
                            <div class="liveChart">
                                <img src="Chartsimg/livelihoodPrice/food_Meat.png" width="355" height="229" alt="肉類" />
                            </div>
                            <!-- liveChart:end -->
                            <div class="liveTb">
                                <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>
                                                日期
                                            </th>
                                            <th>
                                                指數
                                            </th>
                                            <th>
                                                日期
                                            </th>
                                            <th>
                                                指數
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <asp:Literal ID="Literal2" runat="server"></asp:Literal>
                                    </tbody>
                                </table>
                            </div>
                            <!-- liveTb:end -->
                        </div>
                        <!-- 肉類_toolBx:end -->
                        <div class="toolBx Bx">
                            <div class="liveChart">
                                <img src="Chartsimg/livelihoodPrice/food_Eggs.png" width="355" height="229" alt="蛋類" />
                            </div>
                            <!-- liveChart:end -->
                            <div class="liveTb">
                                <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>
                                                日期
                                            </th>
                                            <th>
                                                指數
                                            </th>
                                            <th>
                                                日期
                                            </th>
                                            <th>
                                                指數
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <asp:Literal ID="Literal3" runat="server"></asp:Literal>
                                    </tbody>
                                </table>
                            </div>
                            <!-- liveTb:end -->
                        </div>
                        <!-- 蛋類_toolBx:end -->
                        <div class="toolBx Bx">
                            <div class="liveChart">
                                <img src="Chartsimg/livelihoodPrice/food_Fruit.png" width="355" height="229" alt="水果" />
                            </div>
                            <!-- liveChart:end -->
                            <div class="liveTb">
                                <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>
                                                日期
                                            </th>
                                            <th>
                                                指數
                                            </th>
                                            <th>
                                                日期
                                            </th>
                                            <th>
                                                指數
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <asp:Literal ID="Literal4" runat="server"></asp:Literal>
                                    </tbody>
                                </table>
                            </div>
                            <!-- liveTb:end -->
                        </div>
                        <!-- 水果_toolBx:end -->
                        <div class="toolBx Bx">
                            <div class="liveChart">
                                <img src="Chartsimg/livelihoodPrice/food_Vegetables.png" width="355" height="229"
                                    alt="蔬菜" />
                            </div>
                            <!-- liveChart:end -->
                            <div class="liveTb">
                                <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>
                                                日期
                                            </th>
                                            <th>
                                                指數
                                            </th>
                                            <th>
                                                日期
                                            </th>
                                            <th>
                                                指數
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <asp:Literal ID="Literal5" runat="server"></asp:Literal>
                                    </tbody>
                                </table>
                            </div>
                            <!-- liveTb:end -->
                        </div>
                        <!-- 蔬菜_toolBx:end -->
                        <div class="toolBx Bx">
                            <div class="liveChart">
                                <img src="Chartsimg/livelihoodPrice/food_Fisheries.png" width="355" height="229"
                                    alt="水產" />
                            </div>
                            <!-- liveChart:end -->
                            <div class="liveTb">
                                <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>
                                                日期
                                            </th>
                                            <th>
                                                指數
                                            </th>
                                            <th>
                                                日期
                                            </th>
                                            <th>
                                                指數
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <asp:Literal ID="Literal6" runat="server"></asp:Literal>
                                    </tbody>
                                </table>
                            </div>
                            <!-- liveTb:end -->
                        </div>
                        <!-- 水產_toolBx:end -->
                    </div>
                    <!-- 食物類_liveBox:end -->
                    <div class="liveBox">
                        <cite class="mrelt4">∥ 交通類</cite>
                        <ul class="hdNavs wdpd">
                            <li><a href="javascript:;" title="油料費">油料費</a></li>
                            <li><a href="javascript:;" title="交通服務及維修零件">交通服務及維修零件</a></li>
                        </ul>
                        <!-- hdNavs wdpd:end -->
                        <span class="dataInfo mRt">
                            <asp:Literal ID="Literal20" runat="server"></asp:Literal>
                        </span>
                        <div class="toolBx Bx">
                            <div class="liveChart">
                                <img src="Chartsimg/livelihoodPrice/traffic_FuelFee.png" width="355" height="229"
                                    alt="油料費" />
                            </div>
                            <!-- liveChart:end -->
                            <div class="liveTb">
                                <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>
                                                日期
                                            </th>
                                            <th>
                                                指數
                                            </th>
                                            <th>
                                                日期
                                            </th>
                                            <th>
                                                指數
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <asp:Literal ID="Literal7" runat="server"></asp:Literal>
                                    </tbody>
                                </table>
                            </div>
                            <!-- liveTb:end -->
                        </div>
                        <!-- 油料費_toolBx:end -->
                        <div class="toolBx Bx">
                            <div class="liveChart">
                                <img src="Chartsimg/livelihoodPrice/traffic_ServiceParts.png" width="355" height="229"
                                    alt="交通服務及維修零件" />
                            </div>
                            <!-- liveChart:end -->
                            <div class="liveTb">
                                <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>
                                                日期
                                            </th>
                                            <th>
                                                指數
                                            </th>
                                            <th>
                                                日期
                                            </th>
                                            <th>
                                                指數
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <asp:Literal ID="Literal8" runat="server"></asp:Literal>
                                    </tbody>
                                </table>
                            </div>
                            <!-- liveTb:end -->
                        </div>
                        <!-- 交通服務及維修零件_toolBx:end -->
                    </div>
                    <!-- 交通類_liveBox:end -->
                    <div class="liveBox">
                        <cite class="mrelt4">∥ 居住類</cite>
                        <ul class="hdNavs wdpd">
                            <li><a href="javascript:;" title="水電瓦斯">水電瓦斯</a></li>
                            <li><a href="javascript:;" title="房租">房租</a></li>
                            <li><a href="javascript:;" title="家庭用品">家庭用品</a></li>
                        </ul>
                        <!-- hdNavs wdpd:end -->
                        <span class="dataInfo mRt">
                            <asp:Literal ID="Literal21" runat="server"></asp:Literal>
                        </span>
                        <div class="toolBx Bx">
                            <div class="liveChart">
                                <img src="Chartsimg/livelihoodPrice/live_HydropowerGas.png" width="355" height="229"
                                    alt="水電瓦斯" />
                            </div>
                            <!-- liveChart:end -->
                            <div class="liveTb">
                                <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>
                                                日期
                                            </th>
                                            <th>
                                                指數
                                            </th>
                                            <th>
                                                日期
                                            </th>
                                            <th>
                                                指數
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <asp:Literal ID="Literal9" runat="server"></asp:Literal>
                                    </tbody>
                                </table>
                            </div>
                            <!-- liveTb:end -->
                        </div>
                        <!-- 水電瓦斯_toolBx:end -->
                        <div class="toolBx Bx">
                            <div class="liveChart">
                                <img src="Chartsimg/livelihoodPrice/live_Rent.png" width="355" height="229" alt="房租" />
                            </div>
                            <!-- liveChart:end -->
                            <div class="liveTb">
                                <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>
                                                日期
                                            </th>
                                            <th>
                                                指數
                                            </th>
                                            <th>
                                                日期
                                            </th>
                                            <th>
                                                指數
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <asp:Literal ID="Literal10" runat="server"></asp:Literal>
                                    </tbody>
                                </table>
                            </div>
                            <!-- liveTb:end -->
                        </div>
                        <!-- 房租_toolBx:end -->
                        <div class="toolBx Bx">
                            <div class="liveChart">
                                <img src="Chartsimg/livelihoodPrice/live_HouseholdGoods.png" width="355" height="229"
                                    alt="家庭用品" />
                            </div>
                            <!-- liveChart:end -->
                            <div class="liveTb">
                                <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>
                                                日期
                                            </th>
                                            <th>
                                                指數
                                            </th>
                                            <th>
                                                日期
                                            </th>
                                            <th>
                                                指數
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <asp:Literal ID="Literal11" runat="server"></asp:Literal>
                                    </tbody>
                                </table>
                            </div>
                            <!-- liveTb:end -->
                        </div>
                        <!-- 家庭用品_toolBx:end -->
                    </div>
                    <!-- 居住類_liveBox:end -->
                    <div class="liveBox">
                        <cite class="mrelt4">∥ 衣服類</cite>
                        <ul class="hdNavs wdpd">
                            <li><a href="javascript:;" title="成衣">成衣</a></li>
                            <li><a href="javascript:;" title="鞋襪">鞋襪</a></li>
                            <li><a href="javascript:;" title="衣著服務及配件">衣著服務及配件</a></li>
                        </ul>
                        <!-- hdNavs wdpd:end -->
                        <span class="dataInfo mRt">
                            <asp:Literal ID="Literal22" runat="server"></asp:Literal>
                        </span>
                        <div class="toolBx Bx">
                            <div class="liveChart">
                                <img src="Chartsimg/livelihoodPrice/clothing_Clothing.png" width="355" height="229"
                                    alt="成衣" />
                            </div>
                            <!-- liveChart:end -->
                            <div class="liveTb">
                                <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>
                                                日期
                                            </th>
                                            <th>
                                                指數
                                            </th>
                                            <th>
                                                日期
                                            </th>
                                            <th>
                                                指數
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <asp:Literal ID="Literal12" runat="server"></asp:Literal>
                                    </tbody>
                                </table>
                            </div>
                            <!-- liveTb:end -->
                        </div>
                        <!-- 成衣_toolBx:end -->
                        <div class="toolBx Bx">
                            <div class="liveChart">
                                <img src="Chartsimg/livelihoodPrice/clothing_ShoesSocks.png" width="355" height="229"
                                    alt="鞋襪" />
                            </div>
                            <!-- liveChart:end -->
                            <div class="liveTb">
                                <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>
                                                日期
                                            </th>
                                            <th>
                                                指數
                                            </th>
                                            <th>
                                                日期
                                            </th>
                                            <th>
                                                指數
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <asp:Literal ID="Literal13" runat="server"></asp:Literal>
                                    </tbody>
                                </table>
                            </div>
                            <!-- liveTb:end -->
                        </div>
                        <!-- 鞋襪_toolBx:end -->
                        <div class="toolBx Bx">
                            <div class="liveChart">
                                <img src="Chartsimg/livelihoodPrice/clothing_ServicesAccessories.png" width="355"
                                    height="229" alt="衣著服務及配件" />
                            </div>
                            <!-- liveChart:end -->
                            <div class="liveTb">
                                <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>
                                                日期
                                            </th>
                                            <th>
                                                指數
                                            </th>
                                            <th>
                                                日期
                                            </th>
                                            <th>
                                                指數
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <asp:Literal ID="Literal14" runat="server"></asp:Literal>
                                    </tbody>
                                </table>
                            </div>
                            <!-- liveTb:end -->
                        </div>
                        <!-- 衣著服務及配件_toolBx:end -->
                    </div>
                    <!-- 衣服類_liveBox:end -->
                    <div class="liveBox">
                        <cite class="mrelt4">∥ 其他類</cite>
                        <ul class="hdNavs wdpd">
                            <li><a href="javascript:;" title="醫療費用">醫療費用</a></li>
                            <li><a href="javascript:;" title="教養費用">教養費用</a></li>
                            <li><a href="javascript:;" title="香菸及檳榔">香菸及檳榔</a></li>
                            <li><a href="javascript:;" title="美容及衛生用品">美容及衛生用品</a></li>
                        </ul>
                        <!-- hdNavs wdpd:end -->
                        <span class="dataInfo mRt">
                            <asp:Literal ID="Literal23" runat="server"></asp:Literal>
                        </span>
                        <div class="toolBx Bx">
                            <div class="liveChart">
                                <img src="Chartsimg/livelihoodPrice/other_MedicalExpenses.png" width="355" height="229"
                                    title="其他類" alt="醫療費用" />
                            </div>
                            <!-- liveChart:end -->
                            <div class="liveTb">
                                <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>
                                                日期
                                            </th>
                                            <th>
                                                指數
                                            </th>
                                            <th>
                                                日期
                                            </th>
                                            <th>
                                                指數
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <asp:Literal ID="Literal15" runat="server"></asp:Literal>
                                    </tbody>
                                </table>
                            </div>
                            <!-- liveTb:end -->
                        </div>
                        <!-- 醫療費用_toolBx:end -->
                        <div class="toolBx Bx">
                            <div class="liveChart">
                                <img src="Chartsimg/livelihoodPrice/other_RearingCosts.png" width="355" height="229"
                                    alt="教養費用" />
                            </div>
                            <!-- liveChart:end -->
                            <div class="liveTb">
                                <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>
                                                日期
                                            </th>
                                            <th>
                                                指數
                                            </th>
                                            <th>
                                                日期
                                            </th>
                                            <th>
                                                指數
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <asp:Literal ID="Literal16" runat="server"></asp:Literal>
                                    </tbody>
                                </table>
                            </div>
                            <!-- liveTb:end -->
                        </div>
                        <!-- 教養費用_toolBx:end -->
                        <div class="toolBx Bx">
                            <div class="liveChart">
                                <img src="Chartsimg/livelihoodPrice/other_CigarettesBetelNuts.png" width="355" height="229"
                                    alt="香菸及檳榔" />
                            </div>
                            <!-- liveChart:end -->
                            <div class="liveTb">
                                <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>
                                                日期
                                            </th>
                                            <th>
                                                指數
                                            </th>
                                            <th>
                                                日期
                                            </th>
                                            <th>
                                                指數
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <asp:Literal ID="Literal17" runat="server"></asp:Literal>
                                    </tbody>
                                </table>
                            </div>
                            <!-- liveTb:end -->
                        </div>
                        <!-- 香菸及檳榔_toolBx:end -->
                        <div class="toolBx Bx">
                            <div class="liveChart">
                                <img src="Chartsimg/livelihoodPrice/other_BeautyHygieneProducts.png" width="355"
                                    height="229" alt="美容及衛生用品" />
                            </div>
                            <!-- liveChart:end -->
                            <div class="liveTb">
                                <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>
                                                日期
                                            </th>
                                            <th>
                                                指數
                                            </th>
                                            <th>
                                                日期
                                            </th>
                                            <th>
                                                指數
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <asp:Literal ID="Literal18" runat="server"></asp:Literal>
                                    </tbody>
                                </table>
                            </div>
                            <!-- liveTb:end -->
                        </div>
                        <!-- 美容及衛生用品_toolBx:end -->
                    </div>
                    <!-- 其他類_liveBox:end -->
                </div>
                <!-- fmTb:end -->
            </div>
            <!-- bdBk:end -->
        </div>
        <!-- main:end -->
        <asp:Literal ID="Literal24" runat="server"></asp:Literal>
        <!-- side:end -->
    </div>
    </form>
</asp:Content>
