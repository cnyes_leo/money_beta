﻿<%@ Page Language="VB" MasterPageFile="~/MPs/MP1.master" AutoEventWireup="false" CodeFile="tDefault.aspx.vb" Inherits="_tDefault" %>

<%@ Register Src="~/UCs/Home_NewsDiv.ascx" TagPrefix="UCHome_NewsDiv" TagName="Home_NewsDiv" %>
<%@ Register Src="~/UCs/HomeOilPrice.ascx" TagPrefix="UCHomeOilPrice" TagName="HomeOilPrice" %>
<%@ Register Src="~/UCs/HomeGovStat.ascx" TagPrefix="UCHomeGovStat" TagName="HomeGovStat" %>
<%@ Register Src="~/UCs/HomeMagLink.ascx" TagPrefix="UCHomeMagLink" TagName="HomeMagLink" %>
<%@ Register Src="~/UCs/HomeFundReport.ascx" TagPrefix="UCHomeFundReport" TagName="HomeFundReport" %>
<%@ Register Src="~/UCs/HomeMainForexEfficiency.ascx" TagPrefix="UCHomeMainForexEfficiency" TagName="HomeMainForexEfficiency" %>
<%@ Register Src="~/UCs/HomeFundGroupRank.ascx" TagPrefix="UCHomeFundGroupRank" TagName="HomeFundGroupRank" %>
<%@ Register Src="~/UCs/HomeFundDividend.ascx" TagPrefix="UCHomeFundDividend" TagName="HomeFundDividend" %>
<%@ Register Src="~/UCs/HomeTwstockDividend.ascx" TagPrefix="UCHomeTwstockDividend" TagName="HomeTwstockDividend" %>
<%@ Register Src="~/UCs/HomeGlobalMarketRank.ascx" TagPrefix="UCHomeGlobalMarketRank" TagName="HomeGlobalMarketRank" %>
<%@ Register Src="~/UCs/HomeHouseNews.ascx" TagPrefix="UCHomeHouseNews" TagName="HomeHouseNews" %>
<%@ Register Src="~/UCs/HomeMurMurNews.ascx" TagPrefix="UCHomeMurMurNews" TagName="HomeMurMurNews" %>
<%@ Register Src="~/UCs/HomeWinnerBLOG.ascx" TagPrefix="UCHomeWinnerBLOG" TagName="HomeWinnerBLOG" %>
<%@ Register Src="~/UCs/HomeBarDIV.ascx" TagPrefix="UCHomeBarDIV" TagName="HomeBarDIV" %>


<%@ Register Src="~/UCs/HomeSearchBar.ascx" TagPrefix="UCHomeSearchBar" TagName="HomeSearchBar" %>
<%@ Register Src="~/UCs/HomeGoldPrice.ascx" TagPrefix="UCHomeGoldPrice" TagName="HomeGoldPrice" %>
<%@ Register Src="~/UCs/MoneyTools.ascx" TagPrefix="UCMoneyTools" TagName="MoneyTools"  %>
<%@ Register Src="~/UCs/HomeForexExchangeRoom.ascx" TagPrefix="UCHomeForexExchangeRoom" TagName="HomeForexExchangeRoom" %>
<%@ Register Src="~/UCs/HomeIntradayChart.ascx" TagPrefix="UCHomeIntradayChart" TagName="HomeIntradayChart" %>
<%@ Register Src="~/UCs/HomeBankCurrencyExchange.ascx" TagPrefix="UCHomeBankCurrencyExchange" TagName="HomeBankCurrencyExchange" %>
<%@ Register Src="~/UCs/HomeBankRate2.ascx" TagPrefix="UCHomeBankRate2" TagName="HomeBankRate2"  %>

<%@ Register Src="~/UCs/HomeBankRate.ascx" TagPrefix="UCHomeBankRate" TagName="HomeBankRate" %>
<%@ Register Src="~/UCs/FundGood.ascx" TagPrefix="UCFundGood" TagName="FundGood" %>
<%@ Register Src="~/UCs/GLidx.ascx" TagPrefix="UCGLidx" TagName="GLidx" %>
<%@ Register Src="~/UCs/HomeGlobalBondRate.ascx" TagPrefix="UCHomeGlobalBondRate" TagName="HomeGlobalBondRate" %>
<%@ Register Src="~/UCs/HomeGlobalKeyRate.ascx" TagPrefix="UCHomeGlobalKeyRate" TagName="HomeGlobalKeyRate" %>
<%@ Register Src="~/UCs/HomeHouseSearch.ascx" TagPrefix="UCHomeHouseSearch" TagName="HomeHouseSearch" %>
<%@ Register Src="~/UCs/HomeLectureInvestClass.ascx" TagPrefix="UCHomeLectureInvestClass" TagName="HomeLectureInvestClass" %>
<%@ Register Src="~/UCs/HomeCountryStockHoliday.ascx" TagPrefix="UCHomeCountryStockHoliday" TagName="HomeCountryStockHoliday" %>
<%@ Register Src="~/UCs/HomeCnyesInformation.ascx" TagPrefix="UCHomeCnyesInformation" TagName="HomeCnyesInformation" %>

<%@ Register src="UCs/RMBSafeRate.ascx" tagname="RMBSafeRate" tagprefix="uc1" %>

<%@ Register src="UCs/ForeignCurrencyTimeDeposit.ascx" tagname="ForeignCurrencyTimeDeposit" tagprefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">




    <form id="form1" runat="server">




<link href="http://imgcache.cnyes.com/cnews/css_ad/google_1AD_BigFont_CSS.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="http://partner.googleadservices.com/gampad/google_service.js"></script>
<script type="text/javascript">
    GS_googleAddAdSenseService("ca-pub-0246916887946697");
    GS_googleEnableAllServices();
</script>

<script type="text/javascript">
    GA_googleAddSlot("ca-pub-0246916887946697", "GG_FUND_LEFT");
</script>

<script type="text/javascript">
    GA_googleFetchAds();
</script>



    <div id="container">

        <!-- 左側 -->
        <div id="main">
            <!-- 左上方新聞 -->
            <UCHome_NewsDiv:Home_NewsDiv ID="Home_NewsDiv1" runat="server" />
        
            <div class="cyads650x100 mgbm"><em><a href="http://www.cnyes.com/chn/ad/ad.htm" target="_blank" title="廣告刊登">廣告刊登</a></em>
            <iframe src="http://mo.cnyes.com/showad.ad?TAG=MONEY_HOME" frameborder="0"   width="650" height="100" marginheight="0" marginwidth="0" scrolling="no"></iframe>
            </div><!-- cyads650x100:end -->
            
            <!-- 油價 -->
            <UCHomeOilPrice:HomeOilPrice ID="UCHomeOilPrice1" runat="server" />
            
            <!-- 民生物價 -->
            <UCHomeGovStat:HomeGovStat ID="HomeGovStat1" runat="server" />
            
            <div class="blank"></div>
            
            <!-- 理財雜誌 -->
            <UCHomeMagLink:HomeMagLink ID="HomeMagLink1" runat="server" />
            
            <!-- 各國錢幣 -->
            <div id="coinBx" class="mgbm">
                <div class="hd" style="margin:0 0 0 0;">
                    <h3>各國錢幣</h3>
                    <span class="mlinks"><a href="#" title=""></a></span>
                </div>
                <div class="bd">

                    <div id="browsable2" class="scrollable">
                        <div class="items">
                            <div class="item"><a href="javascript:" class="imgBr"><img src="images/coin/sm/asia/twd.jpg" alt="台灣/新台幣元(TWD)" width="130" height="60" /></a><em><a href="#">台灣/新台幣元(TWD)</a></em></div>
                            <div class="item"><a href="javascript:" class="imgBr"><img src="images/coin/sm/asia/jpy.jpg" alt="日本/日元(JPY)" width="130" height="60" /></a><em><a href="#">日本/日元(JPY)</a></em></div>
                            <div class="item"><a href="javascript:" class="imgBr"><img src="images/coin/sm/asia/cny.jpg" alt="中國/人民幣(CNY" width="130" height="60" /></a><em><a href="#">中國/人民幣(CNY)</a></em></div>
                            <div class="item"><a href="javascript:" class="imgBr"><img src="images/coin/sm/america/usd.jpg" alt="美國/美元(USD)" width="130" height="60" /></a><em><a href="#">美國/美元(USD)</a></em></div>
                            <div class="item"><a href="javascript:" class="imgBr"><img src="images/coin/sm/asia/hkd.jpg" alt="香港/港元(HKD)" width="130" height="60" /></a><em><a href="#">香港/港元(HKD)</a></em></div>
                            <div class="item"><a href="javascript:" class="imgBr"><img src="images/coin/sm/europe/eur.jpg" alt="歐盟/歐元(EUR)" width="130" height="60" /></a><em><a href="#">歐盟/歐元(EUR)</a></em></div>
                            <div class="item"><a href="javascript:" class="imgBr"><img src="images/coin/sm/europe/gbp.jpg" alt="英國/英鎊(GBP)" width="130" height="60" /></a><em><a href="#">英國/英鎊(GBP)</a></em></div>
                            <div class="item"><a href="javascript:" class="imgBr"><img src="images/coin/sm/asia/aud.jpg" alt="澳大利亞/澳元(AUD)" width="130" height="60" /></a><em><a href="#">澳大利亞/澳元(AUD)</a></em></div>
                        </div><!-- items:end -->
                    </div><!-- scrollable:end -->
                    
                    <div class="navi"></div>
                </div><!-- bd:end -->
            </div>
            
            
            <!-- 基金研究報告/新聞 -->
            <UCHomeFundReport:HomeFundReport ID="UCHomeFundReport1" runat="server" />
            
            <div class="blank"></div>
            

            <%--人民幣存款利率--%>
            <uc1:RMBSafeRate ID="RMBSafeRate1" runat="server" />
            
            


            <!-- 主要外匯績效表 -->
            <UCHomeMainForexEfficiency:HomeMainForexEfficiency ID="HomeMainForexEfficiency1" runat="server" />



            <div class="gadstxtm2" id="GoogleAD">
                <script type="text/javascript">
                    google_ad_channel = '8161541736'; //  your channel
                    cnyes_max_num_ads = '1';
                </script>
                <script type="text/javascript">
                    GA_googleFillSlot("GG_FUND_LEFT");
                </script>
            </div>


            <!-- 基金績效排行-->
            <UCHomeFundGroupRank:HomeFundGroupRank ID="HomeFundGroupRank1" runat="server" />
            
            
            <!-- 基金配息排行-->
            <UCHomeFundDividend:HomeFundDividend ID="HomeFundDividend1" runat="server" />
            
            <!-- 台股殖利率排行 -->
            <UCHomeTwstockDividend:HomeTwstockDividend ID="HomeTwstockDividend1" runat="server" />
            
            <!-- 全球股市排行 -->
            <UCHomeGlobalMarketRank:HomeGlobalMarketRank ID="HomeGlobalMarketRank1" runat="server" />
            
            
            <div class="cyads650x250 mgbm">
                <iframe src="http://mo.cnyes.com/showad.ad?TAG=MONEY_HOMEA" frameborder="no" vspace="0" hspace="0"  width="650" height="250" marginheight="0" marginwidth="0" scrolling="no">
                </iframe>
            </div>



            <!-- 房產教戰新聞 -->
            <UCHomeHouseNews:HomeHouseNews ID="HomeHouseNews1" runat="server" />
            
            
            <!-- 保險專區 -->
            <div id="insurance" class="mgbm">
                <div class="hd">
                    <h3>保險專區</h3>
                    <span class="mlinks">
                        <a href="http://money.cnyes.com/insurance/" title="保險霤達" target="_blank">保險霤達</a>
                        <a href="http://money.cnyes.com/insurance/market.asp" title="保單查詢" target="_blank">保單查詢</a>
                        <a href="http://money.cnyes.com/insurance/market1.asp" title="保險比較" target="_blank">保險比較</a>
                        <a href="http://money.cnyes.com/insurance/cal.asp" title="需求試算" target="_blank">需求試算</a>
                        <a href="http://money.cnyes.com/insurance/bio.asp" title="保險百科" target="_blank">保險百科</a>
                        <a href="http://money.cnyes.com/insurance/page2space.asp" title="保險" target="_blank">保險好康情報</a></span>
                    </span>
                </div>
                <iframe src="http://www.ins104.com.tw/cnyesPage/IF02BlogNews.aspx" title="" frameborder="0" width="678" height="150" marginheight="0" marginwidth="0" scrolling="no"></iframe>
            </div>



            <!-- 鉅亨看世界 -->
            <UCHomeMurMurNews:HomeMurMurNews ID="HomeMurMurNews1" runat="server" />
            
            <!-- BLOG贏家心法 / 新聞 -->
            <UCHomeWinnerBLOG:HomeWinnerBLOG ID="HomeWinnerBLOG1" runat="server" />
            
            
            <div class="blank"></div>
            
            <!-- 鉅亨吧專區 -->
            <UCHomeBarDIV:HomeBarDIV ID="HomeBarDIV1" runat="server" />
            
            
            <div class="clearfix"></div>


        </div>

        <!-- 右側 -->
        <div id="side">
        
        
        
            <!-- 綜合搜尋框 -->
            <UCHomeSearchBar:HomeSearchBar ID="HomeSearchBar1" runat="server" />
            
            
            <div class="gads300x250 mgbm">
            <iframe src="http://mo.cnyes.com/showad.ad?TAG=MONEYHOME_RIGHT" frameborder="no" vspace="0" hspace="0" width="300" height="250" marginheight="0" marginwidth="0" scrolling="no"></iframe>
            </div><!-- Ads gads300x250:end -->

            <!-- 黃金牌告 -->
            <UCHomeGoldPrice:HomeGoldPrice ID="HomeGoldPrice1" runat="server" />
            

            <!-- 理財小工具 -->
            <UCMoneyTools:MoneyTools ID="MoneyTools1" runat="server" />
            
            <!-- 外匯看盤室 -->
            <UCHomeForexExchangeRoom:HomeForexExchangeRoom ID="HomeForexExchangeRoom1" runat="server" />
            
            <!-- IntradayChart 24合一走勢圖 -->
            <UCHomeIntradayChart:HomeIntradayChart ID="HomeIntradayChart1" runat="server" />
            
            <%--外幣定存--%>
            <uc2:ForeignCurrencyTimeDeposit ID="ForeignCurrencyTimeDeposit1" runat="server" />

            <!-- 銀行換匯 -->
            <UCHomeBankCurrencyExchange:HomeBankCurrencyExchange ID="HomeBankCurrencyExchange1" runat="server" />
            
            <!-- 存款利率 -->
            <UCHomeBankRate2:HomeBankRate2 ID="HomeBankRate21" runat="server" />
            <!-- 各銀行利率 -->
            <UCHomeBankRate:HomeBankRate ID="HomeBankRate1" runat="server" />
            
            <!-- 基金促銷活動 -->
            <UCFundGood:FundGood ID="FundGood1" runat="server" />
            
            <!-- 國際指數報價 -->
            <UCGLidx:GLidx ID="GLidx1" runat="server" />
            
            <!-- 全球債券利率 -->
            <UCHomeGlobalBondRate:HomeGlobalBondRate ID="HomeGlobalBondRate1" runat="server" />
            
            <!-- 全球關鍵利率 -->
            <UCHomeGlobalKeyRate:HomeGlobalKeyRate ID="HomeGlobalKeyRate1" runat="server" />
            
            <!-- 房地產搜尋 -->
            <UCHomeHouseSearch:HomeHouseSearch ID="HomeHouseSearch1" runat="server" />
            
            <!-- 熱門投資講座 -->
            <UCHomeLectureInvestClass:HomeLectureInvestClass ID="HomeLectureInvestClass1" runat="server" />
            
            <!-- 全球休市市場 -->
            <UCHomeCountryStockHoliday:HomeCountryStockHoliday ID="HomeCountryStockHoliday1" runat="server" />
            
            <!-- 鉅亨公告 / 鉅亨推薦 -->
            <UCHomeCnyesInformation:HomeCnyesInformation ID="HomeCnyesInformation1" runat="server" />
        </div>
    </div>

    </form>

</asp:Content>