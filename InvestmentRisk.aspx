<%@ Page Language="C#" EnableViewState="false" AutoEventWireup="true" CodeFile="InvestmentRisk.aspx.cs" Inherits="InvestmentRisk"
    EnableViewStateMac="true" MasterPageFile="~/MPs/MP1.master" %>

<%@ OutputCache Duration="60" VaryByParam="*" %>
<%@ Register Src="UCs/MoneyTools.ascx" TagName="MoneyTools" TagPrefix="uc3" %>
<%@ Register Src="UCs/HomeBankRate.ascx" TagName="HomeBankRate" TagPrefix="uc4" %>
<%@ Register Src="UCs/FundGood.ascx" TagName="FundGood" TagPrefix="uc3" %>
<%@ Register Src="UCs/GLidx.ascx" TagName="GLidx" TagPrefix="uc4" %>
<%@ Register Src="UCs/fundptcalcu_02.ascx" TagName="fundptcalcu_02" TagPrefix="uc2" %>
<%@ Register Src="UCs/fundptcalcu_01.ascx" TagName="fundptcalcu_01" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <form method="post" runat="server" id="Form1">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div id="container">
        <div id="main">
            <div class="hdPath">
                <h3>
                    投資屬性分析</h3>
            </div>
            <div class="bdBk">
                <div class="lfap">
                    <p>
                        您適合積極型還是保守型投資商品？投資總會伴隨一定程度的風險，在您展開投資之前，建議您先衡量自己所能承受的風險，了解適合自己的投資風險屬性！ (共七題)</p>
                    <h4>
                        <label>
                            Q1</label>你目前的年齡？</h4>
                    <ul>
                        <asp:RadioButtonList ID="RadioButtonList1" runat="server">
                            <asp:ListItem Value="5">A. 29歲以下</asp:ListItem>
                            <asp:ListItem Value="4">B. 30-39歲</asp:ListItem>
                            <asp:ListItem Value="3">C. 40-49歲</asp:ListItem>
                            <asp:ListItem Value="2">D. 50-59歲</asp:ListItem>
                            <asp:ListItem Value="1">E. 60歲以上</asp:ListItem>
                        </asp:RadioButtonList>
                    </ul>
                    <h4>
                        <label>
                            Q2</label>你計畫從何時開始提領你投資的部分金額？</h4>
                    <ul>
                        <asp:RadioButtonList ID="RadioButtonList2" runat="server">
                            <asp:ListItem Value="5">A. 至少20年以上</asp:ListItem>
                            <asp:ListItem Value="4">B. 10至20年</asp:ListItem>
                            <asp:ListItem Value="3">C. 6至10年</asp:ListItem>
                            <asp:ListItem Value="2">D. 2至5年</asp:ListItem>
                            <asp:ListItem Value="1">E. 2年以內</asp:ListItem>
                        </asp:RadioButtonList>
                    </ul>
                    <h4>
                        <label>
                            Q3</label>你的理財目標(長期)</h4>
                    <ul>
                        <asp:RadioButtonList ID="RadioButtonList3" runat="server">
                            <asp:ListItem Value="3">A. 資產迅速成長</asp:ListItem>
                            <asp:ListItem Value="2">B. 資產穩健成長</asp:ListItem>
                            <asp:ListItem Value="1">C. 避免財產損失</asp:ListItem>
                        </asp:RadioButtonList>
                    </ul>
                    <h4>
                        <label>
                            Q4</label>以下哪一項描述比較接近你對投資的態度？</h4>
                    <ul>
                        <asp:RadioButtonList ID="RadioButtonList4" runat="server">
                            <asp:ListItem Value="5">A. 我尋求長期投資報酬最大化，所以可以承擔因市場價格波動所造成的較大的投資風險</asp:ListItem>
                            <asp:ListItem Value="4">B. 我比較注重投資報酬率的增加，所以可以承擔一些因市場價格波動所造成的短期風險投資</asp:ListItem>
                            <asp:ListItem Value="3">C. 市場價格波動與投資報酬率對我來說同樣重要</asp:ListItem>
                            <asp:ListItem Value="2">D. 我比較希望市場價格的波動小，投資報酬率低一些沒關係</asp:ListItem>
                            <asp:ListItem Value="1">E. 我想要避開市場價格波動--願意接受低的投資報酬率，而不願意承受資產虧損的風</asp:ListItem>
                        </asp:RadioButtonList>
                    </ul>
                    <h4>
                        <label>
                            Q5</label>通貨膨脹會嚴重侵蝕你的投資獲利，尤其隨著時間的流逝更行嚴重。舉例來說：假設年通貨膨脹率為3%的話，6%的年投資報酬只會提升你的實質購買力3%。請自以下描述中選擇一項最適合說明你對通貨膨脹與投資的態度。</h4>
                    <ul>
                        <asp:RadioButtonList ID="RadioButtonList5" runat="server">
                            <asp:ListItem Value="3">A. 我的目標是讓投資報酬率明顯超出通貨膨脹率，並願意為此承擔較大的投資風險</asp:ListItem>
                            <asp:ListItem Value="2">B. 我的目標是讓投資報酬率稍高於通貨膨脹率，若因而多承擔一些投資的風險是可以的</asp:ListItem>
                            <asp:ListItem Value="1">C. 我的目標是讓投資報酬率等於通貨膨脹的速度，但是要儘量減低投資組合價值變動的幅度</asp:ListItem>
                        </asp:RadioButtonList>
                    </ul>
                    <h4>
                        <label>
                            Q6</label>假設你有一筆龐大的金額投資在有價證券中，並且該投資呈現三級跳的漲幅--比如說：一個月增值了20%。你可能會採取什麼行動？</h4>
                    <ul>
                        <asp:RadioButtonList ID="RadioButtonList6" runat="server">
                            <asp:ListItem Value="5">A. 投入更多的資金在該標的上</asp:ListItem>
                            <asp:ListItem Value="4">B. 繼續持有該標的</asp:ListItem>
                            <asp:ListItem Value="3">C. 賣掉少於一半的部位，實現部分獲利</asp:ListItem>
                            <asp:ListItem Value="2">D. 賣掉大於一半的部位，實現大部分投資獲利</asp:ListItem>
                            <asp:ListItem Value="1">E. 賣掉所有的部位，獲利了結</asp:ListItem>
                        </asp:RadioButtonList>
                    </ul>
                    <h4>
                        <label>
                            Q7</label>假設你有一筆龐大的金額投資在股票中，並且在過去的一年中該筆投資價值持續下滑，比方說：你的資產在這段時期中下跌了25%。你可能會採取什麼行動？</h4>
                    <ul>
                        <asp:RadioButtonList ID="RadioButtonList7" runat="server">
                            <asp:ListItem Value="5">A. 增加投資</asp:ListItem>
                            <asp:ListItem Value="4">B. 繼續持有該標的</asp:ListItem>
                            <asp:ListItem Value="3">C. 賣掉少於一半的部位</asp:ListItem>
                            <asp:ListItem Value="2">D. 賣掉大於一半的部位</asp:ListItem>
                            <asp:ListItem Value="1">E. 賣掉所有的部位</asp:ListItem>
                        </asp:RadioButtonList>
                    </ul>
                    <div class="btnS">
                        <asp:Button ID="Button1" runat="server" Text="送出" OnClick="Button1_Click" /></div>
                    <!-- btnS:end -->
                </div>
                <!-- lfap:end -->
            </div>
            <!-- bdBk:end -->
        </div>
        <!-- main:end -->
        <asp:Literal ID="Literal1" runat="server"></asp:Literal>
        <!-- side:end -->
    </div>
    <!-- container:end -->
    </form>
</asp:Content>
