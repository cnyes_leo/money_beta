<%@ Page Language="C#" MasterPageFile="~/MPs/MP1.master" EnableEventValidation="false" EnableViewState="false"
    EnableViewStateMac="true" AutoEventWireup="true" CodeFile="InvestCalculation.aspx.cs"
    Inherits="InvestCalculation" %>

<%@ OutputCache Duration="60" VaryByParam="*" %>
<%@ Register Src="UCs/SpreadsheetMenu.ascx" TagName="SpreadsheetMenu" TagPrefix="uc3" %>
<%@ Register Src="UCs/MoneyTools.ascx" TagName="MoneyTools" TagPrefix="uc4" %>
<%@ Register Src="UCs/FundGood.ascx" TagName="FundGood" TagPrefix="uc1" %>
<%@ Register Src="UCs/HomeBankRate.ascx" TagName="HomeBankRate" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%--<form method="post" runat="server" id="Form1">--%>
    <script src="js/jquery.blockUI.js" type="text/javascript"></script>
    <div id="container">
        <uc3:SpreadsheetMenu ID="SpreadsheetMenu1" runat="server" />
        <!-- side2:end -->
        <asp:Panel ID="pnContent" runat="server">
        </asp:Panel>
        <!-- mymain:end -->
        <asp:Literal ID="Literal1" runat="server"></asp:Literal>
        <!-- side:end -->
    </div>
    <script type="text/javascript">
        //MenuCurrent
        document.getElementById(<%= strID %>).className = 'current';
        document.getElementById('icmenu').className = 'current';
    </script>
    <%--</form>--%>
</asp:Content>
